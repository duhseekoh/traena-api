# Overview
This is an API application starter that can be forked/reused to build lightweight, simple, database-backed APIs. It uses the following:

* Koa 2: http://koajs.com/
* Webpack: https://webpack.github.io/


# Environment Setup
1. Fork the repo on Github

1. Git clone the forked project locally
  ```shell
  git clone {repo}
  ```

1. Install software dependencies
  - Node v8 (nvm | http://nodejs.org | homebrew)
  - Docker 17.12.0+ (https://docs.docker.com/docker-for-mac/install/)

1. Configure your environment to use pandera private registry
```shell
npm config set registry https://repository.panderacloud.com/repository/npm-all/
npm config set always-auth true
npm login --registry=https://repository.panderacloud.com/repository/npm-all/
yarn config set registry https://repository.panderacloud.com/repository/npm-all/
```

1. Install package dependencies
  ```shell
  yarn install
  ```

1. Setup environment variables
  Create a `.env` file in the root of your project. Copy contents of `.env.example.local-docker` to `.env`. Read through the environment variables, as you
  may have to find and supply some yourself.

1. Start up the docker containers. This will launch the node api, elasticsearch instance,
  and postgres all in their own docker containers.
  ```shell
  docker-compose up
  ```

1. Now that we have an empty database and es instance, we'll need to populate them.
  ```shell
  # setup traena database
  docker container exec trn_api yarn db:migrate
  # populate with seed data
  docker container exec trn_api yarn db:seed
  # index users from database -> elasticsearch
  docker container exec trn_api yarn esUtils index:users
  # index content from database -> elasticsearch
  docker container exec trn_api yarn esUtils index:content
  ```
  or call the convenience command
  ```shell
  docker container exec trn_api yarn resetEnvState
  ```

1. API is up and running in docker. You can access it at http://localhost:3001.
  When making changes to the application code, the api inside docker will auto-restart.

1. Stop the running containers when you're done
  ```shell
  docker-compose stop
  ```
  The database and elasticsearch data persists.

## Starting from Scratch

Each time you run `docker-compose up`, if the containers are already created, it won't
re-create them. The data stays persisted between `up`s and `down`s.

If you'd like to start over with clean containers and remove all persisted data, then
remove the postgres and elasticsearch volumes.

```shell
# view all your docker volumes. you'll see one for dbdata, another for esdata
docker volumes ls
# shutdown, delete containers, delete volumes
docker-compose down --volumes
```

## Verifying what containers are running

`docker ps` will list out which (if any) of the three containers are currently running.

`docker ps -a` will show you all containers you've created. We use a static container name
for traena containers, so you should see `trn_api`, `trn_elasticsearch`, and `trn_db` if you've
ran `docker-compose up` (or `docker-compose build`)

## Access container services from outside docker

With docker, our services all run isolated in a local docker network. But you can
still connect to them from your host machine.

Access:
* API - http://localhost:3001
* Postgres - postgres://postgres@localhost:5433/traena
* Elasticsearch - http://localhost:9201

## Deprecated: Setup Elasticsearch via docker

NOTE: Only set this up if you are not using docker to run the api locally. The docker-compose
configuration includes setup of an elasticsearch instance.

When starting elasticsearch up, you'll want to disable security, so no authentication is required. In production, with ES running on AWS, ES is secured by an AWS IAM Role.
```shell
docker run -p 9200:9200 -e "http.host=0.0.0.0" -e "transport.host=127.0.0.1" -e "xpack.security.enabled=false"  docker.elastic.co/elasticsearch/elasticsearch:5.4.0
```

## Utilities

### esUtils - Index elastic search locally with data from the database
  ```
  yarn run esUtils -- [options] [command]
  ===
  Usage: esUtils [options] [command]


  Commands:

    index:users
    index:content

  Options:

    -h, --help     output usage information
    -V, --version  output the version number
  ===
  ```

### contentUtils - Add content (videos/text) to the database
  *** See [tools/content/batch-example.csv] for an example upload csv to use for the bulk uploader

  ```
  yarn run contentUtils -- <cmd> [options]
  ===
  Usage: tools/contentUtils <cmd> [options]

  Commands:
    content:video:add           Add a video file as content
    content:text:add            Add text content
    content:bulk:create <file>  Add bulk content from csv

  Options:
    --version  Show version number
    --help     Show help
  ===
  ```

## Testing

### Unit Tests

TODO

### Integration Tests

Tests in the `./integration-tests` directory are meant for api endpoint testing.
The term integration test can be interpreted in many ways. For our purposes, *an
integration test is one in which we can hit api endpoints and expect full database
and elasticsearch interactions*. It also uses the dev auth0 instance for authentication
and authorization support.

#### How it works
The integration tests have their own environment variables, which is meant to
target a specific Traena environment. The only supported Traena environment
currently supported to run these tests against is a locally running freshly seeded
docker-compose environment. With a running docker-compose environment, the tests
can be kicked off and ran against that environment.

#### CI
These integration tests are also configured to run on every circleci build. Our
circle job boots up an ephemeral docker-compose environment then runs the tests
against the api running within.

#### Not covered
These integration tests DO NOT cover AWS related functionality such as testing
upload/download from S3, push notifications, lambdas, video transcoding. It's
possible that we could use a sandbox environment to test our api interactions with
AWS. It's also possible a set of non-ephemeral functional api tests could be written
and meant for testing against our full AWS environments.

#### Running the tests
1. Start up your local environment using docker-compose. See 'Environment Setup' above.
2. Reset the state of that environment `docker container exec trn_api yarn resetEnvState`.
3. Setup `./integration-tests/.env` based on `.env.example.local-docker`
4. Kickoff the tests `yarn test:integration`

Hint: Test a specific file e.g. `yarn test:integration organization.test.js`

## Conventions

### Logging
The application uses a singleton instance of winston for logging, but each file should use an instance of the Logger class initialized with the file name, to ensure the filename is included in the log output.
Each public method on services (and equivalent business logic containing ) clients should log method entry in the format of `logger.silly('Class/FileName.methodName', {methodParamA, methodParamB, methodParamC})`.

#### Log Levels
* silly
  * AKA Trace - use for very verbose method-entry level logging
* debug
  * Use to log intermediate application operations / values
* info
  * Default log level in production - Use to log standard diagnostic information that would be useful during normal production operations analysis
* warn
  * Use to log when an unexpected situation has arisen, but one that does is able to be recovered from, or that does not affect the ability of the application to continue operating   
* error
  * Used to log whenever an error occurs that interrupts the normal application / user request flow.
