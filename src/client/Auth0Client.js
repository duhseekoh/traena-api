// @flow
/* eslint-disable class-methods-use-this */

import type { $AxiosXHR, Axios } from 'axios';
import axios from 'axios';
import { ManagementClient } from 'auth0';
import moment from 'moment';
import Logger from '../common/logging/logger';
import { Auth0ApiError, ConflictError } from '../common/errors';
import { type User } from '../domain/models/UserModel'; // eslint-disable-line import/named
import type { AppConfig } from '../common/config';


const logger = new Logger('client/Auth0Client');

function generatePassword(length) {
  const s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_-+={}[]|;:,.<>?';
  return [...Array(length)].map(() => s.charAt(Math.floor(Math.random() * s.length))).join('');
}

// Does the password contain 3 of each: special characters, uppercase, lowercase
function isStrongEnough(password) {
  const specialChars = '!@#$%^&*()_-+={}[]|;:,.<>?';
  // Build a regex pattern that will match special characters
  const scRegex = new RegExp(`[${specialChars.split('').join('\\')}]`, 'g');
  const upperRegex = /[A-Z]/g;
  const lowerRegex = /[a-z]/g;
  const numberRegex = /[0-9]/g;
  const scMatch = password.match(scRegex);
  const ucMatch = password.match(upperRegex);
  const lcMatch = password.match(lowerRegex);
  const numberMatch = password.match(numberRegex);
  return scMatch && scMatch.length >= 1
    && ucMatch && ucMatch.length >= 1
    && lcMatch && lcMatch.length >= 1
    && numberMatch && numberMatch.length >= 1;
}


type TokenResponse = {
  access_token: string,
  expires_in: number,
};

export type Auth0Role = {
  _id: string,
  name: string,
};

export default class Auth0Client {
  config: AppConfig;

  _managementClient: ManagementClient;
  _authorizationClient: Axios;

  _managementApiTokenExpiration: moment$Moment;
  _authorizationApiTokenExpiration: moment$Moment;

  _authorizationApiUri: string;

  constructor(config: AppConfig) {
    this.config = config;
    this._authorizationApiUri = this.config.auth.authorizationApiURI;
    return this;
  }

  _tokenNeedsRefreshed(expiration: moment$Moment): boolean {
    const now = moment();
    // If token expires in the next five minutes, we should refresh it
    return (now.add(5, 'minutes').isAfter(expiration));
  }

  _managementApiTokenNeedsRefreshed(): boolean {
    return this._tokenNeedsRefreshed(this._managementApiTokenExpiration);
  }

  _authorizationApiTokenNeedsRefreshed(): boolean {
    return this._tokenNeedsRefreshed(this._authorizationApiTokenExpiration);
  }


  async _getToken(audience: string): Promise<{ token: string, expires_in: number }> {
    logger.debug(`Getting token for audience: ${audience}`);
    const oauthUrl = `https://${this.config.auth.domain}/oauth/token`;
    const requestBody = {
      grant_type: 'client_credentials',
      client_id: this.config.auth.mgmtClientId,
      client_secret: this.config.auth.mgmtClientSecret,
      audience,
      expires_in: '86400',
    };

    const result: $AxiosXHR<TokenResponse> = await axios.post(oauthUrl, requestBody);

    return {
      token: result.data.access_token,
      expires_in: result.data.expires_in,
    };
  }

  async _getManagementApiToken(): Promise<string> {
    const result = await this._getToken(`https://${this.config.auth.domain}/api/v2/`);
    this._managementApiTokenExpiration = moment().add(result.expires_in, 'seconds');
    logger.debug('Mgmt Api Token Expires In: ', { expiresIn: result.expires_in });
    logger.debug('Mgmt Api Token Expiration: ', { expires: this._managementApiTokenExpiration.toISOString() });

    return result.token;
  }

  async _getAuthorizationApiToken(): Promise<string> {
    const result = await this._getToken(this.config.auth.authorizationServiceAudience);
    this._authorizationApiTokenExpiration = moment().add(result.expires_in, 'seconds');
    logger.debug('Authorization Api Token Expires In: ', { expiresIn: result.expires_in });
    logger.debug(
      'Authorization Api Token Expiration: ',
      { expires: this._authorizationApiTokenExpiration.toISOString() },
    );
    return result.token;
  }


  /**
   * Returns a management client. The result of this method should *NOT* be cached as caching it will cause the token
   * to eventually expire
   * @returns {Promise.<*>}
   */
  async _getManagementClient(): Promise<ManagementClient> {
    if (this._managementClient && !this._managementApiTokenNeedsRefreshed()) {
      return Promise.resolve(this._managementClient);
    }
    logger.debug('Getting new auth0 management client');
    const managementToken = await this._getManagementApiToken();
    this._managementClient = new ManagementClient({
      token: managementToken,
      domain: this.config.auth.domain,
    });
    return this._managementClient;
  }

  /**
   * Returns an Authorization Client. The result of this method should *NOT* be cached as caching it will cause the
   * token to eventually expire
   * @returns {Promise.<*>}
   */
  async _getAuthorizationClient(): Promise<Axios> {
    if (this._authorizationClient && !this._authorizationApiTokenNeedsRefreshed()) {
      return Promise.resolve(this._authorizationClient);
    }
    logger.debug('Getting new auth0 authorization client');
    const authorizationToken = await this._getAuthorizationApiToken();
    this._authorizationClient = axios.create({
      headers: {
        Authorization: `Bearer ${authorizationToken}`,
      },
    });
    return this._authorizationClient;
  }

  async _wrapAxiosAsAuth0Error(message: string, func: (input: *) => Promise<*>): Promise<*> {
    try {
      // must await to ensure we handle exceptions properly
      return await func();
    } catch (err) {
      const errData = err.response && err.response.data;
      const errStatus = err.response && err.response.status;
      logger.error(message, { status: errStatus, data: errData });

      if (errData && errData.statusCode === 400 &&
        errData.message && errData.message.includes('Group with name') && errData.message.includes('already exists')) {
        throw new ConflictError('Auth Group already exists');
      }

      throw new Auth0ApiError(message);
    }
  }

  async _returnNullOnAxiosError(message: string, func: (input: *) => Promise<*>): Promise<*> {
    try {
      return await func();
    } catch (err) {
      logger.error(message, { status: err.response.status, data: err.response.data });
      return null;
    }
  }


  async _returnNullOnError(message: string, func: (input: *) => Promise<*>): Promise<*> {
    try {
      return await func();
    } catch (err) {
      logger.warn(message, { error: err });
      return null;
    }
  }

  async _wrapAsAuth0Error(message: string, func: (input: *) => Promise<*>): Promise<*> {
    try {
      return await func();
    } catch (err) {
      logger.warn(message, { error: err });
      logger.silly(err); // Print out the entire stack trace locally

      // Already wrapped by a nested call
      if (err instanceof Auth0ApiError) {
        throw err;
      }

      // if the error name is APIError we've had to parse the err message, as its serialized JSON.
      let errMsg;
      try {
        errMsg = JSON.parse(err.message);
      } catch (err2) {
        errMsg = err.message;
      }

      // We've seen both of these occur for duplicate user, so checking both scenarios.
      if ((err.name === 'Bad Request' && errMsg === 'The user already exists.')
          || (err.name === 'APIError' && errMsg && errMsg.message === 'The user already exists.')) {
        throw new ConflictError('Auth user already exists');
      }

      throw new Auth0ApiError(message);
    }
  }

  generateTempPassword(): string {
    let password = '';
    while (!isStrongEnough(password)) {
      password = generatePassword(16);
    }
    return password;
  }

  async getUser(id: string): Promise<?auth0$endUserObject> {
    logger.silly('AuthClient.getUser', { id });

    return this._returnNullOnError(
      `Error getting auth user with id: [${id}]`,
      async () => {
        const managementClient = await this._getManagementClient();
        return managementClient.getUser({ id });
      },
    );
  }

  async findUserIdByEmail(email: string): Promise<?string> {
    logger.silly('AuthClient.findUserIdByEmail', { email });

    return this._returnNullOnError(
      `Error locating auth user with email: [${email}]`,
      async () => {
        const findByEmailQuery = `email:"${email}"`;
        const client: ManagementClient = await this._getManagementClient();
        const result = await client.getUsers({ q: findByEmailQuery, search_engine: 'v3' });

        if (result.length !== 1) {
          logger.error(`Could not find single user with email address: ${email}. Found ${result.length} users.`);
          return null;
        }

        const auth0UserId = result[0].user_id;
        logger.silly('Located User with auth0 id: ', { user: auth0UserId });
        return auth0UserId;
      },
    );
  }

  async updateUserEmail(newEmail: string, oldEmail: string): Promise<?auth0$endUserObject> {
    logger.silly('AuthClient.updateUserEmail', { newEmail, oldEmail });
    return this._wrapAsAuth0Error(
      `Error updating email: [${oldEmail} -> ${newEmail}]`,
      async () => {
        const client: ManagementClient = await this._getManagementClient();
        const auth0UserId = await this.findUserIdByEmail(oldEmail);
        if (!auth0UserId) {
          return null;
        }
        const updatedUser = await client.updateUser({ id: auth0UserId }, {
          email: newEmail,
        });
        return updatedUser;
      },
    );
  }

  async addUserToAuthGroup(auth0UserId: string, auth0OrganizationId: string): Promise<*> {
    logger.silly('AuthClient.addUserToAuthGroup', { auth0UserId, auth0OrganizationId });

    return this._wrapAxiosAsAuth0Error(
      `Error adding auth user [${auth0UserId}] to group [${auth0OrganizationId}]`,
      async () => {
        const authorizationClient = await this._getAuthorizationClient();
        let response;
        try {
          response = await authorizationClient.patch(
            `${this._authorizationApiUri}/groups/${auth0OrganizationId}/members`,
            [auth0UserId],
          );
        } catch (err) {
          // try again, because of auth0 intermittent behavior:
          // https://community.auth0.com/questions/8813/authorization-extension-blocked-event-loop-when-si
          logger.error(
            'Error adding user as member of authorization group, trying again',
            { auth0UserId, auth0OrganizationId, err },
          );
          response = await authorizationClient.patch(
            `${this._authorizationApiUri}/groups/${auth0OrganizationId}/members`,
            [auth0UserId],
          );
        }

        return response.data;
      },
    );
  }

  async createUser(
    user: User,
    auth0OrganizationId: string,
    password: ?string,
  ): Promise<auth0$endUserObject> {
    logger.silly('AuthClient.createUser', { email: user.email, auth0OrganizationId });

    return this._wrapAsAuth0Error(
      `Error creating auth user with email: [${user.email}]`,
      async () => {
        const pass = password || this.generateTempPassword();
        const connection = this.config.auth.connectionName;
        const client: ManagementClient = await this._getManagementClient();

        let name;
        if (user.firstName && user.lastName) {
          name = `${user.firstName} ${user.lastName}`;
        }
        const auth0CreateUser: auth0$createUserObject = {
          email: user.email,
          given_name: user.firstName,
          family_name: user.lastName,
          connection,
          password: pass,
          email_verified: true,
          verify_email: false,
          user_metadata: {
            name,
            picture: user.profileImageURI,
          },
        };
        const auth0User = await client.createUser(auth0CreateUser);
        logger.debug('Created user in auth0', auth0User);
        try {
          await this.addUserToAuthGroup(auth0User.user_id, auth0OrganizationId);
          logger.debug('Added auth0 user to group', { auth0User, auth0OrganizationId });
        } catch (err) {
          logger.error(
            'Could not add newly created user to group, so delete auth0 user to revert creation',
            { auth0User, err },
          );
          await client.deleteUser({ id: auth0User.user_id });
          throw err;
        }

        return auth0User;
      },
    );
  }

  async disableUser(email: string) {
    logger.silly('AuthClient.disableUser', { email });

    return this._wrapAsAuth0Error(
      `Error blocking auth user with email: [${email}]`,
      async () => {
        const auth0UserId = await this.findUserIdByEmail(email);
        if (!auth0UserId) {
          throw new Auth0ApiError(`Could not locate user with email: [${email}]`);
        }
        const client: ManagementClient = await this._getManagementClient();
        return client.updateUser({ id: auth0UserId }, { blocked: true });
      },
    );
  }

  async enableUser(email: string) {
    logger.silly('AuthClient.enableUser', { email });

    return this._wrapAsAuth0Error(
      `Error enabling auth user with email: [${email}]`,
      async () => {
        const auth0UserId = await this.findUserIdByEmail(email);
        if (!auth0UserId) {
          throw new Auth0ApiError(`Could not locate user with email: [${email}]`);
        }
        const client: ManagementClient = await this._getManagementClient();
        return client.updateUser({ id: auth0UserId }, { blocked: false });
      },
    );
  }

  async getChangePasswordLink(email: string): Promise<*> {
    logger.silly('AuthClient.getChangePasswordLink', { email });

    return this._wrapAsAuth0Error(
      `Error getting auth change password link for user with email: [${email}]`,
      async () => {
        const client = await this._getManagementClient();
        return client.tickets.changePassword({
          email,
          connection_id: this.config.auth.connectionId,
        });
      },
    );
  }

  async getGroup(id: string): Promise<auth0$authorization$group | null> {
    logger.silly('AuthClient.getOrganization', { id });

    return this._returnNullOnAxiosError(
      `Unable to locate user organization with id: [${id}]`,
      async () => {
        const client = await this._getAuthorizationClient();
        const response = await client.get(`${this._authorizationApiUri}/groups/${id}`);
        return response.data;
      },
    );
  }

  /**
   * Creates the group and returns the created group id
   * @param name
   * @param description
   * @param auth0OrganizationId
   * @returns {Promise.<auth0$authorization$group>}
   */
  async createGroup(
    name: string, description: string,
    auth0OrganizationId?: string,
  ): Promise<auth0$authorization$group> {
    logger.silly('AuthClient.createOrganization', { name, description, auth0OrganizationId });

    return this._wrapAxiosAsAuth0Error(
      `Error calling auth create group api with name: [${name}]`,
      async () => {
        if (auth0OrganizationId) {
          const existingOrganization = await this.getGroup(auth0OrganizationId);

          if (existingOrganization) {
            logger.info('Organization already exists, returning existing organization');
          } else {
            logger.info('Organization does not already exist, attempting to create');
          }
        }

        // TODO: associate default roles_getAuthorizationClient to the created org
        const client = await this._getAuthorizationClient();
        const response = await client.post(`${this._authorizationApiUri}/groups`, { name, description });
        return response.data;
      },
    );
  }

  // TODO: Figure out what we should be doing in this case - Block all the users? Delete the organization?
  async disableOrganization(id: string) { // eslint-disable-line no-unused-vars
    logger.silly('AuthClient.disableOrganization', { id });
    logger.warn('Disable Organization is not currently implemented');
  }

  async getUserRoles(email: string): Promise<Auth0Role[]> {
    logger.silly('AuthClient.getUserRoles', { email });
    return this._wrapAxiosAsAuth0Error(
      `Error retrieving User Roles for ${email}]`,
      async () => {
        const authorizationClient = await this._getAuthorizationClient();
        const auth0UserId = await this.findUserIdByEmail(email);
        if (!auth0UserId) {
          logger.warn('Return empty roles for non-existent user', { email });
          return [];
        }
        const response =
          await authorizationClient.get(`${this._authorizationApiUri}/users/${auth0UserId}/roles`);
        return response.data;
      },
    );
  }

  async addRolesToUser(email: string, auth0RoleIds: string[]): Promise<*> {
    logger.silly('Auth0Client.addRolesToUser', { email, auth0RoleIds });
    return this._wrapAxiosAsAuth0Error(
      `Cannot add roles to auth0 user ${email}`,
      async () => {
        const authorizationClient = await this._getAuthorizationClient();
        const auth0UserId = await this.findUserIdByEmail(email);
        if (!auth0UserId) {
          logger.warn('Cannot add roles to non-existent auth0 user', { email, auth0RoleIds });
          throw new Error('Cannot add roles to non-existent auth0 user');
        }
        return authorizationClient.patch(`${this._authorizationApiUri}/users/${auth0UserId}/roles`, auth0RoleIds);
      },
    );
  }

  async removeRolesFromUser(email: string, auth0RoleIds: string[]): Promise<*> {
    logger.silly('Auth0Client.removeRolesFromUser', { email, auth0RoleIds });
    return this._wrapAxiosAsAuth0Error(
      `Cannot remove roles from auth0 user ${email}]`,
      async () => {
        const authorizationClient = await this._getAuthorizationClient();
        const auth0UserId = await this.findUserIdByEmail(email);
        if (!auth0UserId) {
          logger.warn('Cannot remove roles from non-existent auth0 user', { email, auth0RoleIds });
          throw new Error('Cannot remove roles from non-existent auth0 user');
        }
        // must specify `data` for delete request bodies: https://github.com/axios/axios/issues/736
        return authorizationClient.delete(`${this._authorizationApiUri}/users/${auth0UserId}/roles`, {
          data: auth0RoleIds,
        });
      },
    );
  }

  async getOrganizationRoles(auth0OrganizationId: string): Promise<Auth0Role[]> {
    logger.silly('AuthClient.getOrganizationRoles', { auth0OrganizationId });
    return this._wrapAxiosAsAuth0Error(
      `Error retrieving Group Roles for ${auth0OrganizationId}]`,
      async () => {
        const authorizationClient = await this._getAuthorizationClient();
        const response =
          await authorizationClient.get(`${this._authorizationApiUri}/groups/${auth0OrganizationId}/roles`);
        return response.data;
      },
    );
  }
}
