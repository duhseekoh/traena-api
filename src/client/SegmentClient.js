// @flow
import Analytics from 'analytics-node';
import Logger from '../common/logging/logger';

const logger = new Logger('SegmentClient.js');

type IdentifyMessage = Object;
type TrackMessage = Object;
type ScreenMessage = Object;
type PageMessage = Object;
type AnyMessage = IdentifyMessage | TrackMessage | ScreenMessage | PageMessage;

/**
 * Wrapper around the segment node client.
 * Provides an anonymousId if no userId is passed on an event.
 */
class SegmentClient {
  // segment http client
  analytics: Analytics;
  // server events may not have a user associated with them
  anonymousId: string;
  appName: string;
  appVersion: string;

  constructor(segmentWriteKey: ?string, appName: string, appVersion: string) {
    if (!segmentWriteKey) {
      logger.warn('Created a SegmentClient without a write key. No events will be sent to Segment.');
      // using a stub that does nothing. to be used when running locally.
      this.analytics = {
        identify: () => {},
        track: () => {},
        screen: () => {},
        page: () => {},
      };
    } else {
      this.analytics = new Analytics(segmentWriteKey);
      logger.debug('configured segment integration', { segmentWriteKey, appName, appVersion });
    }
    this.anonymousId = 'server';
    this.appName = appName;
    this.appVersion = appVersion;
  }

  identify(data: IdentifyMessage) {
    this.analytics.identify(this._attachAppMetadata(data));
  }

  track(data: TrackMessage) {
    this.analytics.track(this._attachAppMetadata(this._reconcilePayloadUserId(data)));
  }

  screen(data: ScreenMessage) {
    this.analytics.screen(this._attachAppMetadata(this._reconcilePayloadUserId(data)));
  }

  page(data: PageMessage) {
    this.analytics.page(this._attachAppMetadata(this._reconcilePayloadUserId(data)));
  }

  _attachAppMetadata(data: AnyMessage) {
    return {
      ...data,
      context: {
        app: {
          name: this.appName,
          version: this.appVersion,
        },
      },
    };
  }

  _reconcilePayloadUserId(data: TrackMessage | ScreenMessage | PageMessage) {
    let payload = data;
    if (!payload.userId) {
      payload = { ...payload, anonymousId: this.anonymousId };
    }
    return payload;
  }
}

export default SegmentClient;
