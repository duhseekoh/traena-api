// @flow
import BaseModel from './BaseModel';
import { ResourceValidationError } from '../../common/errors';

export type Tag = {
  contentId: number,
  text: string,
  createdTimestamp: string,
  modifiedTimestamp: string,
}

export default class TagModel extends BaseModel {
  contentId: number;
  text: string;

  static idColumn = ['contentId', 'text'];

  static get tableName(): string {
    return 'Tag';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        contentId: { type: 'integer' },
        text: { type: 'string', pattern: '([A-Z]|[a-z]|[-_]|[0-9])+' },
      },
      additionalProperties: false,
      required: ['contentId', 'text'],
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  /**
   * Associate multiple tags with a given piece of content
   *
   * @param contentId The id of the content to associate with this tag
   * @param tagStrings List of strings to be associated with this piece of content
   * @returns {Promise.<QueryBuilder>}
   */
  static async createTags(contentId: number, tagStrings: string[] = []) {
    if (Array.isArray(tagStrings)) {
      // Convert the strings into tag objects
      const tags = tagStrings.map(txt => ({ contentId, text: txt }));
      return this.query().insert(tags).execute();
    }

    throw new ResourceValidationError('Tags must be an array of strings');
  }

  static async deleteTagsForContent(contentId: number) {
    return this.query().delete().where('contentId', contentId);
  }
}
