// @flow
import { Model } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import BaseModel, { type PagedDbResult } from './BaseModel';
import ChannelModel from './ChannelModel';
import UserModel from './UserModel';
import SubscriptionModel from './SubscriptionModel';

import type { OrganizationType } from '../fieldConstants';
import { ORGANIZATION_TYPES } from '../fieldConstants';

type DbOrganizationPagePromise = Promise<PagedDbResult<OrganizationModel>>; //eslint-disable-line

export type OrganizationDTO = {
  id?: ?number;
  name: string;
  organizationType: OrganizationType;
  commonSearchTerms: ?string[];
  isDisabled: boolean;
  auth0Connection: string;
  authDomains: string[];
  auth0OrganizationName: string;
  auth0OrganizationId: string;
}

export default class OrganizationModel extends BaseModel {
  id: number;
  name: string;
  organizationType: OrganizationType;
  commonSearchTerms: ?string[];
  isDisabled: boolean;
  auth0Connection: string;
  authDomains: string[];
  auth0OrganizationName: string;
  auth0OrganizationId: string;

  static get tableName(): string {
    return 'Organization';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        organizationType: { type: 'string', enum: Object.values(ORGANIZATION_TYPES) },
        isDisabled: { type: 'boolean' },
        auth0Connection: { type: 'string' }, // Connection string
        auth0OrganizationName: { type: 'string' }, // Ties together this record and auth0 record
        auth0OrganizationId: { type: 'string' },
        authDomains: { type: 'array', items: { type: 'string' } }, // email domains of users to match for this org
        commonSearchTerms: { type: 'array', items: { type: 'string' } },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
      },
      required: ['name', 'organizationType', 'auth0OrganizationId'],
      additionalProperties: false,
    };
  }

  static get relationMappings(): Object {
    return {
      // all of the channels owned by this organization
      channels: {
        relation: Model.HasManyRelation,
        modelClass: ChannelModel,
        join: {
          from: 'Organization.id',
          to: 'Channel.organizationId',
        },
      },

      // all of the subscriptions to channels
      subscriptions: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionModel,
        join: {
          from: 'Organization.id',
          to: 'Subscription.organizationId',
        },
      },

      // all of the channels this organization is subscribed to,
      // regardless of level of access given to users
      subscribedChannels: {
        relation: Model.ManyToManyRelation,
        modelClass: ChannelModel,
        join: {
          from: 'Organization.id',
          through: {
            from: 'Subscription.organizationId',
            to: 'Subscription.channelId',
          },
          to: 'Channel.id',
        },
      },

      users: {
        relation: Model.HasManyRelation,
        modelClass: UserModel,
        join: {
          from: 'Organization.id',
          to: 'User.organizationId',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getAllOrganizationIds(): Promise<number[]> {
    return this.query().select('id').execute().map(it => it.id);
  }

  static async getAllOrganizations(pageable: Pageable): DbOrganizationPagePromise {
    return OrganizationModel
      .query()
      .select('Organization.*')
      .page(pageable.page, pageable.size)
      .orderBy('name', 'desc')
      .execute();
  }

  static async getById(organizationId: number): Promise<OrganizationModel> {
    return this
      .query()
      .findById(organizationId).execute();
  }

  static async disable(organizationId: number): Promise<OrganizationModel> {
    return this.query().patch({ isDisabled: true }).where('id', organizationId).execute();
  }

  // eslint-disable-next-line use-before-define Organization
  static async upsert(organization: Organization): Promise<OrganizationModel> {
    if (organization.id) {
      return this.query().patch(organization).where('id', organization.id).first()
        .returning('*')
        .execute();
    }
    return this.query().insert(organization).returning('*').execute();
  }

  static async updateCommonSearchTerms(organizationId: number, terms: string[]): Promise<string[]> {
    return this.query()
      .patch({
        commonSearchTerms: terms,
      })
      .where('Organization.id', organizationId)
      .then(() => terms);
  }

  // Instance methods

  /**
   * Get the orgs subscriptions to channels
   */
  async getSubscriptions(): Promise<SubscriptionModel[]> {
    return this.$relatedQuery('subscriptions');
  }

  /**
   * Get the channels
   */
  async getSubscribedChannels(): Promise<ChannelModel[]> {
    return this.$relatedQuery('subscribedChannels');
  }
}

export type Organization = OrganizationDTO | OrganizationModel;
