// @flow
import type moment$Moment from 'moment';
import { Model, ref } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import type { PagedDbResult } from './BaseModel';
import UserModel, { type User } from './UserModel';
import AnalyticCountByDateResult from '../AnalyticCountByDateResult';
import config from '../../../src/common/config';

type DbEventPagePromise = Promise<PagedDbResult<EventModel>>; // eslint-disable-line no-use-before-define

// All of the events we store in our database and also send over to segment.
// Any event not specifically labeled as a segment type, will be sent to segment
// using this spec https://segment.com/docs/spec/track/
export const EventTypes = {
  // Administrative
  ORG_CREATED: 'ORG_CREATED',
  ORG_DISABLED: 'ORG_DISABLED',
  USER_CREATED: 'USER_CREATED',
  USER_DISABLED: 'USER_DISABLED',
  CHANNEL_CREATED: 'CHANNEL_CREATED',
  CHANNEL_MODIFIED: 'CHANNEL_MODIFIED',
  CHANNEL_SUBSCRIBED: 'CHANNEL_SUBSCRIBED',
  CHANNEL_SUBSCRIPTION_MODIFIED: 'CHANNEL_SUBSCRIPTION_MODIFIED',
  CHANNEL_UNSUBSCRIBED: 'CHANNEL_UNSUBSCRIBED',
  CONTENT_REINDEXED: 'CONTENT_REINDEXED',
  USER_REINDEXED: 'USER_REINDEXED',
  // User Specific - Backend
  ACTIVITY_CREATED: 'ACTIVITY_CREATED',
  ACTIVITY_MODIFIED: 'ACTIVITY_MODIFIED',
  ACTIVITY_DELETED: 'ACTIVITY_DELETED',
  ACTIVITY_RESPONSE_CREATED: 'ACTIVITY_RESPONSE_CREATED',
  ACTIVITY_RESPONSE_MODIFIED: 'ACTIVITY_RESPONSE_MODIFIED',
  CONTENT_CREATED: 'CONTENT_CREATED',
  CONTENT_MODIFIED: 'CONTENT_MODIFIED',
  CONTENT_LIKED: 'CONTENT_LIKED',
  CONTENT_UNLIKED: 'CONTENT_UNLIKED',
  CONTENT_BOOKMARKED: 'CONTENT_BOOKMARKED',
  CONTENT_UNBOOKMARKED: 'CONTENT_UNBOOKMARKED',
  CONTENT_ARCHIVED: 'CONTENT_ARCHIVED',
  COMMENT_ADDED: 'COMMENT_ADDED',
  COMMENT_DELETED: 'COMMENT_DELETED',
  FEED_LOADED: 'FEED_LOADED',
  WHATS_NEW_LOADED: 'WHATS_NEW_LOADED',
  IDENTIFY: 'IDENTIFY', // see https://segment.com/docs/spec/identify/
  SEARCHED_CONTENT: 'SEARCHED_CONTENT',
  TASK_CREATED: 'TASK_CREATED',
  TASK_UPDATED: 'TASK_UPDATED',
  TASK_DELETED: 'TASK_DELETED',
  TASK_COMPLETED: 'TASK_COMPLETED',
  // User Specific - Mobile or Web
  VIDEO_PROGRESSED: 'VIDEO_PROGRESSED',
  CONTENT_VIEWED: 'CONTENT_VIEWED',
  CONTENT_IMPRESSIONED: 'CONTENT_IMPRESSIONED',
  USER_LOGGED_IN: 'USER_LOGGED_IN',
  USER_LOGGED_OUT: 'USER_LOGGED_OUT',
  // User Specific - Mobile Only
  APP_LAUNCHED: 'APP_LAUNCHED',
  APP_ACTIVATED: 'APP_ACTIVATED',
  APP_DEACTIVATED: 'APP_DEACTIVATED',
  SCREEN: 'SCREEN', // see https://segment.com/docs/spec/screen/
  // User Specific - Web Only
  PAGE: 'PAGE', // see https://segment.com/docs/spec/page/
};

// Events related to content that we can display as a raw analytics feed to users.
const ContentFeedEventTypes = [
  EventTypes.CONTENT_BOOKMARKED,
  EventTypes.CONTENT_UNBOOKMARKED,
  EventTypes.CONTENT_CREATED,
  EventTypes.CONTENT_MODIFIED,
  EventTypes.CONTENT_LIKED,
  EventTypes.CONTENT_UNLIKED,
  EventTypes.CONTENT_ARCHIVED,
  EventTypes.COMMENT_ADDED,
  EventTypes.COMMENT_DELETED,
  EventTypes.CONTENT_VIEWED,
  EventTypes.CONTENT_IMPRESSIONED,
  EventTypes.TASK_COMPLETED,
  EventTypes.VIDEO_PROGRESSED,
  // TODO - add 'activities' events when thats a desired requirement
];

export type EventType = $Keys<typeof EventTypes>;

export const EventSource = {
  SERVER: 'SERVER',
  CLIENT: 'CLIENT',
};

export type EventDTO = {
  id?: ?number;
  userId?: ?number;
  type: EventType,
  data: any;
  modifiedTimestamp?: string;
  createdTimestamp?: string;
  source: $Keys<typeof EventSource>;
  // read only properties supplied by relations
  +user: User;
};

export type CreateEventDTO = {
  userId?: number;
  type: EventType,
  data: any;
  source?: $Keys<typeof EventSource>;
};

export default class EventModel extends Model {
  id: number;
  userId: ?number;
  type: EventType;
  data: any;
  modifiedTimestamp: string;
  createdTimestamp: string;
  source: $Keys<typeof EventSource>;
  // read only properties supplied by relations
  +user: User;

  static get eventTypes(): * {
    return EventTypes;
  }

  static get tableName(): string {
    return 'Event';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        type: { type: 'string' },
        userId: { type: 'integer' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        data: { type: 'object' },
        source: { type: 'string', enum: Object.values(EventSource) },
      },
      additionalProperties: false,
      required: ['type', 'source'],
    };
  }

  static get relationMappings(): Object {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Event.userId',
          to: 'User.id',
        },
      },
    };
  }

  $beforeInsert = () => {
    if (this.constructor.hasTimestamps) {
      const now = new Date().toISOString();
      this.modifiedTimestamp = now;
      this.createdTimestamp = now;
    }
  }

  static async insert(event: Event | CreateEventDTO): Promise<EventModel> {
    return this.query().insert(event).returning('*').execute();
  }

  /**
   * Returns an array of AnalyticCountByDateResult objects, containing a date in ISO-8601 format, and the count of
   * events raise by the provided user, for the provided eventTypes on that date.
   *
   * @param userId - The user to locate events for
   * @param offset - The user's timezone offset (e.g. '+05:00') used to calculate the correct date context for events
   * @param eventTypes - The type of events to include in the count
   * @param daysOfHistory - The number of days worth of data to include, defaults to 14
   * @returns {Promise.<void>}
   */
  static async getEventCountForUserByDate(
    userId: number, offset: string, eventTypes: EventType[],
    daysOfHistory?: ?number,
  ): Promise<AnalyticCountByDateResult[]> {
    // The number of days of history to include in the result, by default, two weeks worth.
    const daysHistory = daysOfHistory || config.analytics.user.history.days || 14;

    /* Query string value representing the Date portion of the createdTimestamp in the provided offset. As per
     https://www.postgresql.org/docs/9.6/static/datatype-datetime.html#DATATYPE-TIMEZONES when getting a timestamp in
     a particular zone, postgres uses posix zones which are inverted from ISO (i.e. in posix zones west of utc is +
     instead of the ISO standard -). Thus as per
     https://stackoverflow.com/questions/7117355/in-postgresql-how-to-un-invert-the-timezone-offsets-with-at-time-zone
     we use interval to somehow invert the ISO timezone offset to be usable with the posix function */
    const createdDate = `DATE( timezone( '${offset}'::interval, "createdTimestamp" ))`;
    // The earliest date to get analytics data for
    const startDate = `(DATE(timezone('${offset}'::interval, now())) - interval '${daysHistory} day')`;

    // we cannot pass the 'offset' as a query parameter (i.e. ? or :offset) because the queries emitted by objection
    // treat each reference to a parameter a new parameter (even when using named parameters) and postgres apparently
    // cannot tell that two parameters with the same value are equivalent. This leads to postgres errors stating that
    // the selected value must be part of the 'group by' statement.
    return this.query()
      .select(EventModel.raw(`${createdDate}, COUNT(*)`))
      .whereRaw(`${startDate} <= ${createdDate}`)
      .andWhere(EventModel.raw('type = ANY( ?::text[])', [eventTypes]))
      .andWhere('userId', userId)
      .groupByRaw(createdDate)
      .orderByRaw(`${createdDate} DESC`)
      .map(row => new AnalyticCountByDateResult(row.date, row.count));
  }

  static async getLatestEvent(
    userId: number,
    eventType: EventType,
  ): Promise<?EventModel> {
    return this.query()
      .select('Event.*')
      .where('type', eventType)
      .andWhere('userId', userId)
      .orderBy('createdTimestamp', 'desc')
      .first();
  }

  static async getCurrentActivityStreakForUser(userId: number, offset: string): Promise<number> {
    const createdDate = `DATE( timezone( '${offset}'::interval, "createdTimestamp" ))`;
    const eventTypes = [EventTypes.USER_LOGGED_IN, EventTypes.APP_ACTIVATED, EventTypes.APP_LAUNCHED];
    const result = await EventModel.knex().raw(`
      WITH user_activity AS
          ( SELECT DISTINCT( ${createdDate} ) AS create_date
              FROM "Event"
              WHERE "userId" = ${userId}
                AND type = ANY( ?::text[])
          )
        SELECT   COUNT(*)
          FROM user_activity
          WHERE user_activity.create_date >
            ( SELECT   d.d
                FROM generate_series( CURRENT_DATE, '2017-01-01'::DATE, '-1 day' ) AS d( d )
                LEFT OUTER JOIN user_activity
                ON user_activity.create_date          = d.d::DATE
                WHERE user_activity.create_date IS NULL
                ORDER BY d.d DESC
                LIMIT 1 )
     `, [eventTypes]);

    return result.rows[0].count;
  }

  static async getViewCountByContentId(contentId: number, organizationId?: number): Promise<number> {
    const query = this.query()
      .count()
      .joinRelation('user')
      .where('type', EventTypes.CONTENT_VIEWED)
      .where(ref('data:contentId'), contentId);

    if (organizationId) {
      query.where('user.organizationId', organizationId);
    }

    return query
      .first()
      .then(({ count }) => count);
  }

  static async getEventsInRange(
    userId: number,
    eventType: string,
    minDate?: moment$Moment,
    maxDate?: moment$Moment,
  ): Promise<EventModel[]> {
    const builder = this.query()
      .select('Event.*')
      .joinRelation('user')
      .where('user.id', userId)
      .where('type', eventType);

    if (maxDate) {
      builder.whereRaw(`data->>'timestamp' < '${maxDate.toISOString()}'`);
    }

    if (minDate) {
      builder.whereRaw(`data->>'timestamp' > '${minDate.toISOString()}'`);
    }

    return builder.execute();
  }

  /**
   * All events that can be related to a particular content item. The related user
   * is also attached to each result.
   * Optionally filtered to a specific organization.
   */
  static async getContentItemEventsPaginated(
    contentId: number,
    organizationId?: number,
    pageable: Pageable,
  ): DbEventPagePromise {
    const query = this.query()
      .select('Event.*')
      .joinEager('user')
      .whereIn('type', ContentFeedEventTypes)
      .where(ref('data:contentId'), contentId)
      .orderBy('Event.createdTimestamp', 'desc')
      .page(pageable.page, pageable.size);

    if (organizationId) {
      query.where('user.organizationId', organizationId);
    }

    return query;
  }
}

export type Event = EventDTO | EventModel;
