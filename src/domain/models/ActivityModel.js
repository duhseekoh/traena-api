// @flow
import { Model } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import type { PagedDbResult } from './BaseModel';
import BaseModel from './BaseModel';
import ContentItemModel from './ContentItemModel';
import type { ActivityType } from '../fieldConstants';
import activitySchema from './schema/db/activity.json';

type ActivityModelPagePromise = Promise<PagedDbResult<ActivityModel>>; // eslint-disable-line no-use-before-define

type Choice = {
  id: string,
  choiceText: string,
  correct: boolean,
}

type MultipleChoiceQuestion = {
  id: string,
  questionText: string,
  choices: Choice[],
  summary: string,
};

type QuestionSet = {
  questions: MultipleChoiceQuestion[],
};

type PollChoice = {
  id: string,
  choiceText: string,
};

type Poll = {
  title: string,
  choices: PollChoice[],
};

export type PollChoiceResult = {
  count: number,
  percent: number,
  choiceId: string,
};

export type ActivityDTO = {
  id?: ?number,
  contentId: number,
  version: ?number,
  type: ActivityType,
  body: QuestionSet | Poll,
};

type PollResults = {
  activity: ActivityModel, // eslint-disable-line no-use-before-define
  count: number,
  choiceResults: {
    [choiceId: string]: PollChoiceResult,
  }
}

export type ActivityResults = PollResults; // | otherTypeResults

export default class ActivityModel extends BaseModel {
  id: number;
  contentId: number;
  version: number;
  type: ActivityType;
  body: QuestionSet | Poll;

  static get tableName(): string {
    return 'Activity';
  }

  static get relationMappings(): Object {
    return {
      content: {
        relation: Model.BelongsToOneRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Activity.contentId',
          to: 'Content.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return activitySchema;
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async upsert(activity: Activity): Promise<ActivityModel> {
    if (activity.id) {
      activity.version += 1;
      return this.query().patch(activity).where('id', activity.id).first()
        .returning('*');
    }
    return this.query().insert(activity).returning('*');
  }

  static async delete(activityId: number): Promise<number> {
    return this.query()
      .deleteById(activityId);
  }

  // get specific activity by id
  static getById(activityId: number): Promise<?ActivityModel> {
    return this.query()
      .where({ id: activityId })
      .first()
      .execute();
  }

  // get all activities for a content item
  static getAllByContentId(contentId: number, pageable: Pageable): ActivityModelPagePromise {
    return this.query()
      .where({ contentId })
      .page(pageable.page, pageable.size)
      .execute();
  }
}

export type Activity = ActivityDTO | ActivityModel;
