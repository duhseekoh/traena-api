// @flow
export type CountOption = {
  average: number,
  spread: number,
  sparseness: number
};

export type TimeframeOption = {
  startDaysAgo: number,
  daysToSpan: number
};

export type GenerationOptions = {
  count?: number,
  authorCount?: number,
  timeframe?: {
    startDaysAgo: number,
    daysToSpan: number
  },
  homogenous?: boolean,
  likes?: {
    average: number,
    spread: number,
    sparseness: number
  },
  comments?: {
    average: number,
    spread: number,
    sparseness: number
  },
  useDummyContent?: boolean,
  tags?: {
    tagWords: string[],
    sparseness: number
  },
  datasetPath?: string
};
