// @flow
import { Model } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import type { PagedDbResult } from './BaseModel';
import BaseModel from './BaseModel';
import OrganizationModel from './OrganizationModel';
import ChannelModel from './ChannelModel';
import UserModel from './UserModel';
import {
  SUBSCRIPTION_SOURCES,
  SUBSCRIPTION_VISIBILITIES,
  type ChannelVisibility,
  type SubscriptionSources,
  type SubscriptionVisibility,
} from '../fieldConstants';

type DbSubscriptionPagePromise = Promise<PagedDbResult<SubscriptionModel>>; // eslint-disable-line no-use-before-define
type DbUserPagePromise = Promise<PagedDbResult<UserModel>>;

export type SubscriptionDTO = {
  channelId: number,
  organizationId: number,
  visibility: SubscriptionVisibility,
  subscribedUserIds?: ?(number[]),
};

export type CreateSubscriptionDTO = {
  channelId: number,
  organizationId: number,
  visibility?: SubscriptionVisibility,
};

/**
 * A subscription defines an organizations relationship to a channel.
 * In that definition is the visibility setting of who in the subscribed
 * org can access the channel content.
 */
export default class SubscriptionModel extends BaseModel {
  channelId: number;
  organizationId: number;
  visibility: SubscriptionVisibility;
  // read-only retrieved through relations
  +channel: ?ChannelModel;
  +organization: ?OrganizationModel;
  // read-only retrieved through subquery
  +subscribedUserIds: ?(number[]);

  static get tableName(): string {
    return 'Subscription';
  }

  static get idColumn(): string[] {
    return ['channelId', 'organizationId'];
  }

  static get relationMappings(): Object {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Subscription.organizationId',
          to: 'Organization.id',
        },
      },

      channel: {
        relation: Model.BelongsToOneRelation,
        modelClass: ChannelModel,
        join: {
          from: 'Subscription.channelId',
          to: 'Channel.id',
        },
      },

      // the assigned users to this subscription, which means they have access
      // to the associated channel in the subscription.
      subscribedUsers: {
        relation: Model.ManyToManyRelation,
        modelClass: UserModel,
        join: {
          from: 'Subscription.channelId',
          through: {
            from: 'UserSubscription.channelId',
            to: 'UserSubscription.userId',
          },
          to: 'User.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        channelId: { type: 'integer' },
        organizationId: { type: 'integer' },
        visibility: {
          enum: Object.values(SUBSCRIPTION_VISIBILITIES),
        },
      },
      additionalProperties: false,
      required: ['channelId', 'organizationId', 'visibility'],
    };
  }

  static get hasTimestamps(): boolean {
    return false;
  }

  /**
   * Get a single subscription. Note there is no `id` column on a Subscription.
   * It's a composite primary key.
   * Retrieves subscription fields and an array of user ids explicity subscribed.
   */
  static async getById(
    channelId: number,
    organizationId: number,
  ): Promise<?SubscriptionModel> {
    return this.query()
      .eager('[channel.organization, subscribedUsers]')
      // grab just the user ids
      .modifyEager('subscribedUsers', (builder) => {
        builder.select('User.id');
      })
      // turns [{id}, ...] into [id, ...]
      .runAfter(async subs => subs.map((sub) => {
        // unfortunately, in order to retain the model methods, we can't copy by
        // property spread, so we're mutating each sub model.
        sub.subscribedUserIds = sub.subscribedUsers.map(user => user.id);
        delete sub.subscribedUsers;
        return sub;
      }))
      .findById([channelId, organizationId]);
  }

  /**
   * Get's all subscriptions for a given organization, paginated.
   */
  static async getByOrganizationIdPaginated(
    organizationId: number,
    pageable: Pageable,
    options: {
      channelVisibilities?: ?ChannelVisibility[],
      subscriptionSource?: ?SubscriptionSources,
    } = {
      subscriptionSource: SUBSCRIPTION_SOURCES.ALL,
    },
  ): DbSubscriptionPagePromise {
    const builder = this.query()
      // pulling subscriptions for this org, regardless of where channel originates
      .where('Subscription.organizationId', organizationId)
      .joinEager('channel.organization')
      .page(pageable.page, pageable.size);

    // sort by whatever is passed in
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        builder.orderBy(it.property, it.direction);
      });
    }

    // filter on the channel org, to see our channels vs theirs
    if (options.subscriptionSource === SUBSCRIPTION_SOURCES.INTERNAL) {
      builder.where('channel.organizationId', organizationId);
    } else if (options.subscriptionSource === SUBSCRIPTION_SOURCES.EXTERNAL) {
      builder.where('channel.organizationId', '!=', organizationId);
    }

    // filter on channel visibility with OR logic
    const { channelVisibilities } = options;
    if (channelVisibilities) {
      builder.where((builder2) => {
        channelVisibilities.forEach((visibility) => {
          builder2.orWhere('channel.visibility', visibility);
        });
      });
    }

    return builder.execute();
  }

  /**
   * Subscribe an organization to a channel
   */
  static async insert(subscription: Subscription): Promise<?SubscriptionModel> {
    const existingSubscription = await this.getById(subscription.channelId, subscription.organizationId);
    if (existingSubscription) {
      return existingSubscription;
    }

    return this.query().insert(subscription)
      .then(() =>
        SubscriptionModel.getById(
          subscription.channelId,
          subscription.organizationId,
        ),
      );
  }

  /**
   * Unsubscribe an organization from a channel
   */
  static async delete(organizationId: number, channelId: number): Promise<boolean> {
    return this.query()
      .deleteById([channelId, organizationId])
      .then(() => true);
  }

  /**
   * Update a given subscription then retrieve the subscription
   */
  static async update(subscription: Subscription): Promise<?SubscriptionModel> {
    return this.query()
      .patch(subscription)
      .whereComposite(['channelId', 'organizationId'], '=', [
        subscription.channelId,
        subscription.organizationId,
      ])
      .then(() =>
        SubscriptionModel.getById(
          subscription.channelId,
          subscription.organizationId,
        ),
      );
  }

  /**
   * Unrelates all users subscribed to a channel in a specific organization.
   */
  static async unrelateUsers(channelId: number, organizationId: number): Promise<number> {
    const subscription = await this.query()
      .findById([channelId, organizationId]);
    return subscription.unrelateUsers();
  }

  static async getAllSubscribedUsers(channelId: number, organizationId: number): Promise<UserModel[]> {
    const subscription = await this.query()
      .findById([channelId, organizationId]);
    if (subscription.visibility === SUBSCRIPTION_VISIBILITIES.ORGANIZATION) {
      return UserModel.getByOrganizationId(organizationId);
    }

    return subscription.getSubscribedUsers();
  }

  // INSTANCE METHODS

  /**
   * Unrelates all users subscribed to a channel in a specific organization.
   */
  async unrelateUsers(): Promise<number> {
    return this.$relatedQuery('subscribedUsers')
      .unrelate()
      .where('User.organizationId', this.organizationId)
      .execute();
  }

  /**
   * Subscribes users to a channel, based on *this* org subscription.
   * If users passed in are not from associated org in this org subscription
   * then they are filtered out.
   */
  async relateUsers(userIds: number[]): Promise<number> {
    // filter userIds by organization users.
    const org = await this.$relatedQuery('organization');
    const userIdsInOrg: number[] = await org
      .$relatedQuery('users')
      .whereIn('User.id', userIds)
      .map(user => user.id);

    return this.$relatedQuery('subscribedUsers')
      .relate(userIdsInOrg)
      .execute()
      .then(userSubs => userSubs.length);
  }

  /**
   * Get user directly subscribed to the channel this subscription covers.
   * Filtered down to just users within the subscription's org.
   */
  async getSubscribedUsers(): Promise<UserModel[]> {
    return this.$relatedQuery('subscribedUsers')
      .where('User.organizationId', this.organizationId)
      .execute();
  }

  /**
   * Get user directly subscribed to the channel this subscription covers.
   * Filtered down to just users within the subscription's org.
   */
  async getSubscribedUsersPaginated(pageable: Pageable): DbUserPagePromise {
    return this.$relatedQuery('subscribedUsers')
      .page(pageable.page, pageable.size)
      .orderBy('firstName', 'asc')
      .where('User.organizationId', this.organizationId)
      .execute();
  }
}

export type Subscription = SubscriptionDTO | SubscriptionModel;
