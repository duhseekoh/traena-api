// @flow
import { Pageable } from '@panderalabs/koa-pageable';
import { Model, type QueryBuilder } from 'objection';
import BaseModel from './BaseModel';
import ContentItemModel, {
  _activityCountForContentPartialSelector,
  _likeCountForContentPartialSelector,
  _commentCountForContentPartialSelector,
  _completedCountForContentPartialSelector,
} from './ContentItemModel';
import TaskModel from './TaskModel';
import UserModel from './UserModel';
import ChannelModel from './ChannelModel';
import OrganizationModel from './OrganizationModel';
import type { PagedDbResult } from './BaseModel';
import { CONTENT_ITEM_STATUSES } from '../fieldConstants';

type DbSeriesPagePromise = Promise<PagedDbResult<SeriesModel>>; // eslint-disable-line no-use-before-define
type DbContentItemPagePromise = Promise<PagedDbResult<ContentItemModel>>; // eslint-disable-line no-use-before-define

function _appendContentPreviewPartialSelector(seriesQuery, subscriberOrgId) {
  return seriesQuery
    .mergeEager('[content as contentPreview]')
    .modifyEager('content as contentPreview', (builder) => {
      builder.select(
        '*',
        _likeCountForContentPartialSelector()
          .where('organizationId', subscriberOrgId)
          .as('likeCount'),
        _commentCountForContentPartialSelector()
          .where('organizationId', subscriberOrgId)
          .as('commentCount'),
        _activityCountForContentPartialSelector()
          .as('activityCount'),
      )
        .eager('[author.organization, tags, channel, series]')
        .orderBy('Series_Content.order');
    });
}

/**
 * Gets the timestamp of the latest completed task (completed content) for the
 * related series.
 * For use as a subquery.
 */
function latestProgressTimestampPartialSelector(userId: number): QueryBuilder {
  return SeriesModel.relatedQuery('tasks') // eslint-disable-line no-use-before-define
    // TODO - use completedTimestamp when that field is fixed
    .select('Task.modifiedTimestamp')
    .where('Task.userId', userId)
    .limit(1)
    .first()
    // makes it so we always select the most recently completed content from the series
    .orderBy('Task.modifiedTimestamp', 'desc');
}

/**
 * Gets the number of active content items for the related series
 * For use as a subquery.
 */
export function contentCountForSeriesPartialSelector(): QueryBuilder {
  return SeriesModel.relatedQuery('content') // eslint-disable-line no-use-before-define
    .count()
    .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED);
}

export type SeriesDTO = {
  id?: ?number;
  authorId: number;
  channelId: number;
  title: string;
  description: string;
};

export type CreateSeriesDTO = {
  ...$Exact<SeriesDTO>,
  contentIds: number[],
};

export type UpdateSeriesDTO = {
  ...$Exact<CreateSeriesDTO>,
  channelId: number,
  id: number,
};

export default class SeriesModel extends BaseModel {
  id: number;
  authorId: number;
  channelId: number;
  title: string;
  description: string;
  // extra data possibly included
  +contentCount: ?number;
  // relations (read only)
  +contentPreview: ?ContentItemModel[];
  +author: ?UserModel;
  +channel: ?ChannelModel;
  +organization: ?OrganizationModel;
  // added in queries that we need to sort by a users progress with a series
  +latestProgressTimestamp: ?string;

  static get tableName(): string {
    return 'Series';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        authorId: { type: 'integer' },
        channelId: { type: 'integer' },
        title: { type: 'string', maxLength: 100 },
        description: { type: 'string', maxLength: 280 },
      },
      additionalProperties: false,
      required: ['authorId', 'channelId', 'title'],
    };
  }

  static get relationMappings(): Object {
    return {
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Series.authorId',
          to: 'User.id',
        },
      },
      channel: {
        relation: Model.BelongsToOneRelation,
        modelClass: ChannelModel,
        join: {
          from: 'Series.channelId',
          to: 'Channel.id',
        },
      },
      organization: {
        relation: Model.HasOneThroughRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Series.channelId',
          through: {
            from: 'Channel.id',
            to: 'Channel.organizationId',
          },
          to: 'Organization.id',
        },
      },
      content: {
        relation: Model.ManyToManyRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Series.id',
          through: {
            from: 'Series_Content.seriesId',
            to: 'Series_Content.contentId',
            extra: ['order'],
          },
          to: 'Content.id',
        },
      },
      // Series can access completion status of content items within it by referencing
      // the associated tasks to each post in the series.
      tasks: {
        relation: Model.ManyToManyRelation,
        modelClass: TaskModel,
        join: {
          from: 'Series.id',
          through: {
            from: 'Series_Content.seriesId',
            to: 'Series_Content.contentId',
          },
          to: 'Task.contentId',
        },
      },
    };
  }

  // create or update a series
  static async upsert(series: SeriesModel): Promise<SeriesModel> {
    if (series.id) {
      return this.query().patch(series).where('id', series.id).first()
        .returning('*');
    }
    return this.query().insert(series).returning('*');
  }

  static async relateContentToSeries(seriesId: number, contentIds: number[]): Promise<SeriesModel> {
    const seriesToRelate = await this.query()
      .findById(seriesId);

    const contentRelations = contentIds.map((id, i) => ({
      id,
      order: i,
    }));
    return seriesToRelate.$relatedQuery('content').relate(contentRelations);
  }

  static async unrelateContentFromSeries(seriesId: number) {
    const seriesToUnrelate = await this.query()
      .findById(seriesId);

    return seriesToUnrelate.$relatedQuery('content')
      .unrelate();
  }

  // get specific series by id
  static async getById(
    seriesId: number,
    includeContentPreview: boolean = false,
    subscriberOrgId?: number,
  ): Promise<?SeriesModel> {
    let seriesQuery = this.query()
      .findById(seriesId)
      .select(
        'Series.*',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      );

    if (includeContentPreview) {
      seriesQuery = _appendContentPreviewPartialSelector(seriesQuery, subscriberOrgId);
    }

    seriesQuery.mergeEager('[author, channel, organization]');
    return seriesQuery.execute();
  }

  static getByOrganizationId(
    organizationId: number,
    includeContentPreview: boolean,
    pageable: Pageable,
  ): DbSeriesPagePromise {
    let seriesQuery = this.query()
      .select(
        'Series.*',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      );

    if (includeContentPreview) {
      seriesQuery = _appendContentPreviewPartialSelector(seriesQuery, organizationId);
    }

    return seriesQuery
      .mergeEager('[author, channel]')
      .joinRelation('organization')
      .where('organization.id', organizationId)
      .orderBy('createdTimestamp', 'desc')
      .page(pageable.page, pageable.size)
      .execute();
  }

  /**
   * Get series content meta information for all series passed in
   */
  static getContentMeta(
    seriesIds: number[],
  ): [{
    seriesId: number,
    contentCount: number,
    contentIds: number[],
  }] {
    return this.query()
      .select(
        'Series.id',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      )
      .eager('content')
      .modifyEager('content', (eagerBuilder) => {
        eagerBuilder
          .orderBy('order')
          .where('status', CONTENT_ITEM_STATUSES.PUBLISHED);
      })
      .whereIn('Series.id', seriesIds)
      .execute()
      .then(meta => meta.map(entry => ({
        seriesId: entry.id,
        contentCount: entry.contentCount,
        contentIds: entry.content.map(c => c.id),
      })));
  }

  /**
   * Retrieve all series that match a set of channel ids.
   */
  static getByChannelIds(
    channelIds: number[],
  ): SeriesModel[] {
    return this.query()
      .select(
        'Series.*',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      )
      // default sort
      .orderBy('createdTimestamp', 'desc')
      // join so we can run a where on a joined column
      .mergeEager('[author, channel, organization]')
      .joinRelation('channel')
      .whereIn('channel.id', channelIds)
      .execute();
  }

  /**
   * Retrieve all series that match a set of channel ids. Series are returned
   * in context of a particular subscriber organization.
   */
  static getByChannelIdsPaginated(
    userId?: number,
    channelIds: number[],
    subscriberOrgId: number,
    options?: {
      includeContentPreview?: boolean,
      filterOutEmptySeries?: boolean,
      // if supplied, limits us to only these series
      seriesIds?: number[],
    } = {
      includeContentPreview: false,
      filterOutEmptySeries: false,
      seriesIds: [],
    },
    pageable: Pageable,
  ): DbSeriesPagePromise {
    let builder = this.query()
      .select(
        'Series.*',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      );

    // Get the users latest completion in the series, for sorting
    if (userId) {
      builder.select(
        latestProgressTimestampPartialSelector(userId)
          .as('latestProgressTimestamp'),
      );
    }

    if (options.includeContentPreview) {
      builder = _appendContentPreviewPartialSelector(builder, subscriberOrgId);
    }

    if (options.filterOutEmptySeries) {
      builder.whereExists(
        SeriesModel.relatedQuery('content')
          .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED),
      );
    }

    if (options.seriesIds) {
      builder.whereIn('Series.id', options.seriesIds);
    }

    // sort by whatever is passed in
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        // for dates that may be null want to see non-null dates first
        // if this is undesirable for other columns, make this logic date field specific
        builder.orderByRaw(`"${it.property}" ${it.direction} NULLS LAST`);
      });
    }

    // default sort
    builder.orderBy('createdTimestamp', 'desc');

    return builder
      // join so we can run a where on a joined column
      .mergeEager('[author, channel, organization]')
      .joinRelation('channel')
      .whereIn('channel.id', channelIds)
      .page(pageable.page, pageable.size)
      .execute();
  }

  static async getContent(seriesId: number, subscriberOrgId: number, pageable: Pageable): DbContentItemPagePromise {
    const series = await this.query()
      .findById(seriesId);
    const content = series
      .$relatedQuery('content')
      .select(
        '*',
        _likeCountForContentPartialSelector()
          .where('organizationId', subscriberOrgId)
          .as('likeCount'),
        _commentCountForContentPartialSelector()
          .where('organizationId', subscriberOrgId)
          .as('commentCount'),
        _activityCountForContentPartialSelector()
          .as('activityCount'),
        _completedCountForContentPartialSelector()
          .where('user:organization.id', subscriberOrgId)
          .as('completedCount'),
      )
      .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED)
      .eager('[author.organization, tags, channel, series]')
      .orderBy('order')
      .page(pageable.page, pageable.size);
    return content.execute();
  }

  static getByChannelId(
    channelId: number,
    subscriberOrgId: number,
    includeContentPreview: boolean,
    pageable: Pageable,
  ): DbSeriesPagePromise {
    let seriesQuery = this.query()
      .select(
        'Series.*',
        contentCountForSeriesPartialSelector()
          .as('contentCount'),
      );

    if (includeContentPreview) {
      seriesQuery = _appendContentPreviewPartialSelector(seriesQuery, subscriberOrgId);
    }

    return seriesQuery
      .mergeEager('[author, channel]')
      .where('channelId', channelId)
      .page(pageable.page, pageable.size)
      .orderBy('createdTimestamp', 'desc')
      .execute();
  }

  static delete(seriesId: number) {
    return this.query()
      .delete()
      .where('id', seriesId);
  }

  // INSTANCE METHODS

  async getContent() {
    return this.$relatedQuery('content')
      .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED);
  }
}
