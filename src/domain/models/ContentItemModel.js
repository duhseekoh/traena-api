// @flow
import type { QueryBuilder } from 'objection';
import { Model, ref } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import BaseModel from './BaseModel';
import CommentModel from './CommentModel';
import LikeModel from './LikeModel';
import TaskModel from './TaskModel';
import ChannelModel from './ChannelModel';
import OrganizationModel from './OrganizationModel';
import TagModel from './TagModel';
import SeriesModel from './SeriesModel';
import UserModel from './UserModel';
import ActivityModel from './ActivityModel';
import contentItemSchema from './schema/db/contentItem.json';
import { TRAENA_TASK_STATUSES, CONTENT_ITEM_STATUSES } from '../fieldConstants';
import type { ContentItemStatus, ContentItemType } from '../fieldConstants';
import type { ImageCollection } from '../../services/MediaService';
import type { PagedDbResult } from './BaseModel';
import * as queryUtils from '../../common/queryUtils';

type DbContentItemPagePromise = Promise<PagedDbResult<ContentItemModel>>; // eslint-disable-line no-use-before-define

/**
 * Selector for sub-query to obtain activity count
 * Requires that the caller must specify the appropriate 'where' clause to limit the organization to count likes for
 *
 * @returns {*}
 * @private
 */
export function _activityCountForContentPartialSelector(): QueryBuilder {
  return ActivityModel.query()
    .count('Activity.*')
    .where('Activity.contentId', '=', ref('Content.id'));
}

/**
 * Selector for sub-query to obtain like count
 * Requires that the caller must specify the appropriate 'where' clause to limit the organization to count likes for
 *
 * @returns {*}
 * @private
 */
export function _likeCountForContentPartialSelector(): QueryBuilder {
  return LikeModel.query()
    .count('Like.*')
    .where('Like.contentId', '=', ref('Content.id'));
}

/**
 * Selector for sub-query to obtain comment count.
 * @returns {*}
 * @private
 */
export function _commentCountForContentPartialSelector(): QueryBuilder {
  return CommentModel.query()
    .count('Comment.*')
    .whereNot('Comment.deleted', true)
    .where('Comment.contentId', '=', ref('Content.id'));
}

/**
 * Selector for sub-query to obtain completed count.
 * Requires that the caller must specify the appropriate 'where' clause to limit the organization to count completions
 * for given org
 * @returns {*}
 * @private
 */
export function _completedCountForContentPartialSelector(): QueryBuilder {
  return TaskModel.query()
    .count('Task.*')
    .where('Task.status', '=', TRAENA_TASK_STATUSES.COMPLETE)
    .where('Task.contentId', '=', ref('Content.id'))
    .joinRelation('user.organization');
}

/**
 * Returns a query builder to select all content (enriched with Like/Comment counts) available to the given org.
 * Caller is responsible for adding appropriate where clauses to restrict the results as needed
 * @param subscriberOrgId
 * @returns {Promise.<void>}
 * @private
 */
function _getContentForSubscriberOrg(subscriberOrgId: number): QueryBuilder {
  return ContentItemModel.query() // eslint-disable-line no-use-before-define
    .select(
      'Content.*',
      _likeCountForContentPartialSelector()
        .where('organizationId', subscriberOrgId)
        .as('likeCount'),
      _commentCountForContentPartialSelector()
        .where('organizationId', subscriberOrgId)
        .as('commentCount'),
      _completedCountForContentPartialSelector()
        .where('user:organization.id', subscriberOrgId)
        .as('completedCount'),
      _activityCountForContentPartialSelector()
        .as('activityCount'),
    )
    .joinRelation('channel.subscribers')
    .eager('[author.organization, tags, channel, series]')
    .where('channel:subscribers.id', subscriberOrgId);
}

/**
 * Returns a query builder to select all content (enriched with Like/Comment counts) available to the given user.
 * Caller is responsible for adding appropriate where clauses to restrict the results as needed
 * @param userId
 * @param contentId
 * @returns {Promise.<*|this>}
 * @private
 */
function _getContentForUser(userId: number): QueryBuilder {
  return ContentItemModel.query() // eslint-disable-line no-use-before-define
    .select(
      'Content.*',
      _likeCountForContentPartialSelector()
        .where('organizationId', '=', ref('id').from('channel:subscribers'))
        .as('likeCount'),
      _commentCountForContentPartialSelector()
        .where('organizationId', '=', ref('id').from('channel:subscribers'))
        .as('commentCount'),
      _completedCountForContentPartialSelector()
        .where('organizationId', '=', ref('id').from('channel:subscribers'))
        .as('completedCount'),
      _activityCountForContentPartialSelector()
        .as('activityCount'),
    )
    .joinRelation('channel.subscribers.users')
    .joinRelation('author')
    .eager('[author.organization, tags, channel, series]')
    .where('channel:subscribers:users.id', userId);
}

// For content creation
export type CreateContentItemDTO = {|
  authorId: number,
  channelId: number,
  status: ContentItemStatus,
  body: {
    title: string,
    type: ContentItemType,
    text?: string,
    images?: ImageCollection[],
    video?: {
      videoURI: string,
      previewImageURI: string,
    },
  },
  tags?: string[],
|};

// For the cases where we don't want the ContentItemModel class but we just want to
// represent the properties on it, we use this.
// No good way to share the properties: https://github.com/facebook/flow/issues/3211
export type ContentItemDTO = {
  id?: ?number;
  authorOrganizationId: number;
  authorId: number;
  channelId: number;
  status: ContentItemStatus;
  +publishedTimestamp?: ?string;
  +activityCount: number;
  activitiesOrder: number[];
  +likeCount: number;
  +commentCount: number;
  +completedCount: number;
  body: {
   title: string;
   type: ContentItemType;
   text?: string;
   images?: ImageCollection[];
   video?: {
     videoURI: string;
     previewImageURI: string;
     transcodeId?: ?number;
   }
 };
};

// Content that we want to send over to elasticsearch needs an extra property
export type ESIndexableContentItemDTO = ContentItemDTO & {
  subscriberOrganizationId: number;
};

export default class ContentItemModel extends BaseModel {
  id: number;
  authorOrganizationId: number;
  authorId: number;
  channelId: number;
  status: ContentItemStatus;
  publishedTimestamp: ?string;
  activityCount: number;
  activitiesOrder: number[];
  likeCount: number;
  commentCount: number;
  completedCount: number;
  body: {
    title: string;
    type: ContentItemType;
    text?: string;
    images?: ImageCollection[];
    video?: {
      videoURI: string;
      previewImageURI: string;
      transcodeId: ?number;
    }
  };
  // read only properties supplied by relations
  +activities: any;
  +author: any;
  +channel: any;
  +comments: any;
  +completions: any;
  +likes: any;
  +tags: any;
  +series: ?SeriesModel[];

  constructor(obj: *) {
    super();
    Object.assign(this, obj);
  }

  static get tableName(): string {
    return 'Content';
  }

  static get jsonSchema(): contentItemSchema {
    return contentItemSchema;
  }

  static get relationMappings(): Object {
    return {
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Content.authorId',
          to: 'User.id',
        },
      },

      tags: {
        relation: Model.HasManyRelation,
        modelClass: TagModel,
        join: {
          from: 'Content.id',
          to: 'Tag.contentId',
        },
      },

      likes: {
        relation: Model.HasManyRelation,
        modelClass: LikeModel,
        join: {
          from: 'Content.id',
          to: 'Like.contentId',
        },
      },

      comments: {
        relation: Model.HasManyRelation,
        modelClass: CommentModel,
        join: {
          from: 'Content.id',
          to: 'Comment.contentId',
        },
      },

      completions: {
        relation: Model.HasManyRelation,
        modelClass: TaskModel,
        join: {
          from: 'Content.id',
          to: 'Task.contentId',
        },
      },

      activities: {
        relation: Model.HasManyRelation,
        modelClass: ActivityModel,
        join: {
          from: 'Content.id',
          to: 'Activity.contentId',
        },
      },

      // Channel this content belongs to
      channel: {
        relation: Model.BelongsToOneRelation,
        modelClass: ChannelModel,
        join: {
          from: 'Content.channelId',
          to: 'Channel.id',
        },
      },

      series: {
        relation: Model.ManyToManyRelation,
        modelClass: SeriesModel,
        join: {
          from: 'Content.id',
          through: {
            from: 'Series_Content.contentId',
            to: 'Series_Content.seriesId',
          },
          to: 'Series.id',
        },
      },

      bookmarks: {
        relation: Model.ManyToManyRelation,
        modelClass: UserModel,
        join: {
          from: 'Content.id',
          through: {
            from: 'Bookmark.contentId',
            to: 'Bookmark.userId',
          },
          to: 'User.id',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getContentById(contentId: number): Promise<ContentItemModel> {
    return this
      .query()
      .findById(contentId)
      .execute();
  }

  static async archiveByChannelId(channelId: number): Promise<number> {
    return this.query()
      .patch({ status: 'ARCHIVED' })
      .where('channelId', channelId);
  }

  static async getContentByIdForUser(userId: number, contentId: number): Promise<?ContentItemModel> {
    return _getContentForUser(userId)
      .where('Content.id', contentId)
      .first()
      .execute();
  }

  static async getContentItemsByIdsForUser(userId: number, contentIds: number[]): Promise<ContentItemModel[]> {
    return _getContentForUser(userId)
      .whereIn('Content.id', contentIds)
      .execute();
  }

  static async getContentByIdForSubscriber(contentId: number, subscriberOrgId: number): Promise<ContentItemModel> {
    return _getContentForSubscriberOrg(subscriberOrgId)
      .where('Content.id', contentId)
      .first()
      .execute();
  }

  static async getByChannelIdForUser(
    userId: number,
    channelId: number,
    pageable: Pageable,
  ): DbContentItemPagePromise {
    const builder = _getContentForUser(userId)
      .where('channel.id', channelId)
      .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED)
      .page(pageable.page, pageable.size);

      // sort by whatever is passed in
    if (pageable.sort) {
      // We need to use a raw query for sorting by nested json properties
      const jsonProperties = [
        'body.title',
        'body.type',
      ];
      pageable.sort.orders.forEach((it) => {
        if (jsonProperties.includes(it.property)) {
          const sortClause = queryUtils.buildRawSqlSortClauseForJsonProperty(it.property, it.direction);
          builder.orderByRaw(sortClause);
        } else {
          builder.orderBy(it.property, it.direction);
        }
      });
    } else {
      builder.orderBy('publishedTimestamp', 'desc');
    }

    return builder.execute();
  }

  static async getAllSubscribedContentForOrg(subscriberOrgId: number): Promise<ContentItemModel[]> {
    return _getContentForSubscriberOrg(subscriberOrgId).execute();
  }

  static async getChannelSubscribedContentForOrg(
    channelId: number,
    subscriberOrgId: number,
  ): Promise<ContentItemModel[]> {
    return _getContentForSubscriberOrg(subscriberOrgId)
      .where('Content.channelId', channelId)
      .execute();
  }

  static async upsertContent(contentItem: ContentItemModel): Promise<ContentItemModel> {
    // We return these derived values when retrieving a contentItem,
    // but don't want to pass them if provided in put/post
    delete contentItem.likeCount;
    delete contentItem.commentCount;
    delete contentItem.activityCount;
    delete contentItem.completedCount;

    if (contentItem.id) {
      return this.query().patch(contentItem).where('id', contentItem.id).first()
        .returning('*');
    }
    return this.query().insert(contentItem).returning('*');
  }

  static async _isBookmarked(userId: number, contentId: number): Promise<boolean> {
    return this.query()
      .count('Content.*')
      .joinRelation('bookmarks')
      .where('Content.id', contentId)
      .where('bookmarks.id', userId)
      .first()
      .then(results => results.count > 0);
  }

  static async bookmark(userId: number, contentId: number): Promise<boolean> {
    // check if already bookmarked

    if (await this._isBookmarked(userId, contentId)) {
      return true; // already bookmarked - success!
    }

    return this.query()
      .where('id', contentId)
      .first()
      .then(contentItem => contentItem.$relatedQuery('bookmarks').relate(userId))
      .then(() => true);
  }

  static async unbookmark(userId: number, contentId: number): Promise<boolean> {
    return this.query()
      .where('id', contentId)
      .first()
      .then(contentItem => contentItem.$relatedQuery('bookmarks')
        .unrelate()
        .where('userId', userId))
      .then(() => true);
  }

  /**
   *
   * @param userId
   * @returns {Promise.<void>}
   */
  static async getBookmarkIdsForUser(userId: number): Promise<number[]> {
    return ContentItemModel
      .query()
      .select('Content.id')
      .joinRelation('bookmarks')
      .where('bookmarks.id', userId)
      .execute()
      .map(it => it.id);
  }

  /**
   *
   * @param userId
   * @param pageable
   * @returns {Promise.<void>}
   */
  static async getBookmarksForUserPaginated(userId: number, pageable: Pageable): DbContentItemPagePromise {
    return _getContentForUser(userId)
      .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED)
      .joinRelation('bookmarks')
      .where('bookmarks.id', userId)
      .orderBy('bookmarks_join.createdTimestamp', 'desc')
      .page(pageable.page, pageable.size)
      .execute();
  }

  // INSTANCE METHODS

  /**
   * Get all subscribers for a given piece of content
   * @returns {Promise.<OrganizationModel[]>}
   */
  async getSubscribers(): Promise<OrganizationModel[]> {
    return this.$relatedQuery('channel').select('Channel.*')
      .then(channel => channel.$relatedQuery('subscribers').select('Organization.*'));
  }

  async hasSeries(): Promise<boolean> {
    const series: SeriesModel[] = await this.$relatedQuery('series');
    return series.length > 0;
  }
}

export type ContentItem = ContentItemDTO | ContentItemModel;
