// @flow

import { Model } from 'objection';

/**
 * Result type returned when using objection's QueryBuilder.page(page, size) method
 */
export type PagedDbResult<T> = {
  results: T[],
  total: number,
}

export default class BaseModel extends Model {
  createdTimestamp: ?string;
  modifiedTimestamp: ?string;

  $beforeInsert = () => {
    if (this.constructor.hasTimestamps) {
      const now = new Date().toISOString();
      this.modifiedTimestamp = now;
      this.createdTimestamp = now;
    }
  }

  $beforeUpdate = () => {
    if (this.constructor.hasTimestamps) {
      this.modifiedTimestamp = new Date().toISOString();
    }
  }
}
