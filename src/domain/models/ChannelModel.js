// @flow
import { Model } from 'objection';
import type { QueryBuilder } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import type { PagedDbResult } from './BaseModel';
import BaseModel from './BaseModel';
import OrganizationModel from './OrganizationModel';
import type { ChannelVisibility, SubscriptionSources } from '../fieldConstants';
import {
  CHANNEL_VISIBILITIES,
  CONTENT_ITEM_STATUSES,
  SUBSCRIPTION_VISIBILITIES,
  SUBSCRIPTION_SOURCES,
} from '../fieldConstants';
import ContentItemModel from './ContentItemModel';
import SeriesModel from './SeriesModel';
import UserModel from './UserModel';

type DbChannelPagePromise = Promise<PagedDbResult<ChannelModel>>; // eslint-disable-line no-use-before-define
type DbOrganizationPagePromise = Promise<PagedDbResult<OrganizationModel>>; // eslint-disable-line no-use-before-define

/**
 * Selector for sub-query to obtain subscribed channels for a user.
 * Requires that the caller must specify the appropriate 'where' clause to limit the results to a given organizationId
 * @param {userId}
 * @returns {*}
 * @private
 */
function _subscribedChannelsForUserPartialSelector(
  userId: number,
): QueryBuilder {
  return (
    ChannelModel.query() // eslint-disable-line no-use-before-define
      .select('Channel.*')
      .joinRelation('subscribers.users') // all users of a subscribed org
      .where('subscribers:users.id', userId)
      // user subscribed implicity through ORGANIZATION visibility OR
      // explicity through USERS visibility
      .where((builder) => {
        builder
          .where(
            'subscribers_join.visibility',
            SUBSCRIPTION_VISIBILITIES.ORGANIZATION,
          )
          .orWhere((builder2) => {
            builder2
              .where(
                'subscribers_join.visibility',
                SUBSCRIPTION_VISIBILITIES.USERS,
              )
              .whereExists(
                ChannelModel.relatedQuery('subscribedUsers') // eslint-disable-line no-use-before-define
                  .select(1)
                  .where('UserSubscription.userId', userId),
              );
          });
      })
  );
}


export type ChannelDTO = {
  id?: ?number;
  name: string;
  description: string;
  organizationId: number;
  visibility: ChannelVisibility;
  default: boolean
};

/**
 * A channel is a collection for content. Each channel is owned by one organization. Each organization may have at most
 * a single channel with default=true
 */
export default class ChannelModel extends BaseModel {
  id: number;
  name: string;
  description: string;
  organizationId: number;
  visibility: ChannelVisibility;
  default: boolean;
  // read-only retrieved through relations
  +organization: ?OrganizationModel;

  static get tableName(): string {
    return 'Channel';
  }

  static get relationMappings(): Object {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Channel.organizationId',
          to: 'Organization.id',
        },
      },

      content: {
        relation: Model.HasManyRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Channel.id',
          to: 'Content.channelId',
        },
      },

      series: {
        relation: Model.HasManyRelation,
        modelClass: SeriesModel,
        join: {
          from: 'Channel.id',
          to: 'Series.channelId',
        },
      },

      // all of the subscriber organizations of this channel
      subscribers: {
        relation: Model.ManyToManyRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Channel.id',
          through: {
            from: 'Subscription.channelId',
            to: 'Subscription.organizationId',
          },
          to: 'Organization.id',
        },
      },

      // users directly subscribed to this channel
      subscribedUsers: {
        relation: Model.ManyToManyRelation,
        modelClass: UserModel,
        join: {
          from: 'Channel.id',
          through: {
            from: 'UserSubscription.channelId',
            to: 'UserSubscription.userId',
          },
          to: 'User.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        description: { type: 'string' },
        organizationId: { type: 'integer' },
        visibility: {
          enum: Object.values(CHANNEL_VISIBILITIES),
        },
        default: { type: 'boolean' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
      },
      additionalProperties: false,
      required: ['name', 'description', 'organizationId', 'visibility'],
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getById(channelId: number): Promise<?ChannelModel> {
    return this.query().findById(channelId);
  }

  static async upsert(channel: Channel): Promise<ChannelModel> {
    if (channel.id) {
      return this.query().patch(channel).where('id', channel.id).first()
        .returning('*');
    }
    return this.query().insert(channel).returning('*');
  }

  static async delete(channelId: number): Promise<number> {
    return this.query()
      .deleteById(channelId);
  }

  static getChannelById(channelId: number): Promise<?ChannelModel> {
    return this.query()
      .where({ id: channelId })
      .first()
      .execute();
  }

  /**
   * Returns the default channel for the provided user if one exists.
   * Presently uses the default channel for the user's organization
   *
   * @param userId
   * @returns {Promise.<void>}
   */
  static async getDefaultChannelIdForUser(userId: number): Promise<?number> {
    const result = ChannelModel.query()
      .select('Channel.id')
      .joinRelation('organization.users')
      .where('organization:users.id', userId)
      .where('default', true)
      .first()
      .execute();

    return result.id;
  }

  /**
   * Returns the default channel for the provided channel if one exists.
   * @param organizationId
   * @returns {Promise.<void>}
   */
  static async getDefaultChannelIdForOrganization(organizationId: number): Promise<?number> {
    const result = await ChannelModel.query()
      .select('id')
      .where('organizationId', organizationId)
      .where('default', true)
      .first()
      .execute();

    return result.id;
  }

  /**
   * Get all channel ids a user is subscribed to
   * @param userId
   */
  static async getSubscribedChannelIdsForUser(userId: number): Promise<number[]> {
    const channels = await this.getSubscribedChannelsForUser(userId);
    return channels.map(it => it.id);
  }

  /**
   * Get all channel ids an organization is subscribed to (regardless of access level)
   * @param userId
   * @returns {Promise.<void>}
   */
  static async getSubscriptionIdsForOrganization(organizationId: number): Promise<number[]> {
    return ChannelModel
      .query()
      .select('Channel.id')
      .joinRelation('subscribers')
      .where('subscribers.id', organizationId)
      .execute()
      .map(it => it.id);
  }

  /**
   * Get channels an organization is subscribed to (regardless of access level)
   * Filter by subscription source.
   */
  static async getSubscribedChannelsForOrganization(
    organizationId: number,
    options: {
      subscriptionSource?: ?SubscriptionSources,
    } = {
      subscriptionSource: SUBSCRIPTION_SOURCES.ALL,
    },
  ): Promise<ChannelModel[]> {
    const builder = ChannelModel
      .query()
      .select('Channel.*')
      .joinRelation('subscribers')
      .where('subscribers.id', organizationId);

    if (options.subscriptionSource === SUBSCRIPTION_SOURCES.INTERNAL) {
      builder.where('Channel.organizationId', organizationId);
    } else if (options.subscriptionSource === SUBSCRIPTION_SOURCES.EXTERNAL) {
      builder.where('Channel.organizationId', '!=', organizationId);
    }

    return builder.execute();
  }

  /**
   * Returns a page of all channels to which the provided user's organization is subscribed
   */
  static async getSubscribedChannelsForUser(userId: number): Promise<ChannelModel[]> {
    return _subscribedChannelsForUserPartialSelector(userId)
      .eager('organization')
      .execute();
  }

  /**
   * Returns a page of all channels to which the provided user's organization is subscribed
   * @param userId
   * @param pageable
   */
  static async getSubscribedChannelsForUserPaginated(userId: number, pageable: Pageable): DbChannelPagePromise {
    const builder = _subscribedChannelsForUserPartialSelector(userId)
      .eager('organization')
      .page(pageable.page, pageable.size);

    // sort by whatever is passed in
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        builder.orderBy(it.property, it.direction);
      });
    }
    return builder.execute();
  }

  /**
   * Returns a page of channels the user is subscribed to, owned by the organization to which the user belongs
   * @param userId
   * @param organizationId
   * @param pageable
   * @returns {Promise.<void>}
   */
  static async getInternalSubscribedChannelsForUserPaginated(
    userId: number,
    organizationId: number,
    pageable: Pageable,
  ): DbChannelPagePromise {
    return _subscribedChannelsForUserPartialSelector(userId)
      .where('Channel.organizationId', '=', organizationId)
      .page(pageable.page, pageable.size)
      .execute();
  }

  /**
   * Returns a page of channels the user is subscribed to, owned by an organization to which the user does not belong
   */
  static async getExternalSubscribedChannelsForUserPaginated(
    userId: number,
    organizationId: number,
    pageable: Pageable,
  ): DbChannelPagePromise {
    return _subscribedChannelsForUserPartialSelector(userId)
      .where('Channel.organizationId', '!=', organizationId)
      .page(pageable.page, pageable.size)
      .execute();
  }

  /**
   * Returns a page of all channels owned by the provided organization
   */
  static async getChannelsForOrganizationPaginated(organizationId: number, pageable: Pageable): DbChannelPagePromise {
    return this.query()
      .select('*')
      .where('organizationId', organizationId)
      .page(pageable.page, pageable.size)
      .orderBy('Channel.name', 'asc')
      .execute();
  }

  /**
   * Returns a page of all channels the provided organization is subscribed to
   */
  static async getSubscribedChannelsForOrganizationPaginated(
    organizationId: number,
    pageable: Pageable,
  ): DbChannelPagePromise {
    return ChannelModel
      .query()
      .select('Channel.*')
      .joinRelation('subscribers')
      .where('subscribers.id', organizationId)
      .page(pageable.page, pageable.size)
      .execute();
  }

  /**
   * Returns a page of all organizations subscribed to this channel
   */
  static async getSubscriberOrganizationsForChannelPaginated(
    channelId: number,
    pageable: Pageable,
  ): DbOrganizationPagePromise {
    return ChannelModel
      .query()
      .select('subscribers.*')
      .joinRelation('subscribers')
      .where('Channel.id', channelId)
      .page(pageable.page, pageable.size)
      .execute();
  }

  /**
   * Returns true if the provided channel is owned by the provided user's organization
   * @param channelId
   * @param userId
   * @returns {Promise.<void>}
   */
  static async isOwnedByUsersOrganization(channelId: number, userId: number): Promise<boolean> {
    const result = await this.query()
      .count('Channel.*')
      .joinRelation('organization.users')
      .where('organization:users.id', userId)
      .where('Channel.id', channelId)
      .first();

    return result.count === 1;
  }

  /**
   * Whether org is already subscribed to this channel
   * @param organizationId
   * @param channelId
   * @returns {Promise.<void>}
   * @private
   */
  static async _isSubscribed(organizationId: number, channelId: number): Promise<boolean> {
    return this.query()
      .count('Channel.*')
      .joinRelation('subscribers')
      .where('Channel.id', channelId)
      .where('subscribers.id', organizationId)
      .first()
      .then(results => results.count > 0);
  }

  async getPublishedContent(): Promise<ContentItemModel[]> {
    return this.$relatedQuery('content')
      .where('Content.status', CONTENT_ITEM_STATUSES.PUBLISHED);
  }

  async getSubscribers(): Promise<OrganizationModel[]> {
    return this.$relatedQuery('subscribers');
  }
}

export type Channel = ChannelDTO | ChannelModel;
