// @flow
import { Model, ref } from 'objection';
import knex from 'knex';
import type { Pageable } from '@panderalabs/koa-pageable';
import type ContentItemModel from './ContentItemModel';
import ChannelModel from './ChannelModel';
import BaseModel, { type PagedDbResult } from './BaseModel';
import OrganizationModel from './OrganizationModel';
import LikeModel from './LikeModel';
import CommentModel from './CommentModel';
import EventModel, { EventTypes } from './EventModel';
import SeriesModel from './SeriesModel';
import TaskModel from './TaskModel';
import { TRAENA_TASK_STATUSES } from '../fieldConstants';

type DbUserWithContentStats =
  Promise<PagedDbResult<UserDTOWithContentStats>>; // eslint-disable-line no-use-before-define
type DbUserWithSeriesStats =
  Promise<PagedDbResult<UserDTOWithSeriesStats>>; // eslint-disable-line no-use-before-define

export type UserDTO = {
  id?: ?number;
  email: string;
  organizationId: number;
  firstName?: ?string;
  lastName?: ?string;
  profileImageURI?: ?string;
  isDisabled?: ?boolean;
  position?: ?string;
  tags: string[];
}

export default class UserModel extends BaseModel {
  id: number;
  email: string;
  organizationId: number;
  firstName: ?string;
  lastName: ?string;
  profileImageURI: ?string;
  isDisabled: ?boolean;
  position: ?string;
  tags: string[];
  // relations (read only)
  +organization: ?any;
  // auth middleware adds these
  roles: ?any;
  permissions: ?any;

  static get tableName(): string {
    return 'User';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        email: { type: 'string' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        organizationId: { type: 'integer' },
        firstName: { type: ['string', 'null'] },
        lastName: { type: ['string', 'null'] },
        profileImageURI: { type: ['string', 'null'] },
        isDisabled: { type: ['boolean', 'null'] },
        position: { type: ['string', 'null'] },
        tags: {
          type: 'array',
          items: {
            type: 'string',
          },
        },
        additionalProperties: false,
        required: ['email', 'firstName', 'lastName', 'organizationId'],
      },
    };
  }

  static get relationMappings(): Object {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'User.organizationId',
          to: 'Organization.id',
        },
      },
      tasks: {
        relation: Model.HasManyRelation,
        modelClass: TaskModel,
        join: {
          from: 'User.id',
          to: 'Task.userId',
        },
      },
      events: {
        relation: Model.HasManyRelation,
        modelClass: EventModel,
        join: {
          from: 'User.id',
          to: 'Event.userId',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }


  static async getUserById(userId: number): Promise<UserModel> {
    return this
      .query()
      .select()
      .eager('organization')
      .where('User.id', userId)
      .first()
      .execute();
  }

  static async getUserByEmail(email: string): Promise<UserModel> {
    return this
      .query()
      .select()
      .eager('organization')
      .where('User.email', email)
      .first()
      .execute();
  }

  static async getByOrganizationId(orgId: number): Promise<UserModel[]> {
    return this
      .query()
      .select()
      .where('User.organizationId', orgId)
      .execute();
  }

  static async disable(userId: number): Promise<number> {
    return this.query().patch({ isDisabled: true }).where('id', userId).execute();
  }

  static async disableForOrganization(organizationId: number): Promise<number> {
    return this.query().patch({ isDisabled: true }).where('organizationId', organizationId).execute();
  }

  // eslint-disable-next-line no-user-before-define
  static async upsert(user: User, transaction?: Knex$Transaction): Promise<UserModel> {
    let userToSave = user;
    if (user.email) {
      userToSave = { ...user, email: user.email.toLowerCase() };
    }

    if (user.id) {
      return this.query(transaction).patch(userToSave).where('id', user.id).first()
        .returning('*');
    }
    return this.query(transaction).insert(userToSave).returning('*');
  }

  static async getCountForOrganization(organizationId: number): Promise<number> {
    const result = await this.query().count('*').first()
      .where('organizationId', organizationId)
      .execute();
    return result.count;
  }

  static async getAllUsers(): Promise<UserModel[]> {
    return this
      .query()
      .select()
      .execute();
  }

  static async getUsersWithStatsForContentId(
    contentId: number,
    organizationId: number,
    pageable: Pageable,
  ): DbUserWithContentStats {
    const query = this
      .query()
      .select(
        'User.*',
        LikeModel.query()
          .select(knex.raw('COUNT(*) > 0'))
          .where('Like.userId', ref('User.id'))
          .where('Like.contentId', contentId)
          .as('liked'),
        CommentModel.query()
          .count('Comment.*')
          .where('Comment.authorId', ref('User.id'))
          .where('Comment.contentId', contentId)
          .as('comments'),
        TaskModel.query()
          .select(knex.raw('COUNT(*) > 0'))
          .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
          .where('Task.userId', ref('User.id'))
          .where('Task.contentId', contentId)
          .as('completed'),
        EventModel.query()
          .count('Event.*')
          .where('Event.type', 'CONTENT_VIEWED') // TODO use ENUM
          .where('Event.userId', ref('User.id'))
          .where(ref('data:contentId'), contentId)
          .as('views'),
      )
      .where('User.organizationId', organizationId)
      // Only include a user if they have at least one interaction with the content
      .where(function hasAStat() {
        this.whereExists(
          LikeModel.query()
            .select(1)
            .where('Like.userId', ref('User.id'))
            .where('Like.contentId', contentId),
        ).orWhereExists(
          CommentModel.query()
            .select(1)
            .where('Comment.authorId', ref('User.id'))
            .where('Comment.contentId', contentId),
        ).orWhereExists(
          TaskModel.query()
            .select(1)
            .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
            .where('Task.userId', ref('User.id'))
            .where('Task.contentId', contentId),
        ).orWhereExists(
          EventModel.query()
            .select(1)
            .where('Event.type', EventTypes.CONTENT_VIEWED)
            .where('Event.userId', ref('User.id'))
            .where(ref('data:contentId'), contentId),
        );
      })
      .orderBy('User.firstName', 'asc') // TODO allow sorting by passed in
      .page(pageable.page, pageable.size)
      .then(queryResult => ({
        ...queryResult,
        results: queryResult.results.map((result) => {
          const { liked, comments, views, completed, ...user } = result;
          return {
            user,
            contentItemStats: {
              comments,
              completed,
              liked,
              views,
            },
          };
        }),
      }));
    return query;
  }

  static async getUsersWithStatsForSeriesId(
    seriesId: number,
    userIds: number[],
    pageable: Pageable,
  ): DbUserWithSeriesStats {
    const series = await SeriesModel.query().findById(seriesId);
    const content: ContentItemModel[] = await series.getContent();
    const contentIds = content.map(c => c.id);

    const builder = this
      .query()
      .select(
        'User.*',
        UserModel.relatedQuery('tasks')
          .count()
          .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
          .whereIn('Task.contentId', contentIds)
          .as('completedContentCount'),
        UserModel.relatedQuery('tasks')
          .max('Task.modifiedTimestamp')
          .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
          .whereIn('Task.contentId', contentIds)
          .as('latestContentCompletionTimestamp'),
        UserModel.relatedQuery('events')
          // TODO - db migrate the data->>timestamp jsonb attribute to a sql column, then use that
          // .max(knex.raw(`CAST("Event"."data"->>'timestamp' AS TIMESTAMP)`))
          .max('Event.createdTimestamp')
          .where('Event.type', 'CONTENT_VIEWED')
          .whereIn(ref('Event.data:contentId'), contentIds)
          .as('latestViewedContentTimestamp'),
      )
      // limit users to whatever was passed in
      .whereIn('User.id', userIds)
      .page(pageable.page, pageable.size);

    // sort by whatever is passed in
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        if (it.property === 'latestContentCompletionTimestamp' ||
          it.property === 'latestViewedContentTimestamp'
        ) {
          // for dates that may be null want to see non-null dates first
          builder.orderByRaw(`"${it.property}" ${it.direction} NULLS LAST`);
        } else {
          builder.orderBy(it.property, it.direction);
        }
      });
    }
    // default sort
    builder.orderBy('User.firstName', 'asc');
    // pull out the stats into a sibling object to the user
    return builder.then(queryResult => ({
      ...queryResult,
      // eslint-disable-next-line no-use-before-define
      results: queryResult.results.map((result): UserDTOWithSeriesStats => {
        const {
          completedContentCount,
          latestContentCompletionTimestamp,
          latestViewedContentTimestamp,
          ...user
        } = result;
        return {
          user,
          seriesStats: {
            completedContentCount,
            latestContentCompletionTimestamp,
            latestViewedContentTimestamp,
          },
        };
      }),
    }));
  }

  // Instance methods

  /**
   * Gets channels a user is subscribed to. These may be through access granted
   * to the entire org, or individual subscriptions set up by the org on a per
   * user basis.
   */
  async getSubscribedChannels(): Promise<ChannelModel[]> {
    return ChannelModel.getSubscribedChannelsForUser(this.id);
  }
}

export type UserDTOWithContentStats = {
  user: UserDTO,
  contentItemStats: {
    liked: boolean,
    comments: number,
    views: number,
    completed: boolean,
  },
};

export type UserSeriesStats = {
  completedContentCount: number,
  latestContentCompletionTimestamp: ?string,
  latestViewedContentTimestamp: ?string,
};

export type UserDTOWithSeriesStats = {
  user: UserDTO,
  seriesStats: UserSeriesStats,
};

export type User = UserDTO | UserModel;
