// @flow
import BaseModel from './BaseModel';

export default class PushDeviceModel extends BaseModel {
  static get tableName(): string {
    return 'PushDevice';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        userId: { type: 'integer' },
        deviceToken: { type: 'string' },
        endpointArn: { type: 'string' },
      },
      additionalProperties: false,
      required: ['userId', 'deviceToken', 'endpointArn'],
    };
  }

  static get relationMappings(): Object {
    return {};
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getPushDeviceByDeviceToken(deviceToken: string) {
    const pushDevice = await this
      .query()
      .select()
      .where('PushDevice.deviceToken', deviceToken)
      .first();

    return pushDevice;
  }

  static async getPushDevicesForUser(userId: number) {
    const pushDevice = await this
      .query()
      .select()
      .where('PushDevice.userId', userId);

    return pushDevice;
  }

  static async upsertPushDevice(userId: number, deviceToken: string, endpointArn: ?string) {
    const existingPushDevice = await this.getPushDeviceByDeviceToken(deviceToken);

    // UPDATE
    if (existingPushDevice) {
      existingPushDevice.userId = userId;
      existingPushDevice.endpointArn = endpointArn;

      return this.query()
        .updateAndFetchById(existingPushDevice.id, existingPushDevice);
    }

    // OR INSERT
    return this.query()
      .insert({
        userId,
        deviceToken,
        endpointArn,
      });
  }

  static async deletePushDevice(deviceToken: string) {
    return this.query()
      .delete()
      .where('PushDevice.deviceToken', deviceToken);
  }
}
