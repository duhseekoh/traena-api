// @flow
import { Pageable } from '@panderalabs/koa-pageable';
import { Model } from 'objection';
import BaseModel from './BaseModel';
import UserModel from './UserModel';
import ContentItemModel from './ContentItemModel';
import OrganizationModel from './OrganizationModel';

export default class CommentModel extends BaseModel {
  id: number;
  organizationId: number;
  authorId: number;
  contentId: number;
  text: string;
  // relations (read only)
  +author: any;
  +contentItem: ?any;
  +organization: any;
  +taggedUsers: ?any;

  static get tableName(): string {
    return 'Comment';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        organizationId: { type: 'integer' },
        authorId: { type: 'integer' },
        contentId: { type: 'integer' },
        text: { type: 'string', maxLength: 10000 },
        deleted: { type: 'boolean' },
      },
      additionalProperties: false,
      required: ['organizationId', 'authorId', 'contentId', 'text'],
    };
  }

  static get relationMappings(): Object {
    return {
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Comment.authorId',
          to: 'User.id',
        },
      },
      contentItem: {
        relation: Model.BelongsToOneRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Comment.contentId',
          to: 'Content.id',
        },
      },
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Comment.organizationId',
          to: 'Organization.id',
        },
      },
      taggedUsers: {
        relation: Model.ManyToManyRelation,
        modelClass: UserModel,
        join: {
          from: 'Comment.id',
          through: {
            from: 'Comment_TaggedUser.commentId',
            to: 'Comment_TaggedUser.userId',
          },
          to: 'User.id',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getCommentById(commentId: number) {
    return this
      .query()
      .select('Comment.*', 'Comment.id')
      .eager('[author, taggedUsers, author.organization]')
      .pick(UserModel, ['id', 'firstName', 'lastName', 'profileImageURI', 'organization'])
      .where('Comment.id', commentId)
      .first();
  }

  static async getCommentsForContent(organizationId: number, contentId: number, maxDate: ?Date, pageable: Pageable) {
    // Get top level comments
    const builder = this
      .query()
      .select('Comment.*', 'Comment.id')
      .eager('[author, taggedUsers, author.organization]')
      .pick(UserModel, ['id', 'firstName', 'lastName', 'profileImageURI', 'organization'])
      .where('Comment.contentId', contentId)
      .where('Comment.organizationId', organizationId)
      .whereNot('Comment.deleted', true);

    if (maxDate) {
      builder.where('Comment.createdTimestamp', '<', maxDate.toISOString());
    }

    return builder
      .orderBy('Comment.createdTimestamp', 'desc')
      .page(pageable.page, pageable.size)
      .execute();
  }

  static async getCommentCountByContentId(contentId: number, organizationId?: number): Promise<number> {
    const query = this.query()
      .count()
      .where('Comment.contentId', contentId)
      .whereNot('Comment.deleted', true);

    if (organizationId) {
      query.where('Comment.organizationId', organizationId);
    }

    return query
      .first()
      .then(({ count }) => count);
  }

  // Not Yet Implemented
  // static async getReplyComments(commentIds, sinceDate) {
  //   // Get top level comments
  //   const comments = await this
  //     .query()
  //     .joinRelation('user')
  //     .whereIn('Comment.parentCommentId', commentIds);
  //   return comments;
  // }

  static async insertComment(author: UserModel, contentId: number, text: string) {
    return this.query()
      .insertAndFetch({
        authorId: author.id,
        organizationId: author.organizationId,
        contentId,
        text,
      })
      .eager('[author, taggedUsers]')
      .pick(UserModel, ['id', 'firstName', 'lastName', 'profileImageURI']);
  }

  static async delete(commentId: number): Promise<number> {
    return this.query().patch({ deleted: true }).where('id', commentId).execute();
  }
}
