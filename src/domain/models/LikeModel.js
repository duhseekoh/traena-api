// @flow
import { Model } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import BaseModel, { type PagedDbResult } from './BaseModel';
import UserModel from './UserModel';
import OrganizationModel from './OrganizationModel';
import ContentItemModel from './ContentItemModel';

type DbUserPagePromise = Promise<PagedDbResult<UserModel>>;

export default class LikeModel extends BaseModel {
  userId: number;
  contentId: number;
  organizationId: number;

  static get tableName(): string {
    return 'Like';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        userId: { type: 'integer' },
        contentId: { type: 'number' },
        organizationId: { type: 'number' },
      },
      additionalProperties: false,
      required: ['userId', 'contentId', 'organizationId'],
    };
  }

  static get relationMappings(): Object {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Like.userId',
          to: 'User.id',
        },
      },

      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: OrganizationModel,
        join: {
          from: 'Like.organizationId',
          to: 'Organization.id',
        },
      },

      contentItem: {
        relation: Model.BelongsToOneRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Like.contentId',
          to: 'Content.id',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async likeContent(
    userId: number,
    userOrganizationId: number,
    contentId: number,
  ): Promise<boolean> {
    // check if user has already liked it
    const existingLike = await this.query()
      .select()
      .where({ userId, contentId })
      .then(results => results.length > 0);

    if (existingLike) {
      // already liked, results in success
      return true;
    }

    // hasn't liked, attempt to like it
    return this.query()
      .returning(['userId', 'contentId'])
      .insert({
        userId,
        organizationId: userOrganizationId,
        contentId,
      })
      .then(() => true); // results in success
  }

  static async unlikeContent(
    userId: number,
    userOrganizationId: number,
    contentId: number,
  ): Promise<boolean> {
    return this.query()
      .where({
        userId,
        organizationId: userOrganizationId,
        contentId,
      })
      .del()
      .then(() => true); // results in success
  }

  /**
   * Returns a list of contentIds liked by this user
   * @param userId
   * @returns {Promise.<void>}
   */
  static async getAllLikedContentForUser(userId: number): Promise<number[]> {
    return this.query()
      .select('contentId')
      .where('userId', userId)
      .orderBy('contentId')
      .map(it => it.contentId);
  }

  static async getUsersByContentIdPaginated(
    contentId: number,
    organizationId?: number,
    pageable: Pageable,
  ): DbUserPagePromise {
    const query = this.query()
      .select('user.*')
      .joinRelation('user')
      .where('Like.contentId', contentId)
      .page(pageable.page, pageable.size)
      .orderBy('user.firstName');

    if (organizationId) {
      query.where('Like.organizationId', organizationId);
    }

    return query;
  }

  static async getLikeCountByContentId(
    contentId: number,
    organizationId?: number,
  ): Promise<number> {
    const query = this.query()
      .count()
      .where('Like.contentId', contentId);

    if (organizationId) {
      query.where('Like.organizationId', organizationId);
    }

    return query.first().then(({ count }) => count);
  }
}
