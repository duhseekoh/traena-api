// @flow
import { Model } from 'objection';
import type moment$Moment from 'moment';
import BaseModel from './BaseModel';
import UserModel from './UserModel';
import type { TranscodeJobStatus } from '../fieldConstants';
import { TRANSCODE_JOB_STATUSES } from '../fieldConstants';

export default class TranscodeJobModel extends BaseModel {
  id: number;
  userId: number;
  jobId: string; // AWS jobId
  videoUri: string;
  thumbnailUri: string;
  status: TranscodeJobStatus;
  completedTimestamp: ?moment$Moment;
  +user: ?UserModel;

  static get tableName(): string {
    return 'TranscodeJob';
  }

  static get relationMappings(): Object {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'TranscodeJob.userId',
          to: 'User.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        jobId: { type: 'string' },
        videoUri: { type: 'string' },
        thumbnailUri: { type: 'string' },
        userId: { type: 'number' },
        status: { type: 'string', enum: Object.values(TRANSCODE_JOB_STATUSES) },
        completedTimestamp: { type: 'string', format: 'date-time' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
      },
      additionalProperties: false,
      required: ['status', 'jobId', 'videoUri', 'thumbnailUri'],
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getById(id: number): Promise<?TranscodeJobModel> {
    return this.query().findById(id);
  }

  /**
   * Get by Elastic Transcoder native job id
   */
  static async getByJobId(
    elasticTranscoderJobId: string,
  ): Promise<?TranscodeJobModel> {
    return this.query()
      .where('jobId', elasticTranscoderJobId)
      .first()
      .execute();
  }

  static async create(
    userId: number,
    jobId: string,
    videoUri: string,
    thumbnailUri: string,
  ): Promise<TranscodeJobModel> {
    return this.query()
      .insert({
        userId,
        jobId,
        videoUri,
        thumbnailUri,
        status: TRANSCODE_JOB_STATUSES.IN_PROGRESS,
      })
      .returning('*')
      .execute();
  }

  // INSTANCE METHODS
  async update(status: string): Promise<TranscodeJobModel> {
    // can't be null as it will cause the patch to break, so set to undefined if not existing
    let completedTimestamp = this.completedTimestamp || undefined;

    // only update completed timestamp if we are newly marking this job as complete
    if (
      this.status !== TRANSCODE_JOB_STATUSES.COMPLETE &&
      status === TRANSCODE_JOB_STATUSES.COMPLETE
    ) {
      completedTimestamp = new Date().toISOString();
    }

    return this.$query()
      .patch({ status, completedTimestamp })
      .returning('*')
      .execute();
  }
}
