// @flow
export type TimeframeOption = {
  startDaysAgo: number,
  daysToSpan: number
};
