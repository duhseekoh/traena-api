// @flow
import knex from 'knex';
import { Model, ref } from 'objection';
import { Pageable } from '@panderalabs/koa-pageable';
import type { PagedDbResult } from './BaseModel';
import BaseModel from './BaseModel';
import ActivityModel from './ActivityModel';
import UserModel from './UserModel';

type ActivityResponseModelPagePromise =
  Promise<PagedDbResult<ActivityResponseModel>>; // eslint-disable-line no-use-before-define

export type PollResponseCount = {
  selectedChoiceId: string,
  count: number,
};

type PollResponse = {
  selectedChoiceId: string,
};

type QuestionSetResponse = {
  progress: number,
};

export type ActivityResponseDTO = {
  id?: number,
  activityId: number,
  activityVersion?: number,
  userId: number,
  body: QuestionSetResponse | PollResponse,
};

export default class ActivityResponseModel extends BaseModel {
  id: number;
  activityId: number;
  activityVersion: number;
  userId: number;
  body: PollResponse | QuestionSetResponse;

  static get tableName(): string {
    return 'ActivityResponse';
  }

  static get relationMappings(): Object {
    return {
      activity: {
        relation: Model.BelongsToOneRelation,
        modelClass: ActivityModel,
        join: {
          from: 'ActivityResponse.activityId',
          to: 'Activity.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'ActivityResponse.userId',
          to: 'User.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        activityId: { type: 'integer' },
        activityVersion: { type: 'integer' },
        body: { type: 'object' },
        userId: { type: 'integer' },
      },
      additionalProperties: false,
      required: ['activityId', 'activityVersion', 'body', 'userId'],
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async upsert(activityResponse: ActivityResponse): Promise<ActivityResponseModel> {
    const activity = await ActivityModel.getById(activityResponse.activityId);
    activityResponse.activityVersion = activity ? activity.version : 1;
    if (activityResponse.id) {
      return this.query()
        .patch(activityResponse)
        .where('id', activityResponse.id)
        .first()
        .returning('*');
    }
    return this.query().insert(activityResponse).returning('*');
  }

  // get specific activity response
  static getById(activityResponseId: number): Promise<?ActivityResponseModel> {
    return this.query()
      .where({ id: activityResponseId })
      .first()
      .execute();
  }

  // get reponse for current version for a specific user
  static async getByActivityId(
    userId: number,
    activityId: number, pageable: Pageable,
  ): ActivityResponseModelPagePromise {
    return this.query()
      .where({ userId, activityId })
      .joinRelation('activity')
      .where('activity.version', '=', ref('ActivityResponse.activityVersion'))
      .page(pageable.page, pageable.size)
      .execute();
  }

  // get the sums of all the choices for a poll
  static async getPollResults(activityId: number): Promise<PollResponseCount[]> {
    const choiceResults = await this.query()
      .select(knex.raw('"ActivityResponse"."body"->>\'selectedChoiceId\' as "selectedChoiceId"'))
      .count('ActivityResponse.*')
      .where({ activityId })
      .joinRelation('activity')
      .where('activity.version', '=', ref('ActivityResponse.activityVersion'))
      .groupBy('selectedChoiceId')
      .execute()
      .then(results => results.map(r => ({ count: r.count, selectedChoiceId: r.selectedChoiceId })));
    return choiceResults;
  }

  static deleteByActivityId(activityId: number): Promise<number> {
    return this.query()
      .where('activityId', activityId)
      .delete();
  }
}

export type ActivityResponse = ActivityResponseDTO | ActivityResponseModel;
