// @flow
import { Model } from 'objection';
import type moment$Moment from 'moment';
import { Pageable } from '@panderalabs/koa-pageable';
import BaseModel from './BaseModel';
import UserModel from './UserModel';
import CommentModel from './CommentModel';
import ContentItemModel from './ContentItemModel';
import PushDeviceModel from './PushDeviceModel';
import type { NotificationStatus } from '../fieldConstants';
import { NOTIFICATION_STATUSES } from '../fieldConstants';

export default class NotificationModel extends BaseModel {
  id: number;
  fromUserId: number;
  toUserId: number;
  contentId: number;
  commentId: number;
  notificationType: string;
  status: NotificationStatus;

  static get timestamps(): string {
    return 'true';
  }

  static get tableName(): string {
    return 'Notification';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        fromUserId: { type: 'integer' },
        toUserId: { type: 'integer' },
        contentId: { type: 'integer' },
        commentId: { type: 'integer' },
        notificationType: { type: 'string' },
        status: { type: ['string'], enum: Object.values(NOTIFICATION_STATUSES) },
      },
      additionalProperties: false,
      required: ['status', 'toUserId', 'notificationType'],
    };
  }

  static get relationMappings(): Object {
    return {
      from: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Notification.fromUserId',
          to: 'User.id',
        },
      },
      comment: {
        relation: Model.BelongsToOneRelation,
        modelClass: CommentModel,
        join: {
          from: 'Notification.commentId',
          to: 'Comment.id',
        },
      },
      content: {
        relation: Model.BelongsToOneRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Notification.contentId',
          to: 'Content.id',
        },
      },
      pushDevices: {
        relation: Model.HasManyRelation,
        modelClass: PushDeviceModel,
        join: {
          from: 'Notification.toUserId',
          to: 'PushDevice.userId',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getNotificationsForUser(userId: number, maxDate?: moment$Moment, pageable: Pageable) {
    const builder = this
      .query()
      .select('Notification.*')
      .eager('[from, comment, content]')
      .where('Notification.toUserId', userId);
    if (maxDate && maxDate.isValid()) {
      builder.where('Notification.createdTimestamp', '<', maxDate.toISOString());
    }
    return builder
      .orderBy('createdTimestamp', 'desc')
      .page(pageable.page, pageable.size)
      .execute();
  }

  static async deleteNotificationsForComment(commentId: number) {
    return this.query().delete().where('commentId', commentId);
  }

  static async getUnsentNotificationsForUser(userId: number) {
    return this
      .query()
      .select('Notification.*')
      .eager('[from, comment, pushDevices, content]')
      .where('Notification.toUserId', userId)
      .where('status', 'READY_TO_SEND');
  }

  static async getUnacknowledgedNotificationCount(userId: number) {
    const result = await this
      .query()
      .count('*')
      .where('Notification.toUserId', userId)
      .whereIn('Notification.status', [
        NOTIFICATION_STATUSES.READY_TO_SEND,
        NOTIFICATION_STATUSES.SENT,
      ])
      .first();

    return result.count;
  }

  // Can't use Array<Notification> because we're actual using an object literal, womp womp
  static async insertNotifications(notifications: Array<*>) {
    return this
      .query()
      .insert(notifications);
  }

  static async updateAllToAcknowledged(toUserId: number) {
    return this
      .query()
      .patch({
        status: NOTIFICATION_STATUSES.ACKNOWLEDGED,
      })
      .whereIn('Notification.status', [
        NOTIFICATION_STATUSES.READY_TO_SEND,
        NOTIFICATION_STATUSES.SENT,
      ])
      .where('Notification.toUserId', toUserId);
  }
}
