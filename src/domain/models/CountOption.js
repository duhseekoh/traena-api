// @flow
/* eslint-disable */
/**
* This type is used as an argument to our testContentGeneration tool.
* The value of a number property, (i.e. likeCount, commentCount) can be given a value with a probability
*
* For example, { average: 10, spread: 0, sparseness: 1 }  => All contentItems will be given a value of 10
*              { average: 10, spread: 1, sparseness: .5 } => 50% contentItems will be given a value between 9 and 11. 50% get a random value
* {average} the average value a contentItem will receive
* {spread} the range around the average that the given value for a contentItem will fall
* {sparseness} between 0 and 1.  The probability a contentItem will get the value of average +/- spread, vs a random value
*/
export type CountOption = {
  average: number,
  spread: number,
  sparseness: number
};
