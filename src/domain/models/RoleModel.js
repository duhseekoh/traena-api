// @flow
import BaseModel from './BaseModel';

export type RoleDTO = {
  id?: ?number;
  auth0RoleId: string;
  name: string;
  description: string;
  isAssignable: boolean;
};

export default class RoleModel extends BaseModel {
  id: number;
  auth0RoleId: string;
  name: string;
  description: string;
  isAssignable: boolean;

  static get tableName(): string {
    return 'Role';
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'number' },
        auth0RoleId: { type: 'string' },
        name: { type: 'string' },
        description: { type: 'string' },
        isAssignable: { type: ['boolean', 'null'] },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        additionalProperties: false,
        required: ['auth0RoleId', 'name', 'description'],
      },
    };
  }

  static get relationMappings(): Object {
    return {};
  }

  static get hasTimestamps(): boolean {
    return true;
  }

  static async getAssignableRoles(): Promise<RoleModel[]> {
    return this
      .query()
      .select()
      .where('isAssignable', true)
      .execute();
  }

  static async getRoles(): Promise<RoleModel> {
    return this
      .query()
      .select()
      .execute();
  }
}

export type Role = RoleDTO | RoleModel;
