// @flow
import { Model } from 'objection';
import BaseModel from './BaseModel';
import UserModel from './UserModel';

export default class CommentTaggedUserModel extends BaseModel {
  static get tableName(): string {
    return 'Comment_TaggedUser';
  }

  static get relationMappings(): Object {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Comment_TaggedUser.userId',
          to: 'User.id',
        },
      },
    };
  }

  static get hasTimestamps(): boolean {
    return false;
  }

  static async insertUsers(userIds: number[], commentId: number) {
    return this.query()
      .returning(['userId', 'commentId'])
      .insert(userIds.map(userId => ({
        userId,
        commentId,
      })));
  }
}
