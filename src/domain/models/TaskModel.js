// @flow
import { Model } from 'objection';
import moment from 'moment';
import BaseModel from './BaseModel';
import type { TaskStatus } from '../fieldConstants';
import { CONTENT_ITEM_STATUSES, TRAENA_TASK_STATUSES } from '../fieldConstants';
import ContentItemModel from './ContentItemModel';
import UserModel from './UserModel';

export default class TaskModel extends BaseModel {
  id: number;
  // TODO - Task.completedTimestamp is never being set... Need a db migration
  //  that sets all existing null completedTimestamps to modifiedTimestamp and
  //  update service methods to set completedTimestamp whenever a task is set
  //  to as completed.
  completedTimestamp: Date;
  userId: number;
  contentId: number;
  status: TaskStatus;
  rating: ?number;

  static get hasTimestamps(): boolean {
    return true;
  }

  static get tableName(): string {
    return 'Task';
  }

  static get relationMappings(): Object {
    return {
      content: {
        relation: Model.BelongsToOneRelation,
        modelClass: ContentItemModel,
        join: {
          from: 'Task.contentId',
          to: 'Content.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'Task.userId',
          to: 'User.id',
        },
      },
    };
  }

  static get jsonSchema(): Object {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        modifiedTimestamp: { type: 'string', format: 'date-time' },
        createdTimestamp: { type: 'string', format: 'date-time' },
        completedTimestamp: { type: ['string', 'null'], format: 'date-time' },
        userId: { type: 'integer' },
        contentId: { type: 'integer' },
        rating: { type: ['integer', 'null'], minimum: 1, maximum: 5 },
        status: { type: ['string'], enum: Object.values(TRAENA_TASK_STATUSES) },
      },
      additionalProperties: false,
      required: ['userId', 'contentId', 'status'],
    };
  }

  static async getById(taskId: number) {
    return this
      .query()
      .findById(taskId).execute();
  }

  static async findIncompleteForUser(userId: number) {
    return this.query().select('Task.contentId')
      .joinRelation('content')
      .where('content.status', CONTENT_ITEM_STATUSES.PUBLISHED)
      .where('Task.userId', userId)
      .where('Task.status', TRAENA_TASK_STATUSES.INCOMPLETE)
      .orderBy('Task.createdTimestamp', 'desc')
      .execute();
  }

  static async findCompleteForUser(userId: number, maxDate?: moment): Promise<TaskModel[]> {
    const builder = this.query().select('Task.contentId')
      .joinRelation('content')
      .where('content.status', CONTENT_ITEM_STATUSES.PUBLISHED)
      .where('Task.userId', userId)
      .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
      .orderBy('Task.completedTimestamp', 'desc');

    if (maxDate) {
      builder.where('Task.createdTimestamp', '<', maxDate.toISOString());
    }

    return builder.execute();
  }

  static async findByUserStatusAndMaxDate(userId: number, status: string, count: number = 10, maxDate: Date) {
    const builder = this
      .query()
      .select()
      .where('Task.userId', userId)
      .where('Task.status', status)
      .orderBy('Task.createdTimestamp', 'desc')
      .limit(count);

    if (maxDate) {
      builder.where('createdTimestamp', '<', maxDate.toISOString());
    }

    return builder.execute();
  }

  /**
   * When you already know the content you need the traena list items for.
   * No filtering on statuses, want everything back.
   */
  static async findByUserAndContentIds(userId: number, contentIds: number[]) {
    const builder = this
      .query()
      .select()
      .where('Task.userId', userId)
      .whereIn('Task.contentId', contentIds);

    return builder.execute();
  }

  static async getCompletedCountByContentId(contentId: number, organizationId?: number): Promise<number> {
    const query = this.query()
      .count()
      .joinRelation('user')
      .where('Task.contentId', contentId)
      .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE);

    if (organizationId) {
      query.where('user.organizationId', organizationId);
    }

    return query
      .first()
      .then(({ count }) => count);
  }

  /**
   * Get a scoped view of what users have completed what content.
   */
  static async getCompletedContentIdsByUser(
    userIds: number[], // Users we want this info about
    contentIds: number[], // Content we are checking for completions of
  ): Promise<{userId: number, contentId: number}[]> {
    return this.query()
      .select('Task.userId', 'Task.contentId')
      .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
      .whereIn('Task.userId', userIds)
      .whereIn('Task.contentId', contentIds);
  }

  /**
   * Get a count of how much content has been completed by a particular user
   * from the passed in content list.
   */
  static async getCompletedContentCountForUser(
    userId: number, // User looking to get the progress of
    contentIds: number[], // Content we are checking for completions of
  ): Promise<number> {
    const result = await TaskModel
      .query()
      .countDistinct('Task.contentId')
      .where('Task.userId', userId)
      .where('Task.status', TRAENA_TASK_STATUSES.COMPLETE)
      .whereIn('Task.contentId', contentIds)
      .first();

    return result.count;
  }

  static async getItem(userId: number, contentId: number, status: TaskStatus) {
    return this
      .query()
      .first()
      .where({
        userId,
        contentId,
        status,
      }).execute();
  }

  static async get(taskId: number) {
    return this.query().first().where({ id: taskId }).execute();
  }

  static async create(userId: number, contentId: number, complete: boolean) {
    return this
      .query()
      .insert({
        userId,
        contentId,
        status: complete ? TRAENA_TASK_STATUSES.COMPLETE : TRAENA_TASK_STATUSES.INCOMPLETE,
      })
      .returning('*').execute();
  }

  static async update(task: TaskModel) {
    return this
      .query()
      .patchAndFetchById(task.id, {
        status: task.status,
        rating: task.rating,
        completedTimestamp: task.completedTimestamp,
      })
      .execute();
  }

  static async deleteTask(taskId: number) {
    return this
      .query()
      .deleteById(taskId)
      .execute()
      .then(numberDeleted => !!numberDeleted);
  }
}
