All emails in this folder are generated and then copied from https://github.com/Traena/traena-email-templates.

If a change or new email template is needed, then it should be completed in the traena-email-templates repo and copied over to this directoy.
