// @flow

export const TRAENA_TASK_STATUSES = {
  INCOMPLETE: 'INCOMPLETE',
  COMPLETE: 'COMPLETE',
};

export const NOTIFICATION_STATUSES = {
  READY_TO_SEND: 'READY_TO_SEND',
  SENT: 'SENT',
  ACKNOWLEDGED: 'ACKNOWLEDGED',
};

export const ORGANIZATION_TYPES = {
  CLIENT: 'CLIENT',
  TRAINING_COMPANY: 'TRAINING_COMPANY',
};

export const CONTENT_ITEM_TYPES = {
  DAILY_ACTION: 'DailyAction',
  TRAINING_VIDEO: 'TrainingVideo',
  IMAGE: 'Image',
};

export const MEDIA_TYPES = {
  VIDEO: 'video',
  IMAGE: 'image',
};

export const CONTENT_ITEM_TYPES_TO_MEDIA_TYPES = {
  [CONTENT_ITEM_TYPES.TRAINING_VIDEO]: [MEDIA_TYPES.VIDEO],
  [CONTENT_ITEM_TYPES.IMAGE]: [MEDIA_TYPES.IMAGE],
  [CONTENT_ITEM_TYPES.DAILY_ACTION]: [],
};

export const CONTENT_ITEM_STATUSES = {
  DRAFT: 'DRAFT',
  PUBLISHED: 'PUBLISHED',
  ARCHIVED: 'ARCHIVED',
};

export const CONTENT_INCLUDE_SOURCES = {
  SELF: 'self',
  INTERNAL: 'internal',
  EXTERNAL: 'external',
};

export const CHANNEL_VISIBILITIES = {
  MARKET: 'MARKET', // publicly available for any subscriber
  INTERNAL: 'INTERNAL', // not subscribable other than to owning org
  LIMITED: 'LIMITED', // contented created by an org for a specific set of subscriber orgs
};

export const SUBSCRIPTION_SOURCES = {
  INTERNAL: 'internal',
  EXTERNAL: 'external',
  ALL: 'all',
};

export const SUBSCRIPTION_VISIBILITIES = {
  ADMINS: 'ADMINS',
  ORGANIZATION: 'ORGANIZATION',
  USERS: 'USERS',
};

export const TRANSCODE_JOB_STATUSES = {
  IN_PROGRESS: 'IN_PROGRESS',
  ERROR: 'ERROR',
  COMPLETE: 'COMPLETE',
};

export const NOTIFICATION_TYPES = {
  COMMENT_ON_POST: 'COMMENT_ON_POST',
  LIKE_ON_POST: 'LIKE_ON_POST',
  TAG_IN_COMMENT: 'TAG_IN_COMMENT',
};

export const ACTIVITY_TYPES = {
  QUESTION_SET: 'QuestionSet',
  POLL: 'Poll',
};

export type TaskStatus = $Values<typeof TRAENA_TASK_STATUSES>;
export type NotificationType = $Values<typeof NOTIFICATION_TYPES>;
export type NotificationStatus = $Values<typeof NOTIFICATION_STATUSES>;
export type OrganizationType = $Values<typeof ORGANIZATION_TYPES>;
export type ContentItemStatus = $Values<typeof CONTENT_ITEM_STATUSES>;
export type ContentItemType = $Values<typeof CONTENT_ITEM_TYPES>;
export type MediaType = $Values<typeof MEDIA_TYPES>;
export type ChannelVisibility = $Values<typeof CHANNEL_VISIBILITIES>;
export type TranscodeJobStatus = $Values<typeof TRANSCODE_JOB_STATUSES>;
export type ContentIncludeSource = $Values<typeof CONTENT_INCLUDE_SOURCES>;
export type SubscriptionSources = $Values<typeof SUBSCRIPTION_SOURCES>;
export type SubscriptionVisibility = $Values<typeof SUBSCRIPTION_VISIBILITIES>;
export type ActivityType = $Values<typeof ACTIVITY_TYPES>;
