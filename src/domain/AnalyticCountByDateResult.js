// @flow
import { localDate } from '../common/dateUtils';

export default class AnalyticCountByDateResult {
  date: string;
  count: number;

  constructor(date: Date, count: number) {
    this.date = localDate(date);
    this.count = count;
  }
}

