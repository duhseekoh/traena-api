// @flow

export default class ContentStatus {
  // List of all incomplete tasks
  incomplete: number[];
  // Ordered SET of completed tasks containing the deduped list of completed task ids in reverse chronological order
  // of completion date
  complete: number[];

  constructor(incomplete: number[], complete: number[]) {
    this.incomplete = incomplete;
    this.complete = complete;
  }
}
