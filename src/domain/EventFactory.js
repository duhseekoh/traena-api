// @flow
import type { Event } from './models/EventModel'; // eslint-disable-line import/named
import EventModel, { EventSource } from './models/EventModel';
import type ActivityModel from './models/ActivityModel';
import type ActivityResponseModel from './models/ActivityResponseModel';
import type ContentItemModel from './models/ContentItemModel';
import type OrganizationModel from './models/OrganizationModel';
import type CommentModel from './models/CommentModel';
import type TaskModel from './models/TaskModel';
import type UserModel from './models/UserModel';
import type ChannelModel from './models/ChannelModel';
import type SubscriptionModel from './models/SubscriptionModel';

import Logger from '../common/logging/logger';

const logger = new Logger('domain/EventFactory');

// Helper function to ensure we do not ever throw an exception when creating events
function _errorSink(retValFunc: () => Event): ?Event {
  try {
    return retValFunc();
  } catch (err) {
    logger.warn('Error creating event record. Discarding and continuing');
    logger.warn(err);
    return null;
  }
}

export function createActivityEvent(activity: ActivityModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ACTIVITY_CREATED,
    userId,
    data: {
      activityId: activity.id,
      activityVersion: activity.version,
      activityType: activity.type,
      contentId: activity.contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function modifyActivityEvent(activity: ActivityModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ACTIVITY_MODIFIED,
    userId,
    data: {
      activityId: activity.id,
      activityVersion: activity.version,
      activityType: activity.type,
      contentId: activity.contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function deleteActivityEvent(activity: ActivityModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ACTIVITY_DELETED,
    userId,
    data: {
      activityId: activity.id,
      activityVersion: activity.version,
      activityType: activity.type,
      contentId: activity.contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function createActivityResponseEvent(activityResponse: ActivityResponseModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ACTIVITY_RESPONSE_CREATED,
    userId: activityResponse.userId,
    data: {
      activityResponseId: activityResponse.id,
      activityId: activityResponse.activityId,
      activityVersion: activityResponse.activityVersion,
    },
    source: EventSource.SERVER,
  }));
}

export function modifyActivityResponseEvent(activityResponse: ActivityResponseModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ACTIVITY_RESPONSE_MODIFIED,
    userId: activityResponse.userId,
    data: {
      activityResponseId: activityResponse.id,
      activityId: activityResponse.activityId,
      activityVersion: activityResponse.activityVersion,
    },
    source: EventSource.SERVER,
  }));
}

export function addCommentEvent(comment: CommentModel, taggedUsers: number[]): ?Event {
  return _errorSink((): Event => ({
    type: EventModel.eventTypes.COMMENT_ADDED,
    userId: comment.authorId,
    data: {
      contentId: comment.contentId,
      commentId: comment.id,
      taggedUsers,
    },
    source: EventSource.SERVER,
  }));
}

export function deleteCommentEvent(commentId: number, contentId: number): ?Event {
  return _errorSink((): Event => ({
    type: EventModel.eventTypes.COMMENT_DELETED,
    data: {
      commentId,
      contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function createContentEvent(contentItem: ContentItemModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_CREATED,
    userId: contentItem.authorId || contentItem.author.id,
    data: {
      contentId: contentItem.id,
      contentType: contentItem.body.type,
      organizationId: contentItem.authorOrganizationId || contentItem.author.organization.id,
      status: contentItem.status,
    },
    source: EventSource.SERVER,
  }));
}

export function modifyContentEvent(contentItem: ContentItemModel, userId: ?number): ?Event {
  return _errorSink((): Event => ({
    type: EventModel.eventTypes.CONTENT_MODIFIED,
    userId,
    data: {
      contentId: contentItem.id,
      contentType: contentItem.body.type,
      organizationId: contentItem.authorOrganizationId || contentItem.author.organization.id,
      status: contentItem.status,
    },
    source: EventSource.SERVER,
  }));
}

export function archiveContentEvent(contentItem: ContentItemModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_ARCHIVED,
    userId,
    data: {
      contentId: contentItem.id,
      contentType: contentItem.body.type,
      organizationId: contentItem.authorOrganizationId || contentItem.author.organization.id,
      status: contentItem.status,
    },
    source: EventSource.SERVER,
  }));
}

export function likeContentItemEvent(contentId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_LIKED,
    userId,
    data: {
      contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function unlikeContentItemEvent(contentId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_UNLIKED,
    userId,
    data: {
      contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function createOrganizationEvent(organization: OrganizationModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ORG_CREATED,
    userId,
    data: {
      organizationId: organization.id,
      organizationName: organization.name,
    },
    source: EventSource.SERVER,
  }));
}

export function disableOrganizationEvent(organization: OrganizationModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.ORG_DISABLED,
    userId,
    data: {
      organizationId: organization.id,
      organizationName: organization.name,
    },
    source: EventSource.SERVER,
  }));
}

export function createTaskEvent(task: TaskModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.TASK_CREATED,
    userId: task.userId,
    data: {
      taskId: task.id,
      contentId: task.contentId,
      status: task.status,
    },
    source: EventSource.SERVER,
  }));
}

export function updateTaskEvent(task: TaskModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.TASK_UPDATED,
    userId: task.userId,
    data: {
      taskId: task.id,
      contentId: task.contentId,
      status: task.status,
    },
    source: EventSource.SERVER,
  }));
}

export function deleteTaskEvent(taskId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.TASK_DELETED,
    userId,
    data: {
      taskId,
    },
    source: EventSource.SERVER,
  }));
}

export function completeTaskEvent(task: TaskModel): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.TASK_COMPLETED,
    userId: task.userId,
    data: {
      taskId: task.id,
      contentId: task.contentId,
      status: task.status,
    },
    source: EventSource.SERVER,
  }));
}


export function createUserEvent(createdUser: UserModel, authorizedUserId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.USER_CREATED,
    userId: authorizedUserId,
    data: {
      createdUserId: createdUser.id,
      firstName: createdUser.firstName,
      lastName: createdUser.lastName,
      email: createdUser.email,
    },
    source: EventSource.SERVER,
  }));
}

export function disableUserEvent(disabledUserId: number, authorizedUserId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.USER_DISABLED,
    userId: authorizedUserId,
    data: {
      disabledUserId,
    },
    source: EventSource.SERVER,
  }));
}

export function createChannelEvent(channel: ChannelModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CHANNEL_CREATED,
    userId,
    data: {
      id: channel.id,
      name: channel.name,
      visibility: channel.visibility,
      organizationId: channel.organizationId,
    },
    source: EventSource.SERVER,
  }));
}

export function modifyChannelEvent(channel: ChannelModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CHANNEL_MODIFIED,
    userId,
    data: {
      id: channel.id,
      name: channel.name,
      visibility: channel.visibility,
      organizationId: channel.organizationId,
    },
    source: EventSource.SERVER,
  }));
}

export function contentBookmarkedEvent(contentId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_BOOKMARKED,
    userId,
    data: {
      contentId,
    },
    source: EventSource.SERVER,
  }));
}

export function contentUnbookmarkedEvent(contentId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_UNBOOKMARKED,
    userId,
    data: {
      contentId,
    },
    source: EventSource.SERVER,
  }));
}


export function channelSubscribedEvent(channelId: number, subscriberOrganizationId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CHANNEL_SUBSCRIBED,
    userId,
    data: {
      channelId,
      subscriberOrganizationId,
    },
    source: EventSource.SERVER,
  }));
}

export function modifySubscriptionEvent(subscription: SubscriptionModel, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CHANNEL_SUBSCRIPTION_MODIFIED,
    userId,
    data: {
      channelId: subscription.channelId,
      subscriberOrganizationId: subscription.organizationId,
      visibility: subscription.visibility,
    },
    source: EventSource.SERVER,
  }));
}

export function channelUnsubscribedEvent(channelId: number, subscriberOrganizationId: number, userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CHANNEL_UNSUBSCRIBED,
    userId,
    data: {
      channelId,
      subscriberOrganizationId,
    },
    source: EventSource.SERVER,
  }));
}

export function userReindexedEvent(userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.USER_REINDEXED,
    userId,
    data: {},
    source: EventSource.SERVER,
  }));
}

export function contentReindexedEvent(userId: ?number): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.CONTENT_REINDEXED,
    userId,
    data: {},
    source: EventSource.SERVER,
  }));
}

export function searchedContentEvent(
  userId: ?number,
  searchText: ?string,
  tags: ?string[],
  page: number,
  contentIds: ?number[],
): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.SEARCHED_CONTENT,
    userId,
    data: {
      contentIds, page, searchText, tags,
    },
    source: EventSource.SERVER,
  }));
}

export function feedLoadedEvent(userId: ?number, page: number, contentIds: ?number[]): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.FEED_LOADED,
    userId,
    data: { contentIds, page },
    source: EventSource.SERVER,
  }));
}

export function whatsNewLoadedEvent(userId: ?number, page: number, contentIds: ?number[]): ?Event {
  return _errorSink(() => ({
    type: EventModel.eventTypes.WHATS_NEW_LOADED,
    userId,
    data: { contentIds, page },
    source: EventSource.SERVER,
  }));
}
