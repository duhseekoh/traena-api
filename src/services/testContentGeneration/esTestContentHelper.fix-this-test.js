import moment from 'moment';
import * as esTestContentHelper from './esTestContentHelper';

/**
 * TODO: These tests should just use the generateTestContentFromOptions method
 * the FromCLI method was just a proxy to take args from the command line but we've decided to deprecate it
 */

function countUniqueAuthors(content) {
  /**
   * Counts unique authors by building object literal with keys as author names.  Because
   * keys are overwritten in the case of duplicates, the final length of the object literal is
   * the count of unique authors.
   */
  const uniqueAuthors = {};
  content.forEach((contentItem) => {
    uniqueAuthors[
      `${contentItem.author.firstName} ${contentItem.author.lastName}`
    ] = true;
  });
  console.log(uniqueAuthors);
  return Object.keys(uniqueAuthors).length;
}

function countUniqueText(content) {
  const uniqueText = {};
  content.forEach((contentItem) => {
    uniqueText[contentItem.body.text] = true;
  });
  return Object.keys(uniqueText).length;
}

test('when told to generate 100 content items with 4 different authors the content should have 4 authors', () => {
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100,
    authorCount: 4,
  });
  const authorCount = countUniqueAuthors(content);
  const expectedAuthorCount = 4;

  expect(authorCount).toEqual(expectedAuthorCount);
});

test(`when told to generate 2 sets of 100 content items with 4 different authors
    each the content should have 8 authors total`, () => {
  const content = esTestContentHelper.generateTestContentFromJSONDefinition([
    { count: 100, authorCount: 4 },
    { count: 100, authorCount: 4 },
  ]);
  const authorCount = countUniqueAuthors(content);
  const expectedAuthorCount = 8;
  expect(authorCount).toEqual(expectedAuthorCount);
});

test(`when told to generate 100 content items where every item has 14 likes the
    content set should have 1400 likes total`, () => {
  // Date with UTC timezone - year, month, date, hours, minutes, milliseconds
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100,
    likes: { average: 14, spread: 0, sparseness: 1 },
    useDummyContent: true,
  });
  let likeCount = 0;
  content.forEach((contentItem) => {
    likeCount += contentItem.likeCount;
  });
  const expectedTotalLikes = 1400;
  expect(likeCount).toEqual(expectedTotalLikes);
});

test(`when told to generate 100 content items where every item has 14 comments
    the content set should have 1400 comments total`, () => {
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100,
    comments: { average: 14, spread: 0, sparseness: 1 },
    useDummyContent: true,
  });
  let commentCount = 0;
  content.forEach((contentItem) => {
    commentCount += contentItem.commentCount;
  });
  const expectedTotalComments = 1400;
  expect(commentCount).toEqual(expectedTotalComments);
});

test(`when told to generate 100,000 content items where 75% of the content items
  have the tags [these, are tags]`, () => {
  const tagsToAdd = ['these', 'are', 'tags'];
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100000,
    tags: { tagWords: tagsToAdd, sparseness: 0.75 },
    useDummyContent: true,
  });

  const contentWithMatchingTags = content.filter((contentItem) => {
    let hasAllTags = true;
    contentItem.tags.forEach((tag) => {
      if (!tagsToAdd.includes(tag.text)) {
        hasAllTags = false;
      }
    });
    return hasAllTags;
  });
  const percentWithMatchingTags = parseFloat(
    (contentWithMatchingTags.length / content.length).toFixed(2),
  );
  const expected = 0.75;
  // This will always only be apx. 75%.. with some error.  Because we are taking 100,000 samples we have a very low
  // probability of our percentage == .76 or .74 after rounding.
  expect(percentWithMatchingTags).toEqual(expected);
});

test('when told to generate 100 content items starting 8 days ago, spanning 2 days', () => {
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100,
    timeframe: { startDaysAgo: 8, daysToSpan: 2 },
    useDummyContent: true,
  });

  const sortedContentDesc = content.sort(
    (x, y) =>
      moment(x.publishedTimestamp).valueOf() -
      moment(y.publishedTimestamp).valueOf(),
  );
  const firstPostDate = moment(sortedContentDesc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const sortedContentAsc = content.sort(
    (x, y) =>
      moment(y.publishedTimestamp).valueOf() -
      moment(x.publishedTimestamp).valueOf(),
  );
  const lastPostDate = moment(sortedContentAsc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const expectedFirstPostDate = moment()
    .subtract(8, 'd')
    .format('MMDDYYYY');
  const expectedLastPostDate = moment()
    .subtract(6, 'd')
    .format('MMDDYYYY');

  // The most recent content item should have been posted 6 days ago
  expect(lastPostDate).toEqual(expectedLastPostDate);

  // The oldest content item should have been posted 8 days ago
  expect(firstPostDate).toEqual(expectedFirstPostDate);
});

test(`when told to generate 100 content items starting 365 days ago, spanning 1
    day, and to generate 100 content items starting 88 days ago, spanning 2 days`, () => {
  // eslint-disable-line
  const content = esTestContentHelper.generateTestContentFromJSONDefinition([
    {
      count: 100,
      timeframe: { startDaysAgo: 365, daysToSpan: 1 },
      useDummyContent: true,
    },
    {
      count: 100,
      timeframe: { startDaysAgo: 88, daysToSpan: 2 },
      useDummyContent: true,
    },
  ]);

  const sortedContentDesc = content.sort(
    (x, y) =>
      moment(x.publishedTimestamp).valueOf() -
      moment(y.publishedTimestamp).valueOf(),
  );
  const firstPostDate = moment(sortedContentDesc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const sortedContentAsc = content.sort(
    (x, y) =>
      moment(y.publishedTimestamp).valueOf() -
      moment(x.publishedTimestamp).valueOf(),
  );
  const lastPostDate = moment(sortedContentAsc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const expectedFirstPostDate = moment()
    .subtract(365, 'd')
    .format('MMDDYYYY');
  const expectedLastPostDate = moment()
    .subtract(86, 'd')
    .format('MMDDYYYY');

  // The most recent content item should have been posted 86 days ago
  expect(lastPostDate).toEqual(expectedLastPostDate);

  // The oldest content item should have been posted 365 days ago
  expect(firstPostDate).toEqual(expectedFirstPostDate);
});

test('when told to generate 100 content items starting 265 days ago, spanning 261 days, by 6 different authors', () => {
  const content = esTestContentHelper.generateContentFromOptions({
    count: 100,
    timeframe: { startDaysAgo: 88, daysToSpan: 2 },
    authorCount: 4,
  });

  const authorCount = countUniqueAuthors(content);
  const expectedAuthorCount = 4;

  const sortedContentDesc = content.sort(
    (x, y) =>
      moment(x.publishedTimestamp).valueOf() -
      moment(y.publishedTimestamp).valueOf(),
  );
  const firstPostDate = moment(sortedContentDesc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const sortedContentAsc = content.sort(
    (x, y) =>
      moment(y.publishedTimestamp).valueOf() -
      moment(x.publishedTimestamp).valueOf(),
  );
  const lastPostDate = moment(sortedContentAsc[0].publishedTimestamp).format(
    'MMDDYYYY',
  );

  const expectedFirstPostDate = moment()
    .subtract(88, 'd')
    .format('MMDDYYYY');
  const expectedLastPostDate = moment()
    .subtract(86, 'd')
    .format('MMDDYYYY');

  // The content should have 6 total authors
  expect(authorCount).toEqual(expectedAuthorCount);

  // The oldest content item should have been posted 265 days ago
  expect(firstPostDate).toEqual(expectedFirstPostDate);

  // The newest content item should have been posted 4 days ago
  expect(lastPostDate).toEqual(expectedLastPostDate);
});

test('when told to generate 100 content items where every element has the same properties (i.e. homogenous)', () => {
  const content = esTestContentHelper.generateContentFromOptions({
    useDummyContent: true,
    homogenous: true,
  });

  const uniqueCount = countUniqueText(content);
  const expectedUniqueCount = 1;

  // There should only be 1 unique text in the content (i.e. all content has same text).
  expect(uniqueCount).toEqual(expectedUniqueCount);
});
