import fs from 'fs';
// import { random } from 'lodash';

const TEST_USERS_DATA_SET_TYPES = {
  PANDERA_ORG_USERS: 'PANDERA_ORG_USERS',
};

const generatePanderaTestUsers = () => {
  const users = fs.readFileSync('./dataSets/pandera-org-users.json');
  return JSON.parse(users);
};

export const generateTestUsers = (type) => { // eslint-disable-line
  switch (type) {
    case TEST_USERS_DATA_SET_TYPES.PANDERA_ORG_USERS:
      return generatePanderaTestUsers();
    default:
      return null;
  }
};
