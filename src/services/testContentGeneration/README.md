## What is this for?

From the command line, you can generate sets of Traena content (for elasticsearch indexing or other purposes). You can control the distribution of comments, likes, tags, and authors in your generated data sets.

## How do I use it?

To generate a set of data with your cusomizations, and index it into elasticsearch:

`yarn run esUtils -- index:test:content -j -n // (all available options)`

Using arguments in a JSON configuration, you can define different properties you'd like the data set you generate to have.  For example,

  - Content that was all generated last week.  Or over the last year.
  - Content that was written by N different authors.
  - Content that receives many likes.
  - Content that receives many comments.
  - Use random dummy content to fill in untouched fields, or use content pulled from a real Traena organization.
  - etc.  To see a full list & description of all options, run `yarn run esUtils -- index:test:content --help`

```
Usage: index:test:content --name [name] --json "{ //stringified JSON object }"

  Properties within JSON object:

    - count                        The number of items to index (default 200)
    - authorCount                  The number of authors involved in creating the content set (default 1)
    - timeframe                    i.e. 8 days ago, all the content was posted over the course of 1 day (requires {startDaysAgo, daysToSpan} (default 365, 365))
    - homogenous                   All content identical other than edited options (default false)
    - likes                        Change the distribution and number of likes content tends to have (requires {average, spread, sparseness}) (default random(0, 15) for dummy content)
    - comments                     Change the distribution and number of comments content tends to have (requires {average, spread, sparseness}) (default random(0, 15) for dummy content)
    - tags                         A set of tags to apply to content, and what percentage of they will apply to (requires Array tagWords, sparseness)
    - datasetPath                  Path to a JSON file for which you would like to take data (defaut generates dummy content)

```

## More complex data sets.

With the CLI options -j or --json, you can generate a data set where different portions of the content each have their own set of the above arguments. i.e, 1/2 of the content has one set of arguments for `-c -a -e -h -l -m -t`, and the second half has a separate set of arguments `-c -a -e -h -l -m -t`.

```
[  
   {  
      "count":6,
      "authorCount":4,
      "timeframe":[  
         7,
         1
      ],
      "homogenous":false,
      "tags":[  
         [  
            "these",
            "are",
            "tags"
         ],
         1
      ],
      "likes":[  
         14,
         4,
         1
      ],
      "useDummyContent":false
   },
   {  
      "count":4,
      "authorCount":4,
      "timeframe":[  
         400,
         80
      ],
      "homogenous":false,
      "tags":[  
         [  
            "more",
            "great",
            "tags"
         ],
         1
      ],
      "likes":[  
         72,
         4,
         1
      ],
      "useDummyContent":false
   }
]
```
