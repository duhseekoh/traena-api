// @flow
import fs from 'fs';
import { random } from 'lodash';
import moment from 'moment';
import faker from 'faker';
import type { ContentItemDTO } from '../../domain/models/ContentItemModel';
import type {
  CountOption,
  TimeframeOption,
  GenerationOptions,
} from '../../domain/models/ContentGeneratorModels';

// ContentItemDTOStripped but without the read only property enforcements
type ContentItemDTOStripped = { ...ContentItemDTO };

const DAY_LENGTH_MS = 86400000;

const generateFromDataset = (datasetPath) => {
  const content = fs.readFileSync(`${__dirname}${datasetPath}`, 'utf8');
  return JSON.parse(content);
};

const generateDummyContent = (count: number = 200): ContentItemDTOStripped[] => {
  const content = [];
  for (let i = 0; i < count; i += 1) {
    content.push({
      id: random(0, 1000000),
      authorOrganizationId: 10,
      authorId: 50,
      body: {
        type: 'TrainingVideo',
        title: faker.fake('{{random.word}}'),
      },
      status: 'PUBLISHED',
      createdTimestamp: '2017-10-12T16:45:27.036Z',
      modifiedTimestamp: '2017-10-12T16:45:27.036Z',
      publishedTimestamp: '2017-10-12T16:45:26.525Z',
      channelId: 10,
      likeCount: random(0, 15),
      commentCount: random(0, 15),
      completedCount: 0,
      author: {
        id: 50,
        email: faker.fake('{{internet.email}}'),
        organizationId: 10,
        firstName: faker.fake('{{name.firstName}}'),
        lastName: faker.fake('{{name.lastName}}'),
        isDisabled: null,
        position: null,
        tags: null,
        organization: { id: 10 },
      },
      activityCount: 0,
      activitiesOrder: [],
      tags: [
        {
          contentId: 2,
          text: faker.fake('{{random.word}}'),
          createdTimestamp: '2017-10-12T16:45:27.043Z',
          modifiedTimestamp: '2017-10-12T16:45:27.043Z',
        },
        {
          contentId: 2,
          text: faker.fake('{{random.word}}'),
          createdTimestamp: '2017-10-12T16:45:27.043Z',
          modifiedTimestamp: '2017-10-12T16:45:27.043Z',
        },
        {
          contentId: 2,
          text: faker.fake('{{random.word}}'),
          createdTimestamp: '2017-10-12T16:45:27.043Z',
          modifiedTimestamp: '2017-10-12T16:45:27.043Z',
        },
      ],
      channel: {},
      subscriberOrganizationId: 10,
    });
  }
  return content;
};

/**
 * Generates a set of content where every element is the same element.
 * Can be used as a base to help focus on particular attributes isolated from others
 * @param {content} array the content items we will transform to all be the same content
 */
const transformToHomogenousContent = (content: ContentItemDTOStripped[]) => {
  const randomContentItem = content[random(0, content.length - 1, false)]; // This false flag forces an int
  return content.map(() => randomContentItem);
};

/**
 * Takes a list of content items, and sets their createdTimestamp to be evenly distributed over a time period
 * i.e. 7 content items, 7 days => 1 content item per day
 * @param {content} array the contentItem set we want transformed
 * @param {startDaysAgo} number how many days ago we want the first contentItem to have been created
 * @param {daysToSpan} number the number of days over which we want the content to have been posted
 */
const transformToEvenlyDistributedContentOverTime = (
  content: ContentItemDTOStripped[],
  timeframe: TimeframeOption,
) => {
  const { startDaysAgo, daysToSpan } = timeframe;
  const startTimestamp = moment()
    .subtract(startDaysAgo, 'd')
    .valueOf();
  const time = daysToSpan * DAY_LENGTH_MS;
  const timeBetweenContent = time / content.length;
  let publishedTimestamp = startTimestamp;
  return content.map((contentItem) => {
    publishedTimestamp += timeBetweenContent;
    const formattedPublishedTimestamp = moment(
      publishedTimestamp,
    ).toISOString();
    contentItem.publishedTimestamp = formattedPublishedTimestamp;
    return contentItem;
  });
};

/**
 * @param {content} array the content item set we want transformed
 * @param {avgLikes} number this is apx. how many likes each content item will have if modified by this method
 * @param {spread} number the range around avgLikes, within which the true likes count of a content item will fall
 * @param {sparseness} number betwen 0 and 1. The chance that a content item will be returned with avgLikes +/- spread
 */
const transformLikeCounts = (content: ContentItemDTOStripped[], likes: CountOption) =>
  content.map((contentItem) => {
    const { average, spread, sparseness } = likes;

    // (i.e. sparseness = 1.0 => picked is always true.. see generateConsistentlyLikedContent())
    const picked = Math.random() < sparseness;
    if (picked) {
      const likeCount = random(average - spread, average + spread, false); // `false` forces this to an int
      contentItem.likeCount = likeCount;
    }
    return contentItem;
  });

/**
 * @param {content} array the content item set we want transformed
 * @param {avgComments} number this is apx. how many comments each content item will have if modified by this method
 * @param {spread} number the range around avgComments, within which the true comment count of a content item will fall
 * @param {sparseness} number betwen 0 and 1. The chance that a content item will
 *                            be returned with avgComments +/- spread
 */
const transformCommentCounts = (
  content: ContentItemDTOStripped[],
  comments: CountOption,
) =>
  content.map((contentItem) => {
    const { average, spread, sparseness } = comments;
    // (i.e. sparseness = 1.0 => picked is always true.. see generateConsistentlyLikedContent())
    const picked = Math.random() < sparseness;
    if (picked) {
      const commentCount = random(average - spread, average + spread, false); // `false` forces this to an int
      contentItem.commentCount = commentCount;
    }
    return contentItem;
  });

const transformAuthorCount = (
  content: ContentItemDTOStripped[],
  authorCount: number = 1,
) =>
  // eslint-disable-line
  /**
   * TODO: allow for multiple organizationIds and have the mix of authors sit within those organizations
   */
  content.map((contentItem, index) => {
    if (index % (content.length / authorCount) === 0) {
      // $FlowFixMe$ - We are faking creation of a model here, the only time tags would ever be writeable
      contentItem.author = {
        id: random(0, 20),
        email: faker.fake('{{internet.email}}'),
        organizationId: 10,
        firstName: faker.fake('{{name.firstName}}'),
        lastName: faker.fake('{{name.lastName}}'),
        profileImageURI: '',
        isDisabled: null,
        position: null,
        tags: null,
        organization: {},
      };
    }
    return contentItem;
  });
const transformTags = (content: ContentItemDTOStripped[], tags) =>
  content.map((contentItem) => {
    const { tagWords, sparseness } = tags;
    // (i.e. sparseness = 1.0 => picked is always true.. see generateConsistentlyLikedContent())
    const picked = Math.random() < sparseness;
    if (picked) {
      // $FlowFixMe$ - We are faking creation of a model here, the only time tags would ever be writeable
      contentItem.tags = tagWords.map(tag => ({
        contentId: 2,
        text: tag,
        createdTimestamp: '2017-10-12T16:45:27.043Z',
        modifiedTimestamp: '2017-10-12T16:45:27.043Z',
      }));
    }
    return contentItem;
  });

export const generateContentFromOptions = (options: GenerationOptions) => {
  const {
    count,
    authorCount,
    timeframe,
    homogenous,
    likes,
    comments,
    tags,
    datasetPath,
  } = options;
  let content = [];
  if (datasetPath) {
    content = generateFromDataset(datasetPath);
  } else {
    content = generateDummyContent(count);
  }

  if (homogenous) {
    content = transformToHomogenousContent(content);
  }

  if (authorCount) {
    content = transformAuthorCount(content, authorCount);
  }

  if (timeframe) {
    content = transformToEvenlyDistributedContentOverTime(content, timeframe);
  }

  if (likes) {
    content = transformLikeCounts(content, likes);
  }

  if (comments) {
    content = transformCommentCounts(content, comments);
  }
  if (tags) {
    content = transformTags(content, tags);
  }
  return content;
};

export const generateTestContentFromJSONDefinition = (
  optionsSet: GenerationOptions[],
) => {
  // eslint-disable-line
  let fullContent = [];
  /**
   * Each element in JSON options is a definition for some percentage of the content we generate
   */
  optionsSet.forEach((options) => {
    const content = generateContentFromOptions(options);
    fullContent = [...fullContent, ...content];
  });
  // console.log('Raw generated content', JSON.stringify(fullContent));
  return fullContent;
};
