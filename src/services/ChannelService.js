// @flow
import { IndexablePage, ArrayPage, Pageable } from '@panderalabs/koa-pageable';
import { transaction } from 'objection';
import ChannelModel, { type Channel } from '../domain/models/ChannelModel';
import ContentItemModel from '../domain/models/ContentItemModel';
import SubscriptionModel, { type Subscription } from '../domain/models/SubscriptionModel';
import OrganizationModel from '../domain/models/OrganizationModel';
import ElasticSearchGatheringService from './ElasticSearchGatheringService';
import EventService from './EventService';
import * as EventFactory from '../domain/EventFactory';
import Logger from '../common/logging/logger';
import { BadRequestError, ConflictError, ResourceNotFoundError, ResourceValidationError } from '../common/errors';
import { CHANNEL_VISIBILITIES, SUBSCRIPTION_SOURCES, SUBSCRIPTION_VISIBILITIES } from '../domain/fieldConstants';
import { isDatabaseConflict } from '../common/errorUtils';
import type { SubscriptionSources, ChannelVisibility } from '../domain/fieldConstants';
import type { CreateSubscriptionDTO } from '../domain/models/SubscriptionModel';

const logger = new Logger('services/ChannelService');

// ugh - index is used to ensure that there is at MOST a single default channel within an organization
const SINGLE_DEFAULT_CHANNEL_INDEX = 'Idx_Channel_singleDefaultChannel';

function _isSingleDefaultIndexError(err: *): boolean {
  return isDatabaseConflict(err) && err.constraint === SINGLE_DEFAULT_CHANNEL_INDEX;
}

/**
 * Determines of the requested channel exists and is publicly available for subscription
 * @param organizationId
 * @param channelId
 * @returns {Promise.<boolean>}
 * @private
 */
async function _isSubscribable(organizationId: number, channelId: number): Promise<boolean> {
  const channel = await ChannelModel.getChannelById(channelId);
  if (!channel) {
    return false;
  }
  // nullable in the model, but in reality will always be populated
  const isOwnedByOrg = channel.organizationId === organizationId;
  const isExternal = channel.visibility !== CHANNEL_VISIBILITIES.INTERNAL;

  return isExternal || isOwnedByOrg;
}

/**
 * Determines if the requested channel is able to be unsubscribed from. Specifically organizations cannot unsubscribe
 * from their own internal channels.
 * @param organizationId
 * @param channelId
 * @returns {Promise.<void>}
 * @private
 */
async function _isUnsubscribable(organizationId: number, channelId: number): Promise<boolean> {
  const channel = await ChannelModel.getChannelById(channelId);
  if (!channel) {
    return false;
  }
  const isOwnedByOrg = channel.organizationId === organizationId;
  const isInternal = channel.visibility === CHANNEL_VISIBILITIES.INTERNAL;
  return !(isOwnedByOrg && isInternal);
}

export default class ChannelService {
  eventService: EventService;
  esGatheringService: ElasticSearchGatheringService;

  constructor(eventService: EventService, esGatheringService: ElasticSearchGatheringService) {
    this.eventService = eventService;
    this.esGatheringService = esGatheringService;
  }

  async get(channelId: number): Promise<?ChannelModel> {
    logger.silly('ChannelService.get', { channelId });
    return ChannelModel.getById(channelId);
  }

  async create(channel: Channel, userId: number): Promise<?ChannelModel> {
    logger.debug('ChannelService.create', { channel });

    try {
      const createdChannel = await ChannelModel.upsert(channel);
      this.eventService.raiseEvent(EventFactory.createChannelEvent(createdChannel, userId));

      // Subscribe the org to their own channel
      const subscription = {
        organizationId: createdChannel.organizationId,
        channelId: createdChannel.id,
        visibility: SUBSCRIPTION_VISIBILITIES.ADMINS,
      };
      await this.subscribe(subscription, userId);

      return createdChannel;
    } catch (err) {
      if (_isSingleDefaultIndexError(err)) {
        logger.warn('Cannot have more than one default channel', { userId, channel, err });
        throw new ConflictError('Cannot have more than one default channel');
      }
      logger.error('Unhandled channel creation error', { userId, channel, err });
      throw err;
    }
  }

  async delete(channelId: number): Promise<number> {
    const channel = await this.get(channelId);
    if (!channel) {
      logger.error('Channel does not exist to delete', { channelId });
      throw new ResourceValidationError('A channel must exist to delete it');
    }

    // check if has published posts
    const content = await channel.getPublishedContent();
    if (content.length > 0) {
      throw new BadRequestError('Channel to be deleted must not have PUBLISHED content related');
    }

    const deleted = await transaction(
      ChannelModel,
      ContentItemModel,
      async (BoundChannelModel, BoundContentItemModel) => {
        await BoundContentItemModel.archiveByChannelId(channelId);
        await BoundChannelModel.delete(channelId);
        return true;
      },
    );

    return deleted;
  }

  async update(channel: Channel, userId: number): Promise<?ChannelModel> {
    logger.debug('ChannelService.update', { userId, channel });

    if (!channel.id) {
      throw new BadRequestError('Channel to update must be supplied with an id');
    }

    const existing = await this.get(channel.id);

    if (!existing) {
      logger.warn('Channel does not exist', { userId, channel, existing });
      throw new ResourceNotFoundError(`Channel with id: ${channel.id ? channel.id : 'unknown'} does not
        exist or cannot be modified`);
    }

    // TODO, enhance by checking if there are subscriber orgs. Allow restriction
    //  if there are no subscriber orgs.
    if (existing.visibility === CHANNEL_VISIBILITIES.MARKET && channel.visibility !== CHANNEL_VISIBILITIES.MARKET) {
      logger.warn('Cannot restrict public channel', { userId, channel, existing });
      throw new ResourceValidationError('Cannot restrict public Channel');
    }

    try {
      const updatedChannel = await ChannelModel.upsert({
        id: channel.id,
        name: channel.name,
        organizationId: existing.organizationId,
        description: channel.description,
        visibility: channel.visibility,
        default: channel.default,
      });
      logger.info('Updated channel', { userId, updatedChannel });
      this.esGatheringService.indexChannelContents(updatedChannel.id);
      this.eventService.raiseEvent(EventFactory.modifyChannelEvent(updatedChannel, userId));
      return updatedChannel;
    } catch (err) {
      if (_isSingleDefaultIndexError(err)) {
        logger.warn('Cannot have more than one default channel', { userId, channel, err });
        throw new ConflictError('Cannot have more than one default channel');
      }
      logger.error('Unhandled channel update error', { userId, channel, err });
      throw err;
    }
  }

  async getSubscribedChannelIdsForUser(userId: number): Promise<number[]> {
    logger.silly('ChannelService.getSubscribedChannelIdsForUser', { userId });
    return ChannelModel.getSubscribedChannelIdsForUser(userId);
  }

  async getSubscriptionIdsForOrganization(organizationId: number): Promise<number[]> {
    logger.silly('ChannelService.getSubscriptionIdsForOrganization', { organizationId });
    return ChannelModel.getSubscriptionIdsForOrganization(organizationId);
  }

  /**
   * Get channels an organization is subscribed to (regardless of access level)
   * Filter by subscription source.
   */
  async getSubscribedChannelsForOrganization(
    organizationId: number,
    options: {
      subscriptionSource?: ?SubscriptionSources,
    },
  ): Promise<ChannelModel[]> {
    return ChannelModel.getSubscribedChannelsForOrganization(organizationId, options);
  }

  /**
   * Get a paginated list of all channels a user is subscribed to.
   */
  async getSubscribedChannelsForUserPaginated(
    userId: number,
    organizationId: number,
    subscriptionSource: ?SubscriptionSources = SUBSCRIPTION_SOURCES.ALL,
    pageable: Pageable,
  ): Promise<IndexablePage<number, ChannelModel>> {
    logger.silly('ChannelService.getSubscribedChannelsForUserPaginated', { userId, subscriptionSource, pageable });

    let result;
    if (subscriptionSource === SUBSCRIPTION_SOURCES.INTERNAL) {
      result = await ChannelModel.getInternalSubscribedChannelsForUserPaginated(userId, organizationId, pageable);
    } else if (subscriptionSource === SUBSCRIPTION_SOURCES.EXTERNAL) {
      result = await ChannelModel.getExternalSubscribedChannelsForUserPaginated(userId, organizationId, pageable);
    } else {
      result = await ChannelModel.getSubscribedChannelsForUserPaginated(userId, pageable);
    }
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Get all channels a user is subscribed to.
   */
  async getSubscribedChannelsForUser(userId: number): Promise<ChannelModel[]> {
    logger.silly('ChannelService.getSubscribedChannelsForUser', { userId });

    const channels: ChannelModel[] = await ChannelModel.getSubscribedChannelsForUser(userId);
    return channels;
  }

  /*
   * Get all channels the provided organization owns.
   */
  async getChannelsForOrganization(
    organizationId: number,
    pageable: Pageable,
  ): Promise<IndexablePage<number, ChannelModel>> {
    logger.silly('ChannelService.getChannelsForOrganization', { organizationId, pageable });

    const result = await ChannelModel.getChannelsForOrganizationPaginated(organizationId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Get's all subscriptions for a given organization, paginated.
   * Filter by (subscriptionSource AND (channelVisibility1 OR channelVisibilityN))
   */
  async getSubscriptionsForOrganization(
    organizationId: number,
    pageable: Pageable,
    options: {
      channelVisibilities?: ?ChannelVisibility[],
      subscriptionSource?: ?SubscriptionSources,
    },
  ): Promise<ArrayPage<SubscriptionModel>> {
    logger.silly('ChannelService.getSubscriptionsForOrganization', { organizationId, pageable });
    const result = await SubscriptionModel.getByOrganizationIdPaginated(
      organizationId, pageable, options,
    );
    return new ArrayPage(result.results, result.total, pageable);
  }

  /**
   * Retrieves a subscription by the two properties that make it unique.
   */
  async getSubscription(channelId: number, organizationId: number): Promise<?SubscriptionModel> {
    logger.silly('ChannelService.getSubscription', { organizationId, channelId });
    return SubscriptionModel.getById(channelId, organizationId);
  }

  async getSubscriberOrganizationsForChannel(
    channelId: number,
    pageable: Pageable,
  ): Promise<IndexablePage<number, OrganizationModel>> {
    logger.silly('ChannelService.getSubscriberOrganizationsForChannel', { channelId, pageable });
    const result = await ChannelModel.getSubscriberOrganizationsForChannelPaginated(channelId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Subscribe an org to a channel. Creates the database subscription and
   * indexes all of the channels content for the org.
   * @param  subscriptions  a minimum of channelId and organizationId to create the relationship
   * @param  userId         user performing this action
   * @return                did the subscribe succeed
   */
  async subscribe(subscription: CreateSubscriptionDTO, userId: number): Promise<SubscriptionModel> {
    const { channelId, organizationId } = subscription;
    logger.silly('ChannelService.subscribe', { organizationId, channelId });

    if (!await _isSubscribable(organizationId, channelId)) {
      logger.warn('Channel to subscribe to not found', { userId, organizationId, channelId });
      throw new ResourceNotFoundError(`Channel with id ${channelId} not found`);
    }

    // If visibility wasn't supplied, default it
    const newSubscription = {
      ...subscription,
      visibility: subscription.visibility || SUBSCRIPTION_VISIBILITIES.ADMINS,
    };

    const result: ?SubscriptionModel = await SubscriptionModel.insert(newSubscription);
    this.esGatheringService.indexChannelContents(channelId, organizationId);
    this.eventService.raiseEvent(EventFactory.channelSubscribedEvent(channelId, organizationId, userId));
    logger.info('Organization subscribed to channel', { userId, organizationId, channelId });

    if (!result) {
      logger.warn('Channel subscription failed', { userId, organizationId, channelId });
      throw new BadRequestError('Could not create subscription');
    }

    return result;
  }

  /**
   * Update an orgs subscription to a channel. If the subscription type is 'USERS'
   * then the user subscriptions are also updated, otherwise they are removed.
   */
  async updateSubscription(subscription: Subscription, userId: number): Promise<SubscriptionModel> {
    logger.silly('ChannelService.updateSubscription', { subscription });

    if (!await _isSubscribable(subscription.organizationId, subscription.channelId)) {
      logger.warn('Channel to subscribe to not found', { userId, subscription });
      throw new ResourceNotFoundError(`Channel with id ${subscription.channelId} not found`);
    }

    const updatedSubscription = await transaction(SubscriptionModel,
      async (BoundSubscriptionModel: Class<SubscriptionModel>) => {
        // update direct properties
        const txSubscription: ?SubscriptionModel = await BoundSubscriptionModel.update({
          channelId: subscription.channelId,
          organizationId: subscription.organizationId,
          visibility: subscription.visibility,
        });

        if (!txSubscription) {
          logger.warn('Channel subscription updated failed', { userId, subscription });
          throw new BadRequestError('Could not update subscription');
        }

        // set the updated user subscriptions, start by removing all existing user subs
        await txSubscription.unrelateUsers();
        if (
          subscription.visibility === SUBSCRIPTION_VISIBILITIES.USERS
          && subscription.subscribedUserIds
        ) {
          // the only visibility that can setup specific users is 'USERS'
          const userIds = subscription.subscribedUserIds;
          const count = await txSubscription.relateUsers(userIds);
          if (count < userIds.length) {
            logger.warn(`Not all passed in users were subscribed to channel.
              Are all passed in users members of subscription org?`,
            { userId, subscription, related: count, attempted: userIds.length });
          }
        }

        return BoundSubscriptionModel.getById(subscription.channelId, subscription.organizationId);
      });

    this.eventService.raiseEvent(EventFactory.modifySubscriptionEvent(updatedSubscription, userId));
    logger.info('Organization channel subscription updated', { userId, updatedSubscription });

    return updatedSubscription;
  }

  /**
   * Unsubscribe an org from a channel. Deletes the database connection *but* keeps
   * the content indexed in elasticsearch for the org. There is no need to get rid of
   * the content since all queries for es content are filtered by only the subscribed channel ids.
   * @param  organizationId org unsubscribing from channel
   * @param  channelId      channel being unsubscribed
   * @param  userId         user performing this action
   * @return                did the unsubscribe succeed
   */
  async unsubscribe(organizationId: number, channelId: number, userId: number): Promise<boolean> {
    logger.silly('ChannelService.unsubscribe', { organizationId, channelId });

    if (!await _isUnsubscribable(organizationId, channelId)) {
      logger.warn('Cannot unsubscribe from channel', { userId, organizationId, channelId });
      throw new ResourceNotFoundError(`Cannot unsubscribe from channel ${channelId}`);
    }

    const deleted: boolean = await transaction(SubscriptionModel,
      async (BoundSubscriptionModel: Class<SubscriptionModel>) => {
        await BoundSubscriptionModel.unrelateUsers(channelId, organizationId);
        return BoundSubscriptionModel.delete(organizationId, channelId);
      });

    this.eventService.raiseEvent(EventFactory.channelUnsubscribedEvent(channelId, organizationId, userId));
    logger.info('Organization unsubscribed from channel', { userId, organizationId, channelId });

    return deleted;
  }
}
