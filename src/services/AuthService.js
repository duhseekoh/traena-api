/* @flow */
import 'babel-polyfill';
import type EmailService from '../../src/services/EmailService';
import Auth0Client, { type Auth0Role } from '../client/Auth0Client';
import Logger from '../common/logging/logger';
import RoleModel from '../domain/models/RoleModel';
import { type User } from '../domain/models/UserModel';
import { type Organization } from '../domain/models/OrganizationModel';
import type { AppConfig } from '../common/config';

const logger = new Logger('services/AuthService');

export default class AuthService {
  config: AppConfig;
  emailService: EmailService;
  auth0Client: Auth0Client;

  constructor(config: AppConfig, emailService: EmailService, auth0Client: Auth0Client) {
    this.config = config;
    this.emailService = emailService;
    this.auth0Client = auth0Client;
    return this;
  }

  async getUser(id: string): Promise<?auth0$endUserObject> {
    logger.silly('AuthService.getUser', { userId: id });
    return this.auth0Client.getUser(id);
  }

  async createUser(
    user: User,
    organization: Organization,
    password: ?string,
  ): Promise<auth0$endUserObject> {
    logger.debug('AuthService.createUser', { user, organization });
    const auth0User = await this.auth0Client.createUser(user, organization.auth0OrganizationId, password);

    const changePasswordLink = await this.auth0Client.getChangePasswordLink(user.email);
    if (!password) {
      logger.info('Sending new user email', { userId: user.id, email: user.email, changePasswordLink });
      await this.emailService.sendEmail({
        from: this.config.email.from,
        to: [user.email],
        body: this.emailService.buildWelcomePasswordHtmlBody(changePasswordLink.ticket),
        subject: 'Welcome to Traena! Please set your password.',
      });
    }

    return auth0User;
  }

  async updateUserEmail(newEmail: string, oldEmail: string) {
    logger.debug('AuthService.updateUserEmail', { newEmail, oldEmail });
    const updatedAuth0User = await this.auth0Client.updateUserEmail(newEmail, oldEmail);
    return updatedAuth0User;
  }

  async disableUser(email: string) {
    logger.debug('AuthService.disableUser', { email });
    return this.auth0Client.disableUser(email);
  }

  async enableUser(email: string) {
    logger.debug('AuthService.enableUser', { email });
    return this.auth0Client.enableUser(email);
  }

  async getNewPasswordLink(email: string) {
    logger.silly('AuthService.getNewPassword', { email });
    return this.auth0Client.getChangePasswordLink(email);
  }

  async createOrganization(organization: Organization): Promise<auth0$authorization$group> {
    logger.debug('AuthService.createOrganization', { organization });
    const auth0Description = `${organization.name}_${organization.organizationType}`;
    return this.auth0Client.createGroup(organization.name, auth0Description);
  }

  async disableOrganization(organization: Organization): Promise<void> {
    logger.debug('AuthService.disableOrganization', { organization });
    return this.auth0Client.disableOrganization(organization.auth0OrganizationId);
  }

  async getAssignableRoles(): Promise<RoleModel[]> {
    logger.silly('AuthService.getAssignableRoles');
    return RoleModel.getAssignableRoles();
  }

  /**
   * Retrieve the auth0 roles a user is directly assigned to
   */
  async getUserRoles(email: string): Promise<Auth0Role[]> {
    logger.silly('AuthService.getUserRoles', { email });
    return this.auth0Client.getUserRoles(email);
  }

  async addRolesToUser(email: string, roles: RoleModel[] = []): Promise<*> {
    logger.silly('AuthService.addRolesToUser', { email, roles });
    const auth0RoleIds = roles.map(role => role.auth0RoleId);
    return this.auth0Client.addRolesToUser(email, auth0RoleIds);
  }

  async removeRolesFromUser(email: string, roles: RoleModel[] = []): Promise<*> {
    logger.silly('AuthService.removeRolesFromUser', { email, roles });
    const auth0RoleIds = roles.map(role => role.auth0RoleId);
    return this.auth0Client.removeRolesFromUser(email, auth0RoleIds);
  }

  /**
   * Retrieve the auth0 roles directly assigned to an organization (auth0 group)
   */
  async getOrganizationRoles(auth0OrganizationId: string): Promise<Auth0Role[]> {
    logger.silly('AuthService.getOrganizationRoles', { auth0OrganizationId });
    return this.auth0Client.getOrganizationRoles(auth0OrganizationId);
  }
}
