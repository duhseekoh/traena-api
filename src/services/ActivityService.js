// @flow
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import { transaction } from 'objection';
import Logger from '../common/logging/logger';
// import EventModel, { EventTypes } from '../domain/models/EventModel';
import ActivityModel from '../domain/models/ActivityModel';
import type { ActivityResults, PollChoiceResult } from '../domain/models/ActivityModel';
import ActivityResponseModel from '../domain/models/ActivityResponseModel';
import type { PollResponseCount } from '../domain/models/ActivityResponseModel';
import { ACTIVITY_TYPES } from '../domain/fieldConstants';
import {
  BadRequestError,
  ResourceNotFoundError,
} from '../common/errors';
import { toUTCTimestampString } from '../common/dateUtils';
import * as EventFactory from '../domain/EventFactory';
import type EventService from './EventService';

const logger = new Logger('services/AnalyticsService');
export default class ActivityService {
  eventService: EventService;

  constructor(eventService: EventService) {
    this.eventService = eventService;
  }

  async _updateQuestionSetActivity(activity: ActivityModel) {
    const updatedActivity = await ActivityModel.upsert(activity);
    return updatedActivity;
  }

  async _updateQuestionSetActivityResponse(activityResponse: ActivityResponseModel) {
    const updatedActivityResponse = await ActivityResponseModel.upsert(activityResponse);
    return updatedActivityResponse;
  }

  async _updatePollActivity(activity: ActivityModel) {
    const updatedActivity = await ActivityModel.upsert(activity);
    return updatedActivity;
  }

  async _updatePollActivityResponse(activityResponse: ActivityResponseModel) {
    const updatedActivityResponse = await ActivityResponseModel.upsert(activityResponse);
    return updatedActivityResponse;
  }

  _formatPollResult(results: PollResponseCount[], count: number): {[choiceId: string]: PollChoiceResult} {
    const formattedResult = results.reduce((acc, result) => {
      acc[result.selectedChoiceId] = {
        count: result.count,
        choiceId: result.selectedChoiceId,
        percent: count ? (result.count / count) * 100 : 0,
      };
      return acc;
    }, {});
    return formattedResult;
  }

  async create(activity: ActivityModel, userId: number) {
    logger.silly('ActivityService.create', { activity, userId });
    const createdActivity = await ActivityModel.upsert(activity);
    this.eventService.raiseEvent(EventFactory.createActivityEvent(createdActivity, userId));
    return createdActivity;
  }

  async get(activityId: number) {
    logger.silly('ActivityService.get', { activityId });
    return ActivityModel.getById(activityId);
  }

  async update(activity: ActivityModel, userId: number) {
    logger.silly('ActivityService.update', { activity, userId });

    /**
    * Set non-editable properties based on the existing activity
    */
    const existingActivity = await ActivityModel.getById(activity.id);
    if (!existingActivity) {
      throw new ResourceNotFoundError('Content Not Found');
    }

    activity.version = existingActivity.version;
    activity.id = existingActivity.id;
    activity.contentId = existingActivity.contentId;
    activity.createdTimestamp = toUTCTimestampString(existingActivity.createdTimestamp);
    activity.modifiedTimestamp = toUTCTimestampString(existingActivity.modifiedTimestamp);

    if (activity.type === ACTIVITY_TYPES.QUESTION_SET) {
      const updatedActivity = await this._updateQuestionSetActivity(activity);
      this.eventService.raiseEvent(EventFactory.modifyActivityEvent(updatedActivity, userId));
      return updatedActivity;
    } else if (activity.type === ACTIVITY_TYPES.POLL) {
      const updatedActivity = await this._updatePollActivity(activity);
      this.eventService.raiseEvent(EventFactory.modifyActivityEvent(updatedActivity, userId));
      return updatedActivity;
    }

    throw new BadRequestError('Activity type is not supported');
  }

  async delete(activityId: number, userId: number) {
    logger.silly('ActivityService.delete', { activityId, userId });
    const activity = await this.get(activityId);
    const deleted = await transaction(
      ActivityModel,
      ActivityResponseModel,
      async (BoundActivityModel, BoundActivityResponseModel) => {
        await BoundActivityResponseModel.deleteByActivityId(activityId);
        await BoundActivityModel.delete(activityId);
        return true;
      },
    );
    if (activity) { // should never get to this point if its already deleted, but flow
      this.eventService.raiseEvent(EventFactory.deleteActivityEvent(activity, userId));
    }
    return deleted;
  }

  async getActivitiesForContentId(contentId: number, pageable: Pageable) {
    logger.silly('ActivityService.getActivitiesForContentId', { contentId });
    const result = await ActivityModel.getAllByContentId(contentId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  async createActivityResponse(activityResponse: ActivityResponseModel) {
    logger.silly('ActivityService.createActivityResponse', { activityResponse });
    const createdActivityResponse = await ActivityResponseModel.upsert(activityResponse);
    this.eventService.raiseEvent(EventFactory.createActivityResponseEvent(createdActivityResponse));
    return createdActivityResponse;
  }

  async getActivityResponse(activityResponseId: number) {
    logger.silly('ActivityService.getActivityResponse', { activityResponseId });
    return ActivityResponseModel.getById(activityResponseId);
  }

  async updateActivityResponse(activityId: number, activityResponse: ActivityResponseModel) {
    logger.silly('ActivityService.updateActivityResponse', { activityId, activityResponse });
    const activity = await ActivityModel.getById(activityId);
    if (activity && activity.type === ACTIVITY_TYPES.QUESTION_SET) {
      const updatedActivityResponse = await this._updateQuestionSetActivityResponse(activityResponse);
      this.eventService.raiseEvent(EventFactory.modifyActivityResponseEvent(updatedActivityResponse));
      return updatedActivityResponse;
    } else if (activity && activity.type === ACTIVITY_TYPES.POLL) {
      const updatedActivityResponse = await this._updatePollActivityResponse(activityResponse);
      this.eventService.raiseEvent(EventFactory.modifyActivityResponseEvent(updatedActivityResponse));
      return updatedActivityResponse;
    }

    throw new BadRequestError('Activity type is not supported');
  }

  async getRecentActivityResponseForActivity(userId: number, activityId: number, pageable: Pageable) {
    logger.silly('ActivityService.getRecentActivityResponseForActivity', { activityId });
    const result = await ActivityResponseModel.getByActivityId(userId, activityId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  async getActivityResults(activityId: number): Promise<ActivityResults> {
    logger.silly('ActivityService.getActivityResults', { activityId });
    const activity = await ActivityModel.getById(activityId);
    if (activity && activity.type === ACTIVITY_TYPES.POLL) {
      const pollResults = await ActivityResponseModel.getPollResults(activityId);
      const count = pollResults ? pollResults.reduce((acc, c) => acc + c.count, 0) : 0;
      const choiceResults = this._formatPollResult(pollResults, count);
      return {
        activity,
        count,
        choiceResults,
      };
    }
    throw new BadRequestError('Activity type is not supported');
  }
}
