// @flow

import _ from 'lodash';
import type moment$Moment from 'moment';
import Logger from '../common/logging/logger';
import TaskModel from '../domain/models/TaskModel';
import UserModel from '../domain/models/UserModel';
import ContentStatus from '../domain/ContentStatus';
import { TRAENA_TASK_STATUSES } from '../domain/fieldConstants';
import type ContentService from './ContentService';
import EventService from './EventService';
import * as EventFactory from '../domain/EventFactory';
import type ElasticSearchGatheringService from './ElasticSearchGatheringService';


const logger = new Logger('services/TaskService');

export default class TaskService {
  contentService: ContentService;
  eventService: EventService;
  elasticSearchGatheringService: ElasticSearchGatheringService;

  constructor(
    contentService: ContentService, eventService: EventService,
    elasticSearchGatheringService: ElasticSearchGatheringService,
  ) {
    this.contentService = contentService;
    this.eventService = eventService;
    this.elasticSearchGatheringService = elasticSearchGatheringService;
  }

  /**
   * Returns a list containing two lists of content ids, one for which the user has incomplete tasks,
   * the other for which the user has completed tasks
   * @param userId
   * @returns {Promise.<ContentStatus>}
   */
  async getContentStatuses(userId: number): Promise<ContentStatus> {
    logger.silly('TaskService.getContentStatuses', { userId });

    const [incomplete, complete] =
      await Promise.all([TaskModel.findIncompleteForUser(userId), TaskModel.findCompleteForUser(userId)]);

    return new ContentStatus(incomplete.map(item => item.contentId), _.uniq(complete.map(item => item.contentId)));
  }

  /**
   * Returns a list of content ids for which the user has completed tasks
   * @param userId
   * @returns {Promise.<number[]>}
   */
  async getCompletedTaskContentIds(userId: number, maxDate?: moment$Moment): Promise<number[]> {
    logger.silly('TaskService.getCompletedTaskContentIds', { userId, maxDate });

    const complete = await TaskModel.findCompleteForUser(userId, maxDate);

    return _.uniq(complete.map(item => item.contentId));
  }

  async getTasksForContentId(userId: number, contentId: number) {
    logger.silly('TaskService.getTasksForContentId', { userId, contentId });
    return TaskModel.findByUserAndContentIds(userId, [contentId]);
  }

  async get(taskId: number): Promise<TaskModel> {
    logger.silly('TaskService.get', { taskId });

    return TaskModel.get(taskId);
  }

  /**
   * Add a new traena list item, given a content id to associate with
   */
  async create(userId: number, contentId: number): Promise<TaskModel> {
    logger.debug('TaskService.create', { userId, contentId });
    const alreadyActiveItem = await TaskModel.getItem(userId, contentId, TRAENA_TASK_STATUSES.INCOMPLETE);
    if (alreadyActiveItem) {
      logger.warn('Content association already exists, returning existing', { userId, contentId, completed: false });
      return alreadyActiveItem;
    }

    const result = await TaskModel.create(userId, contentId, false);
    this.eventService.raiseEvent(EventFactory.createTaskEvent(result));
    return result;
  }

  /**
   * Add a new COMPLETED traena list item, given a content id to associate with
   */
  async createCompleted(userId: number, contentId: number): Promise<TaskModel> {
    logger.debug('TaskService.create', { userId, contentId });
    const alreadyActiveItem = await TaskModel.getItem(userId, contentId, TRAENA_TASK_STATUSES.COMPLETE);
    if (alreadyActiveItem) {
      logger.warn('Content association already exists, returning existing', { userId, contentId, completed: true });
      return alreadyActiveItem;
    }

    const result = await TaskModel.create(userId, contentId, true);
    this.eventService.raiseEvent(EventFactory.createTaskEvent(result));
    this.eventService.raiseEvent(EventFactory.completeTaskEvent(result));

    return result;
  }

  /**
   * Update an existing traena list item. This will happen when the status changes,
   * or a rating is given.
   * This will not change the user or content id associations.
   * TODO: Maybe -Add guard for not allowing multiple incomplete statuses on single piece of content
   */
  async update(userId: number, task: TaskModel): Promise<TaskModel> {
    logger.silly('TaskService.update', { userId, task });
    logger.debug('updating traena list item', { userId, task: task && task.id });

    const existingTask = await TaskModel.getById(task.id);

    const result = TaskModel.update(task);

    if (existingTask.status !== TRAENA_TASK_STATUSES.COMPLETE && task.status === TRAENA_TASK_STATUSES.COMPLETE) {
      const user = await UserModel.getUserById(userId);
      this.elasticSearchGatheringService.indexContentItem(task.contentId, user.organizationId);
      this.eventService.raiseEvent(EventFactory.completeTaskEvent(task));
    } else {
      this.eventService.raiseEvent(EventFactory.updateTaskEvent(task));
    }

    return result;
  }

  /**
   * Delete an traena list item
   */
  async deleteTask(userId: number, taskId: number): Promise<boolean> {
    logger.debug('TaskService.deleteTask', { userId, taskId });

    const result = await TaskModel.deleteTask(taskId);
    this.eventService.raiseEvent(EventFactory.deleteTaskEvent(taskId, userId));
    return result;
  }
}
