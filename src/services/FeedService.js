// @flow
import moment from 'moment';
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import type ElasticSearchService from './ElasticSearchService';
import type ContentService from './ContentService';
import type ContentItemModel from '../domain/models/ContentItemModel';
import UserModel from '../domain/models/UserModel';
import type EventService from './EventService';
import type TaskService from './TaskService';
import { feedLoadedEvent, whatsNewLoadedEvent } from '../domain/EventFactory';
import { EventTypes } from '../domain/models/EventModel';

const logger = new Logger('services/FeedService');

export default class FeedService {
  contentService: ContentService;
  elasticSearchService: ElasticSearchService;
  eventService: EventService;
  taskService: TaskService;

  constructor(
    contentService: ContentService, elasticSearchService: ElasticSearchService,
    eventService: EventService, taskService: TaskService,
  ) {
    this.contentService = contentService;
    this.elasticSearchService = elasticSearchService;
    this.eventService = eventService;
    this.taskService = taskService;
  }

  async getFeedForUser(user: UserModel, pageable: Pageable): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('FeedService.getFeedForUser. ', { userId: user.id, pageable });

    const channels = await user.getSubscribedChannels();
    const channelIds = channels.map(channel => channel.id);
    const feed = await this.elasticSearchService.getFeed(user.id, user.tags, user.organizationId, channelIds, pageable);
    const totalElements = feed.hits.total;
    const contentItems = feed.hits.hits.map(item => item._source);
    this.eventService.raiseEvent(feedLoadedEvent(user.id, pageable.page, contentItems.map(item => item.id)));

    return new IndexablePage(contentItems, totalElements, pageable);
  }

  /**
  * Given a userId, this method returns a date before which content items do not qualify as "Whats New" content
  * for that user.  This date is passed as a `gte` filter to the elasticsearch query for Whats New content.
  * @param userId id of the user for which we want the expiration date for "Whats New" content
  * @param expirationLength a number of days ago that "Whats New" content can never be older than
  */
  async getWhatsNewContentExpiryForUser(userId: number, expirationLength: number): Promise<moment> {
    let expiryDate = moment().subtract(expirationLength, 'days');
    const latestAppLaunchEvent = await this.eventService.getLatestEvent(userId, EventTypes.APP_LAUNCHED);

    if (latestAppLaunchEvent) {
      const userLastAppLaunchDate = latestAppLaunchEvent.data.timestamp;
      const adjustedDate = moment(userLastAppLaunchDate, 'YYYY-MM-DD').subtract(5, 'days');
      const daysAgo = moment().diff(adjustedDate, 'days');

      if (daysAgo < expirationLength) {
        expiryDate = adjustedDate;
      }
    }
    return expiryDate;
  }

  async getWhatsNewForUser(user: UserModel, pageable: Pageable): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('FeedService.getWhatsNewForUser. ', { userId: user.id, pageable });

    const whatsNewExpiration = 30;
    const publishedAfter = await this.getWhatsNewContentExpiryForUser(user.id, 30);

    const userCompletedContentIds = await this.taskService.getCompletedTaskContentIds(
      user.id,
      moment().subtract(1, 'day'),
    );
    const userViewedContentIds = await this.eventService.getViewedContentIds(
      user.id,
      moment().subtract(whatsNewExpiration, 'days'),
      moment().subtract(1, 'day'),
    );
    const excludedContentIds = [
      ...userCompletedContentIds,
      ...userViewedContentIds,
    ];

    const channels = await user.getSubscribedChannels();
    const channelIds = channels.map(channel => channel.id);

    const whatsNew = await this.elasticSearchService.getWhatsNew(
      user.id,
      user.organizationId,
      channelIds,
      publishedAfter,
      excludedContentIds,
    );
    const totalElements = whatsNew.hits.total;
    const contentItems = whatsNew.hits.hits.map(item => item._source);
    this.eventService.raiseEvent(whatsNewLoadedEvent(user.id, pageable.page, contentItems.map(item => item.id)));

    return new IndexablePage(contentItems, totalElements, pageable);
  }
}
