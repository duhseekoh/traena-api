// @flow
import moment from 'moment';
import type moment$Moment from 'moment';
import esBuilder from 'elastic-builder';

import { ArrayPage, IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import type UserModel from '../domain/models/UserModel';
import type { ContentItemStatus, ContentIncludeSource } from '../domain/fieldConstants';
import { CONTENT_ITEM_STATUSES, CONTENT_INCLUDE_SOURCES } from '../domain/fieldConstants';
import type ContentItemModel from '../domain/models/ContentItemModel';
import * as stringUtils from '../common/stringUtils';
import * as queryUtils from '../common/queryUtils';

const logger = new Logger('services/ElasticSearchService');

const CONTENT_ID_FILTER = 'id';
const SUBSCRIBER_ORG_FILTER = 'subscriberOrganizationId';
const CONTENT_CHANNEL_FILTER = 'channel.id';
const CONTENT_STATUS_FILTER = 'status.keyword';

type expFunction = { exp: { createdTimestamp: { scale: string } } };
type randomFunction = { random_score: { seed: string } };
type ESTermsBucket = { doc_count: number, key: string };

type searchQuery = {
  index: string,
  from?: ?number,
  size?: ?number,
  body: {
    _source?: string | {},
    query: {
      query_string?: {
        query: string,
        fields: Array<string>,
      },
      function_score?: {
        query: {
          bool?: {
            must: { match_all: {} },
            filter: { term: {} },
          }
        },
        functions: Array<expFunction | randomFunction>,
      },
    },
  },
};

type elasticSearchResponse = {
  took: number,
  timed_out: string,
  aggregations?: {
    tags?: {
      doc_count_error_upper_bound: number,
      sum_other_doc_count: number,
      buckets: Array<{
        key: string,
        doc_count: number,
      }>,
    }
  },
  hits: {
    total: number,
    max_score: number,
    hits: Array<{
      _index: string,
      _type: string,
      _id: string,
      _score: number,
      _source: *,
    }>
  }
}

type awsElasticSearchClient = {
  search(query: searchQuery): Promise<elasticSearchResponse>;
}

function esRequest(index: string, from: ?number, size: ?number, body) {
  return {
    index,
    from,
    size,
    body,
  };
}

function subscriberOrgFilter(subscriberOrgId: number) {
  return esBuilder.termQuery(SUBSCRIBER_ORG_FILTER, subscriberOrgId);
}

function contentStatusFilter(status: ContentItemStatus) {
  return esBuilder.termQuery(CONTENT_STATUS_FILTER, status);
}

function contentStatusesFilter(statuses: ContentItemStatus[]) {
  return esBuilder.termsQuery(CONTENT_STATUS_FILTER, statuses);
}

function contentChannelsFilter(channelIds: number[]) {
  return esBuilder.termsQuery(CONTENT_CHANNEL_FILTER, channelIds);
}

export default class ElasticSearchService {
  client: awsElasticSearchClient;

  constructor(elasticSearchClient: awsElasticSearchClient) {
    this.client = elasticSearchClient;
  }

  async getUsersForOrg(orgId: number, pageable: Pageable): Promise<IndexablePage<number, UserModel>> {
    logger.silly('ElasticSearchService.getUsersForOrg');
    const filterQuery = esBuilder.boolQuery()
      .must(esBuilder.matchAllQuery())
      .filter(esBuilder.termQuery('organizationId', orgId));

    const searchBody = esBuilder.requestBodySearch()
      .query(filterQuery);

    if (pageable.sort) {
      // In order for ES to sort by a text-field property, `.keyword` must be appended
      // to the property for which we are try to sort.  i.e. `body.title` => `body.title.keyword`
      const textProperties = [
        'firstName',
        'email',
        'position',
      ];
      const formattedOrders = queryUtils.formatSortForElasticsearch(pageable.sort.orders, textProperties);
      formattedOrders.forEach((it) => {
        searchBody.sort(esBuilder.sort(it.property, it.direction));
      });
    } else {
      searchBody.sort(esBuilder.sort('firstName.keyword', 'asc'));
    }

    const searchRequest = esRequest('users', pageable.page * pageable.size, pageable.size, searchBody.toJSON());
    logger.debug('Search Request: ', searchRequest);

    const results = await this.client.search(searchRequest);

    logger.silly('getUsersForOrg query results: ', { results: JSON.stringify(results) });

    const totalResultsCount = results.hits.total;
    const userItems: UserModel[] = results.hits.hits.map(hit => hit._source);
    return new IndexablePage(userItems, totalResultsCount, pageable);
  }

  /**
   * TODO - Switch this over to use elasticsearch suggester:
   *  https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-completion.html
   */
  async searchUsers(orgId: number, searchText: string): Promise<UserModel[]> {
    logger.silly('ElasticSearchService.searchUsers', { orgId, searchText });
    const searchTextWithWildcards = stringUtils.stringToWildcardString(searchText);

    const filterQuery = esBuilder.boolQuery()
      .should(esBuilder.queryStringQuery(searchTextWithWildcards)
        .defaultOperator('AND')
        .fields(['firstName', 'lastName'])
        .fuzziness(3))
      .minimumShouldMatch(1)
      .filter(esBuilder.termQuery('organizationId', orgId));

    const searchBody = esBuilder.requestBodySearch().query(filterQuery);
    const searchRequest = esRequest('users', undefined, undefined, searchBody.toJSON());
    const results = await this.client.search(searchRequest);
    return results.hits.hits.map(hit => hit._source);
  }

  async searchContent(
    orgId: number, channelIds: number[], searchText: ?string,
    tags: ?string[], pageable: Pageable,
  ) {
    logger.silly('ElasticSearchService.searchContent', { orgId, searchText, pageable });

    const searchFields =
      ['tags.text^2', 'body.title^2', 'body.text', 'author.firstName', 'author.lastName', 'author.organization.name'];

    const filterQuery = esBuilder.boolQuery()
      .filter(contentStatusFilter(CONTENT_ITEM_STATUSES.PUBLISHED))
      .filter(contentChannelsFilter(channelIds))
      .filter(subscriberOrgFilter(orgId));

    if (searchText) {
      filterQuery.must(esBuilder.multiMatchQuery(
        searchFields,
        searchText,
      ));
    }

    if (tags) {
      filterQuery.must(esBuilder.termsQuery('tags.text.keyword', tags));
    }

    const searchBody = esBuilder.requestBodySearch().query(filterQuery);
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        searchBody.sort(esBuilder.sort(it.property, it.direction));
      });
    }

    const searchRequest = esRequest('contents', pageable.page * pageable.size, pageable.size, searchBody.toJSON());
    logger.debug('Search Request: ', searchRequest);
    return this.client.search(searchRequest);
  }

  async getAllContent(
    contentOrgId: number, channelIds: number[], include: ContentIncludeSource = 'all',
    pageable: Pageable, includeStatuses: ?ContentItemStatus[] = [
      CONTENT_ITEM_STATUSES.PUBLISHED,
      CONTENT_ITEM_STATUSES.DRAFT], userId: number, searchText: ?string,
  ): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('ElasticSearchService.getAllContent', { contentOrgId, pageable });
    const filterQuery = esBuilder.boolQuery()
      .filter(contentChannelsFilter(channelIds))
      .filter(subscriberOrgFilter(contentOrgId));
    const searchBody = esBuilder.requestBodySearch().query(filterQuery);

    // if only searching for internal or external, filter appropriately
    if (include.toLowerCase() === CONTENT_INCLUDE_SOURCES.INTERNAL) {
      filterQuery.must(esBuilder.termQuery('authorOrganizationId', contentOrgId));
    } else if (include.toLowerCase() === CONTENT_INCLUDE_SOURCES.EXTERNAL) {
      filterQuery.mustNot(esBuilder.termQuery('authorOrganizationId', contentOrgId));
    } else if (include.toLowerCase() === CONTENT_INCLUDE_SOURCES.SELF && userId) {
      filterQuery.must(esBuilder.termQuery('authorId', userId));
    }

    // filter to the content with appropiate statuses
    if (includeStatuses) {
      filterQuery.must(contentStatusesFilter(includeStatuses));
    }

    if (searchText) {
      filterQuery.must(esBuilder.multiMatchQuery([
        'body.title',
        'channel.name',
        'author.firstName',
        'author.lastName',
      ], searchText).type('phrase_prefix').slop(2));
    }

    if (pageable.sort) {
      // In order for ES to sort by a text-field property, `.keyword` must be appended
      // to the property for which we are try to sort.  i.e. `body.title` => `body.title.keyword`
      const textProperties = [
        'body.title',
        'author.firstName',
        'body.type',
        'channel.name',
      ];
      const formattedOrders = queryUtils.formatSortForElasticsearch(pageable.sort.orders, textProperties);
      formattedOrders.forEach((it) => {
        searchBody.sort(esBuilder.sort(it.property, it.direction));
      });
    } else {
      searchBody.sort(esBuilder.sort('createdTimestamp', 'asc'));
    }

    const searchRequest = esRequest('contents', pageable.page * pageable.size, pageable.size, searchBody.toJSON());
    logger.debug('Search Request: ', searchRequest);

    const results = await this.client.search(searchRequest);
    const totalResultsCount = results.hits.total;
    const contentItems: ContentItemModel[] = results.hits.hits.map(hit => hit._source);
    return new IndexablePage(contentItems, totalResultsCount, pageable);
  }

  async getFeed(userId: number, tags: string[] = [], orgId: number, channelIds: number[], pageable: Pageable) {
    logger.silly('ElasticSearchService.getFeed', {
      userId, tags, orgId, pageable,
    });

    const seed = `${userId}-${moment().format('YYYY-MM-DD')}`;
    /**
    * If a user has many tags associated with their account, the weight of a given tag match diminishes
    * The function we use for the weight has a diminishing rate of decrease, for example:
    * 1 tags => .95 weight
    * 3 tags => .86 weight
    * 5 tags => .78 weight
    * 7 tags => .70 weight
    * 9 tags => .63 weight
    * 15 tag => .47 weight
    * 20 tag => .36 weight
    */
    let tagScoreFunctions = [];
    if (tags) {
      const tagMatchWeight = 1.01 ** (-5 * tags.length);
      tagScoreFunctions = tags.map(tag =>
        esBuilder.weightScoreFunction(tagMatchWeight)
          .filter(esBuilder.termQuery('tags.text', tag)));
    }

    const searchBody = esBuilder.requestBodySearch()
      .query(esBuilder.functionScoreQuery()
        .functions([
          ...tagScoreFunctions,
          esBuilder.decayScoreFunction('exp', 'publishedTimestamp').scale('3d').weight(2),
          esBuilder.randomScoreFunction().seed(seed).weight(2),
          esBuilder.fieldValueFactorFunction('likeCount')
            .factor(1)
            .modifier('ln2p')
            .missing(1),
          esBuilder.fieldValueFactorFunction('commentCount')
            .factor(1)
            .modifier('ln2p')
            .missing(1),
        ]).scoreMode('sum')
        .query(esBuilder.boolQuery()
          .should(esBuilder.matchAllQuery())
          .filter(contentStatusFilter(CONTENT_ITEM_STATUSES.PUBLISHED))
          .filter(contentChannelsFilter(channelIds))
          .filter(subscriberOrgFilter(orgId))));

    const searchRequest = esRequest('contents', pageable.page * pageable.size, pageable.size, searchBody.toJSON());
    logger.debug('Search Request: ', searchRequest);
    const result = await this.client.search(searchRequest);
    return result;
  }

  async getWhatsNew(
    userId: number,
    orgId: number,
    channelIds: number[],
    publishedAfter: moment$Moment,
    excludeContentIds: number[],
  ) {
    logger.silly('ElasticSearchService.getWhatsNew', {
      userId, publishedAfter, excludeContentIds,
    });
    const searchBody = esBuilder.requestBodySearch()
      .query(esBuilder.boolQuery()
        .mustNot(esBuilder.termsQuery(CONTENT_ID_FILTER, excludeContentIds))
        .should(esBuilder.matchAllQuery())
        .filter(contentStatusFilter(CONTENT_ITEM_STATUSES.PUBLISHED))
        .filter(contentChannelsFilter(channelIds))
        .filter(subscriberOrgFilter(orgId))
        .filter(esBuilder.rangeQuery('publishedTimestamp').gte(publishedAfter.format('YYYY-MM-DD'))));

    // page and size are undefined
    const searchRequest = esRequest('contents', undefined, undefined, searchBody.toJSON());
    logger.debug('Search Request: ', searchRequest);
    const result = await this.client.search(searchRequest);
    return result;
  }

  buildTagsQuery(channelIds: number[], orgId: number, publishedAfter?: moment$Moment) {
    const result = esBuilder.boolQuery()
      .must(esBuilder.matchAllQuery())
      .filter(contentStatusFilter(CONTENT_ITEM_STATUSES.PUBLISHED))
      .filter(contentChannelsFilter(channelIds))
      .filter(subscriberOrgFilter(orgId));

    if (publishedAfter) {
      result.filter(esBuilder.rangeQuery('publishedTimestamp').gte(publishedAfter.format('YYYY-MM-DD')));
    }
    return result;
  }

  /**
   * Retrieve tags from available content narrowed down by the subscribing org
   * and channels specified.
   * @param  orgId      filter to an orgs content to get tags from
   * @param  channelIds filter to channel content to get tags from
   * @param  pageable
   * @return            es aggregation bucket with tag text and count
   */
  async getTagsFromContent(
    orgId: number, channelIds: number[],
    pageable: Pageable, publishedAfter?: moment$Moment,
  ): Promise<ArrayPage<ESTermsBucket>> {
    const aggregation = esBuilder.termsAggregation('tags', 'tags.text.keyword');
    if (pageable.sort) {
      pageable.sort.orders.forEach((it) => {
        aggregation.order(it.property, it.direction);
      });
    }
    const searchBody = esBuilder.requestBodySearch()
      .query(this.buildTagsQuery(channelIds, orgId, publishedAfter))
      .agg(aggregation.size(pageable.size));
    const searchRequest = esRequest('contents', undefined, 0, searchBody.toJSON());
    const results = await this.client.search(searchRequest);
    if (!results.aggregations || !results.aggregations.tags) {
      return new ArrayPage([], 0, pageable);
    }

    const totalResultsCount = results.aggregations.tags.buckets.length;
    const tags = results.aggregations.tags.buckets;
    return new ArrayPage(tags, totalResultsCount, pageable);
  }
}
