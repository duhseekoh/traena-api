// @flow
import _ from 'lodash';
import SNS from 'aws-sdk/clients/sns';
import type moment$Moment from 'moment';
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import NotificationModel from '../domain/models/NotificationModel';
import UserModel from '../domain/models/UserModel';
import PushDeviceModel from '../domain/models/PushDeviceModel';
import type { NotificationStatus } from '../domain/fieldConstants';
import { NOTIFICATION_STATUSES, NOTIFICATION_TYPES } from '../domain/fieldConstants';
import type { AppConfig } from '../common/config';

const logger = new Logger('services/NotificationService');

export default class NotificationService {
  snsClient: SNS;
  config: AppConfig;

  constructor(config: AppConfig, snsClient: SNS) {
    this.snsClient = snsClient;
    this.config = config;
  }

  async getNotificationsForUser(userId: number, maxDate?: moment$Moment, pageable: Pageable) {
    logger.silly('NotificationService.getNotificationsForUser', { userId, maxDate });

    const notifications = await NotificationModel.getNotificationsForUser(userId, maxDate, pageable);
    return new IndexablePage(notifications.results, notifications.total, pageable);
  }

  async addCommentTaggedNotifications(
    fromUserId: number, toUserIds: number[], contentId: number, commentId: number,
    status: NotificationStatus = NOTIFICATION_STATUSES.READY_TO_SEND,
  ) {
    logger.silly('NotificationService.addCommentTaggedNotifications', {
      fromUserId, toUserIds, contentId, status,
    });
    logger.debug(
      'adding notifications for being tagged in comment',
      {
        fromUserId, toUserIds, contentId, commentId,
      },
    );
    const notifications = toUserIds.map(toUserId => ({
      contentId,
      toUserId,
      commentId,
      fromUserId,
      status,
      notificationType: NOTIFICATION_TYPES.TAG_IN_COMMENT,
    }));
    await NotificationModel.insertNotifications(notifications);

    await this._sendSNSNotificationsToUsers(toUserIds);
  }

  async addCommentNotification(
    fromUserId: number, toUserId: number, contentId: number, commentId: number,
    status: NotificationStatus = NOTIFICATION_STATUSES.READY_TO_SEND,
  ) {
    logger.silly('NotificationService.addCommentNotification', {
      fromUserId, toUserId, contentId, status,
    });
    logger.debug(
      'adding notifications for post being commented on',
      {
        fromUserId, toUserId, contentId, commentId,
      },
    );
    const notifications = [{
      contentId,
      toUserId,
      commentId,
      fromUserId,
      status,
      notificationType: NOTIFICATION_TYPES.COMMENT_ON_POST,
    }];
    await NotificationModel.insertNotifications(notifications);

    await this._sendSNSNotificationsToUser(toUserId);
  }

  async addLikeNotification(
    fromUserId: number, toUserId: number, contentId: number,
    status: NotificationStatus = NOTIFICATION_STATUSES.READY_TO_SEND,
  ) {
    logger.silly('NotificationService.addLikeNotification', {
      fromUserId, toUserId, contentId, status,
    });
    logger.debug(
      'adding notifications someone liking yout post',
      { fromUserId, toUserId, contentId },
    );
    const notifications = [{
      contentId,
      toUserId,
      fromUserId,
      status,
      notificationType: NOTIFICATION_TYPES.LIKE_ON_POST,
    }];
    await NotificationModel.insertNotifications(notifications);
    await this._sendSNSNotificationsToUser(toUserId);
  }

  async acknowledgeNotificationsForUser(userId: number) {
    logger.silly('NotificationService.acknowledgeNotificationsForUser', { userId });
    logger.debug('Acknowledging notifications', { userId });
    return NotificationModel.updateAllToAcknowledged(userId);
  }

  async getUnacknowledgedNotificationCount(userId: number) {
    logger.silly('NotificationService.getUnacknowledgedNotificationCount', { userId });
    return NotificationModel.getUnacknowledgedNotificationCount(userId);
  }

  // ---------
  // SENDING NOTIFICATIONS VIA SNS
  // ---------

  async _sendSNSNotificationsToUsers(userIds: number[]) {
    logger.debug('NotificationService._sendSNSNotificationsToUsers', { userIds });

    if (!userIds || userIds.length === 0) {
      return false;
    }

    return userIds.map(userId => this._sendSNSNotificationsToUser(userId));
  }

  async _sendSNSNotificationsToUser(userId: number) {
    logger.silly('NotificationService._sendSNSNotificationsToUser', { userId });

    const notificationsToSend = await NotificationModel.getUnsentNotificationsForUser(userId);
    const unreadNotificationCount = await NotificationModel.getUnacknowledgedNotificationCount(userId);
    logger.debug('Sending user notifications', { userId, count: notificationsToSend.length });

    const promisesNested = notificationsToSend.map((notification) => {
      // Go through all devices of the user this notification is targeted for
      // If there are no devices, thats ok, we'll just mark it as sent anyways.
      const devicePromises = notification.pushDevices.map((pushDevice) => {
        const {
          notificationType, from, comment, content,
        } = notification;
        let notificationMessage = 'You have a new notification in Traena.';

        const truncatedTitle = _.truncate(content.body.title.trim(), { length: 30 });
        let truncatedComment;
        if (comment) {
          truncatedComment = _.truncate(comment.text, { length: 70 });
        }

        switch (notificationType) {
          case NOTIFICATION_TYPES.TAG_IN_COMMENT:
            if (truncatedComment && truncatedTitle) {
              notificationMessage = `${from.firstName} tagged you on "${truncatedTitle}" : "${truncatedComment}"`;
            }
            break;
          case NOTIFICATION_TYPES.LIKE_ON_POST:
            if (truncatedTitle) {
              notificationMessage = `${from.firstName} likes your post "${truncatedTitle}"!`;
            }
            break;
          case NOTIFICATION_TYPES.COMMENT_ON_POST:
            if (truncatedComment && truncatedTitle) {
              notificationMessage = `${from.firstName} : "${truncatedComment}" on your post "${truncatedTitle}"`;
            }
            break;
          default:
            break;
        }
        return this._sendSNSNotification(
          pushDevice.endpointArn, notificationMessage, {},
          unreadNotificationCount,
        );
      });

      return Promise.all(devicePromises)
        .catch((err) => {
          logger.warn(
            'Error sending notification for at least one device. Oh well. Continue with err:',
            { err, notification },
          );
        })
        .then(() => {
          logger.debug(
            'Updating notification status to SENT',
            { userId, notificationId: notification.id, deviceCount: notification.pushDevices.length },
          );
          return notification.$query()
            .patch({
              status: NOTIFICATION_STATUSES.SENT,
            });
        });
    });

    // All of the notifications... sent.
    return Promise.all(_.flatten(promisesNested));
  }

  async _sendSNSNotification(endpointArn: string, message: string, payload: *, badgeNumber: *) {
    logger.silly('NotificationService._sendSNSNotification', { message, endpointArn, badgeNumber });

    return this.snsClient.publish({
      TargetArn: endpointArn,
      MessageStructure: 'json',
      Message: JSON.stringify({
        default: `${message}`,
        APNS_SANDBOX: JSON.stringify({
          aps: {
            alert: `${message}`,
            badge: badgeNumber,
          },
          payload,
        }),
        APNS: JSON.stringify({
          aps: {
            alert: `${message}`,
            badge: badgeNumber,
          },
          payload,
        }),
        GCM: JSON.stringify({
          data: {
            message: `${message}`,
            badge: badgeNumber,
          },
          payload,
        }),
      }),
    }).promise().then(() => {
      logger.debug('Successfully sent SNS notification', { endpointArn, message, badgeNumber });
    })
      .catch((err) => {
        logger.error('Failed to send SNS notification', {
          endpointArn, message, err, badgeNumber,
        });
        throw err;
      });
  }

  // ---------
  // DEVICE REGISTRATION (SNS and DB)
  //
  // TODO - This entire section could be moved to an SNS device registration service
  // ---------

  /**
   * Register a device to a user for push notifications.
   * Sets up the platform endpoint in SNS.
   * Also saves the deviceToken:endpointArn combo on the traena user in the db.
   * Follows the pseudo-code flow described at:
   *  http://docs.aws.amazon.com/sns/latest/dg/mobile-platform-endpoint.html
   */
  async registerDeviceToken(userId: number, deviceToken: string, platform: string) {
    logger.silly('NotificationService.registerDeviceToken', { userId, deviceToken, platform });

    if (!deviceToken || !platform) {
      logger.error(
        'Must specify a device token and platform when registering a device token',
        { userId, deviceToken, platform },
      );
      throw Error('Must specify a device token and platform when registering a device token');
    }

    const user = await UserModel.getUserById(userId);
    const pushDevice = await PushDeviceModel.getPushDeviceByDeviceToken(deviceToken);
    // check to see if deviceToken / endpointArn exists on user
    if (!pushDevice) {
      logger.debug('No existing push device, create platform endpoint', { user, deviceToken });
      await this._createAndSavePlatformEndpoint(user, deviceToken, platform);
    }

    // reconcile attributes
    return this._reconcileEndpointAttributes(userId, deviceToken, platform);
  }

  /**
   * Given a device token...
   * 1. Delete the associated endpoint arn in SNS
   * 2. Delete it from the db
   */
  async deregisterDeviceToken(deviceToken: string) {
    logger.silly('NotificationService.registerDeviceToken', { deviceToken });

    // check to see if deviceToken+endpointArn exists in db
    const pushDevice = await PushDeviceModel.getPushDeviceByDeviceToken(deviceToken);
    if (!pushDevice) {
      logger.warn('No existing endpoint for device token, cannot delete');
      return;
    }
    // Delete from SNS
    try {
      await this.snsClient.deleteEndpoint({
        EndpointArn: pushDevice.endpointArn,
      }).promise();
    } catch (err) {
      logger.error('Couldnt delete endpoint arn from SNS', err);
    }

    // Delete from db, even if sns call fails.
    await PushDeviceModel.deletePushDevice(deviceToken);
    logger.info('Deleted device token from db', { deviceToken, pushDevice });
  }

  /**
   * Given a device token...
   * 1. Grab our stored endpoint arn and ask SNS what the associated attributes are to the token
   * 2. If attributes dont match what we expect (user, enabled, token), set them in SNS
   * 3. If the call fails (not found), then attempt creating a new entry in SNS for the token
   */
  async _reconcileEndpointAttributes(userId: number, deviceToken: string, platform: string) {
    logger.silly('NotificationService._reconcileEndpointAttributes', { userId, deviceToken, platform });
    const user = await UserModel.getUserById(userId);
    const pushDevice = await PushDeviceModel.getPushDeviceByDeviceToken(deviceToken);

    if (!pushDevice) {
      logger.warn(
        'Reconciliation of sns endpoint attributes requires the user has an existing endpoint arn',
        { userId, deviceToken, platform },
      );
      throw Error('Reconciliation of sns endpoint attributes requires the user has an existing endpoint arn');
    }

    const { endpointArn } = pushDevice;

    try {
      // call 'get endpoint atrributes' with stored endpointArn
      const response = await this._getEndpointAttributes(endpointArn);
      const { Attributes } = response;
      const { Token, Enabled, CustomUserData } = Attributes;
      const parsedCustomUserData = JSON.parse(CustomUserData);
      logger.debug('Received endpoint attributes', Attributes);
      // make sure token matches the token we have stored for the endpoint arn and enabled == true
      if (Token !== deviceToken || Enabled === 'false'
        || parsedCustomUserData.userId !== userId
        || parsedCustomUserData.organizationId !== user.organizationId) {
        // something doesn't match, call 'set endpoint attributes' to keep SNS up to date
        logger.warn('Mismatch in device token or endpoint arn is disabled, update both.');
        await this._setEndpointAttributes(user, endpointArn);
        await PushDeviceModel.upsertPushDevice(user.id, deviceToken, endpointArn);
      }
    } catch (err) {
      // if the attributes call fails, then we need to call 'create platform endpoint'
      logger.warn('Get or Set endpoint attributes failed (endpoint arn may not exist in sns). Attempt to register again.', { err, userId: user.id, deviceToken, endpointArn }); // eslint-disable-line
      await this._createAndSavePlatformEndpoint(user, deviceToken, platform);
    }
  }

  async _createAndSavePlatformEndpoint(user: UserModel, deviceToken: string, platform: string) {
    logger.silly('NotificationService._createAndSavePlatformEndpoint', { userId: user.id, deviceToken, platform });

    const platformApplicationArn = platform === 'ios' ?
      this.config.notifications.iosPlatformApplicationArn :
      this.config.notifications.androidPlatformApplicationArn;

    const params = {
      PlatformApplicationArn: platformApplicationArn,
      Token: deviceToken,
      CustomUserData: JSON.stringify({
        userId: user.id,
        organizationId: user.organizationId,
        // add username here when db is refactored
      }),
    };
    let createResponse;
    let endpointArn;
    try {
      createResponse = await this.snsClient.createPlatformEndpoint(params).promise();
      logger.info('Created platform endpoint', { userId: user.id, deviceToken, platform });
    } catch (err) {
      const { code, message } = err;
      // Literally the recommended approach for determing the error type recommend by aws
      //  https://aws.amazon.com/blogs/mobile/mobile-token-management-with-amazon-sns/
      // This case will only occur when db has no record of token AND SNS has record of it for
      //  a different user.
      if (code === 'InvalidParameter' && message.includes('already exists with the same Token')) {
        logger.warn('Reconfiguring existing endpointArn because it doesnt match the current user');
        const reg = /(arn:aws:sns[^\s]+)/g;
        const regResults = reg.exec(message);
        if (regResults && regResults[1]) {
          // We either dont have the token:endpointArn combo stored in db.
          // OR we changed the user data.
          // OR we never deregistered a former user that was on the device.
          // Reconfigure ARN. We want to update the attributes of the ARN to match THIS user.
          endpointArn = regResults[1]; // eslint-disable-line prefer-destructuring
          await this._setEndpointAttributes(user, endpointArn);
        } else {
          logger.error('Unexpected SNS code', {
            userId: user.id, deviceToken, platform, err,
          });
          throw Error(err); // Not an error we were expecting
        }
      } else {
        logger.error('Error creating platform endpoint', {
          userId: user.id, deviceToken, platform, err,
        });
        throw Error(err); // Not an error we were expecting
      }
    }

    if (createResponse) {
      endpointArn = createResponse.EndpointArn;
    }

    // perform the save to db
    await PushDeviceModel.upsertPushDevice(user.id, deviceToken, endpointArn);
    logger.info('Created and saved new (OR update existing) platform endpoint', { userId: user.id, deviceToken });

    return endpointArn;
  }

  async _getEndpointAttributes(endpointArn: string) {
    logger.silly('NotificationService._getEndpointAttributes', { endpointArn });

    const params = {
      EndpointArn: endpointArn,
    };
    return this.snsClient.getEndpointAttributes(params).promise();
  }

  async _setEndpointAttributes(user: UserModel, endpointArn: *) {
    logger.silly('NotificationService._setEndpointAttributes', { userId: user.id, endpointArn });

    const params = {
      Attributes: {
        Enabled: 'true',
        CustomUserData: JSON.stringify({
          userId: user.id,
          organizationId: user.organizationId,
          // add username here when db is refactored
        }),
      },
      EndpointArn: endpointArn,
    };
    return this.snsClient.setEndpointAttributes(params).promise();
  }
}
