// @flow
import { Pageable, IndexablePage, ArrayPage } from '@panderalabs/koa-pageable';
import _ from 'lodash';
import { ResourceNotFoundError } from '../common/errors';
import Logger from '../common/logging/logger';
import EventModel, { EventTypes } from '../domain/models/EventModel';
import LikeModel from '../domain/models/LikeModel';
import CommentModel from '../domain/models/CommentModel';
import SeriesModel from '../domain/models/SeriesModel';
import SubscriptionModel from '../domain/models/SubscriptionModel';
import TaskModel from '../domain/models/TaskModel';
import UserModel, { type UserDTOWithContentStats, type UserDTOWithSeriesStats } from '../domain/models/UserModel';

const logger = new Logger('services/AnalyticsService');

export default class AnalyticsService {
  async getUserActivityAnalytics(userId: number, offset: string, daysOfHistory?: ?number) {
    logger.silly('AnalyticsService.getUserActivityAnalytics', { userId, offset, daysOfHistory });
    const activityEventTypes = [EventTypes.USER_LOGGED_IN, EventTypes.APP_LAUNCHED, EventTypes.APP_ACTIVATED];

    return EventModel.getEventCountForUserByDate(userId, offset, activityEventTypes, daysOfHistory);
  }

  async getUserTasksCompletedAnalytics(userId: number, offset: string, daysOfHistory?: ?number) {
    logger.silly('AnalyticsService.getUserTasksCompletedAnalytics', { userId, offset, daysOfHistory });
    const taskCompletedEventTypes = [EventTypes.TASK_COMPLETED];

    return EventModel.getEventCountForUserByDate(userId, offset, taskCompletedEventTypes, daysOfHistory);
  }

  async getUserCommentsAddedAnalytics(userId: number, offset: string, daysOfHistory?: ?number) {
    logger.silly('AnalyticsService.getUserCommentsAddedAnalytics', { userId, offset, daysOfHistory });
    const addedCommentsEventTypes = [EventTypes.COMMENT_ADDED];

    return EventModel.getEventCountForUserByDate(userId, offset, addedCommentsEventTypes, daysOfHistory);
  }

  async getCurrentActivityStreakForUser(userId: number, offset: string) {
    logger.silly('AnalyticsService.getUserCommentsAddedAnalytics', { userId, offset });
    return EventModel.getCurrentActivityStreakForUser(userId, offset);
  }

  /**
   * High level stats info about a single content item. Information is gathered from
   * analytics events and domain tables.
   */
  async getContentItemSummary(
    contentId: number,
    organizationId?: number, // when provided, filtered to stats within this organization
  ) {
    logger.silly('AnalyticsService.getContentItemSummary', { contentId, organizationId });
    const likesPromise = LikeModel.getLikeCountByContentId(contentId, organizationId);
    const commentsPromise = CommentModel.getCommentCountByContentId(contentId, organizationId);
    const tasksPromise = TaskModel.getCompletedCountByContentId(contentId, organizationId);
    const viewsPromise = EventModel.getViewCountByContentId(contentId, organizationId);

    return Promise.all([likesPromise, commentsPromise, tasksPromise, viewsPromise])
      .then(([likesCount, commentsCount, tasksCount, viewsCount]) => ({
        likes: likesCount,
        comments: commentsCount,
        completedTasks: tasksCount,
        views: viewsCount,
      }));
  }

  /**
   * Gets a list of all analytics events that are focused on a single content item. An event
   * might be that the content was modified or that a particular user liked that content.
   */
  async getContentItemEvents(
    contentId: number,
    organizationId?: number, // when provided, filtered to stats within this organization
    pageable: Pageable,
  ): Promise<IndexablePage<number, EventModel>> {
    logger.silly('AnalyticsService.getContentItemFeed', { contentId, organizationId });

    const result = await EventModel.getContentItemEventsPaginated(contentId, organizationId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * In the context of a single content item, a view of users that have had any
   * interactions with that content and the stats associated to each of those users.
   * Note: This currently only supports viewing stats in a single organization.
   *  That can be adjusted if needed.
   */
  async getUsersWithStatsForContentId(
    contentId: number,
    organizationId: number, // filtered to stats within this organization
    pageable: Pageable,
  ): Promise<ArrayPage<UserDTOWithContentStats>> {
    logger.silly('AnalyticsService.getUsersWithStatsForContentId', { contentId, organizationId });

    const result = await UserModel.getUsersWithStatsForContentId(contentId, organizationId, pageable);
    return new ArrayPage(result.results, result.total, pageable);
  }

  /**
   * High level stats info about a single series. Information is gathered from
   * analytics events and domain tables.
   */
  async getSeriesSummary(
    seriesId: number,
    organizationId: number,
  ): Promise<{
    userCompletionCount: number,
    userNotStartedCount: number,
    userInProgressCount: number,
    contentCount: number,
  }> {
    logger.silly('AnalyticsService.getSeriesSummary', { seriesId, organizationId });
    const series = await SeriesModel.getById(seriesId);

    if (!series) {
      throw new ResourceNotFoundError('Series not found');
    }

    // Figure out what content exists in the series so we know what to scope
    // the aggregate stats to.
    const content = await series.getContent();
    const contentIds = content.map(c => c.id);
    const contentCount = contentIds.length;
    logger.debug('Series content ids', { seriesId, contentIds });

    // Just like content, grab users with access to the series.
    const subscribedUsers = await SubscriptionModel.getAllSubscribedUsers(series.channelId, organizationId);
    const userIds = subscribedUsers.map(u => u.id);
    logger.debug('Users with access to series', { seriesId, userIds });

    // Retrieve all completed user, content completion pairings for series
    const completedContentByUser = await TaskModel.getCompletedContentIdsByUser(userIds, contentIds);
    logger.debug('User Content completion pairings for series', { seriesId, completedContentByUser });

    // Create a mapping of users to number of posts they completed
    const countsByUser: { [userId: number]: number } = _
      .chain(completedContentByUser)
      .uniqWith(_.isEqual)
      .reduce((hashMap, pairing: { userId: number, contentId: number }) => {
        if (hashMap[pairing.userId]) {
          hashMap[pairing.userId] += 1;
        } else {
          hashMap[pairing.userId] = 1;
        }
        return hashMap;
      }, {})
      .value();
    logger.debug('User completion counts for series', { seriesId, countsByUser });

    // Get the aggregates
    let userCompletionCount = 0;
    let userNotStartedCount = 0;
    let userInProgressCount = 0;
    userIds.forEach((userId) => {
      const count = countsByUser[userId] || 0;
      if (count >= contentCount) {
        userCompletionCount += 1;
      } else if (count === 0) {
        userNotStartedCount += 1;
      } else {
        userInProgressCount += 1;
      }
    });
    logger.debug('Series aggregate stats', { seriesId,
      userCompletionCount,
      userNotStartedCount,
      userInProgressCount,
      contentCount,
    });

    return {
      userCompletionCount,
      userNotStartedCount,
      userInProgressCount,
      contentCount,
    };
  }

  /**
   * In the context of a single series, a view of users that have access to the
   * series and the stats associated to each of those users.
   */
  async getUsersWithStatsForSeriesId(
    seriesId: number,
    organizationId: number, // filtered to stats within this organization
    pageable: Pageable,
  ): Promise<ArrayPage<UserDTOWithSeriesStats>> {
    logger.silly('AnalyticsService.getUsersWithStatsForSeriesId', { seriesId, organizationId });

    const series = await SeriesModel.getById(seriesId);

    if (!series) {
      throw new ResourceNotFoundError('Series not found');
    }
    // grab users ahead of time, so we don't have to do it in the stats query.
    // the logic around who has access is complex enough to handle beforehand here.
    const subscribedUsers = await SubscriptionModel.getAllSubscribedUsers(series.channelId, organizationId);
    const userIds = subscribedUsers.map(u => u.id);

    // get the stats scoped to the list of subscribed users
    const result = await UserModel.getUsersWithStatsForSeriesId(seriesId, userIds, pageable);
    return new ArrayPage(result.results, result.total, pageable);
  }
}
