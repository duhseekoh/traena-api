// @flow
import _ from 'lodash';
import Kinesis from 'aws-sdk/clients/kinesis';
import EventService from './EventService';
import ContentItemModel, { type ContentItem, type ESIndexableContentItemDTO } from '../domain/models/ContentItemModel';
import UserModel from '../domain/models/UserModel';
import OrganizationModel from '../domain/models/OrganizationModel';
import ChannelModel from '../domain/models/ChannelModel';
import * as EventFactory from '../domain/EventFactory';
import Logger from '../common/logging/logger';
import { INDEXER_ACTION_TYPES } from './constants';
import { ResourceNotFoundError } from '../common/errors';

const logger = new Logger('services/ElasticSearchGatheringService');

function _toIndexableContentItem(
  contentItem: ContentItem,
  subscriberOrganizationId: number,
): ESIndexableContentItemDTO {
  return {
    ...contentItem,
    subscriberOrganizationId,
  };
}

export default class ElasticSearchGatheringService {
  kinesisClient: Kinesis;
  kinesisIndexStream: ?string; // when used in users cli, kinesisIndexStream is not used
  kinesisIndexRealtimeStream: string;
  eventService: EventService;

  constructor(
    kinesisClient: Kinesis, kinesisIndexStream: string,
    kinesisIndexRealtimeStream: string, eventService: EventService,
  ) {
    this.kinesisClient = kinesisClient;
    this.kinesisIndexStream = kinesisIndexStream;
    this.kinesisIndexRealtimeStream = kinesisIndexRealtimeStream;
    this.eventService = eventService;
  }

  /**
   * Perform a full reindex of all traena content. It sends a series of records to kinesis
   * in the order of creating a temp index, indexing content to that temp index, then
   * swapping the live index for the temp index, and deleting the old live index.
   * @param  userId Who triggered the reindex
   * @return an object with a message about the success
   */
  async reindexContents(userId: number) {
    logger.silly('ElasticSearchGatheringService.indexContents');
    logger.info('Reindexing all ContentItems into ElasticSearch');
    const partitionKey = 'INDEX_THE_CONTENTS'; // same partition puts all records on same shard
    const streamName = this.kinesisIndexStream;
    const items = await this.getAllSubscribedContentForAllOrgs();
    const chunks = _.chunk(items, 20);

    // Create new index marked as temp
    const newIndexAction = {
      actionType: INDEXER_ACTION_TYPES.CREATE_CONTENT_INDEX,
    };

    logger.debug('Creating temporary content index');
    await this.kinesisClient.putRecord({
      PartitionKey: partitionKey,
      Data: Buffer.from(JSON.stringify(newIndexAction)),
      StreamName: streamName,
    }).promise()
      .catch((err) => {
        logger.error('Could not create kinesis record (create content index)', err);
        throw err;
      });

    // Index Items
    const chunkPromises = chunks.map((chunk) => {
      const records = chunk.map((item) => {
        const updateAction = {
          actionType: INDEXER_ACTION_TYPES.CONTENT_ITEM_REINDEX,
          payload: item,
        };
        return {
          Data: Buffer.from(JSON.stringify(updateAction)),
          PartitionKey: partitionKey,
        };
      });
      logger.debug('Indexing contents', { count: records.length });
      return this.kinesisClient.putRecords({
        Records: records,
        StreamName: streamName,
      }).promise()
        .catch((err) => {
          logger.error('Could not create kinesis record (indexing content)', err);
          throw err;
        });
    });
    await Promise.all(chunkPromises);

    // Change the temp alias to the live alias
    const swapIndexAction = {
      actionType: INDEXER_ACTION_TYPES.SWAP_CONTENT_INDEX,
    };
    logger.debug('Swap temp content index to primary');

    await this.kinesisClient.putRecord({
      PartitionKey: partitionKey,
      Data: Buffer.from(JSON.stringify(swapIndexAction)),
      StreamName: streamName,
    }).promise()
      .catch((err) => {
        logger.error('Could not create kinesis record (swap content index)', err);
        throw err;
      });

    this.eventService.raiseEvent(EventFactory.contentReindexedEvent(userId));
    logger.info('Successfully submitted ContentItem records to kinesis');
    return { message: `Successfully submitted ${items.length} items to create a new index` };
  }

  async _indexContentItems(items: ESIndexableContentItemDTO[]): Promise<{message: string}> {
    // Index Items
    const partitionKey = 'INDEX_CONTENT_ITEM'; // same partition puts all records on same shard
    const streamName = this.kinesisIndexRealtimeStream;
    const chunks = _.chunk(items, 20);
    const chunkPromises = chunks.map((chunk) => {
      const records = chunk.map((item) => {
        const updateAction = {
          actionType: INDEXER_ACTION_TYPES.CONTENT_ITEM_UPDATED,
          payload: item,
        };
        return {
          Data: Buffer.from(JSON.stringify(updateAction)),
          PartitionKey: partitionKey,
        };
      });
      logger.debug('Indexing realtime contents', { count: records.length });
      return this.kinesisClient.putRecords({
        Records: records,
        StreamName: streamName,
      }).promise()
        .catch((err) => {
          logger.error('Could not create kinesis record (indexing realtime content)', err);
          throw err;
        });
    });
    await Promise.all(chunkPromises);
    return { message: `Successfully submitted ${items.length} items` };
  }

  /**
   * Indexes a single content item in elasticsearch. Used when someone creates or updates
   * a content item.
   * Supply subscriberOrgId if the change to content is subscriber org specific. (e.g. likes, comments)
   * @param contentId the content to index
   * @param subscriberOrgId passed in: item is only updated in ES for supplied subscriber org
   *                        not passed in: item is updated in ES for all subscriber orgs
   */
  async indexContentItem(contentId: number, subscriberOrgId: ?number): * {
    logger.silly('ElasticSearchGatheringService.indexContentItem');
    try {
      const items = await this._getContentItemByOrgForIndexing(contentId, subscriberOrgId);
      if (!subscriberOrgId) {
        // event to clear out the content item from all orgs in ES before providing
        // the updates versions. namely, this is for the case that the channelId has
        // changed and the post should no longer exist in orgs without exist to the
        // new channelId.
        await this._deleteContentItem(contentId);
      }
      const result = await this._indexContentItems(items);
      logger.info('Successfully submitted one content item to one or more subscribers', { contentId, subscriberOrgId });
      return result;
    } catch (err) {
      logger.error('Could not index a single content item', { err, contentId, subscriberOrgId });
      throw err;
    }
  }

  /**
   * Add all of the content in a channel to elasticsearch for a specific org that is
   * subscribed to that channel
   */
  async indexChannelContents(
    channelId: number, // Channel we are subscribing an org to
    subscriberOrgId?: number, // Org that is subscribing to the channel
  ): * {
    logger.silly('ElasticSearchGatheringService.indexChannelContent');
    logger.info('Indexing entire channel of content for subscriber organization', { channelId, subscriberOrgId });
    try {
      const items = await this._getChannelSubscribedContent(channelId, subscriberOrgId);
      const result = await this._indexContentItems(items);
      logger.info(
        'Successfully submitted a channel of content to one or more subscribers',
        { channelId, subscriberOrgId, count: items.length },
      );
      return result;
    } catch (err) {
      logger.error('Could not index a channel of content', { err, channelId, subscriberOrgId });
      throw err;
    }
  }


  /**
   * Removes a content item in elasticsearch for each org that has a copy of it.
   * Used whenever a content item is updated outside the context of a single org.
   */
  async _deleteContentItem(
    contentId: number, // content item to be removed from all orgs in ES
  ): Promise<true> {
    const partitionKey = 'INDEX_CONTENT_ITEM'; // same partition puts all records on same shard
    const streamName = this.kinesisIndexRealtimeStream;
    const deleteAction = {
      actionType: INDEXER_ACTION_TYPES.CONTENT_ITEM_DELETED,
      payload: contentId,
    };
    const record = {
      Data: Buffer.from(JSON.stringify(deleteAction)),
      PartitionKey: partitionKey,
      StreamName: streamName,
    };
    logger.debug('Deleting content item realtime', { contentId });
    try {
      await this.kinesisClient.putRecord(record).promise();
    } catch (err) {
      logger.error('Could not create kinesis record (deleting realtime content)', { contentId, err });
      throw err;
    }

    return true;
  }
  /**
   * Same as reindexContents except for users
   * @param userId Who triggered the reindex
   * @return       an object with a message about the success
   */
  async reindexUsers(userId: number) {
    logger.silly('ElasticSearchGatheringService.indexUsers');
    logger.info('Reindexing all Users into ElasticSearch');
    const partitionKey = 'INDEX_THE_USERS'; // same partition puts all records on same shard
    const streamName = this.kinesisIndexStream;
    const users = await this.getAllUsersForIndexing();
    const userChunks = _.chunk(users, 20);
    // Create new index marked as temp
    const newIndexAction = {
      actionType: INDEXER_ACTION_TYPES.CREATE_USER_INDEX,
    };
    logger.debug('Creating temporary user index');
    await this.kinesisClient.putRecord({
      PartitionKey: partitionKey,
      Data: Buffer.from(JSON.stringify(newIndexAction)),
      StreamName: streamName,
    }).promise()
      .catch((err) => {
        logger.error('Could not create kinesis record (create user index)', err);
        throw err;
      });

    // Index Users
    const chunkPromises = userChunks.map((chunk) => {
      const records = chunk.map((user) => {
        const userAction = {
          actionType: INDEXER_ACTION_TYPES.USER_REINDEX,
          payload: user,
        };
        return {
          Data: Buffer.from(JSON.stringify(userAction)),
          PartitionKey: partitionKey,
        };
      });
      logger.debug('Indexing users', { count: records.length });
      return this.kinesisClient.putRecords({
        Records: records,
        StreamName: streamName,
      }).promise()
        .catch((err) => {
          logger.error('Could not create kinesis record (indexing user)', err);
          throw err;
        });
    });
    await Promise.all(chunkPromises);

    // Change the temp alias to the live alias
    const swapIndexAction = {
      actionType: INDEXER_ACTION_TYPES.SWAP_USER_INDEX,
    };
    logger.debug('Swap temp user index to primary');
    await this.kinesisClient.putRecord({
      PartitionKey: partitionKey,
      Data: Buffer.from(JSON.stringify(swapIndexAction)),
      StreamName: streamName,
    }).promise()
      .catch((err) => {
        logger.error('Could not create kinesis record (swap user index)', err);
        throw err;
      });

    this.eventService.raiseEvent(EventFactory.userReindexedEvent(userId));
    logger.info('Successfully submitted User records to kinesis');
    return { message: `Successfully submitted ${users.length} users to create a new index` };
  }

  /**
   * Indexes a single user in elasticsearch. Used when someone creates or updates.
   * a content item.
   * @param userId user to index
   */
  async indexUser(userId: number) {
    logger.silly('ElasticSearchGatheringService.indexUser');
    const partitionKey = 'INDEX_USER'; // same partition puts all records on same shard
    const streamName = this.kinesisIndexRealtimeStream;
    const user = await this._getUserForIndexing(userId);
    if (!user) {
      logger.error('User not found while trying to index them', { userId });
      throw new ResourceNotFoundError(`User not found while trying to index them ${userId}`);
    }
    // Index User
    const userAction = {
      actionType: INDEXER_ACTION_TYPES.USER_UPDATED,
      payload: user,
    };
    logger.debug('Indexing user', { userId: user.id });
    await this.kinesisClient.putRecord({
      Data: Buffer.from(JSON.stringify(userAction)),
      PartitionKey: partitionKey,
      StreamName: streamName,
    }).promise()
      .catch((err) => {
        logger.error('Could not create kinesis record (indexing user)', err);
        throw err;
      });

    logger.info('Successfully submitted User record to kinesis', { userId });
  }

  /**
   * Retrieve a content item from the db, make modifications for insertion into elasticsearch.
   * Use when updating elasticsearch when a content item is liked, commented on, modified, created, etc...
   * @param  contentId     Id of content to lookup in db
   * @param  subscriberOrgId If specified, only this org will get the content update
   * @return Resolves to array of content items to insert into ES
   * @private
   */
  async _getContentItemByOrgForIndexing(
    contentId: number,
    subscriberOrgId: ?number,
  ): Promise<ESIndexableContentItemDTO[]> {
    logger.silly('ElasticSearchGatheringService._getContentItemsToIndex');
    // Get the content item so we can grab the provider organization
    const contentItem = await ContentItemModel.getContentById(contentId);

    // Get all orgs that have access to this content item (or just the specific org if specified)
    let subscriberIds: number[];
    if (subscriberOrgId) {
      logger.debug('targeting one subscriber', { subscriberOrgId });
      subscriberIds = [subscriberOrgId];
    } else {
      logger.debug('no specific subscriber, get all subscribers for content item', { contentId: contentItem.id });
      const subscribers = await contentItem.getSubscribers();
      subscriberIds = subscribers.map(it => it.id);
      logger.debug('found subscribers for content', { contentId: contentItem.id, subscribers });
    }

    // Get the subscriber org variant of content (likes and comment counts specific to org)
    const contentItemPromises = subscriberIds.map(async (orgId) => {
      const _contentItem = await ContentItemModel.getContentByIdForSubscriber(contentId, orgId);
      return _toIndexableContentItem(_contentItem, orgId);
    });

    return Promise.all(contentItemPromises);
  }

  /**
   * Retrieve all content items for all subscribers (organizations) for insertion into elasticsearch.
   * Used for full content reindex
   * @return indexable content
   */
  async getAllSubscribedContentForAllOrgs(): Promise<ESIndexableContentItemDTO[]> {
    logger.silly('ElasticSearchGatheringService.getAllSubscribedContentForAllOrgs');
    const organizationIds = await OrganizationModel.getAllOrganizationIds();

    const orgContentPromises = organizationIds.map(async (organizationId) => {
      const content = await ContentItemModel.getAllSubscribedContentForOrg(organizationId);
      const indexableContent = content.map(it => _toIndexableContentItem(it, organizationId));
      logger.debug(
        'Gathered content for subscriber org',
        { organizationId, count: indexableContent.length },
      );
      return indexableContent;
    });

    const nestedArrayOfContent = await Promise.all(orgContentPromises);
    return _.flatten(nestedArrayOfContent);
  }

  /**
   * Retrieve indexable content on a single channel for a single subscriber organization
   * @param channelId Channel id that org is subscribed to and we want the content for
   * @param subscriberOrgId Org that we are going to index the content for
   * @private
   * @return indexable content
   */
  async _getChannelSubscribedContent(
    channelId: number,
    subscriberOrgId?: number,
  ): Promise<ESIndexableContentItemDTO[]> {
    logger.silly('ElasticSearchGatheringService._getChannelSubscribedContent');

    // Get the channel so we can retrieve all subscriber orgs
    const channel = await ChannelModel.getById(channelId);
    if (!channel) {
      logger.warn('Cannot gather content for channel as channel does not exist', { channelId, subscriberOrgId });
      throw new Error('Cannot gather content for channel as channel does not exist');
    }

    // Get all orgs that have access to this channel (or just the specific org if specified)
    let subscriberIds: number[];
    if (subscriberOrgId) {
      logger.debug('targeting one subscriber of channel', { subscriberOrgId, channelId: channel.id });
      subscriberIds = [subscriberOrgId];
    } else {
      logger.debug('no specific subscriber, get all subscribers for channel', { channelId: channel.id });
      const subscribers = await channel.getSubscribers();
      subscriberIds = subscribers.map(it => it.id);
      logger.debug('found subscribers for channel', { channel: channel.id, subscribers });
    }

    // Get the subscriber org variant of content (likes and comment counts specific to org)
    const channelPromises: Promise<ESIndexableContentItemDTO[]>[] = subscriberIds.map(async (orgId) => {
      const contentItemsForOrg = await ContentItemModel.getChannelSubscribedContentForOrg(channelId, orgId);
      return contentItemsForOrg.map(item => _toIndexableContentItem(item, orgId));
    });

    const channelBuckets: Array<ESIndexableContentItemDTO[]> = await Promise.all(channelPromises);
    return _.flatten(channelBuckets);
  }

  async _getUserForIndexing(userId: number) {
    logger.silly('ElasticSearchGatheringService._getUserToIndex');
    return UserModel.getUserById(userId);
  }

  async getAllUsersForIndexing() {
    logger.silly('ElasticSearchGatheringService.getAllUsersToIndex');
    return UserModel.getAllUsers();
  }
}
