// @flow
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import type { User } from '../domain/models/UserModel'; // eslint-disable-line import/named
import UserModel from '../domain/models/UserModel';
import RoleModel from '../domain/models/RoleModel';
import LikeModel from '../domain/models/LikeModel';
import SubscriptionModel from '../domain/models/SubscriptionModel';
import AuthService from './AuthService';
import { type Auth0Role } from '../client/Auth0Client';
import OrganizationService from './OrganizationService';
import type ElasticSearchService from './ElasticSearchService';
import type ElasticSearchGatheringService from './ElasticSearchGatheringService';
import type EventService from './EventService';
import * as EventFactory from '../domain/EventFactory';

import { isDatabaseConflict } from '../common/errorUtils';
import {
  BadRequestError,
  ConflictError,
  ResourceNotFoundError,
  ResourceValidationError,
  ServerError,
} from '../common/errors';

const objection = require('objection');

const logger = new Logger('services/UserService');

export default class UserService {
  authService: AuthService;
  elasticSearchService: ElasticSearchService;
  organizationService: OrganizationService;
  eventService: EventService;
  elasticSearchGatheringService: ElasticSearchGatheringService;

  constructor(
    authService: AuthService, organizationService: OrganizationService,
    eventService: EventService, elasticSearchGatheringService: ElasticSearchGatheringService,
    elasticSearchService?: ElasticSearchService,
  ) {
    this.authService = authService;
    this.organizationService = organizationService;

    if (elasticSearchService) {
      this.elasticSearchService = elasticSearchService;
    }
    this.elasticSearchGatheringService = elasticSearchGatheringService;
    this.eventService = eventService;
  }

  async getProfileForUser(userId: number): Promise<UserModel> {
    logger.silly('UserService.getProfileForUser Retrieving user by id', { userId });
    return UserModel.getUserById(userId);
  }

  async get(userId: number): Promise<UserModel> {
    logger.silly('UserService.get', { userId });
    return UserModel.getUserById(userId);
  }

  async findByEmail(email: string): Promise<UserModel> {
    logger.silly('UserService.findByEmail', { email });
    return UserModel.getUserByEmail(email);
  }

  async create(authUserId?: number, user: User, password: ?string): Promise<UserModel> {
    logger.silly('UserService.create', { user });
    const knex = UserModel.knex();

    // Wrap the db insert in a transaction, and fail if auth0 returns an error other than the user already existing
    // If the user already exists for some reason in auth0 we *do* want to create user in our database
    try {
      const persistedUser = await objection.transaction(knex, async (tx) => {
        const createdUser = await UserModel.upsert(user, tx);
        try {
          const organization = await this.organizationService.get(user.organizationId);
          if (!organization) {
            // We should already have validated the organization at this point, but due to typing, we have to check
            throw ServerError('Could not locate organization for user');
          }

          await this.authService.createUser(createdUser, organization, password);
        } catch (auth0err) {
          if (auth0err instanceof ConflictError) {
            logger.warn('User already exists in auth0 -  adding user to database', { authUserId, user });
          } else {
            logger.error('Unknown auth0 user creation error', { auth0err, authUserId, user });
            throw auth0err;
          }
        }
        return createdUser;
      });

      this.eventService.raiseEvent(EventFactory.createUserEvent(persistedUser, authUserId));
      // outside transaction, otherwise no access to user because gathering service reads from outside transcation
      this.elasticSearchGatheringService.indexUser(persistedUser.id);
      logger.info('Created User', { user: persistedUser, authUserId });
      return persistedUser;
    } catch (err) {
      if (isDatabaseConflict(err)) {
        logger.error('ConflictError Cause: ', { message: err.message, detail: err.detail });
        throw new ConflictError(`User with email: ${user.email} already exists`);
      }
      logger.error('Unknown user creation error', { err, authUserId, user });
      throw err;
    }
  }

  async deleteUser(authUserId?: number, userId: number): Promise<boolean> {
    logger.info('UserService.deleteUser', { userId });

    const user = await UserModel.getUserById(userId);
    const disabledCount: number = await UserModel.disable(userId);
    const result = await this.authService.disableUser(user.email);

    this.eventService.raiseEvent(EventFactory.disableUserEvent(userId, authUserId));
    this.elasticSearchGatheringService.indexUser(userId);
    logger.info('After disable ', { isDisabledInDb: disabledCount === 1, auth0Result: result });
    return disabledCount === 1;
  }

  async update(user: UserModel): Promise<UserModel> {
    logger.debug('UserService.update', { user });

    if (!user.id) {
      throw new BadRequestError('User to update must be supplied with an id');
    }

    const existingUser = await this.get(user.id);

    if (!existingUser) {
      throw new ResourceNotFoundError(`User with id: ${user.id ? user.id : 'unknown'} does not exist`);
    }

    if (user.organizationId && user.organizationId !== existingUser.organizationId) {
      logger.warn('User\'s organizationId cannot be modified', { user, existingUser });
      throw new ResourceValidationError('User\'s organizationId cannot be modified');
    }

    if (user.isDisabled === true && existingUser.isDisabled !== true) {
      logger.warn('Users must be disabled via the DELETE operation', { user });
      throw new BadRequestError('Users must be disabled via the DELETE operation');
    }

    if (user.isDisabled !== true && existingUser.isDisabled === true) {
      await this.authService.enableUser(user.email);
    }

    /*
    * We try to update the email in auth0.  If it doesn't succeed, this transaction gets rolled back
    * so we don't wind up with a user in our DB who's email address is out of sync with auth0
    */
    const knex = UserModel.knex();
    const persistedUser = await objection.transaction(knex, async (tx) => {
      const updatedUser: UserModel = await UserModel.upsert(user, tx);
      try {
        if (updatedUser.email && updatedUser.email !== existingUser.email) {
          await this.authService.updateUserEmail(updatedUser.email, existingUser.email);
        }
        if (updatedUser.id) {
          this.elasticSearchGatheringService.indexUser(updatedUser.id);
        }
        logger.info('Updated user', { updatedUser });
      } catch (auth0err) {
        logger.error('User\'s email failed to update in auth0', { auth0err, existingUser });
        throw auth0err;
      }
      return updatedUser;
    });
    return persistedUser;
  }

  async getUsersForOrganizationPaginated(
    organizationId: number,
    pageable: Pageable,
  ): Promise<IndexablePage<number, UserModel>> {
    logger.silly(
      'UserService.getUsersForOrganizationPaginated: Getting user page for organization',
      { organizationId, pageable },
    );
    const users = await this.elasticSearchService.getUsersForOrg(organizationId, pageable);
    return users;
  }

  /**
   * Retrieves users directly subscribed to a channel. This is relevant in the
   * context of an organization subscribing to a channel and setting its visibility
   * to 'USERS'
   */
  async getUsersForSubscriptionPaginated(
    channelId: number,
    organizationId: number,
    pageable: Pageable,
  ): Promise<IndexablePage<number, UserModel>> {
    logger.silly(
      'UserService.getUsersForSubscriptionPaginated: Getting users subscribed to channel',
      { channelId, organizationId, pageable },
    );
    const subscription = await SubscriptionModel.getById(channelId, organizationId);
    if (!subscription) {
      throw new ResourceNotFoundError('Organization Subscription could not be found');
    }
    const result = await subscription.getSubscribedUsersPaginated(pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Returns a list of content ids liked by this user
   * @param userId
   */
  async getLikesForUser(userId: number): Promise<number[]> {
    logger.silly('UserService.getLikesForUser: Getting likes for user', { userId });
    return LikeModel.getAllLikedContentForUser(userId);
  }

  /**
   * Retrieve the roles a user is directly assigned
   */
  async getUserRoles(userId: number): Promise<RoleModel[]> {
    logger.silly('UserService.getUserRoles', { userId });
    const user = await this.get(userId);
    // roles from auth0 that we are exposing
    const assignableRoles = (await RoleModel.getAssignableRoles());
    // roles from auth0 assigned to this user
    const userAuth0Roles: Auth0Role[] = await this.authService.getUserRoles(user.email);
    const userAuth0RoleIds = userAuth0Roles.map(role => role._id);
    // return just the user roles that are assignable (e.g. exclude traena admin role)
    return assignableRoles.filter(role => userAuth0RoleIds.includes(role.auth0RoleId));
  }

  async addRolesToUser(userId: number, roleIds: string[] = []): Promise<*> {
    logger.silly('UserService.addRolesToUser', { userId });
    const user = await this.get(userId);
    // roles from auth0 that we allow a user to be added to
    const assignableRoles = (await RoleModel.getAssignableRoles());
    // filter assignable roles to list of roles we want to add to the user
    const rolesToAdd: RoleModel[] = assignableRoles.filter(role => roleIds.includes(role.id));
    return this.authService.addRolesToUser(user.email, rolesToAdd);
  }

  async removeRolesFromUser(userId: number, roleIds: string[] = []): Promise<*> {
    logger.silly('UserService.removeRolesFromUser', { userId });
    const user = await this.get(userId);
    // roles from auth0 that we allow a user to be added to
    const assignableRoles = (await RoleModel.getAssignableRoles());
    // filter assignable roles to list of roles we want to remove from the user
    const rolesToAdd: RoleModel[] = assignableRoles.filter(role => roleIds.includes(role.id));
    return this.authService.removeRolesFromUser(user.email, rolesToAdd);
  }
}
