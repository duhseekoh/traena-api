// @flow
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import CommentModel from '../domain/models/CommentModel';
import NotificationModel from '../domain/models/NotificationModel';
import ContentItemModel from '../domain/models/ContentItemModel';
import CommentTaggedUserModel from '../domain/models/CommentTaggedUserModel';
import UserModel from '../domain/models/UserModel';
import * as EventFactory from '../domain/EventFactory';
import type ContentService from './ContentService';
import type NotificationService from './NotificationService';
import type EventService from './EventService';
import type ElasticSearchGatheringService from './ElasticSearchGatheringService';
import {
  ResourceValidationError,
} from '../common/errors';

const logger = new Logger('services/CommentService');

// For some reason `import objection from 'objection'` doesn't work...
const objection = require('objection');

export default class CommentService {
  contentService: ContentService;
  notificationService: NotificationService;
  eventService: EventService;
  elasticSearchGatheringService: ElasticSearchGatheringService;

  constructor(
    contentService: ContentService, notificationService: NotificationService,
    eventService: EventService, elasticSearchGatheringService: ElasticSearchGatheringService,
  ) {
    this.contentService = contentService;
    this.notificationService = notificationService;
    this.eventService = eventService;
    this.elasticSearchGatheringService = elasticSearchGatheringService;
  }

  /**
   * Get comments for a content item.
   * If maxDate is null, then the 10 most recent comments will be received.
   * If maxDate is a date, it will receive the 10 comments prior to that timestamp.
   */
  async getCommentsForContent(userId: number, contentId: number, maxDate: ?Date, pageable: Pageable) {
    logger.silly('CommentService.getCommentsForContent', { userId, contentId, maxDate });

    // get users organization
    const { organizationId } = await UserModel.getUserById(userId);
    // get comments for content, only for specific org
    const comments = await CommentModel.getCommentsForContent(organizationId, contentId, maxDate, pageable);

    return new IndexablePage(comments.results, comments.total, pageable);
  }

  async getCommentById(commentId: number) {
    logger.silly('CommentService.getCommentById', { commentId });
    return CommentModel.getCommentById(commentId);
  }

  /**
   * Insert a comment on a piece of content.
   * Includes ability to pass in user ids to tag to the comment.
   * If a tagged user id doesnt exist, this will fail.
   */
  async addComment(contentId: number, user: UserModel, text: string, taggedUserIds: number[] = []) {
    logger.debug('CommentService.addComment', { contentId, userId: user.id });
    const userId = user.id;

    // TODO - add guard for attaching tagged user ids that aren't part of organization!

    // Transaction -- If inserting the tagged users fails, then the entire comment entry fails.
    const insertedCommentId = await objection.transaction(
      CommentTaggedUserModel, CommentModel,
      async (BoundCommentTaggedUser, BoundComment) => {
        const comment = await BoundComment.insertComment(user, contentId, text);
        logger.debug('in transaction - inserted comment', { userId, contentId, commentId: comment.id });
        await BoundCommentTaggedUser.insertUsers(taggedUserIds, comment.id);
        logger.debug('in transaction - inserted tagged users', { userId, commentId: comment.id, taggedUserIds });
        return comment.id;
      },
    );

    const comment = await CommentModel.getCommentById(insertedCommentId);
    const contentItem = await ContentItemModel.getContentById(contentId);

    // No need to await this
    this.notificationService.addCommentTaggedNotifications(userId, taggedUserIds, contentId, comment.id);

    // if the user is tag in a comment on their own post we don't want them to recieve multiple notifications.
    if (!taggedUserIds.includes(contentItem.authorId)) {
      this.notificationService.addCommentNotification(userId, contentItem.authorId, contentId, comment.id);
    }
    this.eventService.raiseEvent(EventFactory.addCommentEvent(comment, taggedUserIds));
    this.elasticSearchGatheringService.indexContentItem(contentId, user.organizationId);

    return comment;
  }

  async deleteComment(contentId: number, commentId: number) {
    const comment = await CommentModel.getCommentById(commentId);
    if (comment.contentId !== contentId) {
      logger.warn('Comment\'s contentId incorrect', { contentId, commentId });
      throw new ResourceValidationError('Comment\'s contentId incorrect');
    }
    if (comment.deleted) {
      throw new ResourceValidationError('This comment was already deleted');
    }
    const persistedDeletedCount = await objection.transaction(
      CommentModel, NotificationModel,
      async (BoundCommentModel, BoundNotificationModel) => {
        try {
          const [deletedCount, deletedNotifications] = await Promise.all([ // eslint-disable-line
            BoundCommentModel.delete(commentId),
            BoundNotificationModel.deleteNotificationsForComment(commentId),
          ]);
          this.eventService.raiseEvent(EventFactory.deleteCommentEvent(commentId, contentId));
          return deletedCount;
        } catch (err) {
          logger.error('Error deleting comment', { commentId, err });
          throw err;
        }
      },
    );
    return persistedDeletedCount === 1;
  }
}
