// @flow
import _ from 'lodash';
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import { transaction } from 'objection';
import SeriesModel, { type CreateSeriesDTO, type UpdateSeriesDTO } from '../domain/models/SeriesModel';
import Logger from '../common/logging/logger';
import type UserModel from '../domain/models/UserModel';
import type ElasticSearchGatheringService from './ElasticSearchGatheringService';
import type EventService from './EventService';
import type ChannelService from './ChannelService';
import type { SubscriptionSources } from '../domain/fieldConstants';
import type UserService from './UserService';
import { ResourceValidationError } from '../common/errors';
import TaskModel from '../domain/models/TaskModel';

const logger = new Logger('services/SeriesService');

const SERIES_PROGRESS_STATUSES = {
  NOT_STARTED: 'NOT_STARTED',
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETED: 'COMPLETED',
};
type SeriesProgressStatus = $Values<typeof SERIES_PROGRESS_STATUSES>;

export default class SeriesService {
  eventService: EventService;
  channelService: ChannelService;
  userService: UserService;
  esGatheringService: ElasticSearchGatheringService;

  constructor(
    eventService: EventService,
    channelService: ChannelService,
    userService: UserService,
    esGatheringService: ElasticSearchGatheringService,
  ) {
    this.eventService = eventService;
    this.esGatheringService = esGatheringService;
    this.userService = userService;
    this.channelService = channelService;
  }

  async create(series: CreateSeriesDTO, userId: number) {
    logger.silly('SeriesService.create', { series });
    series.authorId = userId;
    const { contentIds } = series;
    try {
      const persistedSeries = await transaction(SeriesModel, async (BoundSeriesModel) => {
        delete series.contentIds;
        const createdSeries = await BoundSeriesModel.upsert(series);
        await BoundSeriesModel.relateContentToSeries(createdSeries.id, contentIds);

        return createdSeries;
      });
      contentIds.map(id =>
        this.esGatheringService.indexContentItem(id));

      return persistedSeries;
    } catch (err) {
      logger.error('Series create transacation failed', { series, userId, err });
      throw err;
    }
  }

  async get(seriesId: number, subscriberOrgId?: number, includeContentPreview?: boolean): Promise<?SeriesModel> {
    logger.silly('SeriesService.get', { seriesId });
    return SeriesModel.getById(seriesId, includeContentPreview, subscriberOrgId);
  }

  async getByOrganizationId(organizationId: number, includeContentPreview: boolean, pageable: Pageable) {
    logger.silly('SeriesService.getByOrganizationId', { organizationId });
    const result = await SeriesModel.getByOrganizationId(organizationId, includeContentPreview, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  async getByChannelId(channelId: number, subscriberOrgId: number, includeContentPreview: boolean, pageable: Pageable) {
    logger.silly('SeriesService.getByChannelId', { channelId });
    const result = await SeriesModel.getByChannelId(channelId, subscriberOrgId, includeContentPreview, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  async getContent(seriesId: number, subscriberOrgId: number, pageable: Pageable) {
    logger.silly('SeriesService.getContent', { seriesId });
    const seriesContent = await SeriesModel.getContent(seriesId, subscriberOrgId, pageable);
    return new IndexablePage(seriesContent.results, seriesContent.total, pageable);
  }

  async getChannelForSeriesId(seriesId: number) {
    const series = await SeriesModel.getById(seriesId);
    if (!series) {
      throw new ResourceValidationError('Series must exist to get channel');
    }

    return this.channelService.get(series.channelId);
  }

  /**
   * Get's all series a given organization has access to, paginated.
   * Filter by subscription source, to view series owned by organization OR
   *  through a subscription.
   */
  async getSubscribedSeriesForOrganization(
    organizationId: number,
    pageable: Pageable,
    options: {
      subscriptionSource?: ?SubscriptionSources,
    },
  ): Promise<IndexablePage<number, SeriesModel>> {
    logger.silly('SeriesService.getSubscribedSeriesForOrganization', { organizationId, pageable });

    // Get channels the org is subscribed to, filtered by whether the org owns the channel or not
    const channels = await this.channelService.getSubscribedChannelsForOrganization(organizationId, {
      subscriptionSource: options.subscriptionSource,
    });
    const channelIds = channels.map(c => c.id);
    logger.silly('filtered channel ids', { channelIds });

    // Now retrieve series based on the filtered list of channels
    const result = await SeriesModel.getByChannelIdsPaginated(
      undefined, channelIds, organizationId, {}, pageable,
    );
    return new IndexablePage(result.results, result.total, pageable);
  }

  async update(series: UpdateSeriesDTO) {
    logger.silly('SeriesService.update', { series });
    const existingSeries = await this.get(series.id);
    if (!existingSeries) {
      throw new ResourceValidationError('A series must already exist to update it');
    }
    const existingContent = await existingSeries.getContent();

    series.authorId = existingSeries.authorId;

    const existingChannel = await this.channelService.get(existingSeries.channelId);

    if (!existingChannel) {
      throw new ResourceValidationError('A series must have a channel to update it');
    }

    const channel = await this.channelService.get(series.channelId);
    if (channel.organizationId !== existingChannel.organizationId) {
      throw new ResourceValidationError('A channel can not be moved between organizations');
    }

    if (channel.id !== existingChannel.id) {
      throw new ResourceValidationError('A series can not be moved between channels');
    }
    const contentIdsToIndex = _.union(existingContent.map(c => c.id), series.contentIds);
    try {
      const persistedSeries = await transaction(SeriesModel, async (BoundSeriesModel) => {
        const { contentIds } = series;
        delete series.contentIds;
        const updatedSeries = await BoundSeriesModel.upsert(series);
        await BoundSeriesModel.unrelateContentFromSeries(series.id);
        await BoundSeriesModel.relateContentToSeries(series.id, contentIds);
        return updatedSeries;
      });

      contentIdsToIndex.map(id =>
        this.esGatheringService.indexContentItem(id));

      return persistedSeries;
    } catch (err) {
      logger.error('Series create transacation failed', { series, err });
      throw err;
    }
  }

  async delete(seriesId: number) {
    logger.silly('SeriesService.delete', { seriesId });
    const existingSeries = await this.get(seriesId);
    const deletedSeries = await transaction(SeriesModel, async (BoundSeriesModel) => {
      await BoundSeriesModel.unrelateContentFromSeries(seriesId);
      await BoundSeriesModel.delete(seriesId);
      return true;
    });

    if (existingSeries && existingSeries.contentIds) {
      existingSeries.contentIds.map(id =>
        this.esGatheringService.indexContentItem(id));
    }

    return deletedSeries;
  }

  /**
   * Gets the user progress info about series. Can select a single series or
   * all series a user has access to.
   */
  async getSeriesProgressForUser(
    userId: number,
    // when provided, only get progress for this series
    // otherwise get progress for all series a user has access to
    seriesId?: ?number,
  ): Promise<{
    seriesId: number,
    completedContentCount: number,
    contentCount: number,
    contentIds: number[],
    status: SeriesProgressStatus,
  }[]> {
    logger.silly('SeriesService.getSeriesProgressForUser', { seriesId });
    // Get the series we want user progress for
    let subscribedSeries: SeriesModel[];
    if (seriesId) {
      const singleSeries = await this.get(seriesId);
      subscribedSeries = singleSeries ? [singleSeries] : [];
    } else {
      subscribedSeries = await this.getSubscribedSeriesForUser(userId);
    }

    // Get the first set of info we need for each series, not user specific
    const contentMetaBySeries = await SeriesModel.getContentMeta(subscribedSeries.map(s => s.id));

    // Now grab the user specific completion info for each series in question
    const getUserCompletionPromises = contentMetaBySeries.map(async (contentMeta) => {
      // Get number of content that has been completed by user
      const completedContentCount = await TaskModel
        .getCompletedContentCountForUser(userId, contentMeta.contentIds);

      // Determine the users status with this series
      const { contentCount } = contentMeta;
      let status;
      if (contentCount > 0 && completedContentCount >= contentCount) {
        status = SERIES_PROGRESS_STATUSES.COMPLETED;
      } else if (contentCount > 0 && completedContentCount > 0) {
        status = SERIES_PROGRESS_STATUSES.IN_PROGRESS;
      } else {
        status = SERIES_PROGRESS_STATUSES.NOT_STARTED;
      }

      return {
        ...contentMeta,
        completedContentCount,
        status,
      };
    });
    return Promise.all(getUserCompletionPromises);
  }

  /**
   * Get's all series a given user has access to.
   */
  async getSubscribedSeriesForUser(
    userId: number,
  ): Promise<SeriesModel[]> {
    logger.silly('SeriesService.getSubscribedSeriesForUser', { userId });

    // Get channels the user is subscribed to.
    const channels = await this.channelService.getSubscribedChannelsForUser(userId);
    const channelIds = channels.map(c => c.id);
    logger.silly('filtered channel ids', { channelIds });

    // Now retrieve series based on the filtered list of channels
    const seriesList = await SeriesModel.getByChannelIds(channelIds);
    return seriesList;
  }

  /**
   * Get's all series a given user has access to, paginated.
   */
  async getSubscribedSeriesForUserPaginated(
    userId: number,
    options: {
      includeContentPreview?: boolean,
      // typically end users will not want to see series that have no posts
      filterOutEmptySeries?: boolean,
      // when a channelId is provided, only series within that channel are returned
      channelId?: ?number,
      // only include series where the users progress with that series matches these
      filterProgressStatus?: ?SeriesProgressStatus[],
    },
    pageable: Pageable,
  ): Promise<IndexablePage<number, SeriesModel>> {
    logger.silly('SeriesService.getSubscribedSeriesForUserPaginated', { userId, options, pageable });
    const user: UserModel = await this.userService.get(userId);

    // Get channels the user is subscribed to.
    const channels = await this.channelService.getSubscribedChannelsForUser(userId);
    let channelIds = channels.map(c => c.id);

    // Filter to just the channel provided (ensures user is subscribed to provided channel)
    if (options.channelId) {
      channelIds = channelIds.filter(id => id === options.channelId);
    }

    logger.silly('filtered channel ids', { channelIds });

    // If filtering to specific progress statuses for this user, only include those seriesIds
    const { filterProgressStatus } = options;
    let includedSeriesIds;
    if (filterProgressStatus && filterProgressStatus.length > 0) {
      const seriesProgress = await this.getSeriesProgressForUser(userId);
      includedSeriesIds = seriesProgress
        .filter(sp => filterProgressStatus.includes(sp.status))
        .map(sp => sp.seriesId);
    }

    // Now retrieve series based on the filtered list of channels
    const result = await SeriesModel.getByChannelIdsPaginated(
      userId, channelIds, user.organizationId, {
        ...options,
        seriesIds: includedSeriesIds,
      }, pageable,
    );

    logger.silly('Series retrieved', result);
    return new IndexablePage(result.results, result.total, pageable);
  }
}
