// @flow
import moment from 'moment';
import Ajv from 'ajv';
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import ContentItemModel from '../domain/models/ContentItemModel';
import LikeModel from '../domain/models/LikeModel';
import TagModel from '../domain/models/TagModel';
import UserModel from '../domain/models/UserModel';
import ChannelModel from '../domain/models/ChannelModel';
import OrganizationModel from '../domain/models/OrganizationModel';
import Logger from '../common/logging/logger';
import TranscodeJobModel from '../domain/models/TranscodeJobModel';
import type { ContentItemStatus, TranscodeJobStatus } from '../domain/fieldConstants';
import { CONTENT_ITEM_STATUSES, CONTENT_ITEM_TYPES, TRANSCODE_JOB_STATUSES } from '../domain/fieldConstants';
import * as EventFactory from '../domain/EventFactory';
import {
  BadRequestError,
  ResourceForbiddenError,
  ResourceNotFoundError,
  ResourceValidationError,
} from '../common/errors';
import type EventService from './EventService';
import type ElasticSearchService from './ElasticSearchService';
import type ElasticSearchGatheringService from './ElasticSearchGatheringService';
import type MediaService from './MediaService';
import type NotificationService from './NotificationService';

import publishedContentItemSchema from '../domain/models/schema/validation/publishedContentItem.json';
import { toUTCTimestampString } from '../common/dateUtils';

// For some reason `import objection from 'objection'` doesn't work...
const objection = require('objection');

const logger = new Logger('services/ContentService');

const CONTENT_ITEM_SCHEMA_ID = 'http://traena.io/validation/contentItem#';

function validatePublishedSchema(ajv, contentItem) {
  logger.debug('Validating published ContentItem against json schema');
  const valid = ajv.validate(CONTENT_ITEM_SCHEMA_ID, contentItem);

  if (!valid) {
    logger.error('Error validating ContentItem against json schema', { error: ajv.errorsText() });
    // errorsText will return a single concatted string if you pass it an array and we want an array of strings so
    // instead we have to call it for each element
    const errors = ajv.errors.map(error => ajv.errorsText([error]));
    throw new ResourceValidationError('Invalid Content Item', errors);
  }
}

export default class ContentService {
  _ajv: Ajv;
  eventService: EventService;
  elasticSearchService: ElasticSearchService;
  elasticSearchGatheringService: ElasticSearchGatheringService;
  mediaService: MediaService;
  notificationService: NotificationService;

  constructor(
    eventService: EventService, mediaService: MediaService, elasticSearchService: ?ElasticSearchService,
    elasticSearchGatheringService: ElasticSearchGatheringService, notificationService: NotificationService,
  ) {
    this.eventService = eventService;
    if (elasticSearchService) {
      this.elasticSearchService = elasticSearchService;
    }
    this.elasticSearchGatheringService = elasticSearchGatheringService;
    this.mediaService = mediaService;
    this.notificationService = notificationService;

    const ajvOpts = {
      allErrors: true,
      verbose: true,
      extendRefs: true,
      schemas: [publishedContentItemSchema],
    };
    this._ajv = new Ajv(ajvOpts);
  }


  async _persistContentItem(
    contentItem: ContentItemModel, userId: number,
    organizationId: number,
  ): Promise<ContentItemModel> {
    // The tags are stored separately from the content
    const { tags } = contentItem;
    delete contentItem.tags;

    logger.debug('beginning transaction for contentItem');
    const newContentId = await objection.transaction(
      ContentItemModel, TagModel,
      async (BoundContentItem, BoundTag) => {
        const createdContentItem = await BoundContentItem.upsertContent(contentItem);
        logger.debug(
          'in transaction - upserted contentItem',
          { userId, organizationId, contentId: createdContentItem.id },
        );
        // Since tags are just lists of strings, we can safely just remove any existing and add new ones
        await BoundTag.deleteTagsForContent(createdContentItem.id);
        if (tags && tags.length > 0) {
          await BoundTag.createTags(createdContentItem.id, tags);
        }
        logger.debug('in transaction - inserted tags', { userId, organizationId, contentId: createdContentItem.id });
        return createdContentItem.id;
      },
    );
    logger.debug('transaction complete', { contentId: newContentId });

    // Flow thinks this could be null, but as we just created it, it won't ever be, so we'll cast it to non-nullable
    return ((await ContentItemModel.getContentByIdForUser(userId, newContentId): any): ContentItemModel);
  }

  async createContentItem(
    contentItem: ContentItemModel, userId: number,
    organizationId: number,
  ): Promise<ContentItemModel> {
    logger.debug('ContentService.createContentItem', { userId, organizationId, contentItem });

    if (contentItem.id) {
      throw new BadRequestError('Id cannot be populated on create');
    }

    contentItem.authorId = userId;
    contentItem.authorOrganizationId = organizationId;

    // If not populated, set to the organizations default channel
    if (!contentItem.channelId) {
      const defaultChannelId = await ChannelModel.getDefaultChannelIdForOrganization(organizationId);
      logger.debug('Content does not contain a channelId - looked up default', { defaultChannelId });

      if (!defaultChannelId) {
        logger.warn(
          'Content must contain a channelId when there is no defaultChannelId',
          { contentItem, organizationId },
        );
        throw new ResourceValidationError('Content must contain channelId');
      }
      contentItem.channelId = defaultChannelId;
    }

    const canUserAccessChannel =
      await ChannelModel.isOwnedByUsersOrganization(contentItem.channelId, contentItem.authorId);
    if (!canUserAccessChannel) {
      logger.error('User attempting to write to non-owned channel', { userId, channelId: contentItem.channelId });
      throw new ResourceForbiddenError('User cannot write to requested channel');
    }

    if (contentItem.status === CONTENT_ITEM_STATUSES.PUBLISHED) {
      contentItem.publishedTimestamp = new Date().toISOString();
      validatePublishedSchema(this._ajv, contentItem);
    }

    // As this is a new content item we need to first persist it, to get the db generated contentId before potentially
    // moving any temp images as they depend on the content id
    let persistedContent: ContentItemModel = await this._persistContentItem(contentItem, userId, organizationId);
    const contentId = persistedContent.id;

    if (persistedContent.body.type === CONTENT_ITEM_TYPES.IMAGE && persistedContent.body.images) {
      persistedContent.body.images = await this.mediaService.moveTempImages(
        organizationId,
        userId,
        contentId,
        persistedContent.body.images,
      );
    }

    // Re-persist to properly store any moved images paths
    await ContentItemModel.upsertContent(persistedContent);
    // and load once again as we need the derived properties as well
    persistedContent = ((await ContentItemModel.getContentByIdForUser(userId, contentId): any): ContentItemModel);

    this.eventService.raiseEvent(EventFactory.createContentEvent(persistedContent));
    this.elasticSearchGatheringService.indexContentItem(persistedContent.id);
    return persistedContent;
  }


  async updateContentItem(
    contentItem: ContentItemModel, userId: number,
    organizationId: number,
  ): Promise<ContentItemModel> {
    logger.debug('ContentService.updateContentItem', { userId, organizationId, contentItem });

    const existingContentItem = await ContentItemModel.getContentById(contentItem.id);

    if (!existingContentItem) {
      throw new ResourceNotFoundError('Content Not Found');
    }

    // Set all non-updatable fields from the existing content item
    contentItem.authorId = existingContentItem.authorId;
    contentItem.authorOrganizationId = existingContentItem.authorOrganizationId;
    // because validation below requires it to be a string
    contentItem.createdTimestamp = toUTCTimestampString(existingContentItem.createdTimestamp);
    if (existingContentItem.publishedTimestamp) {
      contentItem.publishedTimestamp = toUTCTimestampString(existingContentItem.publishedTimestamp);
    }

    const canUserAccessChannel =
      await ChannelModel.isOwnedByUsersOrganization(contentItem.channelId, contentItem.authorId);
    if (!canUserAccessChannel) {
      logger.error('User attempting to write to non-owned channel', { userId, channelId: contentItem.channelId });
      throw new ResourceForbiddenError('User cannot write to requested channel');
    }

    if (existingContentItem.status !== CONTENT_ITEM_STATUSES.PUBLISHED
      && contentItem.status === CONTENT_ITEM_STATUSES.PUBLISHED) {
      logger.debug('Content is transitioning from non-published to published', { contentItem });
      contentItem.publishedTimestamp = new Date().toISOString();
    }

    // Don't allow re-assignment of content to different channel if content is
    // already part of a series
    if (existingContentItem.channelId !== contentItem.channelId) {
      const hasSeries = await existingContentItem.hasSeries();
      if (hasSeries) {
        logger.warn('User attempting to change channel when content is associated to series', {
          userId,
          contentId: contentItem.id,
          oldChannelId: existingContentItem.channelId,
          channelId: contentItem.channelId,
        });
        throw new BadRequestError('Cannot change channel of content that is associated to a series');
      }
    }

    if (contentItem.status === CONTENT_ITEM_STATUSES.PUBLISHED) {
      logger.debug('Marking content as published. Attempting to validate against schema.');
      validatePublishedSchema(this._ajv, contentItem);
    }

    if (contentItem.body.type === CONTENT_ITEM_TYPES.IMAGE && contentItem.body.images) {
      await this.mediaService.moveTempImages(
        contentItem.authorOrganizationId,
        contentItem.authorId,
        contentItem.id,
        contentItem.body.images,
      );
    }

    const persistedContent = await this._persistContentItem(contentItem, userId, organizationId);
    this.eventService.raiseEvent(EventFactory.modifyContentEvent(persistedContent, userId));
    this.elasticSearchGatheringService.indexContentItem(persistedContent.id);
    logger.info('Updated content item', { userId, organizationId, persistedContent });
    return persistedContent;
  }

  async archiveContentItem(contentItemId: number, userId: number): Promise<ContentItemModel> {
    logger.debug('ContentService.archiveContentItem', { contentItemId, userId });
    const existingContentItem = await ContentItemModel.getContentById(contentItemId);

    existingContentItem.status = CONTENT_ITEM_STATUSES.ARCHIVED;
    const result = await ContentItemModel.upsertContent(existingContentItem);

    this.eventService.raiseEvent(EventFactory.archiveContentEvent(result, userId));
    this.elasticSearchGatheringService.indexContentItem(result.id);
    logger.info('Archived content item', { existingContentItem, userId });
    return result;
  }

  /**
   * Grab content by specifying the content ids to retrieve
   * @param contentIds - only pulls content for these specific ids
   * @param userId - adds user specific data to content (e.g. if they liked it)
   */
  async getContentForUserByIds(userId: number, contentIds: number[]): Promise<ContentItemModel[]> {
    logger.silly('ContentService.getContentForUserByIds', { userId, contentIds });
    return ContentItemModel.getContentItemsByIdsForUser(userId, contentIds);
  }


  async getContentById(contentId: number): Promise<?ContentItemModel> {
    logger.silly('ContentService.getContentById', { contentId });
    return ContentItemModel.getContentById(contentId);
  }

  async getContentItemForUser(userId: number, contentId: number): Promise<?ContentItemModel> {
    logger.silly('ContentService.getContentItemForUser', { userId, contentId });
    return ContentItemModel.getContentByIdForUser(userId, contentId);
  }

  async getByChannelIdForUser(userId: number, channelId: number, pageable: Pageable): Promise<*> {
    logger.silly('ContentService.getByChannelId', { channelId });
    const result = await ContentItemModel.getByChannelIdForUser(userId, channelId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Validates if user has access to a piece of content
   * @param userId
   * @param contentId
   */
  async doesUserHaveAccessToContent(userId: number, contentId: number): Promise<boolean> {
    logger.silly('ContentService.doesUserHaveAccessToContent', { userId, contentId });
    return !!await this.getContentItemForUser(userId, contentId);
  }

  async likeContent(userId: number, contentId: number): Promise<boolean> {
    logger.silly('ContentService.likeContent', { userId, contentId });

    const user = await UserModel.getUserById(userId);
    const result = await LikeModel.likeContent(user.id, user.organizationId, contentId);

    const contentItem = await ContentItemModel.getContentById(contentId);

    this.notificationService.addLikeNotification(userId, contentItem.authorId, contentId);
    this.eventService.raiseEvent(EventFactory.likeContentItemEvent(contentId, userId));
    this.elasticSearchGatheringService.indexContentItem(contentId, user.organizationId);

    return result;
  }

  async unlikeContent(userId: number, contentId: number): Promise<boolean> {
    logger.silly('ContentService.unlikeContent', { userId, contentId });

    const user = await UserModel.getUserById(userId);
    const result = await LikeModel.unlikeContent(user.id, user.organizationId, contentId);

    this.eventService.raiseEvent(EventFactory.unlikeContentItemEvent(contentId, userId));
    this.elasticSearchGatheringService.indexContentItem(contentId, user.organizationId);

    return result;
  }

  /**
   * Get a page of users that have liked a particular content item. No data about
   * the like or content is returned, just the users themselves in a list.
   */
  async getLikesByContentId(
    contentId: number,
    organizationId?: number, // when provided, filtered to users within this organization
    pageable: Pageable,
  ): Promise<IndexablePage<number, UserModel>> {
    logger.silly('ContentService.getLikesByContentId', { contentId, organizationId });

    const result = await LikeModel.getUsersByContentIdPaginated(contentId, organizationId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  /**
   * Gets a page of content for a given organization
   * @param userId - The authenticated user's id
   * @param userOrganizationId - The authenticated user's organizationId
   * @param contentOrganizationId - The id of the organization content is being requested for
   * @param include - Which content to include in the response
   * @param pageable - The configuration for the page the be returned
   * @returns {Promise.<IndexablePage.<number, ContentItemModel>>}
   */
  async getContentForOrganizationPaginated(
    userId: number,
    userOrganizationId: number,
    contentOrganizationId: number,
    include: string,
    pageable: Pageable,
    includeStatuses: ?ContentItemStatus[],
    searchText: ?string,
  ): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('ContentService.getContentForOrganizationPagination', {
      userId,
      userOrganizationId,
      include,
      contentOrganizationId,
    });
    const organization = await OrganizationModel.getById(contentOrganizationId);
    const channels = await organization.getSubscribedChannels();
    const channelIds = channels.map(channel => channel.id);
    const results = await this.elasticSearchService
      .getAllContent(
        contentOrganizationId,
        channelIds,
        include,
        pageable,
        includeStatuses,
        userId,
        searchText,
      );
    return results;
  }

  /**
   * Get tags of all published content available to an organization.
   */
  async getTagsFromContentForOrganization(organizationId: number, pageable: Pageable): * {
    logger.silly('ContentService.getTagsFromContentForOrganization', { organizationId });

    const organization = await OrganizationModel.getById(organizationId);
    const channels = await organization.getSubscribedChannels();
    const channelIds = channels.map(channel => channel.id);
    const results = await this.elasticSearchService.getTagsFromContent(organizationId, channelIds, pageable);

    return results;
  }

  /**
   * Get tags of all published content available to a particular user.
   */
  async getTagsFromContentForUser(userId: number, pageable: Pageable, daysAgo?: ?number): * {
    logger.silly('ContentService.getTagsFromContentForUser', { userId });
    const user = await UserModel.getUserById(userId);
    const channels = await user.getSubscribedChannels();
    const channelIds = channels.map(channel => channel.id);
    const publishedAfter = daysAgo ? moment().subtract(daysAgo, 'days') : undefined;

    const results = await this.elasticSearchService.getTagsFromContent(
      user.organizationId,
      channelIds,
      pageable,
      publishedAfter,
    );

    return results;
  }

  /**
   * Invokes the Lambda that kicks off Elastic Transcoder for the provided content item and file name.
   * @param userId
   * @param contentId
   * @param fileName
   * @returns {Promise.<TranscodeJobModel>}
   */
  async createTranscoderJob(
    organizationId: number, userId: number, fileName: string,
  ): Promise<TranscodeJobModel> {
    logger.debug('ContentService.createTranscoderJob', { userId, organizationId, fileName });

    const jobId: string = await this.mediaService
      .invokeTranscoderLambda(organizationId, userId, fileName);
    const videoUri = this.mediaService.getVideoCDNPath(organizationId, userId, fileName);
    const thumbnailUri = this.mediaService.getVideoPreviewCDNPath(organizationId, userId, fileName);

    const createdTranscodeJobModel: TranscodeJobModel =
      await TranscodeJobModel.create(userId, jobId, videoUri, thumbnailUri);

    logger.info('Created transcode job model', {
      userId, fileName, jobId, videoUri, thumbnailUri,
    });

    return createdTranscodeJobModel;
  }

  /**
   * Gets the current state of the elastic transcoder job for the provided content id and job id
   * @param id
   * @returns {Promise.<TranscodeJobModel>}
   */
  async getTranscodeJob(id: number): Promise<TranscodeJobModel> {
    logger.silly('ContentService.getTranscodeJob', { id });

    const result = await TranscodeJobModel.getById(id);

    if (!result) {
      throw new ResourceNotFoundError('Job not found');
    }
    return result;
  }

  /**
   * Updates the job indicated by the provided elastic transcoder job id by calling the the Elastic Transcoder service
   * to get the current status of the job
   *
   * @param transcoderJobId
   * @returns {Promise.<*>}
   */
  async updateTranscoderJob(transcoderJobId: string): Promise<TranscodeJobModel> {
    logger.debug('ContentService.updateTranscoderJob', { esJobId: transcoderJobId });

    const job = await TranscodeJobModel.getByJobId(transcoderJobId);
    if (!job) {
      throw new ResourceNotFoundError('Job not found');
    }

    if (job.status === TRANSCODE_JOB_STATUSES.COMPLETE) {
      // job already complete, don't update it
      logger.warn('Repeatedly calling complete on previously completed Elastic Transcoder job', { transcoderJobId });
      return job;
    }

    const { jobId } = job;
    const status: TranscodeJobStatus = await this.mediaService.getTranscoderJobStatus(jobId);

    logger.debug('Retrieved Elastic Transcoder Job status', { jobId, status });

    return job.update(status);
  }

  async bookmark(userId: number, contentId: number): Promise<boolean> {
    logger.silly('ContentService.bookmark', { userId, contentId });

    const result = await ContentItemModel.bookmark(userId, contentId);
    this.eventService.raiseEvent(EventFactory.contentBookmarkedEvent(contentId, userId));
    logger.info('Content bookmarked', { userId, contentId });

    return result;
  }

  async unbookmark(userId: number, contentId: number): Promise<boolean> {
    logger.silly('ContentService.unbookmark', { userId, contentId });

    const result = ContentItemModel.unbookmark(userId, contentId);
    this.eventService.raiseEvent(EventFactory.contentUnbookmarkedEvent(contentId, userId));
    logger.info('Content unbookmarked', { userId, contentId });

    return result;
  }

  async getBookmarkIdsForUser(userId: number): Promise<number[]> {
    logger.silly('ContentService.getBookmarkIdsForUser', { userId });
    return ContentItemModel.getBookmarkIdsForUser(userId);
  }

  async getBookmarksForUser(userId: number, pageable: Pageable): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('ContentService.getBookmarksForUser', { userId, pageable });
    const result = await ContentItemModel.getBookmarksForUserPaginated(userId, pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }
}
