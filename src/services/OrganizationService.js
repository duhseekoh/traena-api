// @flow
import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import Logger from '../common/logging/logger';
import OrganizationModel, { type Organization } from '../domain/models/OrganizationModel';
import UserModel from '../domain/models/UserModel';
import RoleModel from '../domain/models/RoleModel';
import AuthService from './AuthService';
import { type Auth0Role } from '../client/Auth0Client';
import ChannelService from './ChannelService';
import * as EventFactory from '../domain/EventFactory';
import EventService from './EventService';
import { CHANNEL_VISIBILITIES, ORGANIZATION_TYPES } from '../domain/fieldConstants';
import { BadRequestError, ConflictError, ResourceNotFoundError } from '../common/errors';
import { isDatabaseConflict } from '../common/errorUtils';
// eslint-disable-next-line
import type { ChannelDTO } from '../domain/models/ChannelModel';
import type ElasticSearchService from './ElasticSearchService';

// For some reason `import objection from 'objection'` doesn't work...
const objection = require('objection');

const logger = new Logger('services/OrganizationService');

function _createDefaultChannel(organization: OrganizationModel): ChannelDTO {
  let visibility = CHANNEL_VISIBILITIES.INTERNAL;

  if (organization.organizationType === ORGANIZATION_TYPES.TRAINING_COMPANY) {
    visibility = CHANNEL_VISIBILITIES.LIMITED;
  }

  return {
    name: 'General',
    description: '',
    organizationId: organization.id,
    visibility,
    default: true,
  };
}

export default class OrganizationService {
  authService: AuthService;
  eventService: EventService;
  channelService: ChannelService;
  elasticSearchService: ElasticSearchService;
  auth0ConnectionName: string;

  constructor(
    config: *, authService: AuthService, eventService: EventService,
    channelService: ChannelService, elasticSearchService: ElasticSearchService,
  ) {
    this.authService = authService;
    this.eventService = eventService;
    this.channelService = channelService;
    this.elasticSearchService = elasticSearchService;
    this.auth0ConnectionName = config.auth.connectionName;
  }

  async getAllOrganizations(pageable: Pageable): Promise<IndexablePage<number, OrganizationModel>> {
    logger.silly('OrganizationService.getAllOrganizationsPaginated', { pageable });
    const result = await OrganizationModel.getAllOrganizations(pageable);
    return new IndexablePage(result.results, result.total, pageable);
  }

  async get(organizationId: number): Promise<?OrganizationModel> {
    logger.silly('OrganizationService.get', { organizationId });
    return OrganizationModel.getById(organizationId);
  }

  async create(userId: number, organization: Organization): Promise<OrganizationModel> {
    logger.debug('OrganizationService.create', { userId, organization });
    try {
      const auth0Group = await this.authService.createOrganization(organization);

      organization.auth0OrganizationId = auth0Group._id;
      organization.auth0OrganizationName = organization.name;
      organization.auth0Connection = this.auth0ConnectionName;

      const createdOrg = await OrganizationModel.upsert(organization);
      this.eventService.raiseEvent(EventFactory.createOrganizationEvent(createdOrg, userId));

      await this.channelService.create(_createDefaultChannel(createdOrg), userId);
      logger.info('Created organization', { userId, createdOrg });

      return createdOrg;
    } catch (err) {
      if (isDatabaseConflict(err)) {
        logger.warn('Organization with name already exists', { organization });
        throw new ConflictError('Organization with name already exists');
      }
      logger.error('Unhandled organization creation error', { userId, organization });
      throw err;
    }
  }

  async deleteOrganization(userId: number, organizationId: number): Promise<boolean> {
    logger.debug('OrganizationService.deleteOrganization', { userId, organizationId });

    // Run this in a transaction to make sure we at least have all the organization's users marked as disabled
    // TODO: come up with approach to update all disabled users into auth0 as disabled
    const areDisabled = await objection.transaction(
      OrganizationModel, UserModel,
      async (BoundOrganization, BoundUser) => {
        const [orgsUpdated, usersUpdated, totalUsers] = await Promise.all([BoundOrganization.disable(organizationId),
          BoundUser.disableForOrganization(organizationId),
          BoundUser.getCountForOrganization(organizationId),
        ]);
        // Make sure the number of orgs updated is 1 and that the number of users updated is === the total users in org
        if (!orgsUpdated === 1 && usersUpdated === totalUsers) {
          logger.error('Disable record totals did not match', { orgsUpdated, usersUpdated, totalUsers });
          throw new Error('Could not disabled organization');
        }
        return true;
      },
    );

    const disabledOrg = await OrganizationModel.getById(organizationId);
    this.eventService.raiseEvent(EventFactory.disableOrganizationEvent(disabledOrg, userId));

    logger.info('Disabled organization: ', { userId, organizationId, isDisabled: areDisabled });
    return areDisabled;
  }

  async update(organization: OrganizationModel): Promise<OrganizationModel> {
    logger.silly('OrganizationService.update', { organization });

    const existingOrganization = await this.get(organization.id);

    if (!existingOrganization) {
      logger.error('Organization to update does not exist', { organization });
      throw new ResourceNotFoundError(`Organization with id: ${organization.id} does not exist`);
    }

    if (organization.isDisabled === true && existingOrganization.isDisabled !== true) {
      logger.warn('Organizations must be disabled via the DELETE operation', { organization });
      throw new BadRequestError('Organizations must be disabled via the DELETE operation');
    }

    return OrganizationModel.upsert(organization);
  }

  async getCommonSearchTermsForOrganization(organizationId: number): Promise<string[]> {
    logger.silly('OrganizationService.getCommonSearchTermsForOrganization', { organizationId });

    const organization = await OrganizationModel.getById(organizationId);
    return organization.commonSearchTerms || [];
  }

  async setCommonSearchTermsForOrganization(organizationId: number, terms: string[]): Promise<string[]> {
    logger.debug('OrganizationService.setCommonSearchTermsForOrganization', { organizationId, terms });

    return OrganizationModel.updateCommonSearchTerms(organizationId, terms);
  }

  async getOrganizationRoles(organizationId: number): Promise<RoleModel[]> {
    const organization = await this.get(organizationId);

    if (!organization) {
      throw new ResourceNotFoundError(`Organization with id: ${organizationId} does not exist`);
    }
    // roles from auth0 that we are exposing
    const assignableRoles = (await RoleModel.getAssignableRoles());
    // roles from auth0 assigned to this organization
    const organizationAuth0Roles: Auth0Role[] =
      await this.authService.getOrganizationRoles(organization.auth0OrganizationId);
    const organizationAuth0RoleIds = organizationAuth0Roles.map(role => role._id);
    // return just the organization roles that are assignable
    return assignableRoles.filter(role => organizationAuth0RoleIds.includes(role.auth0RoleId));
  }
}
