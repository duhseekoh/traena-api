// @flow
import _ from 'lodash';
import type { SES$20101201 } from 'aws-sdk';
import { SES } from 'aws-sdk';
import Logger from '../common/logging/logger';
import type { AppConfig } from '../common/config';
import welcomePasswordHtml from '../domain/email-templates/welcome-password.html';

const logger = new Logger('services/EmailService');
// Setup the email templates and allow for {{ variable }} interpolation so its the same as auth0 email interpolation
const emailTemplateSettings = { interpolate: /{{([\s\S]+?)}}/g };
const welcomePasswordTpl = _.template(welcomePasswordHtml, emailTemplateSettings);

type SESClient = SES$20101201;

type EmailSendType = {
  from: string,
  to: string[],
  subject: string,
  body: string,
};

export default class EmailService {
  config: AppConfig;
  ses: SESClient;

  constructor(config: AppConfig) {
    this.config = config;
    this.ses = new SES({ apiVersion: '2010-12-01' });
    return this;
  }

  sendEmail(opts: EmailSendType): Promise<*> {
    logger.silly('EmailService.sendEmail', { opts });
    const awsParams = {
      Destination: {
        ToAddresses: opts.to,
      },
      Message: {
        Body: {
          Html: {
            Data: opts.body,
          },
        },
        Subject: {
          Data: opts.subject,
        },
      },
      Source: opts.from,
    };
    logger.debug('Sending email', { awsParams });
    const emailSendPromise: Promise<*> = this.ses.sendEmail(awsParams).promise();
    emailSendPromise.catch((err) => {
      logger.error('Error sending email', { awsParams, err });
      throw err;
    });
    return emailSendPromise;
  }

  buildWelcomePasswordHtmlBody(changePasswordUrl: string) {
    return welcomePasswordTpl({ url: changePasswordUrl, environment: this.config.env.name });
  }
}
