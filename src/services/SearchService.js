// @flow

import { IndexablePage, Pageable } from '@panderalabs/koa-pageable';
import UserModel from '../domain/models/UserModel';
import Logger from '../common/logging/logger';
import type ElasticSearchService from './ElasticSearchService';
import type ContentService from './ContentService';
import type ContentItemModel from '../domain/models/ContentItemModel';
import type EventService from './EventService';
import { searchedContentEvent } from '../domain/EventFactory';

const logger = new Logger('services/SearchService');

export default class SearchService {
  elasticSearchService: ElasticSearchService;
  contentService: ContentService;
  eventService: EventService;

  constructor(elasticSearchService: ElasticSearchService, contentService: ContentService, eventService: EventService) {
    this.elasticSearchService = elasticSearchService;
    this.contentService = contentService;
    this.eventService = eventService;
  }

  /**
   * @param {string} userId - current user
   * @param {string} searchText - query
   */
  async search(
    userId: number, searchText: ?string, tags: ?string[],
    channelIds: ?number[], pageable: Pageable,
  ): Promise<IndexablePage<number, ContentItemModel>> {
    logger.silly('SearchService.search', { userId, searchText, pageable });

    const user: UserModel = await UserModel.getUserById(userId);
    const subscriptions = await user.getSubscribedChannels();
    let subscribedChannelIds = subscriptions.map(s => s.id);
    if (channelIds) {
      subscribedChannelIds = channelIds.filter(c => channelIds && channelIds.includes(c));
    }
    const esContentResults = await this.elasticSearchService.searchContent(
      user.organizationId, subscribedChannelIds,
      searchText, tags, pageable,
    );

    const contentItems: ContentItemModel[] = esContentResults.hits.hits.map(item => item._source);
    const totalElements = esContentResults.hits.total;
    logger.debug('search items being returned', { count: contentItems.length });
    this.eventService.raiseEvent(searchedContentEvent(
      userId,
      searchText,
      tags,
      pageable.page,
      contentItems.map(item => item.id),
    ));

    return new IndexablePage(contentItems, totalElements, pageable);
  }

  /**
   * @param {string} userId - current user
   * @param {string}
   */
  async searchUsers(userId: number, searchText: string): Promise<any> {
    logger.silly('SearchService.searchUsers', { userId, searchText });
    const user = await UserModel.getUserById(userId);
    const results = await this.elasticSearchService.searchUsers(user.organizationId, searchText);

    return results;
  }
}
