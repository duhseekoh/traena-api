// @flow
// Ugh s3 type def doesn't contain upload or getSignedUrl definitions
import Lambda from 'aws-sdk/clients/lambda';
import S3 from 'aws-sdk/clients/s3';
import ElasticTranscoder from 'aws-sdk/clients/elastictranscoder';
import _ from 'lodash';
import fs from 'fs-extra';
import path from 'path';
import uuidv4 from 'uuid/v4';
import mime from 'mime-types';
import type {
  ElasticTranscoder$20120925,
  ElasticTranscoder$20120925$ReadJobResponse,
  Lambda$20150331,
  Lambda$20150331$InvocationResponse,
} from 'aws-sdk';
import Logger from '../common/logging/logger';
import type { TranscodeJobStatus } from '../domain/fieldConstants';
import { removeSpecialChars } from '../common/stringUtils';
import { TRANSCODE_JOB_STATUSES } from '../domain/fieldConstants';
import { AwsError } from '../common/errors';
import { TEMP_IMAGE_UPLOAD_PATH } from './constants';
import type { AppConfig } from '../common/config';

type LambdaClient = Lambda$20150331;
type InvocationResponse = Lambda$20150331$InvocationResponse;
type ElasticTranscoderClient = ElasticTranscoder$20120925;
type ElasticTranscoderJobResponse = ElasticTranscoder$20120925$ReadJobResponse;

export type Image = {
  format: string,
  width: number,
  height: number,
  filename: string,
};

export type ImageCollection = {
  id: string,
  bucket: string,
  baseKey: string,
  baseCDNPath?: string,
  variations: {
    tiny: Image,
    small: Image,
    medium: Image,
    large: Image,
    original: Image,
  }
};

const logger = new Logger('services/MediaService');

export default class MediaService {
  baseUrl: string;
  s3BucketName: string;
  s3: *;
  lambda: LambdaClient;
  elasticTranscoder: ElasticTranscoderClient;
  transcoderLambdaName: string;
  imageProcessorLambdaName: string;

  constructor(config: AppConfig) {
    this.baseUrl = config.aws.cdnBaseUrl;
    this.s3BucketName = config.aws.contentBucket;
    this.transcoderLambdaName = config.aws.transcoderLambdaName;
    this.imageProcessorLambdaName = config.aws.imageProcessorLambdaName;
    this.s3 = new S3();
    this.lambda = new Lambda();
    this.elasticTranscoder = new ElasticTranscoder();
  }

  /**
   * S3 key for the outputted video playlist file
   * The transcoder lambda is what is creating this key, based on the input key.
   */
  getVideoKey(orgId: number, userId: number, fileName: string) {
    const fileNameNoExtension: string = path.parse(fileName).name;
    return `organizations/${orgId}/users/${userId}/videos/${fileNameNoExtension}/video-output/video.m3u8`;
  }

  /**
   * S3 key for the first outputted thumbnail image
   * The transcoder lambda is what is creating this key, based on the input key.
   */
  getVideoPreviewKey(orgId: number, userId: number, fileName: string) {
    const fileNameNoExtension: string = path.parse(fileName).name;
    return `organizations/${orgId}/users/${userId}/videos/${fileNameNoExtension}/images/thumbnail-00001.png`;
  }

  /**
   * S3 'directory', key without filename and extension
   * e.g. where the user is uploading their video file to
   */
  getInputVideoFolderKey(orgId: number, userId: number, fileName: string) {
    const fileNameNoExtension: string = path.parse(fileName).name;
    return `organizations/${orgId}/users/${userId}/videos/${fileNameNoExtension}/video-input/`;
  }

  /**
   * S3 key where an mp4 upload is stored
   */
  getInputVideoKey(orgId: number, userId: number, fileName: string) {
    return `${this.getInputVideoFolderKey(orgId, userId, fileName)}${fileName}`;
  }

  getImageKey(orgId: number, userId: number, contentId: number, imageId: string, fileName: string) {
    return `organizations/${orgId}/users/${userId}/content/${contentId}/images/${imageId}/${fileName}`;
  }

  getTempInputImageKey(filename: string) {
    return `${TEMP_IMAGE_UPLOAD_PATH}${filename}`;
  }

  getTempImagePath(imageId: string) {
    return `${TEMP_IMAGE_UPLOAD_PATH}${imageId}`;
  }

  getTempImageKey(imageId: string, filename: string) {
    return `${this.getTempImagePath(imageId)}/${filename}`;
  }

  getVideoCDNPath(orgId: number, userId: number, fileName: string) {
    return `${this.baseUrl}/${this.getVideoKey(orgId, userId, fileName)}`;
  }

  getVideoPreviewCDNPath(orgId: number, userId: number, fileName: string) {
    return `${this.baseUrl}/${this.getVideoPreviewKey(orgId, userId, fileName)}`;
  }

  getImageBaseCDNPath(orgId: number, userId: number, contentId: number, imageId: string) {
    return `${this.baseUrl}/organizations/${orgId}/users/${userId}/content/${contentId}/images/${imageId}/`;
  }

  getTempImageCDNPath(fileName: string) {
    const fileNameNoExtension: string = path.parse(fileName).name;
    return `${this.baseUrl}/${TEMP_IMAGE_UPLOAD_PATH}${fileNameNoExtension}/`;
  }

  async _getSignedS3UploadUrl(key: string): Promise<{ uploadUrl: string, contentType: string }> {
    logger.silly('MediaService._getSignedS3UploadUrl. ', { key });

    // determine content-type, important this is set here and on the client uploading to this url
    //  https://github.com/jshttp/mime-types#mimecontenttypetype
    const contentType = mime.contentType(path.extname(key)) || 'application/octet-stream';

    return new Promise((resolve, reject) => {
      this.s3.getSignedUrl('putObject', {
        Bucket: this.s3BucketName,
        Key: key,
        ContentType: contentType,
        Expires: 300, // TODO - temp increase while testing
      }, (err, url) => {
        if (err) {
          logger.error('Failed to create signed url', {
            err, key, url, contentType,
          });
          reject(err);
        } else {
          logger.debug('Created signed url', { url, key, contentType });
          resolve({ uploadUrl: url, contentType });
        }
      });
    });
  }

  async getSignedS3VideoUploadUrl(
    orgId: number, userId: number, fileName: string,
  ): Promise<{ uploadUrl: string, fileName: string, contentType: string }> {
    logger.silly('MediaService.getSignedS3VideoUploadUrl. ', {
      orgId, userId, fileName,
    });

    const guid = uuidv4();
    const parsedFileName = path.parse(fileName);
    // add max of 8 characters of original filename, removing special chars and spaces
    const shortName = removeSpecialChars(parsedFileName.name).substring(0, 8);
    const extension = parsedFileName.ext;
    const generatedFileName = `${guid}-${shortName}${extension}`;
    const key = this.getInputVideoKey(orgId, userId, generatedFileName);

    const { uploadUrl, contentType } = await this._getSignedS3UploadUrl(key);
    logger.debug('Created signed video url', {
      uploadUrl, generatedFileName, key, contentType,
    });

    return {
      uploadUrl,
      id: guid,
      fileName: generatedFileName,
      contentType,
    };
  }

  async getSignedS3ImageUploadUrl(fileName: string): Promise<{
    uploadUrl: string, fileName: string, contentType: string
  }> {
    logger.silly('MediaService.getSignedS3ImageUploadUrl. ', { fileName });

    const guid = uuidv4();
    const parsedFileName = path.parse(fileName);
    const extension = parsedFileName.ext;
    const generatedFileName = `${guid}${extension}`;
    const key = this.getTempInputImageKey(generatedFileName);

    const { uploadUrl, contentType } = await this._getSignedS3UploadUrl(key);
    logger.debug('Created signed image url', {
      uploadUrl, generatedFileName, key, contentType,
    });

    return {
      contentType,
      id: guid,
      uploadUrl,
      fileName: generatedFileName,
    };
  }

  /**
   * Uploads files to s3
   * @param  {File, Buffer, String} file Either a file buffer/stream or a path to the file upload on S3
   * @param  {String} s3path Key to
   * @return {Promise<{}>}  Promise that will be called upon successful upload
   */
  uploadFile(file: File | Buffer | String, s3path: string) {
    logger.debug('MediaService.uploadFile. ', { s3path });

    let fileStream = file;
    if (typeof file === 'string') {
      fileStream = fs.createReadStream(file);
    }
    return this.s3.upload({
      Body: fileStream,
      Bucket: this.s3BucketName,
      Key: s3path,
    }).promise();
  }

  async invokeTranscoderLambda(orgId: number, userId: number, fileName: string): Promise<string> {
    logger.silly('MediaService.invokeTranscoderLambda. ', {
      orgId, userId, fileName,
    });

    const key: string = this.getInputVideoKey(orgId, userId, fileName);

    const params = {
      FunctionName: this.transcoderLambdaName,
      InvocationType: 'RequestResponse',
      LogType: 'None',
      Payload: `{ "key": "${key}" }`,
    };

    const data: InvocationResponse = await this.lambda.invoke(params).promise();

    const error = data.FunctionError;
    const status: ?number = data.StatusCode;

    if (error || (status && (status < 200 || status >= 300))) {
      logger.error('Error invoking Elastic Transcoder lambda', { error, status });
      throw new AwsError('Error invoking Elastic Transcoder lambda');
    }

    const payload = data.Payload;
    let jobId: string;

    if (typeof payload === 'string') {
      const parsedPayload: * = JSON.parse(payload);
      jobId = parsedPayload.Job.Id;
    } else {
      // This shouldn't actually ever happen, but cuz flow, we have to check the type
      logger.error('Unexpected Elastic Transcoder lambda Response', { status, payload });
      throw new AwsError('Unexpected Elastic Transcoder lambda response');
    }

    logger.info('Invoked Elastic Transcoder lambda for JobId. ', jobId);

    return jobId;
  }

  /**
   * Retrieves a transcoding job status from AWS. Used in the process of updating
   * @param  jobId - AWS generated transcoder job id
   */
  async getTranscoderJobStatus(jobId: string): Promise<TranscodeJobStatus> {
    logger.silly('MediaService.getTranscoderJobStatus. ', { jobId });

    try {
      const params = { Id: jobId };
      const result: ElasticTranscoderJobResponse = await this.elasticTranscoder.readJob(params).promise();

      if (!result || !result.Job) {
        logger.error('Elastic Transcoder readJob response does not contain expected job information', { result });
        throw new AwsError('Result does not contain Job information');
      }

      // One of: Submitted, Progressing, Complete, Canceled, or Error
      const status = result.Job.Status;

      if (status === 'Error' || status === 'Canceled') {
        return TRANSCODE_JOB_STATUSES.ERROR;
      }

      if (status === 'Complete') {
        return TRANSCODE_JOB_STATUSES.COMPLETE;
      }

      return TRANSCODE_JOB_STATUSES.IN_PROGRESS;
    } catch (error) {
      logger.error('Error reading AWS Job status for Elastic Transcoder job', { jobId, error });
      throw new AwsError(`Error reading AWS job status for Elastic Transcoder Job: ${jobId}`);
    }
  }

  async invokeImageProcessorLambda(fileName: string): Promise<Object> {
    logger.silly('MediaService.invokeImageProcessorLambda. ', { fileName });

    const key: string = this.getTempInputImageKey(fileName);

    const params = {
      FunctionName: this.imageProcessorLambdaName,
      InvocationType: 'RequestResponse',
      LogType: 'None',
      Payload: JSON.stringify({
        key,
        bucket: this.s3BucketName,
      }),
    };

    const data: InvocationResponse = await this.lambda.invoke(params).promise();

    const error = data.FunctionError;
    const status: ?number = data.StatusCode;

    if (error || (status && (status < 200 || status >= 300))) {
      logger.error('Error invoking Image Processor lambda', { error, status, payload: data.Payload });
      throw new AwsError('Error invoking Image Processor lambda');
    }

    const payload = data.Payload;
    let parsedPayload: ImageCollection;

    if (typeof payload === 'string') {
      parsedPayload = JSON.parse(payload);
      parsedPayload.baseCDNPath = this.getTempImageCDNPath(fileName);
      parsedPayload.id = path.parse(fileName).name;
    } else {
      // This shouldn't actually ever happen, but cuz flow, we have to check the type
      logger.error('Unexpected Image Processor lambda Response', { status, payload });
      throw new AwsError('Unexpected Image Processor lambda response');
    }

    logger.info('Received process images from Image Processor lambda', { parsedPayload, fileName });

    return _.omit(parsedPayload, ['bucket', 'baseKey']);
  }

  /**
   * Deletes all images from the temp storage bucket that have a given prefix (aka path) for the fileId. If there is a
   * failure, it is ignored as it will eventually be removed by the cleanup process.
   *
   * @param imageId
   * @returns {Promise.<void>}
   * @private
   */
  async _deleteTempImages(imageId: string): Promise<void> {
    logger.silly('MediaService._deleteTempImages. ', { imageId });

    const tempImagePath = this.getTempImagePath(imageId);

    // Get listing of all the files with this fileid prefix in the temp directory
    const result = await this.s3.listObjects({
      Bucket: this.s3BucketName,
      Prefix: tempImagePath,
    }).promise();

    _.forEach(result.Contents, async (content) => {
      const key = content.Key;
      logger.debug('Deleting temp content with key: ', { key });

      try {
        const deleteResult = await this.s3.deleteObject({
          Bucket: this.s3BucketName,
          Key: key,
        }).promise();
        return deleteResult;
      } catch (e) {
        logger.error('Error Deleting temp file, continuing.: ', { e });
      }
      return Promise.resolve();
    });
  }

  /**
   * Copies a single image from temp to permanent storage
   * @param orgId
   * @param userId
   * @param contentId
   * @param imageId
   * @param image
   * @returns {Promise.<void>}
   * @private
   */
  async _copyImage(orgId: number, userId: number, contentId: number, imageId: string, image: Image) {
    logger.silly('MediaService._moveImage. ', {
      orgId, userId, contentId, image,
    });

    const tempKey = this.getTempImageKey(imageId, image.filename);
    const permanentKey = this.getImageKey(orgId, userId, contentId, imageId, image.filename);
    const copySource = `${this.s3BucketName}/${tempKey}`;

    logger.debug('Copying from s3 source to target ', { source: copySource, target: permanentKey });

    try {
      await this.s3.copyObject({
        Bucket: this.s3BucketName,
        CopySource: copySource,
        Key: permanentKey,
      }).promise();
    } catch (e) {
      logger.error('Error Copying Temporary image File to permanent location: ', { e });
      throw new AwsError('Unable to copy temporary image file. Please try again');
    }
  }

  /**
   * Moves all variations of an image from temp to permanent storage. Will throw an exception if any copy fails
   * @param orgId
   * @param userId
   * @param contentId
   * @param imageCollection
   * @returns {Promise.<ImageCollection>}
   * @private
   */
  async _moveImageCollection(
    orgId: number, userId: number, contentId: number,
    imageCollection: ImageCollection,
  ): Promise<ImageCollection> {
    logger.silly('MediaService._moveImageCollection ', {
      orgId, userId, contentId, imageCollection,
    });

    if (imageCollection.baseCDNPath && imageCollection.baseCDNPath.includes('/tmp/')) {
      const imageId = imageCollection.id;

      logger.debug('Moving temporary images for image', { imageId });

      await this._copyImage(orgId, userId, contentId, imageId, imageCollection.variations.tiny);
      await this._copyImage(orgId, userId, contentId, imageId, imageCollection.variations.small);
      await this._copyImage(orgId, userId, contentId, imageId, imageCollection.variations.medium);
      await this._copyImage(orgId, userId, contentId, imageId, imageCollection.variations.large);
      await this._copyImage(orgId, userId, contentId, imageId, imageCollection.variations.original);

      // if no exceptions from above, we are okay to delete the source images, no need to wait for this to complete
      this._deleteTempImages(imageId);

      imageCollection.baseCDNPath = this.getImageBaseCDNPath(orgId, userId, contentId, imageId);
    }

    logger.info('BaseCDNPath: 2', { basePath: imageCollection.baseCDNPath });
    return imageCollection;
  }

  /**
   * If this Collection contains any images stored in the temp path, move the images to permanent storage, and update
   * the collection's baseCDNPath to point to the correct permanent storage location
   * @param orgId - Content Owner's Organization
   * @param userId - Content Creator's User Id
   * @param contentId - Content Id
   * @param imageCollection
   * @returns {Promise.<*>}
   */
  async moveTempImages(
    orgId: number, userId: number, contentId: number,
    imagesCollections: ImageCollection[],
  ): Promise<ImageCollection[]> {
    logger.silly('MediaService.moveTempImages ', {
      orgId, userId, contentId, imagesCollections,
    });

    return Promise.all(_.map(
      imagesCollections,
      async imageCollection => this._moveImageCollection(orgId, userId, contentId, imageCollection),
    ));
  }
}
