// @flow
import type moment$Moment from 'moment';
import Logger from '../common/logging/logger';
import type { Event, CreateEventDTO } from '../domain/models/EventModel'; // eslint-disable-line import/named
import EventModel, { EventTypes, type EventType } from '../domain/models/EventModel';
import type SegmentClient from '../client/SegmentClient';

const logger = new Logger('services/EventService');

export default class EventService {
  segmentClient: SegmentClient;

  constructor(segmentClient: SegmentClient) {
    this.segmentClient = segmentClient;
  }

  async raiseEvent(event: ?Event | ?CreateEventDTO): Promise<void> {
    logger.silly('EventService.raiseEvent', { event });
    try {
      if (event) {
        const insertedEvent: EventModel = await EventModel.insert(event);
        if (insertedEvent) { // use the insertedEvent because it has a createdTimestamp
          this._sendSegmentMessage(insertedEvent.toJSON());
        }
      } else {
        logger.warn('Not storing event as it was null');
      }
    } catch (err) {
      logger.warn('Error saving event record', { err });
      logger.warn(err);
    }
  }

  /**
  * Get the date of the last time a user performed a given event
  * @param userId the user for whom you want the last event date
  * @param eventType the type of event you want the last instance of for the user
  * @returns {Promise<?EventModel>} the user's last event
  */
  async getLatestEvent(userId: number, eventType: EventType): Promise<?EventModel> {
    logger.silly('EventService.getLatestEvent', { userId, eventType });
    const result = await EventModel.getLatestEvent(
      userId,
      eventType,
    );
    return result;
  }

  async getViewedContentIds(userId: number, minDate: moment$Moment, maxDate: moment$Moment) {
    logger.silly('EventService.getViewedContentIds', { userId, minDate, maxDate });
    const events = await EventModel.getEventsInRange(
      userId,
      EventTypes.CONTENT_VIEWED,
      minDate,
      maxDate,
    );
    return events.map(event => event.data.contentId);
  }

  /**
   * Whenever an event is added to the database, we also want to push it out
   * to segment.
   */
  _sendSegmentMessage(event: Event) {
    if (event.type === EventTypes.IDENTIFY) {
      this.segmentClient.identify({
        userId: event.userId,
        traits: event.data,
      });
      return;
    }

    const eventData = event.data ? event.data : {};
    // use timestamp in event data if it exists, otherwise use the db insertion time
    let timestamp;
    if (eventData.timestamp) {
      timestamp = new Date(eventData.timestamp);
    } else if (event.createdTimestamp) {
      timestamp = new Date(event.createdTimestamp);
    }

    if (event.type === EventTypes.SCREEN) {
      this.segmentClient.screen({
        userId: event.userId,
        name: eventData.name,
        properties: eventData,
        timestamp,
      });
      return;
    }

    if (event.type === EventTypes.PAGE) {
      this.segmentClient.page({
        userId: event.userId,
        name: eventData.name,
        properties: eventData,
        timestamp,
      });
      return;
    }

    this.segmentClient.track({
      userId: event.userId,
      event: event.type,
      properties: eventData,
      timestamp,
    });
  }
}
