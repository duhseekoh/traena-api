// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type moment$Moment from 'moment';
import moment from 'moment';
import type { Context } from 'koa';
import { formatChannel } from '../formatters/channelFormatters';
import { formatContentItem } from '../formatters/contentItemFormatters';
import formatSeries from '../formatters/seriesFormatter';
import { formatUser } from '../formatters/userFormatters';
import { parseIntOrThrow, parseOptionalIntOrThrow } from '../../common/numberUtils';
import { hasPermission, permissionMiddlewareFactory } from '../middleware/auth';
import { Contexts, getPermission, Operations, Resources } from '../permissions/permissionUtils';
import * as r from '../../common/routerUtils';
import type SeriesModel from '../../domain/models/SeriesModel';
import type TaskService from '../../services/TaskService';
import type TaskModel from '../../domain/models/TaskModel';
import type ChannelModel from '../../domain/models/ChannelModel';
import type ActivityResponseModel from '../../domain/models/ActivityResponseModel';

function _getTaskIdFromPath(ctx: Context): number {
  return r.queryParamToInt(ctx.params.taskId, 'taskId');
}

function _getContentIdFromQuery(ctx: Context): number {
  return r.queryParamToInt(ctx.query.contentId, 'contentId');
}

function _getActivityIdFromQuery(ctx: Context): number {
  return r.queryParamToInt(ctx.query.activityId, 'activityId');
}

function _getSeriesIdFromQuery(ctx: Context): ?number {
  return r.optionalQueryParamToInt(ctx.query.seriesId);
}

function _getChannelIdFromQuery(ctx: Context): ?number {
  return r.optionalQueryParamToInt(ctx.query.channelId);
}

function _getActivityResponseIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.responseId);
}

function _getMaxDate(ctx: Context): moment$Moment | void {
  return ctx.query.maxDateTimestamp ? moment(ctx.query.maxDateTimestamp) : undefined;
}

/**
 * Not all permissions are checked as 'read:self:*' is assumed for all users, and all users can register devices and
 * notifications
 */
const selfRouter = new Router();

const modifySelfTaskPermission: ?string = getPermission(Resources.TASKS, Operations.MODIFY, Contexts.SELF);
const modifySelfActivityResponsePermission: ?string =
 getPermission(Resources.ACTIVITY_RESPONSES, Operations.MODIFY, Contexts.SELF);

const canModifyActivityResponse = permissionMiddlewareFactory(
  Resources.ACTIVITY_RESPONSES, Operations.MODIFY,
  ctx => ctx.state.services.activityService.getActivityResponse(_getActivityResponseIdFromPath(ctx)),
  (ctx, activityResponse) => ctx.state.user.id === activityResponse.userId,
  () => false,
);

const canReadActivityResponse = permissionMiddlewareFactory(
  Resources.ACTIVITY_RESPONSES, Operations.READ,
  ctx => ctx.state.services.activityService.getActivityResponse(_getActivityResponseIdFromPath(ctx)),
  (ctx, activityResponse) => ctx.state.user.id === activityResponse.userId,
  () => false,
);

// All users can read their own profile
selfRouter.get('/', async (ctx: Context) => {
  const userService = r.userService(ctx);
  const userId = r.getUserId(ctx);
  const result = r.assertResource(await userService.getProfileForUser(userId));
  ctx.body = formatUser(result);
});

selfRouter.get('/feed', paginate, async (ctx: Context) => {
  const feedService = r.feedService(ctx);
  const user = r.getUser(ctx);
  const { pageable } = ctx.state;
  const result = await feedService.getFeedForUser(user, pageable);
  ctx.body = result.map(formatContentItem);
});

selfRouter.get('/feed/whatsnew', paginate, async (ctx: Context) => {
  const feedService = r.feedService(ctx);
  const user = r.getUser(ctx);
  const { pageable } = ctx.state;
  const result = await feedService.getWhatsNewForUser(user, pageable);
  ctx.body = result.map(formatContentItem);
});

selfRouter.get('/tasks', async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = r.queryParamToInt(ctx.query.contentId, 'contentId');
  ctx.body = await taskService.getTasksForContentId(userId, contentId);
});

/**
 * Add new task (traena list item)
 * Body: {contentId}
 */
selfRouter.post('/tasks', hasPermission(modifySelfTaskPermission), async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = _getContentIdFromQuery(ctx);

  ctx.body = r.assertModifyResourceResult(await taskService.create(userId, contentId));
  ctx.status = 201;
});

/**
 * Get list of completed contentIds
 * this is leveraging this endpoint for the 'done'
 * concept until that data model is implemented
 */
selfRouter.get('/tasks/completions', async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);

  ctx.body = r.assertModifyResourceResult(await taskService.getCompletedTaskContentIds(userId));
  ctx.status = 201;
});

/**
 * Add new completed task (traena list item)
 * this is leveraging this endpoint for the 'done'
 * concept until that data model is implemented
 * Body: {contentId}
 */
selfRouter.post('/tasks/completions', hasPermission(modifySelfTaskPermission), async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = _getContentIdFromQuery(ctx);

  ctx.body = r.assertModifyResourceResult(await taskService.createCompleted(userId, contentId));
  ctx.status = 201;
});

/**
 * Update existing task
 * Body: {status, rating, actions}
 */
selfRouter.put('/tasks/:taskId', hasPermission(modifySelfTaskPermission), async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);
  const taskItem: TaskModel = r.getPutBody(ctx, _getTaskIdFromPath);

  r.assertResource(await taskService.get(_getTaskIdFromPath(ctx)));
  ctx.body = r.assertModifyResourceResult(await taskService.update(userId, taskItem));
});

selfRouter.get('/tasks/:taskId', async (ctx: Context) => {
  const { taskService }: { taskService: TaskService } = ctx.state.services;
  const taskId: number = parseIntOrThrow(ctx.params.taskId);
  ctx.body = r.assertResource(await taskService.get(taskId));
});

/**
 * Update existing traena list item.
 * Body: {status, rating, actions}
 */
selfRouter.delete('/tasks/:taskId', hasPermission(modifySelfTaskPermission), async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);
  const taskId = _getTaskIdFromPath(ctx);

  r.assertResource(await taskService.get(taskId));
  r.assertModifyResourceResult(await taskService.deleteTask(userId, taskId));
  ctx.status = 204;
});

selfRouter.get('/contentStatuses', async (ctx: Context) => {
  const taskService = r.taskService(ctx);
  const userId = r.getUserId(ctx);

  ctx.body = await taskService.getContentStatuses(userId);
});

/**
 * Get how many unique content items a user has completed for a particular series,
 * or for all series if no seriesId is provided
 */
selfRouter.get('/seriesprogress', async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const seriesId = _getSeriesIdFromQuery(ctx);
  const userId = r.getUserId(ctx);

  ctx.body = await seriesService.getSeriesProgressForUser(userId, seriesId);
});

/**
 * Get user notifications
 */
selfRouter.get('/notifications', paginate, async (ctx: Context) => {
  const notificationService = r.notificationService(ctx);
  const userId = r.getUserId(ctx);
  const maxDate = _getMaxDate(ctx);
  const { pageable } = ctx.state;

  ctx.body = await notificationService.getNotificationsForUser(userId, maxDate, pageable);
});

/**
 * Get unacknowledged notification count
 */
selfRouter.get('/notifications/new/count', async (ctx: Context) => {
  const notificationService = r.notificationService(ctx);
  const userId = r.getUserId(ctx);

  const count = await notificationService.getUnacknowledgedNotificationCount(userId);
  ctx.body = {
    count,
  };
});

/**
 * Mark all unacknowledged notifications as acknowledged
 */
selfRouter.put('/notifications/new/count', async (ctx: Context) => {
  const notificationService = r.notificationService(ctx);
  const userId = r.getUserId(ctx);

  await notificationService.acknowledgeNotificationsForUser(userId);
  ctx.body = { status: 'acknowledged' };
});


selfRouter.put('/devices/:deviceToken', async (ctx: Context) => {
  const notificationService = r.notificationService(ctx);
  const userId = r.getUserId(ctx);
  const deviceToken: string = r.assertPathParam(ctx.params.deviceToken);
  const platform: string = r.assertQueryParam(ctx.query.platform, 'platform');

  await notificationService.registerDeviceToken(userId, deviceToken, platform);
  ctx.status = 204;
});

selfRouter.delete('/devices/:deviceToken', async (ctx: Context) => {
  const notificationService = r.notificationService(ctx);
  const deviceToken: string = r.assertPathParam(ctx.params.deviceToken);

  await notificationService.deregisterDeviceToken(deviceToken);
  ctx.status = 204;
});

selfRouter.get('/likes', async (ctx: Context) => {
  const userService = r.userService(ctx);
  const userId = r.getUserId(ctx);

  ctx.body = await userService.getLikesForUser(userId);
});


selfRouter.get('/analytics', async (ctx: Context) => {
  const analyticsService = r.analyticsService(ctx);
  const userId = r.getUserId(ctx);
  const offset: string = r.assertQueryParam(ctx.query.offset, 'offset');
  const daysOfHistory: ?number = parseOptionalIntOrThrow(ctx.query.daysOfHistory);

  ctx.body = {
    activityStreak: await analyticsService.getCurrentActivityStreakForUser(userId, offset),
    activity: await analyticsService.getUserActivityAnalytics(userId, offset, daysOfHistory),
    completedTasks: await analyticsService.getUserTasksCompletedAnalytics(userId, offset, daysOfHistory),
    comments: await analyticsService.getUserCommentsAddedAnalytics(userId, offset, daysOfHistory),
  };
});

/**
 * Get all channels that a user has access to.
 *
 * When operating on a user, prefer this to organizations/{id}/subscriptions - as this could be extended at some point
 * to support allowing users to subscribe to channels outside the context of an organization
 */
selfRouter.get('/subscribedchannels', paginate, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const user = r.getUser(ctx);
  const { pageable } = ctx.state;
  const { subscriptionSource } = ctx.query;

  const result: IndexablePage<number, ChannelModel> =
    await channelService.getSubscribedChannelsForUserPaginated(
      user.id, user.organizationId, subscriptionSource, pageable,
    );
  ctx.body = result.map(formatChannel);
});

/**
 * Get all series that a user has access to.
 */
selfRouter.get('/subscribedseries', paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const user = r.getUser(ctx);
  const { pageable } = ctx.state;
  const includeContentPreview: boolean = r.queryParamToBoolean(ctx.query.includeContentPreview);
  const filterProgressStatus = r.queryParamCommaListToStringList(ctx.query.filterProgressStatus);
  const channelId = _getChannelIdFromQuery(ctx);

  const result: IndexablePage<number, SeriesModel> =
    await seriesService.getSubscribedSeriesForUserPaginated(user.id, {
      includeContentPreview,
      filterOutEmptySeries: true, // this endpoint never includes empty series
      filterProgressStatus,
      channelId,
    }, pageable);
  ctx.body = result.map(formatSeries);
});

/**
 * Get the tags available to user through any content they are subscribed to.
 * Similar to /organizations/self/contenttags but more restrictive.
 */
selfRouter.get('/subscribedchannels/contenttags', paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const { pageable } = ctx.state;
  const daysAgo = r.optionalQueryParamToInt(ctx.query.daysAgo);

  ctx.body = await contentService.getTagsFromContentForUser(userId, pageable, daysAgo);
});

/**
* User creates a response to an activity.  Wants request body like:
*  {
*      activityId: number,
*      body: {
*          // Utilizes the interface defined for type of the activity with activityId
*      }
*  }
*/
selfRouter.post('/activityresponses', hasPermission(modifySelfActivityResponsePermission), async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const userId = r.getUserId(ctx);
  const activityResponse = r.getPostBody(ctx);
  activityResponse.userId = userId;
  ctx.body = await r.assertModifyResourceResult(activityService.createActivityResponse(activityResponse));
  ctx.status = 201;
});

/**
* User changes their response to an activity
*/
selfRouter.put('/activityresponses/:responseId', canModifyActivityResponse, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const activityResponse: ActivityResponseModel = r.getPutBody(ctx, _getActivityResponseIdFromPath);

  r.assertResource(await activityService.getActivityResponse(_getActivityResponseIdFromPath(ctx)));
  ctx.body = r.assertModifyResourceResult(await activityService.updateActivityResponse(
    activityResponse.activityId,
    activityResponse,
  ));
});

/**
* Retrieves most recent response for a particular activity
*/
selfRouter.get('/activityresponses', paginate, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const userId = r.getUserId(ctx);
  const activityId = _getActivityIdFromQuery(ctx);

  const { pageable } = ctx.state;

  ctx.body = r.assertResource(await activityService.getRecentActivityResponseForActivity(userId, activityId, pageable));
});

/**
* Retrieves a response to an activity
*/
selfRouter.get('/activityresponses/:responseId', canReadActivityResponse, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const activityResponseId = _getActivityResponseIdFromPath(ctx);
  ctx.body = r.assertResource(await activityService.getActivityResponse(activityResponseId));
});

export default selfRouter;
