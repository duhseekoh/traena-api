// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';

import { Contexts, getPermission, Operations, Resources } from '../../permissions/permissionUtils';
import { hasPermission, permissionMiddlewareFactory } from '../../middleware/auth';
import channelRoutes from './channelRouter';

import subscriptionRoutes from './subscriptionRouter';
import userRoutes from './userRouter';

import { BadRequestError } from '../../../common/errors';
import formatOrganization from '../../formatters/organizationFormatters';
import formatSeries from '../../formatters/seriesFormatter';
import * as r from '../../../common/routerUtils';
import { formatContentItem } from '../../formatters/contentItemFormatters';
import { formatRoles } from '../../formatters/roleFormatters';
import type OrganizationModel from '../../../domain/models/OrganizationModel';
import type ContentItemModel from '../../../domain/models/ContentItemModel';
import type SeriesModel from '../../../domain/models/SeriesModel';

export function _getOrganizationIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.organizationId);
}

// Because we are not actually looking up the organization in the database, we have to specifically check that it exists
// and throw a 404 if not. Though this should *really* never happen unless org somehow got deleted but not its
// associated users
const canReadOrganization = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.READ,
  () => false, // no need to look up an org from the db, as the org id is already available in the context
  () => false, // no need for user ownership predicate, as no user owns an organization
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

const canModifyOrganization = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  () => false,
  () => false,
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

const canReadOrganizationContent = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.READ,
  () => false,
  () => false,
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

const traenaOrgAdminPermission = getPermission(Resources.ORGANIZATIONS, Operations.MODIFY, Contexts.ALL);

const organizationRouter = new Router();


// Add the sub-resource routes
organizationRouter.use('/:organizationId/channels', canReadOrganization, channelRoutes.routes());
organizationRouter.use('/:organizationId/subscriptions', canReadOrganization, subscriptionRoutes.routes());
organizationRouter.use('/:organizationId/users', canReadOrganization, userRoutes.routes());

// Get a list of all organizations
// Only traena admins can view all organizations
organizationRouter.get('/', hasPermission(traenaOrgAdminPermission), paginate, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const { pageable } = ctx.state;

  const result: IndexablePage<number, OrganizationModel> =
    await organizationService.getAllOrganizations(pageable);

  ctx.body = result.map(formatOrganization);
});

// Only traena admins can create organizations
organizationRouter.post('/', hasPermission(traenaOrgAdminPermission), async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const requestOrganization = r.getPostBody(ctx);
  const userId = r.getUserId(ctx);
  ctx.body = formatOrganization(r.assertModifyResourceResult(await organizationService.create(
    userId,
    requestOrganization,
  )));
  ctx.status = 201;
});

organizationRouter.put('/:organizationId', canModifyOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const requestOrganization = r.getPutBody(ctx, _getOrganizationIdFromPath);

  r.assertResource(await organizationService.get(_getOrganizationIdFromPath(ctx)));

  ctx.body = formatOrganization(r.assertModifyResourceResult(await organizationService.update(requestOrganization)));
});

organizationRouter.get('/:organizationId', canReadOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const organizationIdParam = _getOrganizationIdFromPath(ctx);

  ctx.body = formatOrganization(r.assertResource(await organizationService.get(organizationIdParam)));
});

// Only traena admins can delete an organization
organizationRouter.delete('/:organizationId', hasPermission(traenaOrgAdminPermission), async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const organizationIdParam = _getOrganizationIdFromPath(ctx);
  const userId = r.getUserId(ctx);

  r.assertResource(await organizationService.get(organizationIdParam));
  r.assertModifyResourceResult(await organizationService.deleteOrganization(userId, organizationIdParam));
  ctx.status = 204;
});

/**
 * Get common search terms for principal's organization.
 */
// no permissions check because self-reads are always allowed
organizationRouter.get('/self/commonsearchterms', async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const authOrganizationId = r.getOrganizationId(ctx);

  r.assertResource(await organizationService.get(authOrganizationId));
  ctx.body = await organizationService.getCommonSearchTermsForOrganization(authOrganizationId);
});

/**
 * Get common search terms for an organization.
 */
organizationRouter.get('/:organizationId/commonsearchterms', canReadOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const organizationIdParam = _getOrganizationIdFromPath(ctx);
  ctx.body = await organizationService.getCommonSearchTermsForOrganization(organizationIdParam);
});

/**
 * Set the common search terms on a specified organization
 */
organizationRouter.put('/:organizationId/commonsearchterms', canModifyOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const organizationIdParam = _getOrganizationIdFromPath(ctx);

  // not using assertBody here because we allow for an empty array
  if (!ctx.request.body || !Array.isArray(ctx.request.body)) {
    throw new BadRequestError('Request body is required');
  }

  const searchTerms: string[] = (ctx.request.body: any);
  ctx.body = r.assertModifyResourceResult(await organizationService.setCommonSearchTermsForOrganization(
    organizationIdParam,
    searchTerms,
  ));
});

/**
 * Delete common search terms for an organization. This is the same as sending a body of
 * [] to PUT /:organizationId/commonsearchterms
 */
organizationRouter.delete('/:organizationId/commonsearchterms', canModifyOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  const organizationIdParam = _getOrganizationIdFromPath(ctx);

  ctx.body = r.assertModifyResourceResult(await organizationService.setCommonSearchTermsForOrganization(
    organizationIdParam,
    [],
  ));
});

/**
 * Get content for an organization. The query parameter ?include can be one of internal, external, all (default is all)
 * if external - this will return only content this org is subscribed to
 * if internal - this will return only content created by this org
 * if all      - this will return all content available to this org
 */
organizationRouter.get('/:organizationId/content', canReadOrganizationContent, paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const userOrganizationId = r.getOrganizationId(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const { pageable } = ctx.state;

  // Whether to return all content visible to the requested org or the content it created
  const { include, searchText } = ctx.query;
  const includeStatuses = r.queryParamCommaListToContentItemStatusList(ctx.query.includeStatus);

  const result: IndexablePage<number, ContentItemModel> =
    await contentService.getContentForOrganizationPaginated(
      userId,
      userOrganizationId,
      pathOrganizationId,
      include,
      pageable,
      includeStatuses,
      searchText,
    );

  ctx.body = result.map(formatContentItem);
});

/**
 * This is a heavy call and should be used sparingly.
 * Tracking the performance issue here: https://github.com/auth0/auth0-authorization-extension/issues/169
 */
organizationRouter.get('/:organizationId/roles', canReadOrganization, async (ctx: Context) => {
  const organizationService = r.organizationService(ctx);
  ctx.body = formatRoles(await organizationService.getOrganizationRoles(_getOrganizationIdFromPath(ctx)));
});

/**
* Retrieve a list of series an organization owns through its channels
*/
organizationRouter.get('/:organizationId/series', canReadOrganization, paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const { pageable } = ctx.state;
  const organizationId = _getOrganizationIdFromPath(ctx);
  const includeContentPreview = r.queryParamToBoolean(ctx.query.includeContentPreview);
  const result: IndexablePage<number, SeriesModel> =
    r.assertResource(await seriesService.getByOrganizationId(organizationId, includeContentPreview, pageable));
  ctx.body = result.map(formatSeries);
});

/**
 * Retrieve a list of series an organization is subscribed to through its channel subscriptions.
 * Includes all of its own series and the external ones.
 */
organizationRouter.get('/:organizationId/subscribedseries', canModifyOrganization, paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  // TODO - figure out a way to specify what fields can be specified to sort by
  //  and if we should be explicity restricting what fields can be sorted by
  const { pageable } = ctx.state;
  const { subscriptionSource } = ctx.query;

  const result: IndexablePage<number, SeriesModel> =
    await seriesService.getSubscribedSeriesForOrganization(pathOrganizationId, pageable, {
      subscriptionSource,
    });
  ctx.body = result.map(formatSeries);
});

/**
 * Get the tags used within the organization sorted by their frequency
 */
organizationRouter.get('/self/contenttags', paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const { pageable } = ctx.state;

  ctx.body = await contentService.getTagsFromContentForOrganization(organizationId, pageable);
});

export default organizationRouter;
