// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import * as r from '../../../common/routerUtils';
import { Operations, Resources } from '../../permissions/permissionUtils';
import { permissionMiddlewareFactory } from '../../middleware/auth';
import { BadRequestError } from '../../../common/errors';
import formatOrganization from '../../formatters/organizationFormatters';

import ChannelModel, { type Channel } from '../../../domain/models/ChannelModel';
import type OrganizationModel from '../../../domain/models/OrganizationModel';

export function _getOrganizationIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.organizationId);
}

export function _getChannelIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.channelId);
}

const canModifyOrganization = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  () => false,
  () => false,
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

// Implicit path for this router: /organizations/:organizationId/channels
const channelRouter = new Router();

channelRouter.get('/', canModifyOrganization, paginate, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const { pageable } = ctx.state;

  const result: IndexablePage<number, ChannelModel> =
    await channelService.getChannelsForOrganization(pathOrganizationId, pageable);
  ctx.body = result;
});

channelRouter.post('/', canModifyOrganization, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const userId = r.getUserId(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const channel: Channel = r.getPostBody(ctx);

  if (channel.organizationId !== pathOrganizationId) {
    throw new BadRequestError('Organization Id in path must match id in request body');
  }

  ctx.body = r.assertModifyResourceResult(await channelService.create(channel, userId));
  ctx.status = 201;
});

channelRouter.put('/:channelId', canModifyOrganization, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const userId = r.getUserId(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const channel: Channel = r.getPutBody(ctx, _getChannelIdFromPath);

  if (channel.organizationId !== pathOrganizationId) {
    throw new BadRequestError('Organization Id in path must match id in request body');
  }

  ctx.body = r.assertModifyResourceResult(await channelService.update(channel, userId));
});

// eslint-disable-next-line no-unused-vars
channelRouter.delete('/:channelId', canModifyOrganization, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const pathChannelId = _getChannelIdFromPath(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const channel = await channelService.get(pathChannelId);

  if (channel.organizationId !== pathOrganizationId) {
    throw new BadRequestError('Organization Id in path must match id for channel to be deleted');
  }

  r.assertModifyResourceResult(await channelService.delete(pathChannelId));
  ctx.status = 204;
});

// Organizations that are subscribed to this channel
channelRouter.get('/:channelId/subscriberorganizations', canModifyOrganization, paginate, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const pathChannelId = _getChannelIdFromPath(ctx);
  const { pageable } = ctx.state;

  const channel = r.assertResource(await channelService.get(pathChannelId));
  if (channel.organizationId !== _getOrganizationIdFromPath(ctx)) {
    throw new BadRequestError('Organization Id in path does not match the org for the channelId in path');
  }

  const result: IndexablePage<number, OrganizationModel> =
    await channelService.getSubscriberOrganizationsForChannel(pathChannelId, pageable);
  ctx.body = result.map(formatOrganization);
});

export default channelRouter;
