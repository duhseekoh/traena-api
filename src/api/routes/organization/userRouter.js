// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import * as r from '../../../common/routerUtils';
import { Operations, Resources } from '../../permissions/permissionUtils';
import { permissionMiddlewareFactory } from '../../middleware/auth';

import type UserModel from '../../../domain/models/UserModel';


export function _getOrganizationIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.organizationId);
}

const canReadOrganizationUsers = permissionMiddlewareFactory(
  Resources.USERS, Operations.READ,
  () => false,
  () => false,
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

// Implicit path for this router: /organizations/:organizationId/users
const userRouter = new Router();

userRouter.get('/', canReadOrganizationUsers, paginate, async (ctx: Context) => {
  const userService = r.userService(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const { pageable } = ctx.state;

  const result: IndexablePage<number, UserModel> =
    await userService.getUsersForOrganizationPaginated(pathOrganizationId, pageable);
  ctx.body = result;
});

export default userRouter;
