// @flow
import Router from 'koa-router';
import { IndexablePage, ArrayPage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import { BadRequestError } from '../../../common/errors';
import * as r from '../../../common/routerUtils';
import { Operations, Resources } from '../../permissions/permissionUtils';
import { permissionMiddlewareFactory } from '../../middleware/auth';
import { formatSubscription } from '../../formatters/channelFormatters';
import { formatUser } from '../../formatters/userFormatters';
import type UserModel from '../../../domain/models/UserModel';
import type SubscriptionModel, {
  SubscriptionDTO,
  CreateSubscriptionDTO,
} from '../../../domain/models/SubscriptionModel';

export function _getOrganizationIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.organizationId);
}

export function _getChannelIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.channelId);
}

const canModifyOrganization = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  () => false,
  () => false,
  ctx => r.getOrganizationId(ctx) === _getOrganizationIdFromPath(ctx),
);

// Implicit path for this router: /organizations/:organizationId/subscriptions
const subscriptionRouter = new Router();

subscriptionRouter.get('/', canModifyOrganization, paginate, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const { pageable } = ctx.state;
  const { subscriptionSource } = ctx.query;
  const channelVisibilities = r.queryParamCommaListToStringList(ctx.query.channelVisibility);

  const result: ArrayPage<SubscriptionModel> =
    await channelService.getSubscriptionsForOrganization(pathOrganizationId, pageable, {
      channelVisibilities,
      subscriptionSource,
    });
  ctx.body = result.map(formatSubscription);
});

subscriptionRouter.get('/:channelId', canModifyOrganization, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const pathOrganizationId = _getOrganizationIdFromPath(ctx);
  const pathChannelId = _getChannelIdFromPath(ctx);

  const result: SubscriptionModel =
    r.assertResource(await channelService.getSubscription(pathChannelId, pathOrganizationId));
  ctx.body = formatSubscription(result);
});

subscriptionRouter.post(
  '/', canModifyOrganization,
  async (ctx: Context) => {
    const userId = r.getUserId(ctx);
    const channelService = r.channelService(ctx);
    const pathOrganizationId = _getOrganizationIdFromPath(ctx);
    const subscription: CreateSubscriptionDTO = r.getPostBody(ctx);

    if (subscription.organizationId !== pathOrganizationId) {
      throw new BadRequestError('Organization Id in path must match request body org id');
    }

    const result: SubscriptionModel =
      r.assertModifyResourceResult(await channelService.subscribe(subscription, userId));
    ctx.body = formatSubscription(result);
  },
);

subscriptionRouter.put(
  '/:channelId', canModifyOrganization,
  async (ctx: Context) => {
    const userId = r.getUserId(ctx);
    const channelService = r.channelService(ctx);
    const pathOrganizationId = _getOrganizationIdFromPath(ctx);
    const pathChannelId = _getChannelIdFromPath(ctx);
    // using postBody instead of putBody because of composite key
    const subscription: SubscriptionDTO = r.getPostBody(ctx);

    if (subscription.channelId !== pathChannelId
      || subscription.organizationId !== pathOrganizationId) {
      throw new BadRequestError('Organization and Channel Ids in path must match request body properties');
    }

    const result: SubscriptionModel =
      r.assertModifyResourceResult(await channelService.updateSubscription(subscription, userId));
    ctx.body = formatSubscription(result);
  },
);

subscriptionRouter.delete(
  '/:channelId', canModifyOrganization,
  async (ctx: Context) => {
    const userId = r.getUserId(ctx);
    const channelService = r.channelService(ctx);
    const pathOrganizationId = _getOrganizationIdFromPath(ctx);
    const channelId = _getChannelIdFromPath(ctx);

    r.assertModifyResourceResult(await channelService.unsubscribe(pathOrganizationId, channelId, userId));
    ctx.status = 204;
  },
);

subscriptionRouter.get('/:channelId/users', canModifyOrganization, paginate,
  async (ctx: Context) => {
    const userService = r.userService(ctx);
    const pathOrganizationId = _getOrganizationIdFromPath(ctx);
    const pathChannelId = _getChannelIdFromPath(ctx);
    const { pageable } = ctx.state;

    const result: IndexablePage<number, UserModel> =
      await userService.getUsersForSubscriptionPaginated(pathChannelId, pathOrganizationId, pageable);
    ctx.body = result.map(formatUser);
  },
);

export default subscriptionRouter;
