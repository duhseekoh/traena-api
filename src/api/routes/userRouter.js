// @flow

import Router from 'koa-router';
import type { Context } from 'koa';
import { IndexablePage } from '@panderalabs/koa-pageable';
import { Contexts, getAllPermissions, getPermission, Operations, Resources } from '../permissions/permissionUtils';
import { hasAnyPermission, permissionMiddlewareFactory, hasPermission } from '../middleware/auth';
import { BadRequestError } from '../../common/errors';
import { formatUser } from '../formatters/userFormatters';
import { formatRoles } from '../formatters/roleFormatters';
import UserModel from '../../domain/models/UserModel';
import * as r from '../../common/routerUtils';

function _getUserIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.userId);
}

export const canReadUser = permissionMiddlewareFactory(
  Resources.USERS, Operations.READ,
  ctx => ctx.state.services.userService.get(_getUserIdFromPath(ctx)),
  (ctx, user) => r.getUserId(ctx) === user.id,
  (ctx, user) => r.getOrganizationId(ctx) === user.organizationId,
);

const canModifyUser = permissionMiddlewareFactory(
  Resources.USERS, Operations.MODIFY,
  ctx => ctx.state.services.userService.get(_getUserIdFromPath(ctx)),
  (ctx, user) => r.getUserId(ctx) === user.id,
  (ctx, user) => r.getOrganizationId(ctx) === user.organizationId,
);

const allReadUserPermissions: string[] = getAllPermissions(Resources.USERS, Operations.READ);
const orgUserAdminPermission: string = getPermission(Resources.USERS, Operations.MODIFY, Contexts.ORG);
const traenaUserAdminPermission: string = getPermission(Resources.USERS, Operations.MODIFY, Contexts.ALL);
const createUserPermissions: string[] = [orgUserAdminPermission, traenaUserAdminPermission];

const userRouter = new Router();

userRouter.get('/', hasAnyPermission(allReadUserPermissions), async (ctx: Context) => {
  const userService = r.userService(ctx);
  const authOrganizationId = r.getOrganizationId(ctx);
  const { pageable } = ctx.state;
  const result: IndexablePage<number, UserModel> =
    await userService.getUsersForOrganizationPaginated(authOrganizationId, pageable);
  ctx.body = result;
});

userRouter.post('/', hasAnyPermission(createUserPermissions), async (ctx: Context) => {
  const userService = r.userService(ctx);
  const authOrganizationId = r.getOrganizationId(ctx);
  const requestUser: UserModel & { password?: string } = r.getPostBody(ctx);
  const authUserId = r.getUserId(ctx);

  // Traena admin if they can modify user across ANY organization - else Org Admin
  const isTraenaAdmin = ctx.state.user.permissions.includes(traenaUserAdminPermission);

  // Traena Admin's _must_ specify the user's organization
  if (isTraenaAdmin && !requestUser.organizationId) {
    throw new BadRequestError('User must include organizationId');
  } else if (!isTraenaAdmin) { // org admins can only create users in their own org
    requestUser.organizationId = authOrganizationId;
  }

  const { password, ...userWithoutPassword } = requestUser;
  ctx.body = formatUser(r.assertModifyResourceResult(await userService.create(
    authUserId,
    userWithoutPassword,
    password,
  )));
  ctx.status = 201;
});

userRouter.put('/:userId', canModifyUser, async (ctx: Context) => {
  const userService = r.userService(ctx);
  const requestUser: UserModel = r.getPutBody(ctx, _getUserIdFromPath);
  ctx.body = formatUser(r.assertModifyResourceResult(await userService.update(requestUser)));
});

userRouter.get('/:userId', canReadUser, async (ctx: Context) => {
  const userService = r.userService(ctx);
  ctx.body = formatUser(await userService.get(_getUserIdFromPath(ctx)));
});

// Can only delete if authed user has org or traena admin, and can access this particular user
userRouter.delete('/:userId', hasAnyPermission(createUserPermissions), canModifyUser, async (ctx: Context) => {
  const userService = r.userService(ctx);
  const authUserId = r.getUserId(ctx);

  r.assertModifyResourceResult(await userService.deleteUser(authUserId, _getUserIdFromPath(ctx)));
  ctx.status = 204;
});

userRouter.get('/:userId/roles', canReadUser, async (ctx: Context) => {
  const userService = r.userService(ctx);
  ctx.body = formatRoles(await userService.getUserRoles(_getUserIdFromPath(ctx)));
});

/**
 * Add roles to a user.
 */
userRouter.patch('/:userId/roles', canModifyUser, hasPermission(orgUserAdminPermission), async (ctx: Context) => {
  const userService = r.userService(ctx);
  const userId = _getUserIdFromPath(ctx);
  const roleIds: string[] = r.getPostBody(ctx);
  await userService.addRolesToUser(userId, roleIds);
  ctx.status = 200;
});

/**
 * Removes roles from a user.
 */
userRouter.delete('/:userId/roles', canModifyUser, hasPermission(orgUserAdminPermission), async (ctx: Context) => {
  const userService = r.userService(ctx);
  const userId = _getUserIdFromPath(ctx);
  const roleIds: string[] = r.getPostBody(ctx);
  await userService.removeRolesFromUser(userId, roleIds);
  ctx.status = 200;
});
export default userRouter;
