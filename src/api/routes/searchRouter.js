// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { paginate } from '@panderalabs/koa-pageable';
import * as r from '../../common/routerUtils';
import { formatContentItem } from '../formatters/contentItemFormatters';

// No permissions checking as all authenticated users are allowed to search, the searches themslves
// are all filtered to ensure user does not access resources they are not allowed to see
const searchRouter = new Router();

/**
 * Search for content, tailored to user by userId
 */
searchRouter.get('/contents/:searchText?', paginate, async (ctx: Context) => {
  const searchService = r.searchService(ctx);
  const userId = r.getUserId(ctx);
  const { searchText }: {searchText: ?string} = ctx.params;
  const tags: ?string[] = r.queryParamCommaListToStringList(ctx.query.tags);
  const channelIds: ?number[] = r.queryParamCommaListToIntListNullable(ctx.query.channelId);

  const { pageable } = ctx.state;

  const result = await searchService.search(userId, searchText, tags, channelIds, pageable);
  ctx.body = result.map(formatContentItem);
});

/**
 * Search for users, tailored to user by userId
 */
searchRouter.get('/users/:searchText?', async (ctx: Context) => {
  const searchService = r.searchService(ctx);
  const userId = r.getUserId(ctx);
  const searchText: string = r.assertQueryParam(ctx.params.searchText, 'searchText');

  ctx.body = await searchService.searchUsers(userId, searchText);
});

export default searchRouter;
