// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { paginate } from '@panderalabs/koa-pageable';
import * as r from '../../../common/routerUtils';
import { permissionMiddlewareFactory, passesAnyCheck } from '../../middleware/auth';
import analyticsRoutes from './analyticsRouter';
import { formatContentItem } from '../../formatters/contentItemFormatters';
import formatSeries from '../../formatters/seriesFormatter';
import {
  Operations,
  Resources } from '../../permissions/permissionUtils';

const seriesRouter = new Router();

export function _getSeriesIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.seriesId);
}

const canCreateOrganizationSeries = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  ctx => r.channelService(ctx).get(r.getPostBody(ctx).channelId),
  () => false,
  (ctx, channel) => channel.organizationId === ctx.state.user.organizationId,
);

const canModifyOrganizationChannel = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  ctx => r.seriesService(ctx).getChannelForSeriesId(_getSeriesIdFromPath(ctx)),
  () => false,
  (ctx, channel) => channel.organizationId === ctx.state.user.organizationId,
);

// User can read their orgs content, and they are subscribed to this channel.
const canUserReadOrganizationSeries = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.READ,
  ctx => r.seriesService(ctx).getChannelForSeriesId(_getSeriesIdFromPath(ctx)),
  () => false,
  (ctx, channel) => r.channelService(ctx).getSubscribedChannelIdsForUser(r.getUserId(ctx))
    .then(subscriptionIds => subscriptionIds.includes(channel.id)),
);

// User can modify their organization, and their organization is subscribed to this channel.
// Doesn't matter if the user is subscribed to the channel, if they can edit their org, they
// can access any channel the org is subscribed to.
const canAdminReadOrganizationSeries = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  ctx => r.seriesService(ctx).getChannelForSeriesId(_getSeriesIdFromPath(ctx)),
  () => false,
  (ctx, channel) => r.channelService(ctx).getSubscriptionIdsForOrganization(r.getOrganizationId(ctx))
    .then(subscriptionIds => subscriptionIds.includes(channel.id)),
);

const canReadOrganizationSeries = passesAnyCheck([canUserReadOrganizationSeries, canAdminReadOrganizationSeries]);

seriesRouter.use('/:seriesId/analytics', canAdminReadOrganizationSeries, analyticsRoutes.routes());

/**
* Create a series.
*/
seriesRouter.post('/', canCreateOrganizationSeries, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const series = r.getPostBody(ctx);
  const userId = r.getUserId(ctx);

  ctx.body = r.assertModifyResourceResult(await seriesService.create(series, userId));
  ctx.status = 201;
});

/**
* Retrieve a single series.
*/
seriesRouter.get('/:seriesId', canReadOrganizationSeries, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const seriesId = _getSeriesIdFromPath(ctx);
  const subscriberOrgId = ctx.state.user.organizationId;

  const includeContentPreview = r.queryParamToBoolean(ctx.query.includeContentPreview);
  const result = r.assertResource(await seriesService.get(seriesId, subscriberOrgId, includeContentPreview));
  ctx.body = formatSeries(result);
});

/**
* Retrieve a list of content for a series.
*/
seriesRouter.get('/:seriesId/content', canReadOrganizationSeries, paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const seriesId = _getSeriesIdFromPath(ctx);
  const subscriberOrgId = ctx.state.user.organizationId;

  const { pageable } = ctx.state;
  const result = r.assertResource(await seriesService.getContent(seriesId, subscriberOrgId, pageable));
  ctx.body = result.map(formatContentItem);
});

/**
* Update a series.
*/
seriesRouter.put('/:seriesId', canModifyOrganizationChannel, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const series = r.getPutBody(ctx, _getSeriesIdFromPath);

  r.assertResource(await seriesService.get(_getSeriesIdFromPath(ctx)));
  const result = r.assertModifyResourceResult(await seriesService.update(series));
  ctx.body = formatSeries(result);
});

seriesRouter.delete('/:seriesId', canModifyOrganizationChannel, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const seriesId = _getSeriesIdFromPath(ctx);

  r.assertResource(await seriesService.get(seriesId));
  r.assertModifyResourceResult(await seriesService.delete(seriesId));
  ctx.status = 204;
});

export default seriesRouter;
