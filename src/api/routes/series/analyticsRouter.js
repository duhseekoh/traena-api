// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { paginate } from '@panderalabs/koa-pageable';
import * as r from '../../../common/routerUtils';
import { formatUserWithSeriesStatsPage } from '../../formatters/userFormatters';
import { _getSeriesIdFromPath } from './seriesRouter';

// Implicit path for this router: /series/:seriesId/analytics
const analyticsRouter = new Router();

/**
 * View basic stats about a series.
 * In the context of the authed users organization.
 */
analyticsRouter.get('/', async (ctx: Context) => {
  const analyticsService = r.analyticsService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const seriesId = _getSeriesIdFromPath(ctx);

  const result = r.assertResource(
    await analyticsService.getSeriesSummary(seriesId, organizationId),
  );
  ctx.body = result;
});

/**
 * Retrieve a pairing of users to their stats associated to the series.
 * Returns all users that have access to the series in the context of the
 * authed users organization.
 */
analyticsRouter.get('/users', paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const analyticsService = r.analyticsService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const seriesId = _getSeriesIdFromPath(ctx);
  const { pageable } = ctx.state;

  r.assertResource(await seriesService.get(_getSeriesIdFromPath(ctx)));
  const result = formatUserWithSeriesStatsPage(
    r.assertResource(
      await analyticsService.getUsersWithStatsForSeriesId(
        seriesId,
        organizationId,
        pageable,
      ),
    ),
  );
  ctx.body = result;
});

export default analyticsRouter;
