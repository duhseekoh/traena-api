// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import * as r from '../../common/routerUtils';
import { Operations, Resources } from '../permissions/permissionUtils';
import { permissionMiddlewareFactory, passesAnyCheck } from '../middleware/auth';
import { formatContentItem } from '../formatters/contentItemFormatters';
import formatSeries from '../formatters/seriesFormatter';
import { formatChannel } from '../formatters/channelFormatters';
import ContentItemModel from '../../domain/models/ContentItemModel';
import type ChannelModel from '../../domain/models/ChannelModel';

const channelRouter = new Router();

export function _getChannelIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.channelId);
}

// User can read their orgs content, and they are subscribed to this channel.
const canUserReadChannel = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.READ,
  ctx => _getChannelIdFromPath(ctx),
  () => false,
  (ctx, channelId) => r.channelService(ctx).getSubscribedChannelIdsForUser(r.getUserId(ctx))
    .then(subscriptionIds => subscriptionIds.includes(channelId)),
);

// User can modify their organization, and their organization is subscribed to this channel.
// Doesn't matter if the user is subscribed to the channel, if they can edit their org, they
// can access any channel the org is subscribed to.
const canAdminReadChannel = permissionMiddlewareFactory(
  Resources.ORGANIZATIONS, Operations.MODIFY,
  ctx => _getChannelIdFromPath(ctx),
  () => false,
  (ctx, channelId) => r.channelService(ctx).getSubscriptionIdsForOrganization(r.getOrganizationId(ctx))
    .then(subscriptionIds => subscriptionIds.includes(channelId)),
);

// We only need one of these to be true
const canReadChannel = passesAnyCheck([canUserReadChannel, canAdminReadChannel]);

channelRouter.get('/:channelId', canReadChannel, async (ctx: Context) => {
  const channelService = r.channelService(ctx);
  const channelId = _getChannelIdFromPath(ctx);

  const channel: ChannelModel = r.assertResource(await channelService.get(channelId));
  ctx.body = formatChannel(channel);
});

/**
* Retrieve a list of series by channel.
* query param includeContentPreview=true will enable us to show the first few
* content items in the scrollable list of series.
*/
channelRouter.get('/:channelId/series', canReadChannel, paginate, async (ctx: Context) => {
  const seriesService = r.seriesService(ctx);
  const { pageable } = ctx.state;
  const channelId = _getChannelIdFromPath(ctx);
  const includeContentPreview = r.queryParamToBoolean(ctx.query.includeContentPreview);
  const subscriberOrgId = ctx.state.user.organizationId;

  const result = r.assertResource(await seriesService.getByChannelId(
    channelId,
    subscriberOrgId,
    includeContentPreview,
    pageable,
  ));
  ctx.body = result.map(formatSeries);
});

/**
* Retrieve a list of content in a channel
*/
channelRouter.get('/:channelId/content', canReadChannel, paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const { pageable } = ctx.state;
  const channelId = _getChannelIdFromPath(ctx);
  const userId = r.getUserId(ctx);
  const result: IndexablePage<number, ContentItemModel> =
    r.assertResource(await contentService.getByChannelIdForUser(userId, channelId, pageable));
  ctx.body = result.map(formatContentItem);
});

export default channelRouter;
