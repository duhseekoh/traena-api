// @flow
import Router from 'koa-router';
import { IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import * as r from '../../common/routerUtils';
import { formatContentItem } from '../formatters/contentItemFormatters';

import type ContentItemModel from '../../domain/models/ContentItemModel';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}

export function _getOrganizationIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.organizationId);
}

// Implicit path for this router: /users/self/bookmarks
const bookmarkRouter = new Router();

bookmarkRouter.get('/', async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);

  const result = await contentService.getBookmarkIdsForUser(userId);
  ctx.body = result;
});

bookmarkRouter.get('/content', paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const { pageable } = ctx.state;

  const result: IndexablePage<number, ContentItemModel> =
    await contentService.getBookmarksForUser(userId, pageable);
  ctx.body = result.map(formatContentItem);
});

// No bookmark permission check needed, if user can see a piece of content, they can bookmark it
bookmarkRouter.post('/', async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const { contentId } = r.getPostBody(ctx);

  r.assertModifyResourceResult(await contentService.bookmark(userId, contentId));
  ctx.status = 201;
});

bookmarkRouter.delete('/:contentId', async (ctx) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = _getContentId(ctx);

  r.assertModifyResourceResult(await contentService.unbookmark(userId, contentId));
  ctx.status = 204;
});

export default bookmarkRouter;
