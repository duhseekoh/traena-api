// @flow
import Router from 'koa-router';
import jwt from 'koa-jwt';
import config from '../../common/config/index';
import { NotAuthorizedError, ResourceForbiddenError } from '../../common/errors';
import * as r from '../../common/routerUtils';

import type UserModel from '../../domain/models/UserModel';
import type UserService from '../../services/UserService';
import { formatUser } from '../formatters/userFormatters';

/**
 * Router for internal-only, client-credentials grant routes
 * @type {Router}
 */

const MODIFY_CLIENT_USERS_PERMISSION = 'modify:client:users';

// TODO: Genericize as needed
async function validateClientUserMiddleware(ctx, next) {
  // Only allow client credentials users, not human users
  if (!ctx.state.jwtData) {
    throw new NotAuthorizedError('Access Token does not exist');
  }

  if (ctx.state.jwtData.email) {
    throw new ResourceForbiddenError('User access is not allowed');
  }

  const { sub, scope } = ctx.state.jwtData;

  if (!sub || !scope || !sub.endsWith('@clients') || !scope.split(' ').includes(MODIFY_CLIENT_USERS_PERMISSION)) {
    throw new ResourceForbiddenError('Client does not have permission to access this resource');
  }

  return next();
}

const internalRouter = new Router();

internalRouter.get(
  '/users/:userEmail', jwt({ secret: config.auth.clientSecret, key: 'jwtData' }),
  validateClientUserMiddleware, async (ctx) => {
    const userEmail: string = r.assertPathParam(ctx.params.userEmail);
    const userService: UserService = r.userService(ctx);

    const user: UserModel = await userService.findByEmail(userEmail);

    if (!user) {
      throw new NotAuthorizedError('No user located with provided email');
    }

    if (user.isDisabled) {
      // If user is disabled we can mark them as disabled in auth0
      throw new ResourceForbiddenError('User is disabled!');
    }

    const userView = formatUser(user);
    const channels = await user.getSubscribedChannels();
    ctx.body = {
      ...userView,
      subscribedChannelIds: channels.map(it => it.id),
    };
  },
);

/**
 * Unsecured endpoint - called by AWS lambda (upon receipt of an SNS message indicating an update to an Elastic
 * Transcoder Job) Causes the API to call Elastic Transcoder to check and update the status of the indicated job.
 */
internalRouter.post('/transcoder-jobs', async (ctx) => {
  const contentService = r.contentService(ctx);
  const elasticTranscoderJobId = r.assertQueryParam(ctx.query.jobId, 'jobId');

  await contentService.updateTranscoderJob(elasticTranscoderJobId);

  ctx.status = 204;
});

export default internalRouter;
