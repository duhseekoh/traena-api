// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import * as r from '../../common/routerUtils';
import type { CreateEventDTO } from '../../domain/models/EventModel'; // eslint-disable-line import/named
import { EventSource } from '../../domain/models/EventModel';

const eventRouter = new Router();

// No special permissions needed, user just must be authenticated
eventRouter.post('/', async (ctx: Context) => {
  const eventBody: CreateEventDTO = r.getPostBody(ctx);
  eventBody.source = EventSource.CLIENT;
  const eventService = r.eventService(ctx);
  const userId = r.getUserId(ctx);

  eventBody.userId = userId;
  eventService.raiseEvent(eventBody);
  ctx.status = 204;
});


export default eventRouter;
