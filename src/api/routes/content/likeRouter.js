// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { type IndexablePage, paginate } from '@panderalabs/koa-pageable';
import type UserModel from '../../../domain/models/UserModel';
import type UserView from '../../views/UserView';
import { formatUser } from '../../formatters/userFormatters';
import * as r from '../../../common/routerUtils';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}
// Implicit path for this router: /contents/:contentId/likes
const likeRouter = new Router();

// No like permission check needed, if user can see a piece of content, they can like it
likeRouter.post('/', async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = _getContentId(ctx);

  r.assertModifyResourceResult(await contentService.likeContent(userId, contentId));
  ctx.status = 201;
});

likeRouter.delete('/', async (ctx) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const contentId = _getContentId(ctx);

  r.assertModifyResourceResult(await contentService.unlikeContent(userId, contentId));
  ctx.status = 204;
});

likeRouter.get('/', paginate, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const contentId = _getContentId(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const { pageable } = ctx.state;

  const page: IndexablePage<number, UserModel> =
    r.assertResource(await contentService.getLikesByContentId(contentId, organizationId, pageable));
  const formattedPage: IndexablePage<number, UserView> = page.map(formatUser);
  ctx.body = formattedPage;
});

export default likeRouter;
