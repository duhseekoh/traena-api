// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { paginate } from '@panderalabs/koa-pageable';
import { formatEventPage } from '../../formatters/analyticsFormatters';
import { formatUserWithContentStatsPage } from '../../formatters/userFormatters';
import * as r from '../../../common/routerUtils';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}
// Implicit path for this router: /contents/:contentId/analytics
const analyticsRouter = new Router();

/**
 * View basic stats about a content item.
 * In the context of the authed users organization.
 */
analyticsRouter.get('/', async (ctx: Context) => {
  const analyticsService = r.analyticsService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const contentId = _getContentId(ctx);

  const result = r.assertResource(
    await analyticsService.getContentItemSummary(contentId, organizationId),
  );
  ctx.body = result;
});

/**
 * Events that have been logged related to a content item and that we deem worthy of exposing to users.
 * In the context of the authed users organization.
 */
analyticsRouter.get('/feed', paginate, async (ctx: Context) => {
  const analyticsService = r.analyticsService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const contentId = _getContentId(ctx);
  const { pageable } = ctx.state;

  const result = formatEventPage(
    r.assertResource(
      await analyticsService.getContentItemEvents(
        contentId,
        organizationId,
        pageable,
      ),
    ),
  );
  ctx.body = result;
});

/**
 * Retrieve a pairing of users to their stats associated to the content item.
 * Only returns entries where the user has at least one interaction with the
 * content item.
 * In the context of the authed users organization.
 */
analyticsRouter.get('/users', paginate, async (ctx: Context) => {
  const analyticsService = r.analyticsService(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const contentId = _getContentId(ctx);
  const { pageable } = ctx.state;

  const result = formatUserWithContentStatsPage(
    r.assertResource(
      await analyticsService.getUsersWithStatsForContentId(
        contentId,
        organizationId,
        pageable,
      ),
    ),
  );
  ctx.body = result;
});

export default analyticsRouter;
