// @flow

import Router from 'koa-router';
import type { Context } from 'koa';
import { paginate } from '@panderalabs/koa-pageable';
import { ResourceValidationError } from '../../../common/errors';
import * as r from '../../../common/routerUtils';
import { permissionMiddlewareFactory } from '../../middleware/auth';
import { Operations, Resources } from '../../permissions/permissionUtils';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}

function _getActivityIdFromPath(ctx: Context): number {
  return r.pathParamToInt(ctx.params.activityId);
}

const canModifyContent = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.MODIFY,
  ctx => ctx.state.services.contentService.getContentById(_getContentId(ctx)),
  (ctx, contentItem) => ctx.state.user.id === contentItem.authorId,
  (ctx, contentItem) => ctx.state.user.organizationId === contentItem.authorOrganizationId,
);

// Implicit path for this router: /contents/:contentId/activities
const activityRouter = new Router();

/**
* Create an activity associated with a contentItem
* Expects a request body like:
*  {
*      type: 'QUESTION_SET',
*      body: {
*          // Utilizes the interface defined for the given type
*      }
*  }
*/
activityRouter.post('/', canModifyContent, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const contentId = _getContentId(ctx);
  const userId = r.getUserId(ctx);
  const activity = r.getPostBody(ctx);

  if (activity.contentId !== contentId) {
    throw new ResourceValidationError('The activity contentId does not match the route');
  }

  ctx.body = r.assertModifyResourceResult(await activityService.create(activity, userId));
  ctx.status = 201;
});

/**
* Update an activity for a contentItem, and increases the version number for that activity
*/
activityRouter.put('/:activityId', canModifyContent, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const contentId = _getContentId(ctx);
  const userId = r.getUserId(ctx);
  const activity = r.getPutBody(ctx, _getActivityIdFromPath);
  if (activity.contentId !== contentId) {
    throw new ResourceValidationError('The activity contentId does not match the route');
  }
  r.assertResource(await activityService.get(_getActivityIdFromPath(ctx)));
  ctx.body = r.assertModifyResourceResult(await activityService.update(activity, userId));
});

/**
* Retrieves a pageable list of all activities associated with a given contentItem
*/
activityRouter.get('/', paginate, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const contentId = _getContentId(ctx);
  const { pageable } = ctx.state;

  ctx.body = r.assertResource(await activityService.getActivitiesForContentId(contentId, pageable));
});

/**
* Retrieves a particular activity associated with a contentItem by ID
*/
activityRouter.get('/:activityId', async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const activityId = _getActivityIdFromPath(ctx);
  ctx.body = r.assertResource(await activityService.get(activityId));
});

activityRouter.get('/:activityId/results', async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const activityId = _getActivityIdFromPath(ctx);
  const results = r.assertResource(await activityService.getActivityResults(activityId));
  ctx.body = results;
});

/**
* Marks a particular activity as deleted, so it will no longer be retrieved by the GET routes
* defined for activities
*/
activityRouter.delete('/:activityId', canModifyContent, async (ctx: Context) => {
  const activityService = r.activityService(ctx);
  const userId = r.getUserId(ctx);
  const activityId = _getActivityIdFromPath(ctx);

  r.assertResource(await activityService.get(activityId));
  r.assertModifyResourceResult(await activityService.delete(activityId, userId));
  ctx.status = 204;
});

export default activityRouter;
