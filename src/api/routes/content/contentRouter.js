// @flow

import Router from 'koa-router';
import type { Context } from 'koa';
import _ from 'lodash';
import { getAllPermissions, Operations, Resources } from '../../permissions/permissionUtils';
import { hasAnyPermission, permissionMiddlewareFactory, passesAnyCheck } from '../../middleware/auth';
import { formatContentItem, formatContentItemsMap } from '../../formatters/contentItemFormatters';
import activityRoutes from './activityRouter';
import commentRoutes from './commentRouter';
import likeRoutes from './likeRouter';
import analyticsRoutes from './analyticsRouter';
import * as r from '../../../common/routerUtils';
import type ContentItemModel from '../../../domain/models/ContentItemModel';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}

// if you are the author or are subscribed to the channel of the content item
const canUserReadContent = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.READ,
  ctx => r.contentService(ctx).getContentById(_getContentId(ctx)),
  (ctx, contentItem: ContentItemModel) => ctx.state.user.id === contentItem.authorId,
  (ctx, contentItem: ContentItemModel) =>
    r.channelService(ctx).getSubscribedChannelIdsForUser(r.getUserId(ctx))
      .then(subscriptionIds => subscriptionIds.includes(contentItem.channelId)),
);

// if your org is subscribed to the channel of the content item
const canAdminReadContent = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.MODIFY,
  ctx => r.contentService(ctx).getContentById(_getContentId(ctx)),
  () => false,
  (ctx, contentItem: ContentItemModel) =>
    r.channelService(ctx).getSubscriptionIdsForOrganization(r.getOrganizationId(ctx))
      .then(subscriptionIds => subscriptionIds.includes(contentItem.channelId)),
);

const canModifyContent = permissionMiddlewareFactory(
  Resources.CONTENT, Operations.MODIFY,
  ctx => r.contentService(ctx).getContentById(_getContentId(ctx)),
  (ctx, contentItem) => ctx.state.user.id === contentItem.authorId,
  (ctx, contentItem) => ctx.state.user.organizationId === contentItem.authorOrganizationId,
);

export const canReadContent = passesAnyCheck([canUserReadContent, canAdminReadContent]);

// View analytics if you're an admin with access or you can modify the content
const canReadContentAnalytics = passesAnyCheck([canAdminReadContent, canModifyContent]);

const allReadContentPermissions = getAllPermissions(Resources.CONTENT, Operations.READ);
const allModifyContentPermissions = getAllPermissions(Resources.CONTENT, Operations.MODIFY);

const contentRouter = new Router();

// Add the sub-resource routes
contentRouter.use('/:contentId/activities', canReadContent, activityRoutes.routes());
contentRouter.use('/:contentId/analytics', canReadContentAnalytics, analyticsRoutes.routes());
contentRouter.use('/:contentId/comments', canReadContent, commentRoutes.routes());
contentRouter.use('/:contentId/likes', canReadContent, likeRoutes.routes());

// Begin routes
contentRouter.post('/', hasAnyPermission(allModifyContentPermissions), async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const contentItem = r.getPostBody(ctx);

  ctx.body = formatContentItem(r.assertModifyResourceResult(await contentService.createContentItem(
    contentItem,
    userId,
    organizationId,
  )));
  ctx.status = 201;
});

contentRouter.put('/:contentId', canModifyContent, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const contentItem = r.getPutBody(ctx, _getContentId);
  ctx.body = formatContentItem(r.assertModifyResourceResult(await contentService.updateContentItem(
    contentItem,
    userId,
    organizationId,
  )));
});

contentRouter.get('/:contentId', canReadContent, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const contentItem = await contentService.getContentItemForUser(r.getUserId(ctx), _getContentId(ctx));
  if (contentItem) {
    ctx.body = formatContentItem(contentItem);
  } else {
    ctx.status = 404;
  }
});

contentRouter.get('/', hasAnyPermission(allReadContentPermissions), async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const contentIds = r.queryParamCommaListToIntList(ctx.query.contentIds, 'contentIds');

  const contentList: ContentItemModel[] = await contentService.getContentForUserByIds(userId, contentIds);
  ctx.body = formatContentItemsMap(_.keyBy(contentList, 'id'));
});

contentRouter.delete('/:contentId', canModifyContent, async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const contentItemId = _getContentId(ctx);
  const userId = r.getUserId(ctx);

  r.assertModifyResourceResult(await contentService.archiveContentItem(contentItemId, userId));
  ctx.status = 204;
});

export default contentRouter;
