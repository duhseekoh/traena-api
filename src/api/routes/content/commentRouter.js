// @flow

import Router from 'koa-router';
import { paginate } from '@panderalabs/koa-pageable';
import type { Context } from 'koa';
import moment from 'moment';
import { formatComment } from '../../formatters/commentFormatters';
import * as r from '../../../common/routerUtils';
import { getAllPermissions, Operations, Resources } from '../../permissions/permissionUtils';
import { permissionMiddlewareFactory, hasAnyPermission } from '../../middleware/auth';
import CommentModel from '../../../domain/models/CommentModel';

function _getContentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.contentId);
}

function _getCommentId(ctx: Context): number {
  return r.pathParamToInt(ctx.params.commentId);
}

function _getMaxDate(ctx: Context): ?Date {
  return ctx.query.maxDateTimestamp ? moment(ctx.query.maxDateTimestamp).toDate() : null;
}
const allReadCommentPermissions = getAllPermissions(Resources.COMMENTS, Operations.READ);
const allModifyCommentPermissions = getAllPermissions(Resources.COMMENTS, Operations.MODIFY);

const canModifyComment = permissionMiddlewareFactory(
  Resources.COMMENTS, Operations.MODIFY,
  ctx => ctx.state.services.commentService.getCommentById(_getCommentId(ctx)),
  (ctx, comment) => ctx.state.user.id === comment.authorId,
  (ctx, comment) => ctx.state.user.organizationId === comment.organizationId,
);

// Implicit path for this router: /contents/:contentId/comments
const commentRouter = new Router();

commentRouter.get('/', paginate, hasAnyPermission(allReadCommentPermissions), async (ctx: Context) => {
  const commentService = r.commentService(ctx);
  const userId: number = r.getUserId(ctx);
  const contentId = _getContentId(ctx);
  const maxDate = _getMaxDate(ctx);
  const { pageable } = ctx.state;

  ctx.body = await commentService.getCommentsForContent(userId, contentId, maxDate, pageable);
});

commentRouter.post('/', hasAnyPermission(allModifyCommentPermissions), async (ctx: Context) => {
  const commentService = r.commentService(ctx);
  const user = r.getUser(ctx);
  const commentBody: CommentModel & { taggedUserIds?: number[] } = r.getPostBody(ctx);
  const contentId = _getContentId(ctx);
  const { text, taggedUserIds } = commentBody;

  ctx.body = formatComment(r.assertModifyResourceResult(await commentService.addComment(
    contentId,
    user,
    text,
    taggedUserIds,
  )));
  ctx.status = 201;
});

commentRouter.delete('/:commentId', canModifyComment, async (ctx: Context) => {
  const commentService = r.commentService(ctx);
  const contentId = _getContentId(ctx);
  const commentId = _getCommentId(ctx);
  r.assertModifyResourceResult(await commentService.deleteComment(contentId, commentId));
  ctx.status = 204;
});

export default commentRouter;
