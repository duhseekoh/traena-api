// @flow
import Router from 'koa-router';
import { AdminPermissions, hasAdminPermission } from '../middleware/auth';
import * as r from '../../common/routerUtils';

const batchRouter = new Router();

batchRouter.get('/reindexusers', hasAdminPermission(AdminPermissions.ELASTIC_SEARCH), async (ctx) => {
  const userId = r.getUserId(ctx);
  const { elasticSearchGatheringService } = ctx.state.services;
  ctx.body = await elasticSearchGatheringService.reindexUsers(userId);
});

batchRouter.get('/reindexcontents', hasAdminPermission(AdminPermissions.ELASTIC_SEARCH), async (ctx) => {
  const userId = r.getUserId(ctx);
  const { elasticSearchGatheringService } = ctx.state.services;
  ctx.body = await elasticSearchGatheringService.reindexContents(userId);
});

export default batchRouter;
