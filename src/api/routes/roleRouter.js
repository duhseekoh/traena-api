// @flow
import Router from 'koa-router';
import type { Context } from 'koa';
import { formatRoles } from '../formatters/roleFormatters';

import { Contexts, getPermission, Operations, Resources } from '../permissions/permissionUtils';
import { hasPermission } from '../middleware/auth';
import * as r from '../../common/routerUtils';

const roleRouter = new Router();

const modifyOrgUsersPermission = getPermission(Resources.USERS, Operations.MODIFY, Contexts.ORG);

/**
 * Retrieve roles that can be assigned.
 * Will only need to access this endpoint if you can modify org users.
 */
roleRouter.get('/', hasPermission(modifyOrgUsersPermission), async (ctx: Context) => {
  const authService = r.authService(ctx);

  ctx.body = formatRoles(await authService.getAssignableRoles());
});

export default roleRouter;
