// @flow
import Router from 'koa-router';
import path from 'path';
import _ from 'lodash';

import type { Context } from 'koa';

import { getAllPermissions, Operations, Resources } from '../permissions/permissionUtils';
import * as r from '../../common/routerUtils';
import { BadRequestError } from '../../common/errors';
import { MEDIA_TYPES } from '../../domain/fieldConstants';
import { hasAnyPermission } from '../middleware/auth';

const allModifyContentPermissions = getAllPermissions(Resources.CONTENT, Operations.MODIFY);

const mediaRouter = new Router();


// Returns a signed url to upload a file to a temporary storage on s3
mediaRouter.post('/upload-url', hasAnyPermission(allModifyContentPermissions), async (ctx: Context) => {
  const mediaService = r.mediaService(ctx);
  const qFileName = r.assertQueryParam(ctx.query.fileName, 'fileName');
  const qMediaType = r.assertQueryParam(ctx.query.mediaType, 'mediaType');
  const authUserId = r.getUserId(ctx);
  const authUserOrgId = r.getOrganizationId(ctx);

  if (path.extname(qFileName) === '') {
    throw new BadRequestError('fileName param must have an extension');
  }

  if (!_.values(MEDIA_TYPES).includes(qMediaType)) {
    throw new BadRequestError('Unsupported Media Type');
  }

  if (qMediaType === MEDIA_TYPES.IMAGE) {
    ctx.body = await mediaService.getSignedS3ImageUploadUrl(qFileName);
  } else if (qMediaType === MEDIA_TYPES.VIDEO) {
    ctx.body = await mediaService.getSignedS3VideoUploadUrl(authUserOrgId, authUserId, qFileName);
  }
});


/**
 * Kicks off and waits for a lambda image processor to process an input image
 * The image file is expected to be located in S3 at the path returned by the call to GET /media/image-processor-job
 */
mediaRouter.post('/image-processor-job', hasAnyPermission(allModifyContentPermissions), async (ctx: Context) => {
  const mediaService = r.mediaService(ctx);
  const fileName = r.assertQueryParam(ctx.query.fileName, 'fileName');

  ctx.body = await mediaService.invokeImageProcessorLambda(fileName);
});

/**
 * Submits an Elastic Transcoder job for the given contentItem for the video file provided in the query parameter.
 * The video file is expected to be located in S3 at the path returned by the call to GET /:contentId/upload-url
 */
mediaRouter.post('/transcoder-jobs', hasAnyPermission(allModifyContentPermissions), async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const userId = r.getUserId(ctx);
  const organizationId = r.getOrganizationId(ctx);
  const fileName = r.assertQueryParam(ctx.query.fileName, 'fileName');
  ctx.body = await contentService.createTranscoderJob(organizationId, userId, fileName);
});

/**
 * Gets the status of the job identified by the given id
 */
// TODO - could add a param here that forces an AWS transcoder query
// TODO - could extra security here by querying based on userId or videoUri as well
mediaRouter.get('/transcoder-jobs/:id', hasAnyPermission(allModifyContentPermissions), async (ctx: Context) => {
  const contentService = r.contentService(ctx);
  const id = r.pathParamToInt(ctx.params.id);
  ctx.body = await contentService.getTranscodeJob(id);
});


export default mediaRouter;
