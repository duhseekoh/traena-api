// @flow
import type { SubscriptionVisibility } from '../../domain/fieldConstants';
import ChannelView from './ChannelView';
import type SubscriptionModel from '../../domain/models/SubscriptionModel';

/**
 * Standalone channel view, when retrieving on its own or in a list
 */
export default class SubscriptionView {
  channelId: number;
  organizationId: number;
  visibility: SubscriptionVisibility;
  channel: ChannelView;
  subscribedUserIds: ?number[];

  constructor(subscription: SubscriptionModel) {
    // trying this pattern out to pick properties. using _.picked bypasses type
    // safety.
    const { channelId, organizationId, visibility, subscribedUserIds, channel } = subscription;
    const picked = {
      channelId,
      organizationId,
      visibility,
      subscribedUserIds,
      channel: channel ? new ChannelView(channel) : undefined,
    };

    Object.assign(this, picked);
  }
}
