// @flow
import _ from 'lodash';

import OrganizationView from './OrganizationView';
import EmbeddedChannelView from './EmbeddedChannelView';
import UserView from './UserView';
import type ContentItemView from './ContentItemView';
import { formatContentItems } from '../formatters/contentItemFormatters';
import type SeriesModel from '../../domain/models/SeriesModel';

export default class SeriesView {
  id: number;
  author: UserView;
  authorId: number;
  channel: EmbeddedChannelView;
  channelId: number;
  contentCount: number;
  contentPreview: ContentItemView[];
  title: string;
  description: string;
  createdTimestamp: Date;
  modifiedTimestamp: Date;
  organization: OrganizationView;

  constructor(series: SeriesModel) {
    const picked = _.pick(series, [
      'id',
      'title',
      'description',
      'authorId',
      'channelId',
      'contentCount',
      'contentPreview',
      'author',
      'channel',
      'organization',
      'createdTimestamp',
      'modifiedTimestamp',
    ]);
    picked.author = new UserView(picked.author);
    picked.channel = new EmbeddedChannelView(picked.channel);
    picked.organization = new OrganizationView(picked.organization);
    picked.contentPreview = series.contentPreview ? _.take(formatContentItems(series.contentPreview), 3) : undefined;
    Object.assign(this, picked);
  }
}

export type SerializedUserView = {
  ...$Exact<SeriesView>,
  createdTimestamp: string;
  modifiedTimestamp: string;
};
