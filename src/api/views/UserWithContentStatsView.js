// @flow
import UserView from './UserView';
import type UserModel, { UserDTO } from '../../domain/models/UserModel';

export default class UserWithContentStatsView {
  user: UserView;
  contentItemStats: {
    liked: boolean,
    comments: number,
    views: number,
    completed: boolean,
  };

  constructor(user: UserModel | UserDTO, contentItemStats: *) {
    const userView = new UserView(user);
    this.user = userView;
    this.contentItemStats = contentItemStats;
  }
}
