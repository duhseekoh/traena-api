// @flow
import type RoleModel from '../../domain/models/RoleModel';

export default class RoleView {
  id: number;
  name: string;
  description: string;

  constructor(role: RoleModel) {
    const { id, name, description } = role;
    const roleView = { id, name, description };
    Object.assign(this, roleView);
  }
}
