// @flow
import _ from 'lodash';
import AuthorView from './AuthorView';
import EmbeddedChannelView from './EmbeddedChannelView';
import SeriesView from './SeriesView';
import type { ContentItemStatus, ContentItemType } from '../../domain/fieldConstants';
import { CONTENT_ITEM_STATUSES } from '../../domain/fieldConstants';
import type ContentItemModel from '../../domain/models/ContentItemModel';
import { formatTags } from '../formatters/contentItemFormatters';


export default class ContentItemView {
  id: number;
  body: {
    type: ContentItemType,
  };
  status: ContentItemStatus;
  author: AuthorView;
  activityCount: number;
  commentCount: number;
  likeCount: number;
  completedCount: number;
  tags: string[];
  createdTimestamp: string;
  modifiedTimestamp: string;
  publishedTimestamp: string;
  channel: EmbeddedChannelView;
  channelId: number;
  series: ?SeriesView;

  constructor(contentItem: ContentItemModel) {
    const picked = _.pick(contentItem, [
      'id',
      'body',
      'status',
      'activityCount',
      'commentCount',
      'likeCount',
      'createdTimestamp',
      'modifiedTimestamp',
      'publishedTimestamp',
      'channel',
      'completedCount',
      'channelId',
      'activitiesOrder',
      'series',
    ]);
    picked.author = new AuthorView(contentItem.author);
    picked.tags = formatTags(contentItem.tags);
    picked.channel = new EmbeddedChannelView(contentItem.channel);
    picked.series =
      contentItem.series && contentItem.series.length
        ? new SeriesView(contentItem.series[0])
        : null;

    Object.assign(this, picked);

    if (!this.status) {
      this.status = CONTENT_ITEM_STATUSES.DRAFT;
    }
  }
}
