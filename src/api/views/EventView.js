// @flow
import _ from 'lodash';
import UserView from './UserView';
import type { EventType } from '../../domain/models/EventModel';
import type EventModel from '../../domain/models/EventModel';

export default class EventView {
  id: number;
  userId: ?number;
  type: EventType;
  data: any;
  createdTimestamp: string;
  user: UserView;

  constructor(event: EventModel) {
    const picked = _.pick(event, ['id', 'userId', 'type', 'data', 'createdTimestamp']);
    picked.user = new UserView(event.user);
    Object.assign(this, picked);
  }
}
