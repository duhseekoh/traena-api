// @flow
import type { UserSeriesStats } from '../../domain/models/UserModel';
import UserView from './UserView';
import type UserModel, { UserDTO } from '../../domain/models/UserModel';

export default class UserWithSeriesStatsView {
  user: UserView;
  seriesStats: {
    completedContentCount: number,
    latestContentCompletionTimestamp: ?string,
    latestViewedContentTimestamp: ?string,
  };

  constructor(user: UserModel | UserDTO, seriesStats: UserSeriesStats) {
    const userView = new UserView(user);
    this.user = userView;
    this.seriesStats = seriesStats;
  }
}
