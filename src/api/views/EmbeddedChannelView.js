// @flow
import _ from 'lodash';
import type ChannelModel from '../../domain/models/ChannelModel';
/**
 * View to be used when a Channel is embedded within a parent resource, e.g. a ContentItem
 */
export default class EmbeddedChannelView {
  id: number;
  name: string;
  organizationId: number;
  description: string;

  constructor(channel: ChannelModel) {
    const picked = _.pick(channel, ['id', 'name', 'organizationId', 'description']);
    Object.assign(this, picked);
  }
}
