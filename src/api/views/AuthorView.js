// @flow
import _ from 'lodash';
import OrganizationView from './OrganizationView';
import type UserModel from '../../domain/models/UserModel';

export default class AuthorView {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  profileImageURI: string;
  organization: OrganizationView;

  constructor(author: UserModel) {
    const picked = _.pick(author, ['id', 'email', 'firstName', 'lastName', 'profileImageURI']);
    if (author.organization) {
      picked.organization = new OrganizationView(author.organization);
    }
    Object.assign(this, picked);
  }
}
