// @flow
import _ from 'lodash';
import type OrganizationModel from '../../domain/models/OrganizationModel';

export default class OrganizationView {
  id: number;
  name: string;
  organizationType: string;

  constructor(organization: OrganizationModel) {
    const organizationView = _.pick(organization, ['id', 'name', 'organizationType', 'isDisabled']);

    // If null or false don't want to expose isDisabled as a field
    if (!organizationView.isDisabled) {
      delete organizationView.isDisabled;
    }
    Object.assign(this, organizationView);
  }
}
