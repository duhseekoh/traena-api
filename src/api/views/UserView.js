// @flow
import _ from 'lodash';
import OrganizationView from './OrganizationView';
import type UserModel, { UserDTO } from '../../domain/models/UserModel';

export default class UserView {
  id: number;
  email: string;
  organizationId: number;
  organization: ?OrganizationView;
  firstName: string;
  lastName: string;
  position: string;
  profileImageURI: ?string;
  createdTimestamp: Date;
  modifiedTimestamp: Date;
  isDisabled: ?boolean;
  tags: string[];

  constructor(user: UserModel | UserDTO) {
    const userViewProperties = [
      'id', 'firstName', 'lastName', 'email', 'organizationId', 'organization', 'profileImageURI', 'createdTimestamp',
      'modifiedTimestamp', 'isDisabled', 'position', 'tags',
    ];
    const userPicked = _.pick(user, userViewProperties);
    if (userPicked.organization) {
      userPicked.organization = new OrganizationView(userPicked.organization);
    }
    Object.assign(this, userPicked);
  }
}

export type SerializedUserView = {
  ...UserView,
  createdTimestamp: string;
  modifiedTimestamp: string;
};
