// @flow
import _ from 'lodash';
import OrganizationView from './OrganizationView';
import type { ChannelVisibility } from '../../domain/fieldConstants';
import type ChannelModel from '../../domain/models/ChannelModel';
/**
 * Standalone channel view, when retrieving on its own or in a list
 */
export default class ChannelView {
  id: number;
  name: string;
  description: string;
  organization: OrganizationView;
  organizationId: number;
  visibility: ChannelVisibility;
  default: boolean;

  constructor(channel: ChannelModel) {
    const picked = _.pick(channel, [
      'id',
      'name',
      'description',
      'visibility',
      'default',
      'organizationId',
      'organization',
      'createdTimestamp',
    ]);
    if (picked.organization) {
      picked.organization = new OrganizationView(picked.organization);
    }
    Object.assign(this, picked);
  }
}
