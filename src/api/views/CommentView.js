// @flow
import _ from 'lodash';
import type CommentModel from '../../domain/models/CommentModel';
import AuthorView from './AuthorView';

export default class CommentView {
  id: number;
  contentId: number;
  text: string;
  /* eslint-disable camelcase */
  createdTimestamp: Date;
  modifiedTimestamp: Date;
  /* eslint-enable camelcase */
  taggedUsers: number[];
  author: AuthorView;

  constructor(comment: CommentModel) {
    const picked = _.pick(comment, ['id', 'contentId', 'text', 'createdTimestamp', 'modifiedTimestamp', 'taggedUsers']);
    picked.author = new AuthorView(comment.author);
    Object.assign(this, picked);
  }
}
