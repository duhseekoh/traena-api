// @flow
import Router from 'koa-router';
import jwt from 'koa-jwt';
import jwksRsa from 'jwks-rsa';
import contentsRoutes from './routes/content/contentRouter';
import mediaRoutes from './routes/mediaRouter';
import meRoutes from './routes/selfRouter';
import eventRoutes from './routes/eventRouter';
import searchRoutes from './routes/searchRouter';
import batchRoutes from './routes/batchRouter';
import internalRoutes from './routes/internalRouter';
import organizationRoutes from './routes/organization/organizationRouter';
import seriesRoutes from './routes/series/seriesRouter';
import channelRoutes from './routes/channelRouter';
import bookmarkRoutes from './routes/bookmarkRouter';
import userRoutes from './routes/userRouter';
import roleRoutes from './routes/roleRouter';
import config from '../common/config';
import { setAuthorization } from './middleware/auth';
import jsonContentType from './middleware/jsonContentType';
import { NotAuthorizedError } from '../common/errors';
import Logger from '../common/logging/logger';

const API_V1 = '/api/v1';

const logger = new Logger('api/index');

const router = new Router();

router.get(API_V1, (ctx) => {
  ctx.body = 'Traena API v1';
});

// Don't expose internal api endpoints as part of the public functional api
// These are meant as internal only apis to be accessed only with client-credentials grant tokens
router.use('/internal', internalRoutes.routes(), jsonContentType);

// Return the appropriate client secret
const getSecret = async (header) => {
  const signatureAlgorithm = header.alg;

  // Only accept RS256 tokens
  if (signatureAlgorithm !== 'RS256') {
    logger.error('Unsupported token', { signatureAlgorithm });
    throw new NotAuthorizedError('Unsupported token');
  }

  // Else use the jwks to get the RS256 public key from the auth0 endpoint
  return jwksRsa.koaJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 2,
    jwksUri: `${config.auth.issuer}.well-known/jwks.json`,
  })(header);
};

const validateJwt = jwt({
  secret: getSecret,
  audience: config.auth.audience,
  issuer: config.auth.issuer,
  algorithms: ['RS256'],
  key: 'jwtData',
});

router.use(`${API_V1}/*`, validateJwt, setAuthorization, jsonContentType);

router.use(`${API_V1}/contents`, contentsRoutes.routes());
router.use(`${API_V1}/media`, mediaRoutes.routes());
router.use(`${API_V1}/users/self`, meRoutes.routes());
router.use(`${API_V1}/search`, searchRoutes.routes());
router.use(`${API_V1}/batch`, batchRoutes.routes());
router.use(`${API_V1}/organizations`, organizationRoutes.routes());
router.use(`${API_V1}/users`, userRoutes.routes());
router.use(`${API_V1}/events`, eventRoutes.routes());
router.use(`${API_V1}/roles`, roleRoutes.routes());
router.use(`${API_V1}/series`, seriesRoutes.routes());
router.use(`${API_V1}/channels`, channelRoutes.routes());
router.use(`${API_V1}/users/self/bookmarks`, bookmarkRoutes.routes());

export default router;
