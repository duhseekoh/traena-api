// @flow
import Logger from '../../common/logging/logger';

const logger = new Logger('api/middleware/permissionUtils');

// indicates a given permission is not used, and is filtered out when calling getPermission(s),
// but in order to properly flow type we need to fully populate the permissions tree
const INFERRED_PERMISSION: string = 'inferred';

const INVALID_PERMISSION: string = 'invalid';

export const Resources = Object.freeze({
  COMMENTS: 'COMMENTS',
  CONTENT: 'CONTENT',
  DEVICES: 'DEVICES',
  ORGANIZATIONS: 'ORGANIZATIONS',
  TASKS: 'TASKS',
  USERS: 'USERS',
  ACTIVITY_RESPONSES: 'ACTIVITY_RESPONSES',
});


export const Operations = Object.freeze({
  READ: 'READ',
  MODIFY: 'MODIFY',
});

export const Contexts = Object.freeze({
  ALL: 'ALL',
  ORG: 'ORG',
  SELF: 'SELF',
});

export type Resource = $Keys<typeof Resources>;
export type Operation = $Keys<typeof Operations>;
export type Context = $Keys<typeof Contexts>;

// Map of Resource -> Operation -> Context -> permission string
type Permissions = { [Resource]: { [Operation]: { [Context]: string } } };

const _Permissions: Permissions = Object.freeze({
  ACTIVITY_RESPONSES: {
    READ: {
      ALL: 'read:all:activity_responses',
      ORG: 'read:org:activity_responses',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:activity_responses',
      ORG: 'modify:org:activity_responses',
      SELF: 'modify:self:activity_responses',
    },
  },
  COMMENTS: {
    READ: {
      ALL: 'read:all:comments',
      ORG: 'read:org:comments',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:comments',
      ORG: 'modify:org:comments',
      SELF: 'modify:self:comments',
    },
  },
  CONTENT: {
    READ: {
      ALL: 'read:all:content',
      ORG: 'read:org:content',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:content',
      ORG: 'modify:org:content',
      SELF: 'modify:self:content',
    },
  },
  DEVICES: {
    READ: {
      ALL: 'read:all:devices',
      ORG: 'read:org:devices',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:devices',
      ORG: 'modify:org:devices',
      SELF: 'modify:self:devices',
    },
  },
  ORGANIZATIONS: {
    READ: {
      ALL: 'read:all:organizations',
      ORG: 'read:org:organizations',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:organizations',
      ORG: 'modify:org:organizations',
      SELF: INFERRED_PERMISSION,
    },
  },
  TASKS: {
    READ: {
      ALL: 'read:all:tasks',
      ORG: 'read:org:tasks',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:tasks',
      ORG: 'modify:org:tasks',
      SELF: 'modify:self:tasks',
    },
  },
  USERS: {
    READ: {
      ALL: 'read:all:users',
      ORG: 'read:org:users',
      SELF: INFERRED_PERMISSION,
    },
    MODIFY: {
      ALL: 'modify:all:users',
      ORG: 'modify:org:users',
      SELF: 'modify:self:users',
    },
  },
});

export function isValidResource(resource: Resource): boolean {
  return !!Resources[resource];
}

export function isValidOperation(operation: Operation): boolean {
  return !!Operations[operation];
}

export function isValidContext(context: Context): boolean {
  return !!Contexts[context];
}

/**
 * Return a single permission for a given resource, operation, and context. If an argument is invalid or the requested
 * permission is inferred - an error is thrown
 * @param resource
 * @param operation
 * @param context
 * @returns The permission string if the permission exists, else the INVALID_PERMISSION string
 */
export function getPermission(resource: Resource, operation: Operation, context: Context): string {
  if (!Resources[resource] || !Operations[operation] || !Contexts[context]) {
    logger.error('Invalid Permission requested: ', { resource, operation, context });
    throw new Error('Invalid Permission requested');
  }
  return _Permissions[resource][operation][context];
}

/**
 * Return a single permission for a given resource, operation, and context. If an argument is invalid or the requested
 * permission is inferred - the INVALID_PERMISSION is returned
 * @param resource
 * @param operation
 * @param context
 * @returns The permission string if the permission exists, else the INVALID_PERMISSION string
 */
export function getPermissionNoThrow(resource: Resource, operation: Operation, context: Context): string {
  if (!Resources[resource] || !Operations[operation] || !Contexts[context]) {
    return INVALID_PERMISSION;
  }
  return _Permissions[resource][operation][context];
}

/**
 * Return a list of all possible permissions for a given resource & operation
 * @param resource
 * @param operation
 */
export function getAllPermissions(resource: Resource, operation: Operation): string[] {
  // Definitely not the most idiomatic js solution, but needed to properly type as Object.values() returns type any
  const result = [];
  const allPerm = _Permissions[resource][operation].ALL;
  const orgPerm = _Permissions[resource][operation].ORG;
  const selfPerm = _Permissions[resource][operation].SELF;

  if (allPerm !== INFERRED_PERMISSION) {
    result.push(allPerm);
  }

  if (orgPerm !== INFERRED_PERMISSION) {
    result.push(orgPerm);
  }

  if (selfPerm !== INFERRED_PERMISSION) {
    result.push(selfPerm);
  }

  return result;
}
