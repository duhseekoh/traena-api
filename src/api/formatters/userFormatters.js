// @flow
import type { ArrayPage, IndexablePage } from '@panderalabs/koa-pageable';
import UserView from '../views/UserView';
import UserWithContentStatsView from '../views/UserWithContentStatsView';
import UserWithSeriesStatsView from '../views/UserWithSeriesStatsView';
import type UserModel, { UserDTOWithContentStats, UserDTOWithSeriesStats } from '../../domain/models/UserModel';

export function formatUser(user: UserModel): UserView {
  return new UserView(user);
}

export function formatUserWithContentStats(dto: UserDTOWithContentStats): UserWithContentStatsView {
  const { user, contentItemStats } = dto;
  return new UserWithContentStatsView(user, contentItemStats);
}

export function formatUserWithContentStatsPage(
  userPage: ArrayPage<UserDTOWithContentStats>,
): ArrayPage<UserWithContentStatsView> {
  return userPage.map(formatUserWithContentStats);
}

export function formatUserWithSeriesStats(dto: UserDTOWithSeriesStats): UserWithSeriesStatsView {
  const { user, seriesStats } = dto;
  return new UserWithSeriesStatsView(user, seriesStats);
}

export function formatUserWithSeriesStatsPage(
  userPage: ArrayPage<UserDTOWithSeriesStats>,
): ArrayPage<UserWithSeriesStatsView> {
  return userPage.map(formatUserWithSeriesStats);
}

export function formatUserPage(userPage: IndexablePage<number, UserModel>): IndexablePage<number, UserView> {
  return userPage.map(formatUser);
}
