// @flow
import RoleView from '../views/RoleView';
import type RoleModel from '../../domain/models/RoleModel';

export function formatRole(role: RoleModel): RoleView {
  return new RoleView(role);
}

export function formatRoles(roles: RoleModel[] = []) {
  return roles.map(role => formatRole(role));
}
