// @flow
import OrganizationView from '../views/OrganizationView';
import type OrganizationModel from '../../domain/models/OrganizationModel';

export default function formatOrganization(organization: OrganizationModel): OrganizationView {
  return new OrganizationView(organization);
}
