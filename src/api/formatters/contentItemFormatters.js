// @flow
/* eslint-disable no-duplicate-imports */
import _ from 'lodash';

import ContentItemView from '../views/ContentItemView';
import type ContentItemModel from '../../domain/models/ContentItemModel';
import type TagModel from '../../domain/models/TagModel';

/**
 * Convert tags to list of strings
 * @returns {Array<string>}
 */
export function formatTags(tags: TagModel[]): string[] {
  return tags.map(tag => tag.text);
}

export function formatContentItem(
  contentItem: ContentItemModel,
): ContentItemView {
  return new ContentItemView(contentItem);
}

/**
 * Convert map of id -> ContentItemModel to map of id -> ContentItemView
 * @param contentItems
 * @returns {*|Object}
 */
export function formatContentItemsMap(contentItems: {
  [number]: ?ContentItemModel,
}): { [number]: ContentItemView } {
  return _.mapValues(contentItems, contentItem =>
    formatContentItem(contentItem),
  );
}

export function formatContentItems(
  contentItems: ContentItemModel[],
): ContentItemView[] {
  return contentItems.map(contentItem => formatContentItem(contentItem));
}
