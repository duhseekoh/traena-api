// @flow
/* eslint-disable no-duplicate-imports */

import SeriesView from '../views/SeriesView';
import type SeriesModel from '../../domain/models/SeriesModel';

export default function formatSeries(series: SeriesModel): SeriesView {
  return new SeriesView(series);
}
