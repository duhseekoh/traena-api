// @flow
import AuthorView from '../views/AuthorView';
import type UserModel from '../../domain/models/UserModel';

export default function formatAuthor(author: UserModel): AuthorView {
  return new AuthorView(author);
}
