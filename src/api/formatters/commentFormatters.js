// @flow
import _ from 'lodash';
import type CommentModel from '../../domain/models/CommentModel';
import CommentView from '../views/CommentView';

export function formatComment(comment: CommentModel): CommentView {
  return new CommentView(comment);
}

export function formatComments(comments: { [number]: CommentModel }): { [number]: CommentView } {
  return _.mapValues(comments, comment => formatComment(comment));
}
