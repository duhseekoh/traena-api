// @flow
/* eslint-disable no-duplicate-imports */
import EmbeddedChannelView from '../views/EmbeddedChannelView';
import ChannelView from '../views/ChannelView';
import SubscriptionView from '../views/SubscriptionView';
import type SubscriptionModel from '../../domain/models/SubscriptionModel';
import type ChannelModel from '../../domain/models/ChannelModel';

export function formatChannel(channel: ChannelModel): ChannelView {
  return new ChannelView(channel);
}

export function formatEmbeddedChannel(
  channel: ChannelModel,
): EmbeddedChannelView {
  return new EmbeddedChannelView(channel);
}

export function formatSubscription(
  subscription: SubscriptionModel,
): SubscriptionView {
  return new SubscriptionView(subscription);
}
