// @flow
import type { IndexablePage } from '@panderalabs/koa-pageable';
import EventView from '../views/EventView';
import type EventModel from '../../domain/models/EventModel';

export function formatEvent(event: EventModel): EventView {
  return new EventView(event);
}

export function formatEventPage(
  eventPage: IndexablePage<number, EventModel>,
): IndexablePage<number, EventView> {
  return eventPage.map(formatEvent);
}
