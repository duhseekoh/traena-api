// @flow

import type { Context } from 'koa';
import Logger from '../../common/logging/logger';
import config from '../../common/config';

const logger = new Logger('api/middleware/errors');

const returnApiStackTrace = config.logging.apiStackTrace;

type errorBody = {
  type: string,
  message: string,
  details?: string,
  stack?: string,
}

export default () => async (ctx: Context, next: *) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;

    const body: errorBody = {
      type: err.type || err.name,
      message: err.message,
    };

    if (err.details) {
      body.details = err.details;
    }

    if (returnApiStackTrace) {
      body.stack = err.stack;
    }

    ctx.body = body;

    const { state, request, response } = ctx;
    const {
      method, url, query, body: reqBody,
    } = request;
    const { body: resBody, status } = response;

    let user;
    if (state && state.user) {
      user = state.user; // eslint-disable-line prefer-destructuring
    }

    /**
     * Log full info for 400s for debug purposes
     * Log full info for 500s for app errors
     */
    if (status >= 400 && status <= 499) {
      logger.debug('<-req-', {
        method, url, query, user, reqBody,
      });
      logger.debug('-res->', {
        method, url, query, user, status, resBody,
      });
    } else if (status >= 500) {
      logger.error('<-req-', {
        method, url, query, user, reqBody,
      });
      logger.error('-res->', {
        method, url, query, user, status, resBody,
      });
    }

    ctx.app.emit('error', err, this);
  }
};
