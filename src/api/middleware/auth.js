// @flow
import type { Context } from 'koa';
import type { Operation, Resource } from '../permissions/permissionUtils';
import * as Permissions from '../permissions/permissionUtils';
import Logger from '../../common/logging/logger';
import UserModel from '../../domain/models/UserModel';
import { NotAuthorizedError, ResourceForbiddenError, ResourceNotFoundError } from '../../common/errors';
import config from '../../common/config';

const logger = new Logger('api/middleware/auth');

const { jwtUserClaimNamespace } = config.auth;

// see: https://auth0.com/docs/api-auth/tutorials/adoption/scope-custom-claims#custom-claims
function getCustomClaimFromJwt(jwtData: *, claim: string) {
  return jwtData[jwtUserClaimNamespace][claim];
}

/**
 * Parses the authorization entities from the token object (which is set by the preceding jwt middleware
 * as ctx.state.jwtData)
 *
 * If the user does not exist, or has no orgs, an exception is thrown.
 *
 * Else it populates ctx.state.user with an object containing the user object enriched with permissions & roles
 */
export async function setAuthorization(ctx: Context, next: *) {
  if (!ctx.state.jwtData) {
    logger.warn('Request does not contain JWT');
    throw new NotAuthorizedError('User does not have permission to access this resource');
  }

  const { jwtData } = ctx.state;
  const tokenEmail = getCustomClaimFromJwt(jwtData, 'email');

  if (!tokenEmail) {
    logger.warn('JWT does not contain email');
    throw new NotAuthorizedError('User does not have permission to access this resource');
  }

  const email = tokenEmail.toLowerCase();
  const groups = getCustomClaimFromJwt(jwtData, 'groups');
  const roles = getCustomClaimFromJwt(jwtData, 'roles');
  const permissions = getCustomClaimFromJwt(jwtData, 'permissions');

  logger.silly('token results: ', {
    email, groups, roles, permissions,
  });

  if (!Array.isArray(groups) || groups.length === 0) {
    throw new NotAuthorizedError('User is not a member of any organizations');
  }

  if (!Array.isArray(roles) || roles.length === 0) {
    throw new NotAuthorizedError('User token does not contain any roles');
  }

  if (!Array.isArray(permissions) || permissions.length === 0) {
    throw new NotAuthorizedError('User token does not contain any permissions');
  }

  const user = await UserModel.getUserByEmail(email);

  if (!user) {
    throw new NotAuthorizedError('No user located with provided email');
  }

  if (user.isDisabled) {
    throw new NotAuthorizedError('User\'s access is disabled');
  }

  // Enrich user with role / permission data from jwt
  user.roles = roles;
  user.permissions = permissions;

  // Add user to request context
  ctx.state.user = user;

  logger.silly('Authenticated User', { email, org: user.organizationId });

  return next();
}

function _hasAnyPermission(userPermissionsList: string[], checkPermissionsList: string[]): boolean {
  return checkPermissionsList.some(checkPermission => userPermissionsList.includes(checkPermission));
}

function _hasPermission(userPermissionList: string[], checkPermission: ?string): boolean {
  if (typeof checkPermission !== 'string') {
    return false;
  }
  return userPermissionList.includes(checkPermission);
}

/**
 * Middleware to check if principal has the given permission. If not throws a 403 exception.
 *
 * Assumes that user authorization has already been performed by setAuthorization,
 * and thus does not validate assumptions
 * @param checkPermission
 */
export const hasPermission = (checkPermission: ?string) => async (ctx: Context, next: *) => {
  const userPermissions: string[] = ctx.state.user.permissions;

  if (!_hasPermission(userPermissions, checkPermission)) {
    throw new ResourceForbiddenError('User does not have permission to access this resource');
  }

  return next();
};

/**
 * Middleware to check if principal has any of the provided permissions. If not throws a 403 exception.
 * Generally intended for use on operations on a collection of resources - such that there is not an individual resource
 * to check permission against. e.g. get /comments or post /comments
 *
 *
 * Assumes that user authorization has already been performed by setAuthorization,
 * and thus does not validate assumptions
 * @param checkPermissionsList
 */
export const hasAnyPermission = (checkPermissionsList: string[]) => async (ctx: Context, next: *) => {
  const userPermissions: string[] = ctx.state.user.permissions;

  if (!_hasAnyPermission(userPermissions, checkPermissionsList)) {
    throw new ResourceForbiddenError('User does not have permission to access this resource');
  }

  return next();
};

/**
 * Factory to return a koa middleware for validating if a user is able to perform a specific operation on a
 * specific instance of resource. e.g. put /comments/123 or get /comments/123. Will throw a ResourceNotFound exception
 * if the getResourceCallback results in an empty result
 *
 * @param resource The enum from Permissions.Resources for the resource to be accessed
 * @param operation The enum from Permissions.Operations for the operation to be performed on the resource
 * @param getResourceCallback Callback to obtain an instance of the resource. The function is called with the
 * koa ctx.
 * @param selfPermissionPredicate Callback to determine if resource is owned by principal. The function is called with
 * the koa ctx and the item returned by the getResourceCallback.
 * @param orgPermissionPredicate Callback to determine if resource is accessible to principal's org. The function
 * is called with the koa ctx and the item returned by the getResourceCallback.
 * @returns {function(*=, *)} A middleware that will validate the principal has appropriate permisions to perform the
 * requested operation on the requested resource
 */
export function permissionMiddlewareFactory<T>(
  resource: Resource, operation: Operation,
  getResourceCallback: (ctx: Context) => *,
  selfPermissionPredicate: (ctx: Context, resource: T) => boolean | Promise<boolean>,
  orgPermissionPredicate: (ctx: Context, resource: T) => boolean | Promise<boolean>,
) {
  return async (ctx: Context, next: *) => {
    const userPermissions = ctx.state.user.permissions;

    if (!Permissions.isValidResource(resource)) {
      logger.error('Resource does not exist', { resource });
      throw new Error(`Requested resource ${resource} does not exist`);
    }

    if (!Permissions.isValidOperation(operation)) {
      logger.error('Operation does not exist', { operation });
      throw new Error(`Requested Operation does not exist ${operation}`);
    }

    const allPermissions = Permissions.getAllPermissions(resource, operation);

    // If principal has none of the permissions, then they cannot access any resources of this type.
    if (!_hasAnyPermission(userPermissions, allPermissions)) {
      throw new ResourceForbiddenError('Forbidden');
    }

    // Because we always request these permissions, we don't want to throw an exception, we can just back back the
    // invalid permission if it doesn't exist and it will not match
    const allPermission: ?string = Permissions.getPermissionNoThrow(resource, operation, Permissions.Contexts.ALL);
    const selfPermission: ?string = Permissions.getPermissionNoThrow(resource, operation, Permissions.Contexts.SELF);
    const orgPermission: ?string = Permissions.getPermissionNoThrow(resource, operation, Permissions.Contexts.ORG);

    // Load the resource using the provided callback
    const item = await getResourceCallback(ctx);
    // Specifically don't want to check for false, as that is what non-resource specific permissions checks return
    // see: organizationRouter.canReadOrganization
    if (item === null || item === undefined) {
      throw new ResourceNotFoundError('Resource not found');
    }

    // If principal has the 'all' permission - they can access
    if (_hasPermission(userPermissions, allPermission)) {
      return next();
    }

    // If principal has permission to modify their own resource AND they are the owner of this resource
    if (_hasPermission(userPermissions, selfPermission) && await selfPermissionPredicate(ctx, item)) {
      return next();
    }

    // If principal can access all organization content and this resource is accessible to their org
    if (_hasPermission(userPermissions, orgPermission) && await orgPermissionPredicate(ctx, item)) {
      return next();
    }

    // User has permissions to access resources of this TYPE but not this specific resource, as such to avoid exposing
    // the existence of resources the user should not see we return a 404
    throw new ResourceNotFoundError('Resource not found');
  };
}

type CatchMiddlewareFnReturn = Promise<true | Error>;
type CatchMiddlewareFn = () => CatchMiddlewareFnReturn;

/**
 * Provides 'OR' logic on permissioning checks. As long as one of
 * the provided middlewares doesn't throw an exception, then we allow it to continue
 * down the middleware chain.
 * This calls all of the provided middleware at once.
 */
export const passesAnyCheck = (
  permissionMiddlewares: Function[], // OR logic is applied to the return of these
) => async (ctx: Context, next: *) => {
  // curry each middleware by wrapping in a method that in success case, returns
  // true, and in failure case returns the error rather than throwing it
  const catchMiddlewares: CatchMiddlewareFn[] = permissionMiddlewares.map(mw => async () => {
    try {
      await mw(ctx, () => true);
      return true;
    } catch (err) {
      return err;
    }
  });

  // call each middleware
  const promises: CatchMiddlewareFnReturn[] = catchMiddlewares.map(mw => mw());

  // determine what to do with the set of middleware results
  return Promise.all(promises)
    .then((results: (true | Error)[]) => {
      const anyPassed = results.some(result => result === true);
      // if any of the middleware pass, then we're good to go
      if (anyPassed) {
        return next();
      }
      // none pass, throw the first failing middleware result
      const firstError = results.find(result => result !== true);
      if (firstError) {
        throw firstError;
      } else {
        throw new NotAuthorizedError('Not authorized - reason unknown');
      }
    });
};

export const AdminPermissions: { [string]: string } = {
  ADMIN: 'admin',
  ELASTIC_SEARCH: 'admin:elasticsearch',
};

export type AdminPermission = $Keys<typeof AdminPermissions>;


/**
 * Middleware to check if user has Admin (God Mode!) permission, or the provided admin permission
 *
 * Assumes that user authorization has already been performed by setAuthorization,
 * and thus does not validate assumptions
 * @param checkAdminPermission
 */
export const hasAdminPermission = (checkAdminPermission?: AdminPermission) => async (ctx: Context, next: *) => {
  const userPermissions: string[] = ctx.state.user.permissions;

  if (!_hasPermission(userPermissions, AdminPermissions.ADMIN)
    && !_hasPermission(userPermissions, checkAdminPermission)) {
    throw new ResourceForbiddenError('User does not have permission to access this resource');
  }

  return next();
};
