// @flow

import type { Context } from 'koa';

export default async function jsonContentType(ctx: Context, next: *) {
  ctx.type = 'application/json';
  return next();
}
