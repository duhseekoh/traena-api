// @flow
import 'source-map-support/register';
import path from 'path';
import type Application from 'koa';
import Koa from 'koa';

import json from 'koa-json';
import koalogger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import cors from '@koa/cors';
import elasticSearch from 'elasticsearch';
import AWS from 'aws-sdk';
import SNS from 'aws-sdk/clients/sns';
import Kinesis from 'aws-sdk/clients/kinesis';
import awsEs from 'http-aws-es';
import Knex from 'knex';
import { Model } from 'objection';

import config from './common/config';
import routes from './api';

import Auth0Client from './client/Auth0Client';
import SegmentClient from './client/SegmentClient';

import AuthService from './services/AuthService';
import AnalyticsService from './services/AnalyticsService';
import ElasticSearchService from './services/ElasticSearchService';
import FeedService from './services/FeedService';
import SearchService from './services/SearchService';
import CommentService from './services/CommentService';
import ContentService from './services/ContentService';
import UserService from './services/UserService';
import MediaService from './services/MediaService';
import NotificationService from './services/NotificationService';
import OrganizationService from './services/OrganizationService';
import TaskService from './services/TaskService';
import EmailService from './services/EmailService';
import EventService from './services/EventService';
import ActivityService from './services/ActivityService';
import ElasticSearchGatheringService from './services/ElasticSearchGatheringService';
import ChannelService from './services/ChannelService';
import SeriesService from './services/SeriesService';
import errorHandler from './api/middleware/errors';
import Logger from './common/logging/logger';
import { parseOptionalIntOrThrow } from './common/numberUtils';

const logger = new Logger('server.js');

const knex = Knex(config.db); // eslint-disable-line new-cap
Model.knex(knex);

const port: number = parseOptionalIntOrThrow(process.env.PORT) || 3001;
const app: Application = new Koa();

AWS.config.getCredentials(() => {
  logger.silly('Retrieved aws credentials for debugging purposes', AWS.config.credentials);
});

const esConfig = config.elasticSearch.useAws ? ({
  hosts: config.elasticSearch.apiBaseUrl,
  connectionClass: awsEs,
  amazonES: {
    region: AWS.config.region,
    getCredentials: true,
  },
  log: config.elasticSearch.logLevel,
}) : ({
  host: config.elasticSearch.apiBaseUrl,
});
const elasticSearchClient = new elasticSearch.Client(esConfig);

// Setup SNS Client (for push notifications)
const snsClient = new SNS();
// Setup Kinesis Client (for streaming content updates)
const kinesisConfig = config.aws.mockAws ? {
  endpoint: new AWS.Endpoint('http://localstack:4568'),
} : {};
logger.debug('Creating kinesis client with config', kinesisConfig);

const kinesisClient = new Kinesis(kinesisConfig);
const kinesisIndexStream = config.kinesis.indexStream;
const kinesisIndexRealtimeStream = config.kinesis.indexRealtimeStream;

const auth0Client = new Auth0Client(config);
const segmentClient =
  new SegmentClient(config.analytics.segment.writeKey, config.env.name, config.packageJson.version);
// Instantiate any services passing in any necessary params
// // TODO - remove config from service params. Pass in what is needed from config instead.
const analyticsService = new AnalyticsService();
const eventService = new EventService(segmentClient);
const elasticSearchService = new ElasticSearchService(elasticSearchClient);
const elasticSearchGatheringService = new ElasticSearchGatheringService(
  kinesisClient,
  kinesisIndexStream, kinesisIndexRealtimeStream, eventService,
);
const mediaService = new MediaService(config);
const notificationService = new NotificationService(config, snsClient);
const contentService = new ContentService(
  eventService, mediaService,
  elasticSearchService, elasticSearchGatheringService, notificationService,
);
const emailService = new EmailService(config);
const authService = new AuthService(config, emailService, auth0Client);
const channelService = new ChannelService(eventService, elasticSearchGatheringService);
const organizationService = new OrganizationService(
  config,
  authService,
  eventService,
  channelService,
  elasticSearchService,
);
const userService = new UserService(
  authService, organizationService, eventService,
  elasticSearchGatheringService, elasticSearchService,
);
const taskService = new TaskService(contentService, eventService, elasticSearchGatheringService);
const seriesService = new SeriesService(
  eventService,
  channelService,
  userService,
  elasticSearchGatheringService,
);
const activityService = new ActivityService(eventService);

const services = {
  analyticsService,
  contentService,
  eventService,
  elasticSearchService,
  notificationService,
  organizationService,
  authService,
  channelService,
  commentService: new CommentService(
    contentService, notificationService,
    eventService, elasticSearchGatheringService,
  ),
  elasticSearchGatheringService,
  userService,
  feedService: new FeedService(contentService, elasticSearchService, eventService, taskService),
  searchService: new SearchService(elasticSearchService, contentService, eventService),
  taskService,
  mediaService,
  activityService,
  seriesService,
};

app.use(errorHandler());
app.use(json());
app.use(koalogger());
app.use(bodyParser());
// To allow us to split up swagger spec into multiple documents and reference them appropriately in editor.swagger.io
app.use(cors());

app.use(async (ctx, next) => {
  // Apply services to the ctx so we can use them from routes
  Object.assign(ctx, {
    state: {
      ...ctx.state,
      services,
    },
  });
  return next();
});
// Serve swagger on the root
app.use(serve(path.join(__dirname, './src/static/swagger-ui')));
// Serve up any other static files (e.g. api.yaml)
app.use(serve(path.join(__dirname, './src/static')));

app.use(routes.routes());
app.use(routes.allowedMethods());

app.listen(port, undefined, undefined, (err) => {
  if (err) throw err;
  logger.debug('Database configuration:', { dbConfig: config.db });
  logger.debug(`The server is listening on port: ${port}`); // eslint-disable-line no-console
});
