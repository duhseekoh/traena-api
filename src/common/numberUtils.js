// @flow
import _ from 'lodash';
import { NumberFormatError } from './errors';


/**
 * Converts a string into a number, throwing an exception if the string does not parse
 * @param input
 * @returns {*}
 */
export function parseIntOrThrow(input: ?string): number {
  const result = parseInt(input, 10);

  if (!input) {
    throw new NumberFormatError('Could not convert empty string to number');
  }
  if (typeof result !== 'number' || Number.isNaN(result)) {
    throw new NumberFormatError(`Could not convert '${input}' to number`);
  }
  return result;
}

/**
 *  Converts a string into a number, throwing an exception if the string does not parse
 *  If the string is null or undefined return null
 *
 * @param input
 * @returns The parsed number, or null
 */
export function parseOptionalIntOrThrow(input: ?string): ?number {
  if (!input) {
    return null;
  }
  return parseIntOrThrow(input);
}

export function parseIntFromCommaListOrThrow(input: string): number[] {
  if (input.length === 0) {
    throw new NumberFormatError('Could not convert empty string to number');
  }

  return _.split(input, ',').map(it => parseIntOrThrow(it.trim()));
}

export function parseIntFromCommaListNullable(input: ?string): ?number[] {
  if (!input || input.length === 0) {
    return null;
  }

  const ints = _.split(input, ',').map(it => parseInt(it.trim(), 10));
  return _.filter(ints, _.identity);
}


export function parseIntsOrThrow(input: string[]): number[] {
  return input.map(it => parseIntOrThrow(it));
}
