// @flow
import _ from 'lodash';
import type { Context } from 'koa';
import {
  parseIntFromCommaListOrThrow,
  parseIntOrThrow,
  parseIntFromCommaListNullable,
} from './numberUtils';
import { BadRequestError, ResourceNotFoundError, ServerError } from './errors';
import { CONTENT_ITEM_STATUSES } from '../domain/fieldConstants';
import type { ContentItemStatus } from '../domain/fieldConstants';
import type UserModel from '../domain/models/UserModel';
import type ChannelService from '../services/ChannelService';
import type CommentService from '../services/CommentService';
import type ContentService from '../services/ContentService';
import type ElasticSearchGatheringService from '../services/ElasticSearchGatheringService';
import type ElasticSearchService from '../services/ElasticSearchService';
import type FeedService from '../services/FeedService';
import type MediaService from '../services/MediaService';
import type NotificationService from '../services/NotificationService';
import type OrganizationService from '../services/OrganizationService';
import type SearchService from '../services/SearchService';
import type TaskService from '../services/TaskService';
import type EventService from '../services/EventService';
import type AnalyticsService from '../services/AnalyticsService';
import type UserService from '../services/UserService';
import type AuthService from '../services/AuthService';
import type ActivityService from '../services/ActivityService';
import type SeriesService from '../services/SeriesService';

/**
 * Throw an error if the body is null, undefined, or {}, else return the body
 * @param body
 * @returns {?*}
 */
export function assertBody(body: ?*): * {
  if (!body || _.isEmpty(body)) {
    throw new BadRequestError('Request body is required');
  }
  return body;
}

/**
 * Throws ResourceNotFoundError if the given resource is null or undefined, else returns the resource
 *
 * @param resource
 * @returns {?*}
 */
export function assertResource(resource: ?Object): Object {
  if (!resource) {
    throw new ResourceNotFoundError('Resource not found');
  }
  return resource;
}

/**
 * Used to validate that a POST / PUT / DELETE completed successfully.
 * Throws an error if the result is null, undefined, or false.
 *
 * @param result
 * @returns {?Object|boolean}
 */
export function assertModifyResourceResult<T>(result: ?T): T {
  if (!result) {
    throw new ServerError('Unknown error completing requested operation');
  }

  return result;
}

/**
 * In the very few cases where we need a valid url path param that is not a number (e.g. deviceToken),
 * we need to convert it from ?string to string to avoid flow warnings
 *
 * @param pathParam
 */
export function assertPathParam(pathParam: ?string): string {
  if (!pathParam) {
    throw new BadRequestError('Url path variable required');
  }
  return pathParam;
}

/**
 * Simply validates that the parameter exists, throwing BadRequestError with the given parameter name if not.
 * @param param
 * @param msg
 * @returns {*}
 */
export function assertQueryParam(queryParam: ?string, paramName: string): string {
  if (!queryParam) {
    throw new BadRequestError(`Query parameter ${paramName} is required`);
  }
  return queryParam;
}

/**
 * Converts a url path parameter to a number
 * Throws a BadRequestError if it does not exist
 * @param pathParam
 * @returns {number}
 */
export function pathParamToInt(pathParam: ?string): number {
  return parseIntOrThrow(assertPathParam(pathParam));
}

/**
* Converts a query string parameter 'true' or 'false' into the respective boolean value
*/
export function queryParamToBoolean(queryParam: ?string): boolean {
  return queryParam === 'true';
}

/**
 * Converts a query string parameter to a number
 * Throws a BadRequestError with the query parameter name if the query parameter is null or undefined,
 * Throws a NumberFormatError if the query parameter exists, but cannot be parsed into an integer
 * @param queryParam
 * @param paramName
 * @returns {number}
 */
export function queryParamToInt(queryParam: ?string, paramName: string): number {
  return parseIntOrThrow(assertQueryParam(queryParam, paramName));
}

export function optionalQueryParamToInt(queryParam: ?string): ?number {
  if (!queryParam) {
    return undefined;
  }

  return parseIntOrThrow(queryParam);
}

/**
 * Converts a query string parameter containing comma separated numbers into an array of numbers.
 *
 * Throws a BadRequestError with the query parameter name if the query parameter is null or undefined,
 * Throws a NumberFormatError if any element cannot be parsed into an integer
 *
 * @param pathParam
 * @returns {number[]}
 */
export function queryParamCommaListToIntList(queryParam: ?string, paramName: string): number[] {
  return parseIntFromCommaListOrThrow(assertQueryParam(queryParam, paramName));
}

export function queryParamCommaListToIntListNullable(queryParam: ?string): ?number[] {
  return parseIntFromCommaListNullable(queryParam);
}

export function queryParamCommaListToStringList(queryParam: ?string): ?string[] {
  if (!queryParam) {
    return undefined;
  }
  return _.split(queryParam, ',').map(it => it.trim());
}

function parseContentItemStatusOrThrow(input: ?string): ContentItemStatus {
  const result = input ? CONTENT_ITEM_STATUSES[input] : undefined;
  if (!result) {
    throw new BadRequestError('Invalid contentStatusInclude params');
  }

  return result;
}

function parseContentItemsStatusFromListOrThrow(input: ?string): ?ContentItemStatus[] {
  if (!input) {
    return undefined;
  }
  return _.split(input, ',').map(it => parseContentItemStatusOrThrow(it.trim()));
}

export function queryParamCommaListToContentItemStatusList(queryParam: ?string) {
  return parseContentItemsStatusFromListOrThrow(queryParam);
}
/**
 * Functions to read and validate elements from the Context object
 */
export function getUserId(ctx: Context): number {
  return ctx.state.user.id;
}

export function getUser(ctx: Context): UserModel {
  return ctx.state.user;
}

export function getOrganizationId(ctx: Context): number {
  return ctx.state.user.organizationId;
}

/**
 * Return the body from the context
 * Throws a BadRequestError if the body is null, undefined, {} or already contains an id
 * @returns {*}
 */
export function getPostBody(ctx: Context): any {
  const body = assertBody(ctx.request.body);
  if (body.id) {
    throw new BadRequestError('Resource to be created must not contain an Id');
  }
  return body;
}

/**
 * Takes the context, and a function to call on the context to return the put path variable.
 * Throws a BadRequestError if the body is null, undefined, {}
 * or if the id in the request body does not match the output of the getIdCallback
 * @param ctx
 * @param getIdCallback - callback function to return the put's url path variable
 * @returns {*}
 */
export function getPutBody(ctx: Context, getIdCallback: (Context) => number): any {
  const body = assertBody(ctx.request.body);
  const urlIdParam = getIdCallback(ctx);

  if (!urlIdParam) {
    throw new Error('Url id parameter is required');
  }

  if (body.id !== urlIdParam) {
    throw new BadRequestError('Id in request body must match id in url');
  }

  return body;
}


// get services
export function channelService(ctx: Context): ChannelService {
  return ctx.state.services.channelService;
}

export function commentService(ctx: Context): CommentService {
  return ctx.state.services.commentService;
}

export function contentService(ctx: Context): ContentService {
  return ctx.state.services.contentService;
}

export function elasticSearchGatheringService(ctx: Context): ElasticSearchGatheringService {
  return ctx.state.services.elasticSearchGatheringService;
}

export function elasticSearchService(ctx: Context): ElasticSearchService {
  return ctx.state.services.elasticSearchService;
}

export function feedService(ctx: Context): FeedService {
  return ctx.state.services.feedService;
}

export function mediaService(ctx: Context): MediaService {
  return ctx.state.services.mediaService;
}

export function notificationService(ctx: Context): NotificationService {
  return ctx.state.services.notificationService;
}

export function organizationService(ctx: Context): OrganizationService {
  return ctx.state.services.organizationService;
}

export function searchService(ctx: Context): SearchService {
  return ctx.state.services.searchService;
}

export function taskService(ctx: Context): TaskService {
  return ctx.state.services.taskService;
}

export function userService(ctx: Context): UserService {
  return ctx.state.services.userService;
}

export function eventService(ctx: Context): EventService {
  return ctx.state.services.eventService;
}

export function analyticsService(ctx: Context): AnalyticsService {
  return ctx.state.services.analyticsService;
}

export function authService(ctx: Context): AuthService {
  return ctx.state.services.authService;
}

export function activityService(ctx: Context): ActivityService {
  return ctx.state.services.activityService;
}

export function seriesService(ctx: Context): SeriesService {
  return ctx.state.services.seriesService;
}
