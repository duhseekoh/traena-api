// @flow
/**
* Turns a string like 'D Dom  Doo' => 'D* Dom* Doo*'
* @param {string} string A phrase with one or multiple words
*/
export function stringToWildcardString(string: string): string { // eslint-disable-line
  const stringWithWildcards = string
    .replace(/\s\s+/g, ' ') // Turn multiple spaces into a single space
    .trim() // remove spaces at end and beginning of word
    .split(' ')
    .map(word => `${word}*`)
    .join(' ');
  return stringWithWildcards;
}

export function removeSpecialChars(string: string): string {
  return string.replace(/[^A-Z0-9]+/ig, '_');
}
