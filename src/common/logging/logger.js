// @flow
import winston from 'winston';
import config from '../config';

require('winston-loggly-bulk');

// Log levels { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
//  (https://github.com/winstonjs/winston#logging-levels)
class WinstonLogger {
  winston: *;
  levels: {};
  // The maximum log level active across transports
  maxLevel: number;


  constructor() {
    this.winston = winston;

    this.levels = {
      error: 0,
      warn: 1,
      info: 2,
      verbose: 3,
      debug: 4,
      silly: 5,
    };

    this.maxLevel = this.levels[config.logging.consoleLevel];
    this.setupConsole(config.logging.consoleLevel);

    if (config.logging.remoteLevel) {
      const remoteLevel = this.levels[config.logging.remoteLevel];

      if (remoteLevel > this.maxLevel) {
        this.maxLevel = remoteLevel;
      }

      // is only setup if a remoteLevel is defined
      this.setupLoggly(config.logging.logglyToken, config.logging.remoteLevel, config.env.name);
      this.info('Remote log level set', { logLevel: config.logging.remoteLevel, env: config.env.name });
    }

    this.winston.setLevels(this.levels);

    this.warn('Max Logging Level: ', this.maxLevel);
  }

  isActive(level: string) {
    return this.maxLevel >= this.levels[level];
  }

  setupConsole(logLevel: string) {
    this.winston.default.transports.console.level = logLevel; // only for console logs
    this.winston.default.transports.console.colorize = true;
    this.winston.default.transports.console.timestamp = true;
    this.winston.default.transports.console.prettyPrint = true;
  }

  setupLoggly(logglyToken: string, logLevel: string, nodeEnv: string) {
    this.winston.add(winston.transports.Loggly, {
      inputToken: logglyToken,
      subdomain: 'traenaio',
      tags: ['api', nodeEnv],
      json: true,
      level: logLevel,
    });
  }

  log(...params: *) {
    this.silly(...params);
  }

  silly(...params: *) {
    this.winston.log('silly', ...params);
  }

  debug(...params: *) {
    this.winston.log('debug', ...params);
  }

  info(...params: *) {
    this.winston.log('info', ...params);
  }

  warn(...params: *) {
    this.winston.log('warn', ...params);
  }

  error(...params: *) {
    this.winston.log('error', ...params);
  }
}

const logger = new WinstonLogger();

/**
 * Delegate class that injects the name of the file from which the log statement is made into the logs meta information
 * before passing it to the singleton winston log wrapper. Note that this logger does not support interpolation ala
 * logger.method('this is my value %s', value); It only supports a message, and a meta object. If you need to use
 * interpolation, get an instance of the underlying logger via the getWinstonLogger(); method and use that directly
 * (please remember though to include the filename in the meta when logging with that method.
 */
export default class Logger {
  filename: string;
  logger: WinstonLogger;

  constructor(filename: string) {
    this.filename = filename;
    this.logger = logger;
  }

  getWinstonLogger(): WinstonLogger {
    return this.logger;
  }

  _log(level: string, message: string, meta?: {}) {
    // Since we are doing work here, we should check to make sure the log level is actually active
    if (this.logger.isActive(level)) {
      const newMeta = {};

      // if primitive or array type, need to set it to a property when adding to object
      if (meta && (typeof meta !== 'object' || Array.isArray(meta))) {
        newMeta.val = meta;
      } else {
        Object.assign(newMeta, meta);
      }
      newMeta.filename = this.filename;

      // $FlowFixMe - ignore until computed properties are supported. https://github.com/facebook/flow/issues/3460
      this.logger[level](message, newMeta);
    }
  }

  silly(message: string, meta?: {}) {
    this._log('silly', message, meta);
  }

  debug(message: string, meta?: {}) {
    this._log('debug', message, meta);
  }

  info(message: string, meta?: {}) {
    this._log('info', message, meta);
  }

  warn(message: string, meta?: {}) {
    this._log('warn', message, meta);
  }

  error(message: string, meta?: {}) {
    this._log('error', message, meta);
  }
}
