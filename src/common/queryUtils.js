// @flow
import type { Order } from '@panderalabs/koa-pageable';
/**
* Accepts an object name and a property of the form "obj.property", and converts it into a sort clause
* that can be passed into objection's `orderByRaw()` method.
* @param jsonProperty an obj and property name, i.e. `body.title`
* @param direction `asc` or `desc`
* @returns {string} i.e. body->>'title'
*/
export function buildRawSqlSortClauseForJsonProperty(
  jsonProperty: string,
  direction: string,
) {
  const parts = jsonProperty.split('.');
  return parts.reduce((result, part, i) => {
    if (i === 0) {
      return result;
    }

    const lastIteration = i === parts.length - 1;

    const operator = lastIteration ? '->>' : '->';

    result = `${result}${operator}'${part}'`; // eslint-disable-line
    if (lastIteration) {
      result = `${result} ${direction}`; // eslint-disable-line
    }

    return result;
  }, parts[0]);
}

/**
* Appends `.keyword` to the end of a string, so it can be used as a text property
* with which we can sort elasticsearch data
*/
export function formatTextPropertyForEsSort(esSortProperty: string) {
  return `${esSortProperty}.keyword`;
}

export function formatSortForElasticsearch(orders: Order[], textProperties: string[]) {
  return orders.map((it) => {
    if (textProperties.includes(it.property)) {
      it.property = formatTextPropertyForEsSort(it.property);
    }
    return it;
  });
}
