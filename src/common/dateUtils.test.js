import { toUTCTimestampString } from './dateUtils';

describe('dateUtils', () => {
  test('when given a JS date toUTCTimestampString it should match the UTC string', () => {
    // Date with UTC timezone - year, month, date, hours, minutes, milliseconds
    const dateToCheck = new Date(Date.UTC(2017, 2, 20, 13, 54, 55, 400));

    const expected = '2017-03-20T13:54:55.400+00:00';
    const actual = toUTCTimestampString(dateToCheck);

    expect(actual).toEqual(expected);
  });

  test('when given a utc date string toUTCTimestampString it should match the UTC string', () => {
    // Javascript toISOString (always outputs as UTC)
    const dateToCheck = '2017-03-20T13:54:55.400Z';

    const expected = '2017-03-20T13:54:55.400+00:00';
    const actual = toUTCTimestampString(dateToCheck);

    expect(actual).toEqual(expected);
  });

  test('when given a non utc date string toUTCTimestampString it should match the UTC string', () => {
    // Local timezone iso string
    const dateToCheck = '2017-03-20T13:54:55.400-05:00';

    const expected = '2017-03-20T18:54:55.400+00:00';
    const actual = toUTCTimestampString(dateToCheck);

    expect(actual).toEqual(expected);
  });

  test('when given a local JS date toUTCTimestampString it should match the JS formatted UTC string', () => {
    // Date with LOCAL timezone - year, month, date, hours, minutes, milliseconds
    const dateToCheck = new Date(2017, 2, 20, 13, 54, 55, 400);

    // Go through a couple transformations and verify they are still equal on the other side
    const expected = dateToCheck.toISOString();
    const actual = new Date(toUTCTimestampString(dateToCheck)).toISOString();

    expect(actual).toEqual(expected);
  });
});
