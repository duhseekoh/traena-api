// @flow
import moment from 'moment';

/**
 * Returns the date as a LocalDate string formatted as an ISO 8601 date (YYYY-MM-DD)
 * @param date
 * @returns {string}
 */
export function localDate(date: Date): string { // eslint-disable-line import/prefer-default-export
  return moment(date).local().format('YYYY-MM-DD');
}

/**
 * Converts a date (or alternately formatted timestamp string) into an ISO-8601 utc timestamp string
 * @param date
 * @returns {*}
 */
export function toUTCTimestampString(date: ?Date | ?string): ?string {
  if (!date) {
    return date;
  }
  // return moment.utc(date).format();
  return moment.utc(date).format('YYYY-MM-DDTHH:mm:ss.SSSZ');
}
