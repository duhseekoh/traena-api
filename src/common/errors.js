/**
 * Error class to be thrown when a resource does not meet minimum validation requirements. Such as missing required
 * elements or invalid values.
 */
export class ResourceValidationError extends Error {
  /**
   * @param message Required error message
   * @param details Optional object/array containing specific error details. For instance list of multiple validation
   * errors
   */
  constructor(message, details) {
    super(message);
    this.name = 'ResourceValidationError';
    this.message = message;
    this.details = details;
    this.stack = new Error().stack;
    this.status = 400;
  }
}

/**
 * Error class to be thrown when a particular method is not implemented.
 */
export class NotImplementedError extends Error {
  constructor(message) {
    super(message);
    this.name = 'NotImplementedError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 500;
  }
}

export class ResourceNotFoundError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ResourceNotFoundError';
    this.message = message || 'Resource not found';
    this.stack = new Error().stack;
    this.status = 404;
  }
}

export class BadRequestError extends Error {
  constructor(message) {
    super(message);
    this.name = 'BadRequestError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 400;
  }
}

export class NumberFormatError extends Error {
  constructor(message) {
    super(message);
    this.name = 'NumberFormatError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 400;
  }
}

export class NotAuthorizedError extends Error {
  constructor(message) {
    super(message);
    this.name = 'NotAuthorizedError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 401;
  }
}

export class ResourceForbiddenError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ResourceForbiddenError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 403;
  }
}

export class ConflictError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ConflictError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 409;
  }
}

export class Auth0ApiError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Auth0ApiError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 500;
  }
}

// Any other error
export class ServerError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ServerError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 500;
  }
}

export class AwsError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ServerError';
    this.message = message;
    this.stack = new Error().stack;
    this.status = 500;
  }
}
