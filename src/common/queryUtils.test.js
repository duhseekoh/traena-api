import * as queryUtils from './queryUtils';

describe('queryUtils', () => {
  describe('buildRawSqlSortClauseForJsonProperty', () => {
    test("when given `obj.property`, the result is a string like: `obj->>'property'`", () => {
      const expected = "obj->>'property' desc";
      const actual = queryUtils.buildRawSqlSortClauseForJsonProperty('obj.property', 'desc');

      expect(actual).toEqual(expected);
    });
  });

  describe('buildRawSqlSortClauseForJsonProperty', () => {
    test("when given `obj.property.property`, the result is a string like: `obj->>'property'->>'property'`", () => {
      const expected = "obj->'property'->>'property' asc";
      const actual = queryUtils.buildRawSqlSortClauseForJsonProperty('obj.property.property', 'asc');

      expect(actual).toEqual(expected);
    });
  });

  describe('formatTextPropertyForEsSort', () => {
    test('when given a string, `.keyword` is appended to the end of the string', () => {
      const expected = 'property.keyword';
      const actual = queryUtils.formatTextPropertyForEsSort('property');

      expect(actual).toEqual(expected);
    });
  });
});
