import { stringToWildcardString, removeSpecialChars } from './stringUtils';

describe('stringUtils', () => {
  describe('stringToWildcardString', () => {
    test('when given a phrase with one word should be a wildcard after the word', () => {
      const expected = 'This*';
      const actual = stringToWildcardString('This');

      expect(actual).toEqual(expected);
    });

    test('when given a phrase with multiple words should be a wildcard after each word in the phrase', () => {
      const expected = 'This* is* a* string*';
      const actual = stringToWildcardString('This is a string');

      expect(actual).toEqual(expected);
    });

    test(`when given a phrase with multiple words and multiple spaces between some
        words should be a wildcard after each word in the phrase, and no lone wildcards`, () => {
      const expected = 'This* is* a* string*';
      const actual = stringToWildcardString('This   is  a string');

      expect(actual).toEqual(expected);
    });

    test('when given a phrase with trailing spaces should be a wildcard after the word, and no lone wildcards', () => {
      const expected = 'This*';
      const actual = stringToWildcardString('This ');

      expect(actual).toEqual(expected);
    });

    test('when given a phrase with leading spaces should be a wildcard after the word, and no lone wildcards', () => {
      const expected = 'This*';
      const actual = stringToWildcardString(' This');

      expect(actual).toEqual(expected);
    });
  });

  describe('removeSpecialChars', () => {
    test('string with no special characters or spaces should not be modified', () => {
      const expected = 'this_is_a_good_string';
      const actual = removeSpecialChars('this_is_a_good_string');

      expect(actual).toEqual(expected);
    });

    test('string with special characters should be replaced with underscores', () => {
      const expected = 'this_is_a_good_string';
      const actual = removeSpecialChars('this%-%is*a(good)string');

      expect(actual).toEqual(expected);
    });

    test('string with spaces should be replaced with underscores', () => {
      const expected = 'this_is_a_good_string';
      const actual = removeSpecialChars('this is a good string');

      expect(actual).toEqual(expected);
    });
  });
});
