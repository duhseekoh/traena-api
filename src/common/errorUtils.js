// @flow

/* eslint-disable import/prefer-default-export */
export function isDatabaseConflict(err: Error): boolean {
  return !!(err.code && err.schema && err.table && err.code === '23505');
}
