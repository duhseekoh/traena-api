export default {
  elasticSearch: {
    useAws: false,
    logLevel: 'trace', // error / warning / debug / info / trace
  },
  logging: {
    consoleLevel: 'silly',
    // Local is the only environment where remote logging is turned off by default.
    // If you want to test logging itself, then set this to a log level string.
    // Set it back to null before committing.
    remoteLevel: null,
    apiStackTrace: true,
  },
};
