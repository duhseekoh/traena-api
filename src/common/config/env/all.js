// @flow
/* eslint-disable camelcase */
import packageJson from '../../../../package.json';

/**
 * Type-safe configuration values. Need to define them first as typed variables because else for all staticly defined
 * values (e.g. true) flow would infer that type of the variable would be that single value instead of boolean.
 *
 * NOTE: if you add another new REQUIRED parameter to this configuration (one that will throw an error) make sure it's
 * added to:
 * - circleci Environment Variables
 * - .circleci/config.yml - for ci testing,
 * - tools/deploy/deploy-[dev, staging, production].sh - for deployment
 * - tools/deploy/commands/create-task-definition.sh else it will fail at deploy time.
 *   - Note, it must be put into THREE(!?!) different places in this file.
 */
function getOrThrow(param: *, defaultVal?: *) {
  if (!process.env[param]) {
    if (defaultVal) {
      console.log(`Param: ${param} not set. Using default ${defaultVal}`);
      return defaultVal;
    }
    throw new Error(`Required parameter process.env.${param} not set`);
  }
  return process.env[param];
}

const env_name: string = getOrThrow('NODE_ENV');

const db_connection: string = getOrThrow('DATABASE_URL');

// Auth
const auth_userSecret: string = getOrThrow('USER_JWT_SECRET');
const auth_clientSecret: string = getOrThrow('CLIENT_JWT_SECRET');
const auth_audience: string = getOrThrow('AUTH_AUDIENCE', 'https://oidc.traena.io');
const auth_jwtUserClaimNamespace: string = getOrThrow('JWT_USER_CLAIM_NAMESPACE', 'https://traena.io/user');
const auth_mgmtClientId: string = getOrThrow('AUTH_MGMT_CLIENT_ID');
const auth_mgmtClientSecret: string = getOrThrow('AUTH_MGMT_CLIENT_SECRET');
const auth_domain: string = getOrThrow('AUTH_DOMAIN');
const auth_connectionId: string = getOrThrow('AUTH_CONNECTION_ID');
const auth_connectionName: string = getOrThrow('AUTH_CONNECTION_NAME');
const auth_authorizationApiURI: string = getOrThrow('AUTH_AUTHORIZATION_API_URI');
const auth_issuer: string = `https://${auth_domain}/`;
const auth_authorizationServiceAudience: string = 'urn:auth0-authz-api';

// Elastic Search
const es_useAws: boolean = true; // Expands options for connecting to elastic search on aws; false if connecting locally
const es_apiBaseUrl: string = getOrThrow('ELASTIC_SEARCH_API_BASE_URL');
const es_logLevel: string = 'warning'; // error / warning / debug / info / trace

// Notifications
const notifications_iosPlatformApplicationArn: string = getOrThrow('SNS_IOS_PLATFORM_APPLICATION_ARN');
const notifications_androidPlatformApplicationArn: string = getOrThrow('SNS_ANDROID_PLATFORM_APPLICATION_ARN');

// Kinesis
const kinesis_indexStream: string = getOrThrow('KINESIS_INDEX_STREAM');
const kinesis_indexRealtimeStream: string = getOrThrow('KINESIS_INDEX_REALTIME_STREAM');

// Logging
const logging_logglyToken: string = getOrThrow('LOGGLY_TOKEN');
const logging_consoleLevel: string = 'silly';
const logging_remoteLevel: string = 'debug';
const logging_apiStackTrace: boolean = false;

// AWS
const aws_mockAws: boolean = getOrThrow('MOCK_AWS', 'false') === 'true';
const aws_contentBucket: string = getOrThrow('AWS_CONTENT_BUCKET');
const aws_cdnBaseUrl: string = getOrThrow('CDN_BASE_URL');
const aws_transcoderLambdaName = getOrThrow('TRANSCODER_LAMBDA_NAME');
const aws_imageProcessorLambdaName = getOrThrow('IMAGE_PROCESSOR_LAMBDA_NAME');

// Email
const email_from: string = 'support@traena.io';

// Analytics
// don't throw, optional for local dev
const analytics_segment_write_key: ?string = process.env.SEGMENT_WRITE_KEY;
const analytics_user_history_days: number = 14;

export default {
  env: {
    name: env_name,
  },
  db: {
    client: 'pg',
    connection: db_connection,
    // debug: true, // uncomment to see sql queries logged
  },
  auth: {
    userSecret: auth_userSecret,
    clientSecret: auth_clientSecret,
    audience: auth_audience,
    jwtUserClaimNamespace: auth_jwtUserClaimNamespace,
    mgmtClientId: auth_mgmtClientId,
    mgmtClientSecret: auth_mgmtClientSecret,
    domain: auth_domain,
    issuer: auth_issuer,
    connectionId: auth_connectionId,
    connectionName: auth_connectionName,
    authorizationApiURI: auth_authorizationApiURI,
    authorizationServiceAudience: auth_authorizationServiceAudience,
  },
  elasticSearch: {
    useAws: es_useAws,
    apiBaseUrl: es_apiBaseUrl,
    logLevel: es_logLevel,
    apiVersion: '5.x',
  },
  notifications: {
    iosPlatformApplicationArn: notifications_iosPlatformApplicationArn,
    androidPlatformApplicationArn: notifications_androidPlatformApplicationArn,
  },
  kinesis: {
    indexStream: kinesis_indexStream,
    indexRealtimeStream: kinesis_indexRealtimeStream,
  },
  logging: {
    logglyToken: logging_logglyToken,
    consoleLevel: logging_consoleLevel,
    remoteLevel: logging_remoteLevel,
    apiStackTrace: logging_apiStackTrace,
  },
  aws: {
    // If true, use localstack to mock whatever we can from aws
    mockAws: aws_mockAws,
    contentBucket: aws_contentBucket,
    cdnBaseUrl: aws_cdnBaseUrl,
    transcoderLambdaName: aws_transcoderLambdaName,
    imageProcessorLambdaName: aws_imageProcessorLambdaName,
  },
  email: {
    from: email_from,
  },
  analytics: {
    segment: {
      writeKey: analytics_segment_write_key,
    },
    user: {
      history: {
        days: analytics_user_history_days,
      },
    },
  },
  packageJson,
};
