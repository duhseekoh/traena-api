export default {
  logging: {
    remoteLevel: 'silly',
    apiStackTrace: true,
  },
};
