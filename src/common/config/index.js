// @flow

import _ from 'lodash';
import pg from 'pg';
// The code below may seem weird, but it allows all of the
// configs to be bundled via webpack and not dynamically required
import all from './env/all';
import local from './env/local';
import development from './env/development';
import staging from './env/staging';
import production from './env/production';

const configs = {
  all,
  local,
  development,
  staging,
  production,
};

export type AppConfig = typeof all;

const env = process.env.NODE_ENV;
// $FlowFixMe - ignore until computed properties are supported. https://github.com/facebook/flow/issues/3460
const config: AppConfig = _.merge({}, configs.all, configs[env]);

console.log(`process.env.NODE_ENV : ${config.env.name}`);

// Postgres init time configuration
// configure all bigint to be interpreted as int instead of string
//  (https://github.com/tgriesser/knex/issues/387#issuecomment-51554522)
pg.types.setTypeParser(20, 'text', parseInt);

export default config;
