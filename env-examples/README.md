Depending on which environment you're connecting to, copy one of these files over to the project root, rename to `.env`, and fill in the needed variables.
