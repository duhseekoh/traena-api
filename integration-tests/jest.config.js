// configuration only for integration tests!
module.exports = {
  testEnvironment: 'node',
  setupTestFrameworkScriptFile: './setup/setupFile.js',
  verbose: true,
};
