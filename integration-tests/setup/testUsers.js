// @flow
import { AuthenticationClient } from 'auth0';
import restClient from '../restClient';
import config from './config';

const auth0 = new AuthenticationClient({
  domain: config.auth0Domain,
  clientId: config.auth0ClientId,
});

type TestUserCreds = {
  username: string,
  password: string,
};

export type UserConfig = {
  userId: number,
  organizationId: number,
  creds: TestUserCreds,
};

/**
 * These users are in the local docker db, dev db, and dev auth0. Use only these
 * users to write api integration tests with.
 */
export const USER_KEYS = {
  adminGod: 'adminGod',
  pawsOrgAdmin: 'pawsOrgAdmin',
  pawsCatManager: 'catManager',
  pawsDogManager: 'dogManager',
  pawsCatWorker: 'catWorker',
  pawsDogWorker: 'dogWorker',
};

type UserKey = $Values<typeof USER_KEYS>;

const USER_CONFIGS: { [name: string]: UserConfig } = {
  adminGod: {
    creds: {
      username: 'dev+intg.admin.ta@traena.io',
      // TODO instead of requiring us to store passwords as env vars, look into
      // using auth0 user impersonation.
      password: config.userPassword,
    },
    userId: 1172,
    organizationId: 1006,
  },
  pawsOrgAdmin: {
    creds: {
      username: 'dev+intg.paws.oa@traena.io',
      password: config.userPassword,
    },
    userId: 1164,
    organizationId: 1002,
  },
  catManager: {
    creds: {
      username: 'dev+intg.paws.cat.cr@traena.io',
      password: config.userPassword,
    },
    userId: 1168,
    organizationId: 1002,
  },
  dogManager: {
    creds: {
      username: 'dev+intg.paws.dog.cr@traena.io',
      password: config.userPassword,
    },
    userId: 1169,
    organizationId: 1002,
  },
  catWorker: {
    creds: {
      username: 'dev+intg.paws.cat.co@traena.io',
      password: config.userPassword,
    },
    userId: 1171,
    organizationId: 1002,
  },
  dogWorker: {
    creds: {
      username: 'dev+intg.paws.dog.co@traena.io',
      password: config.userPassword,
    },
    userId: 1170,
    organizationId: 1002,
  },
};

export function getUserConfig(userKey: UserKey): UserConfig {
  const userConfig = USER_CONFIGS[userKey];

  if (!userConfig) {
    throw new Error('User test configuration not found');
  }

  return userConfig;
}

/**
 * 'Logs in' to auth0 with some user creds.
 */
export async function getTokens(
  creds: TestUserCreds,
): Promise<{
  access_token: string,
  id_token: string,
}> {
  try {
    const tokens = await auth0.oauth.passwordGrant({
      ...creds,
      grant_type: 'password',
      audience: 'https://oidc.traena.io',
      scope: 'openid email groups details roles permissions',
    });
    return tokens;
  } catch (err) {
    console.log(`could not log ${creds.username} in to auth0`, err);
    throw err;
  }
}

/**
 * Gets a Traena Rest client (currently just an axios instance) prepopulated
 * with the auth headers for a particular user.
 * It does this by logging the user in to auth0!
 * TODO - we should create a Traena api cient with methods for each possible
 * api interaction. would also enable us to run test coverage against the client.
 */
export async function getRestClientForUser(creds: TestUserCreds) {
  const tokens = await getTokens(creds);

  // console.log(`received tokens, getting rest client for ${creds.username}`);

  return restClient({
    baseURL: config.apiBaseUrl,
    accessToken: tokens.access_token,
  });
}
