// @flow
// test configuration
// read environment variables and supply them on an object to use in test setup

function getOrThrow(value: ?string, name: string): string {
  if (!value) {
    throw new Error(`Please supply test env var ${name}`);
  }

  return value;
}

export default {
  auth0Domain: getOrThrow(process.env.TESTS_AUTH0_DOMAIN, 'TESTS_AUTH0_DOMAIN'),
  auth0ClientId: getOrThrow(process.env.TESTS_AUTH0_CLIENT_ID, 'TESTS_AUTH0_CLIENT_ID'),
  apiBaseUrl: getOrThrow(process.env.TESTS_API_BASE_URL, 'TESTS_API_BASE_URL'),
  userPassword: getOrThrow(process.env.TESTS_USER_PASS, 'TESTS_USER_PASS'),
};
