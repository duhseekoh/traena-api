// One of these -> https://facebook.github.io/jest/docs/en/configuration.html#setuptestframeworkscriptfile-string

import { config } from 'dotenv';
import './customMatchers';

const result = config({ path: './integration-tests/.env' });

if (result.error) {
  console.log('Error loading dotenv in test environment', result.error);
  throw result.error;
}

// Longer timeout than would be for unit tests. Integration tests need high timeouts.
// Test data setup in a beforeAll can be a long process.
jest.setTimeout(30000);
