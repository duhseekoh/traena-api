expect.extend({
  toEqualOneOf(received, oneOfTheseValues) {
    const anyPass = oneOfTheseValues.some(value => this.equals(received, value));
    if (anyPass) {
      return {
        message: () => 'Expected no values to equal',
        pass: true,
      };
    }

    return {
      message: () => `Expected one of ${JSON.stringify(oneOfTheseValues)} to equal ${received}`,
      pass: false,
    };
  },
});
