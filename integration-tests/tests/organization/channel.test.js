// @flow
import type ChannelView from '../../../src/api/views/ChannelView';
import type { ChannelDTO } from '../../../src/domain/models/ChannelModel';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../../setup/testUsers';

describe('GET /organizations/:orgId/channels', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
  });

  test('should return the one default channel setup with PAWS org', async () => {
    const response = await client.get(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
    );
    const firstChannel: ChannelView = response.data.content[0];

    expect(response.data.content).toHaveLength(1);
    expect(firstChannel.name).toEqual('General');
    expect(firstChannel.default).toEqual(true);
    expect(firstChannel.visibility).toEqual('INTERNAL');
  });
});

describe('POST /organizations/:orgId/channels', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  const newChannel: ChannelDTO = {
    name: 'Z Last Channel',
    description: 'Sorted to the end',
    organizationId: pawsOAUserConfig.organizationId,
    visibility: 'INTERNAL',
    default: false,
  };
  let createdChannel: ChannelView;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
      newChannel,
    );
    createdChannel = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created channel
    await client.delete(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${
        createdChannel.id
      }`,
    );
  });

  test('should have successfully created a channel', async () => {
    expect(createdChannel).toMatchObject(newChannel);
    expect(createdChannel.id).toEqual(expect.any(Number));
  });

  test('should return that new channel at the end of the org channels list', async () => {
    const response = await client.get(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
    );
    const secondChannel: ChannelView = response.data.content[1];

    expect(response.data.content).toHaveLength(2); // because the General channel already exists
    expect(secondChannel).toMatchObject(createdChannel);
  });
});

describe('PUT /organizations/:orgId/channels/:channelId', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  const newChannel: ChannelDTO = {
    name: 'Integration Test Channel for PUT',
    description: 'My integration test channel',
    organizationId: pawsOAUserConfig.organizationId,
    visibility: 'INTERNAL',
    default: false,
  };
  let createdChannel;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
      newChannel,
    );
    createdChannel = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created channel
    await client.delete(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${
        createdChannel.id
      }`,
    );
  });

  test('should successfully update a channel', async () => {
    const updateChannel: ChannelDTO = {
      ...createdChannel,
      name: 'Updated Channel',
      description: 'Updated Description',
      visibility: 'LIMITED',
    };
    const response = await client.put(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${
        createdChannel.id
      }`,
      updateChannel,
    );
    const updatedChannel: ChannelView = response.data;
    expect(updatedChannel).toMatchObject(updatedChannel);
  });
});

describe('DELETE /organizations/:orgId/channels/:channelId', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  const newChannel: ChannelDTO = {
    name: 'Integration Test Channel for DELETE',
    description: 'My integration test channel delete',
    organizationId: pawsOAUserConfig.organizationId,
    visibility: 'MARKET',
    default: false,
  };
  let createdChannel;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
      newChannel,
    );
    createdChannel = response.data;
  });

  test('should successfully delete a channel', async () => {
    const deleteResponse = await client.delete(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${
        createdChannel.id
      }`,
    );
    expect(deleteResponse.status).toEqual(204);
  });
});
