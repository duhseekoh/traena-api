// @flow
import type UserView, {
  SerializedUserView,
} from '../../../src/api/views/UserView';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../../setup/testUsers';

describe('GET /organizations/:orgId/users', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  const pawsCatManagerConfig = getUserConfig(USER_KEYS.pawsCatManager);
  let client;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
  });

  test('should return the only user (org admin) in the PAWS org', async () => {
    const response = await client.get(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/users`,
    );
    const firstUser: UserView = response.data.content[0];
    // because users are default sorted by first name the cat manager is first.
    const expectedFirstUser: $Shape<SerializedUserView> = {
      id: pawsCatManagerConfig.userId,
      email: pawsCatManagerConfig.creds.username,
      organizationId: pawsCatManagerConfig.organizationId,
      firstName: 'PAWS Cat Manager',
      lastName: 'Support',
      profileImageURI: null,
      isDisabled: null,
      position: 'Cat Manager - Creator',
      tags: ['support'],
    };

    expect(response.data.content).toHaveLength(5);
    expect(firstUser).toEqual({
      ...expectedFirstUser,
      createdTimestamp: expect.any(String),
      modifiedTimestamp: expect.any(String),
    });
  });
});
