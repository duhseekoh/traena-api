// @flow
import type OrganizationView from '../../../src/api/views/OrganizationView';
import type RoleView from '../../../src/api/views/RoleView';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../../setup/testUsers';

describe('GET /organizations/:orgId', () => {
  describe('get my organization as an org admin', () => {
    const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
    let client;
    beforeAll(async () => {
      client = await getRestClientForUser(pawsOAUserConfig.creds);
    });

    test('should return my organization', async () => {
      const response = await client.get(
        `api/v1/organizations/${pawsOAUserConfig.organizationId}`,
      );
      const organization: OrganizationView = response.data;
      const expectedOrganization: $Shape<OrganizationView> = {
        id: pawsOAUserConfig.organizationId,
        name: 'PAWS',
        organizationType: 'CLIENT',
      };

      expect(organization).toEqual(expectedOrganization);
    });
  });
});

// If these roles test fails, auth0 may have been misconfigured or updates made
// there need to be made here.
describe('GET /organizations/:orgId/roles', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
  });

  test("should return my organization's roles", async () => {
    const response = await client.get(
      `api/v1/organizations/${pawsOAUserConfig.organizationId}/roles`,
    );
    const roles: RoleView[] = response.data;
    const expectedRole: $Shape<RoleView> = {
      id: 1,
      name: 'Content Consumer',
      description: 'This role can consume content but cannot create it.',
    };

    expect(roles).toHaveLength(1);
    expect(roles[0]).toEqual(expectedRole);
  });
});
