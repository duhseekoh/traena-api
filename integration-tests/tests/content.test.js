// @flow
import type ContentItemView from '../../src/api/views/ContentItemView';
import type { CreateContentItemDTO } from '../../src/domain/models/ContentItemModel';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../setup/testUsers';
import { assertContentItem } from '../common-assertions/content';

describe('POST /contents (Creating Text Content)', () => {
  const pawsCatManagerConfig = getUserConfig(USER_KEYS.pawsCatManager);
  let client;
  const newTextContent: CreateContentItemDTO = {
    authorId: pawsCatManagerConfig.userId,
    // TODO get dynamically from subscribed channels
    channelId: 1064,
    status: 'DRAFT', // TODO use enum
    body: {
      title: 'INTG Test Creating Text Content',
      type: 'DailyAction', // TODO use enum
      text: 'Including some text',
    },
    tags: ['test', 'tags', 'integration'],
  };
  let createdContent: ContentItemView;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsCatManagerConfig.creds);
    const response = await client.post('api/v1/contents', newTextContent);
    createdContent = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created content
    await client.delete(`api/v1/contents/${createdContent.id}`);
  });

  test('should have successfully created the content', async () => {
    assertContentItem(createdContent);
    expect(createdContent.author.id).toEqual(pawsCatManagerConfig.userId);
    expect(createdContent.status).toEqual('DRAFT');
    // TODO - look into why our api is returning tags in a different order
    // from which they were entered. after thats fixed, can switch this to
    // expect(actualTags).toEqual(expectedTags)
    expect(createdContent.tags).toEqual(
      expect.arrayContaining(['test', 'tags', 'integration']),
    );
    expect(createdContent.body).toMatchObject(newTextContent.body);
  });

  test(
    'should return that new content in the authors draft list',
    async () => {
      expect.hasAssertions();
      return new Promise((resolve) => {
        setTimeout(async () => {
          const response = await client.get(
            `api/v1/organizations/${
              pawsCatManagerConfig.organizationId
            }/content`,
            {
              params: {
                include: 'SELF',
                includeStatus: 'DRAFT',
                sort: 'createdTimestamp:desc',
              },
            },
          );
          const firstContentItem: ContentItemView = response.data.content[0];

          expect(response.data.content).toHaveLength(1);
          expect(firstContentItem.author.id).toEqual(
            pawsCatManagerConfig.userId,
          );
          expect(firstContentItem.status).toEqual('DRAFT');
          expect(firstContentItem.tags).toEqual(
            expect.arrayContaining(['test', 'tags', 'integration']),
          );
          expect(firstContentItem.body).toMatchObject(newTextContent.body);
          assertContentItem(firstContentItem);
          resolve(); // signals to jest that test is complete
        }, 9000); // need time for lambdas that populate ES to run
      });
    },
  );
});

// TODO PUT /contents/:contentId
// TODO GET /contents/:contentId
// TODO DELETE /contents/:contentId
