// @flow
import { getRestClientForUser, USER_KEYS, getUserConfig } from '../setup/testUsers';

// Example test, hitting the api root endpoint
describe('example test', () => {
  test('root endpoint should return text', async () => {
    const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
    const client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client
      .get('/api/v1');

    expect(response.status).toBe(200);
    expect(response.data).toBe('Traena API v1');
  });
});
