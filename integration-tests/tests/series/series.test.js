// @flow
import type ChannelView from '../../../src/api/views/ChannelView';
import type ContentItemView from '../../../src/api/views/ContentItemView';
import type SeriesView from '../../../src/api/views/SeriesView';
import type { CreateContentItemDTO } from '../../../src/domain/models/ContentItemModel';
import type {
  UpdateSeriesDTO,
  CreateSeriesDTO,
} from '../../../src/domain/models/SeriesModel';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../../setup/testUsers';

let pawsDefaultChannel: ChannelView;
// get the default channel for the paws org so we can run our series tests
// in the context of this existing channel
beforeAll(async () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  const client = await getRestClientForUser(pawsOAUserConfig.creds);
  const response = await client.get(
    `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
  );
  [pawsDefaultChannel] = response.data.content;
});

test('test setup should be correct', () => {
  expect(pawsDefaultChannel).toBeTruthy();
});

describe('GET /series/:seriesId', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  let newSeries: CreateSeriesDTO;
  let createdSeries: SeriesView;
  beforeAll(async () => {
    newSeries = {
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'for testing GET requests',
      channelId: pawsDefaultChannel.id,
      contentIds: [],
    };
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post('api/v1/series', newSeries);
    createdSeries = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created series
    await client.delete(`api/v1/series/${createdSeries.id}`);
  });

  test('should return a series', async () => {
    const response = await client.get(`api/v1/series/${createdSeries.id}`);
    const series: SeriesView = response.data;

    expect(series).toMatchObject({
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'for testing GET requests',
      channelId: pawsDefaultChannel.id,
    });
  });

  test('should return a ResourceValidationError when series doesnt exist', async () => {
    expect.assertions(2);
    try {
      await client.get('api/v1/series/123456');
    } catch (err) {
      // TODO shouldn't this be a ResourceNotFoundError? - it currently gives a weird
      // message "Series must exist to get channel", indicitive of incorrect error handling
      expect(err.response.data.type).toEqual('ResourceValidationError');
      expect(err.response.status).toEqual(400);
    }
  });

  // TODO test - should return a series with a preview of content included
});

describe('POST /series', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  let newSeries: CreateSeriesDTO;
  let createdSeries: SeriesView;
  beforeAll(async () => {
    newSeries = {
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'some series description',
      channelId: pawsDefaultChannel.id,
      contentIds: [],
    };
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post('api/v1/series', newSeries);
    createdSeries = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created series
    await client.delete(`api/v1/series/${createdSeries.id}`);
  });

  test('should have successfully created a series', async () => {
    expect(createdSeries).toMatchObject({
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'some series description',
      channelId: pawsDefaultChannel.id,
      // note return value does *not* include contentIds
    });
    expect(createdSeries.id).toEqual(expect.any(Number));
  });

  test('should return that new series in the channels series list', async () => {
    const response = await client.get(
      `api/v1/channels/${newSeries.channelId}/series`,
    );
    const firstSeries: SeriesView = response.data.content[0];

    expect(response.data.content).toHaveLength(1); // only series in the channel
    expect(firstSeries).toMatchObject(createdSeries);
  });
});

describe('PUT /series/:seriesId', () => {
  // TODO another test which adds content to a series and tests /:seriesId/content

  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  let newSeries: CreateSeriesDTO;
  let createdSeries: SeriesView;
  beforeAll(async () => {
    newSeries = {
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'some series description',
      channelId: pawsDefaultChannel.id,
      contentIds: [],
    };
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post('api/v1/series', newSeries);
    createdSeries = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created series
    await client.delete(`api/v1/series/${createdSeries.id}`);
  });

  test('should successfully update a series', async () => {
    const updateSeries: UpdateSeriesDTO = {
      ...createdSeries,
      title: 'An integration Series updated',
      description: 'Updated Description',
      contentIds: [],
    };
    const response = await client.put(
      `api/v1/series/${createdSeries.id}`,
      updateSeries,
    );
    const updatedSeries: SeriesView = response.data;
    expect(updatedSeries).toMatchObject({
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series updated',
      description: 'Updated Description',
      channelId: pawsDefaultChannel.id,
      // note return value does *not* include contentIds
    });
  });
});

describe('DELETE /series/:seriesId', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  let newSeries: CreateSeriesDTO;
  let createdSeries: SeriesView;
  beforeAll(async () => {
    newSeries = {
      authorId: pawsOAUserConfig.userId,
      title: 'Integration Test Series for DELETE',
      description: 'My integration test series delete',
      channelId: pawsDefaultChannel.id,
      contentIds: [],
    };
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    const response = await client.post('api/v1/series', newSeries);
    createdSeries = response.data;
  });

  test('should successfully delete a series', async () => {
    const deleteResponse = await client.delete(
      `api/v1/series/${createdSeries.id}`,
    );
    expect(deleteResponse.status).toEqual(204);
  });
});

describe('series that have content', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  let newSeries: CreateSeriesDTO;
  let createdSeries: SeriesView;
  let newContent: CreateContentItemDTO;
  let createdContent: ContentItemView;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
    // create content
    newContent = {
      authorId: pawsOAUserConfig.userId,
      channelId: pawsDefaultChannel.id,
      status: 'PUBLISHED',
      body: {
        title: 'test content integration title',
        type: 'DailyAction',
        text: 'test content integration text',
      },
      tags: ['Atag'],
    };
    const contentResponse = await client.post('api/v1/contents', newContent);
    createdContent = contentResponse.data;
    // create series with content set on it
    newSeries = {
      authorId: pawsOAUserConfig.userId,
      title: 'An integration Series',
      description: 'for testing GET requests',
      channelId: pawsDefaultChannel.id,
      contentIds: [createdContent.id], // adding content to series
    };
    const response = await client.post('api/v1/series', newSeries);
    createdSeries = response.data;
  });

  afterAll(async () => {
    // cleanup, delete created series and content
    await client.delete(`api/v1/series/${createdSeries.id}`);
    await client.delete(`api/v1/contents/${createdContent.id}`);
  });

  test('GET /series/:seriesId should return a series with content preview', async () => {
    const response = await client.get(`api/v1/series/${createdSeries.id}?includeContentPreview=true`);
    const series: SeriesView = response.data;

    expect(series.contentPreview).toHaveLength(1);
    expect(series.contentPreview[0].id).toEqual(createdContent.id);
  });
});
