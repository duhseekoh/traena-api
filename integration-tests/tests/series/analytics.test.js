// @flow
import type { Axios } from 'axios';
import type ChannelView from '../../../src/api/views/ChannelView';
import type ContentItemView from '../../../src/api/views/ContentItemView';
import type SeriesView from '../../../src/api/views/SeriesView';
import type { CreateContentItemDTO } from '../../../src/domain/models/ContentItemModel';
import type { CreateEventDTO } from '../../../src/domain/models/EventModel';
import type { CreateSeriesDTO } from '../../../src/domain/models/SeriesModel';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
  type UserConfig,
} from '../../setup/testUsers';

let pawsOAUserConfig: UserConfig;
let pawsCMUserConfig: UserConfig;
let pawsCWUserConfig: UserConfig;
let pawsOAClient: Axios;
let pawsDefaultChannel: ChannelView;
let newSeries: CreateSeriesDTO;
let createdSeries: SeriesView;
let newContent1: CreateContentItemDTO;
let newContent2: CreateContentItemDTO;
let newContent3: CreateContentItemDTO;
let createdContent1: ContentItemView;
let createdContent2: ContentItemView;
let createdContent3: ContentItemView;
let viewTimestamp: string;

// sets up a series with content and the interactions with that content in the series
beforeAll(async () => {
  pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  pawsOAClient = await getRestClientForUser(pawsOAUserConfig.creds);
  // get default channel
  const channelResponse = await pawsOAClient.get(
    `api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
  );
  [pawsDefaultChannel] = channelResponse.data.content;

  // create content
  newContent1 = {
    authorId: pawsOAUserConfig.userId,
    channelId: pawsDefaultChannel.id,
    status: 'PUBLISHED',
    body: {
      title: '1 test content integration series analytics',
      type: 'DailyAction',
      text: '1 test content integration series analytics text',
    },
    tags: ['Atag1'],
  };
  newContent2 = {
    authorId: pawsOAUserConfig.userId,
    channelId: pawsDefaultChannel.id,
    status: 'PUBLISHED',
    body: {
      title: '2 test content integration series analytics',
      type: 'DailyAction',
      text: '2 test content integration series analytics',
    },
    tags: ['Atag2'],
  };
  newContent3 = {
    authorId: pawsOAUserConfig.userId,
    channelId: pawsDefaultChannel.id,
    status: 'PUBLISHED',
    body: {
      title: '3 test content integration series analytics',
      type: 'DailyAction',
      text: '3 not in a series',
    },
    tags: ['Atag3'],
  };
  const contentResponse1 = await pawsOAClient.post(
    'api/v1/contents',
    newContent1,
  );
  const contentResponse2 = await pawsOAClient.post(
    'api/v1/contents',
    newContent2,
  );
  const contentResponse3 = await pawsOAClient.post(
    'api/v1/contents',
    newContent3,
  );
  createdContent1 = contentResponse1.data;
  createdContent2 = contentResponse2.data;
  createdContent3 = contentResponse3.data;
  // create series with content set on it
  newSeries = {
    authorId: pawsOAUserConfig.userId,
    title: 'An integration Series',
    description: 'for testing GET requests',
    channelId: pawsDefaultChannel.id,
    // adding content to series, note content 3 is not in here
    contentIds: [createdContent1.id, createdContent2.id],
  };
  const seriesResponse = await pawsOAClient.post('api/v1/series', newSeries);
  createdSeries = seriesResponse.data;

  // setup user interactions with the content
  pawsCMUserConfig = getUserConfig(USER_KEYS.pawsCatManager);
  pawsCWUserConfig = getUserConfig(USER_KEYS.pawsCatWorker);
  const pawsCMClient = await getRestClientForUser(pawsCMUserConfig.creds);
  const pawsCWClient = await getRestClientForUser(pawsCWUserConfig.creds);

  // setup content completions
  await pawsCMClient.post(
    `api/v1/users/self/tasks/completions?contentId=${createdContent1.id}`,
  );
  await pawsCMClient.post(
    `api/v1/users/self/tasks/completions?contentId=${createdContent2.id}`,
  );
  await pawsCMClient.post(
    `api/v1/users/self/tasks/completions?contentId=${createdContent3.id}`,
  );
  await pawsCWClient.post(
    `api/v1/users/self/tasks/completions?contentId=${createdContent2.id}`,
  );
  // setup content views
  viewTimestamp = new Date().toISOString();
  const viewEvent: CreateEventDTO = {
    type: 'CONTENT_VIEWED',
    data: {
      contentId: createdContent1.id,
      timestamp: viewTimestamp,
    },
  };
  await pawsCMClient.post('api/v1/events', viewEvent);
  await pawsCWClient.post('api/v1/events', viewEvent);
});

afterAll(async () => {
  // cleanup, delete created series and content
  await pawsOAClient.delete(`api/v1/series/${createdSeries.id}`);
  await pawsOAClient.delete(`api/v1/contents/${createdContent1.id}`);
  await pawsOAClient.delete(`api/v1/contents/${createdContent2.id}`);
  await pawsOAClient.delete(`api/v1/contents/${createdContent3.id}`);
});

test('test setup should be correct', () => {
  expect(pawsDefaultChannel).toBeTruthy();
  expect(createdContent1).toBeTruthy();
  expect(createdContent2).toBeTruthy();
  expect(createdSeries).toBeTruthy();
});

describe('GET /series/:seriesId/analytics/users', () => {
  let userAnalyticsResponse;
  let usersWithSeriesStats: [];

  beforeAll(async () => {
    userAnalyticsResponse = await pawsOAClient.get(
      `api/v1/series/${
        createdSeries.id
      }/analytics/users?sort=latestViewedContentTimestamp:desc`,
    );
    usersWithSeriesStats = userAnalyticsResponse.data.content;
  });

  test('should have stats for all users with access to the channel', async () => {
    const totalUsers: number = userAnalyticsResponse.data.totalElements;
    // 5 users in PAWS org, all with access to this channel
    expect(totalUsers).toEqual(5);
  });

  describe('number of completed posts for each user', () => {
    test('cat manager should have 2 completed posts', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCMUserConfig.userId }),
            seriesStats: expect.objectContaining({ completedContentCount: 2 }),
          }),
        ]),
      );
    });

    test('cat worker should have 1 completed post', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCWUserConfig.userId }),
            seriesStats: expect.objectContaining({ completedContentCount: 1 }),
          }),
        ]),
      );
    });

    test('paws org admin should have 0 completed posts', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsOAUserConfig.userId }),
            seriesStats: expect.objectContaining({ completedContentCount: 0 }),
          }),
        ]),
      );
    });
  });

  describe('timestamp of each users latest post completion', () => {
    test('cat manager should have a latest post completion timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCMUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestContentCompletionTimestamp: expect.any(String),
            }),
          }),
        ]),
      );
    });

    test('cat worker should have a latest post completion timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCWUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestContentCompletionTimestamp: expect.any(String),
            }),
          }),
        ]),
      );
    });

    test('paws org admin should not have a latest post completion timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCMUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestContentCompletionTimestamp: expect.any(String),
            }),
          }),
        ]),
      );
    });
  });

  describe('timestamp of each users latest post view', () => {
    test('cat manager should have a latest post view timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCMUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestViewedContentTimestamp: expect.any(String),
            }),
          }),
        ]),
      );
    });

    test('cat worker should have a latest post view timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsCWUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestViewedContentTimestamp: expect.any(String),
            }),
          }),
        ]),
      );
    });

    test('paws org admin should not have a latest post view timestamp', () => {
      expect(usersWithSeriesStats).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            user: expect.objectContaining({ id: pawsOAUserConfig.userId }),
            seriesStats: expect.objectContaining({
              latestViewedContentTimestamp: null,
            }),
          }),
        ]),
      );
    });
  });

  // TODO test('should sort descendingly on latest post view where users with no
  // views are sorted to the end', async () => {});
});
