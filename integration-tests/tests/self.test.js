// @flow
import moment from 'moment';
import { getRestClientForUser, getUserConfig, USER_KEYS } from '../setup/testUsers';
// import { assertContentItem } from '../common-assertions/content';
import type ChannelView from '../../src/api/views/ChannelView';
import type ContentItemView from '../../src/api/views/ContentItemView';
import type { CreateContentItemDTO } from '../../src/domain/models/ContentItemModel';
import { assertArrayPage } from '../common-assertions/koa-pageable';

const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
const adminGodUserConfig = getUserConfig(USER_KEYS.adminGod);
let pawsOAClient;
let adminGodClient;
beforeAll(async () => {
  pawsOAClient = await getRestClientForUser(pawsOAUserConfig.creds);
  adminGodClient = await getRestClientForUser(adminGodUserConfig.creds);
});

describe('GET /users/self', () => {
  test('should return my user', async () => {
    const response = await pawsOAClient.get('/api/v1/users/self');

    expect(response.data).toMatchObject({
      id: pawsOAUserConfig.userId,
      email: pawsOAUserConfig.creds.username,
      organizationId: pawsOAUserConfig.organizationId,
      firstName: 'PAWS Org Admin',
      lastName: 'Support',
      profileImageURI: null,
      isDisabled: null,
      position: 'Org Admin',
      tags: ['dummy', 'test', 'admin'],
      createdTimestamp: expect.any(String),
      modifiedTimestamp: expect.any(String),
    });
    expect(moment(response.data.createdTimestamp).isValid()).toBe(true);
    expect(moment(response.data.modifiedTimestamp).isValid()).toBe(true);
  });
});

describe('GET /users/self/feed', () => {
  describe('no accessible content, so no results should be in feed', () => {
    let response;
    beforeAll(async () => {
      response = await pawsOAClient.get('/api/v1/users/self/feed');
    });

    test('matches first page in koa-pageable ArrayPage', () => {
      assertArrayPage(response.data);
      expect(response.data.number).toBe(0);
      expect(response.data.size).toBe(10);
      expect(response.data.first).toBe(true);
    });

    test('no results should come back', () => {
      expect(response.data.totalElements).toEqual(0);
      expect(response.data.content).toHaveLength(0);
    });
  });
});

describe('GET /users/self/subscribedchannels', () => {
  test('paws user should be subscribed to one default channel', async () => {
    const response = await pawsOAClient.get('/api/v1/users/self/subscribedchannels');
    expect(response.data.totalElements).toEqual(1);
  });

  describe(`when a channel is subscribed to by paws organization with ORGANIZATION
    visibility and subscribed to by another org with USERS visibility and users assigned to it`, () => {
    let createdChannel: ChannelView;
    beforeAll(async () => {
      // create channel to test subscriptions for
      const createChannelResponse = await pawsOAClient.post(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
        {
          name: 'testing subscribedchannels',
          description: 'dummy',
          organizationId: pawsOAUserConfig.organizationId,
          visibility: 'LIMITED',
        },
      );
      createdChannel = createChannelResponse.data;
      // set to be visible by entire organization
      await pawsOAClient.put(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/subscriptions/${createdChannel.id}`,
        {
          channelId: createdChannel.id,
          organizationId: createdChannel.organizationId,
          visibility: 'ORGANIZATION',
        },
      );
      // setup other org to be subscribed to channel
      await adminGodClient.post(`/api/v1/organizations/${adminGodUserConfig.organizationId}/subscriptions`, {
        channelId: createdChannel.id,
        organizationId: adminGodUserConfig.organizationId,
        visibility: 'USERS',
      });
      // assign users to channel
      await adminGodClient.put(
        `/api/v1/organizations/${adminGodUserConfig.organizationId}/subscriptions/${createdChannel.id}`,
        {
          channelId: createdChannel.id,
          organizationId: adminGodUserConfig.organizationId,
          visibility: 'USERS',
          subscribedUserIds: [adminGodUserConfig.userId],
        },
      );
    });

    afterAll(async () => {
      // delete channel we tested subscriptions for
      await pawsOAClient.delete(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${createdChannel.id}`,
      );
    });

    test('paws users should see their subscription to the new channel', async () => {
      const response = await pawsOAClient.get('/api/v1/users/self/subscribedchannels');
      expect(response.data.totalElements).toEqual(2);
    });

    test(`individually subscribed users from the other org should see their
      subscriptions to the new channel`, async () => {
      const response = await adminGodClient.get('/api/v1/users/self/subscribedchannels');
      expect(response.data.totalElements).toEqual(2);
      expect.arrayContaining([
        expect.objectContaining({
          name: 'testing subscribedchannels',
        }),
      ]);
    });
  });
});

describe('GET /users/self/subscribedseries', () => {
  describe('when user is subscribed to channel that has a series in it', () => {
    let createdChannel: ChannelView;
    let createdContent: ContentItemView;
    beforeAll(async () => {
      // create channel so we have something to add a series to
      const createChannelResponse = await pawsOAClient.post(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/channels`,
        {
          name: 'testing subscribedseries',
          description: 'dummy',
          organizationId: pawsOAUserConfig.organizationId,
          visibility: 'INTERNAL',
        },
      );
      createdChannel = createChannelResponse.data;
      // set to be visible by entire organization
      await pawsOAClient.put(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/subscriptions/${createdChannel.id}`,
        {
          channelId: createdChannel.id,
          organizationId: pawsOAUserConfig.organizationId,
          visibility: 'ORGANIZATION',
        },
      );
      // create a content item, so it can be set on a series
      const newTextContent: CreateContentItemDTO = {
        authorId: pawsOAUserConfig.userId,
        channelId: createdChannel.id,
        status: 'PUBLISHED',
        body: {
          title: 'INTG Test content for series',
          type: 'DailyAction',
          text: 'Text for series!',
        },
        tags: ['dummy'],
      };
      const response = await pawsOAClient.post('api/v1/contents', newTextContent);
      createdContent = response.data;
      // create a series for the channel that has content
      await pawsOAClient.post('/api/v1/series', {
        channelId: createdChannel.id,
        title: 'intg subscribedseries with content',
        description: 'nah',
        contentIds: [createdContent.id],
      });
      // create a series for the channel that has no content
      await pawsOAClient.post('/api/v1/series', {
        channelId: createdChannel.id,
        title: 'intg subscribedseries without content',
        description: 'nah',
        contentIds: [],
      });
    });

    afterAll(async () => {
      // delete content we created
      await pawsOAClient.delete(`api/v1/contents/${createdContent.id}`);
      // delete channel we tested subscriptions for
      await pawsOAClient.delete(
        `/api/v1/organizations/${pawsOAUserConfig.organizationId}/channels/${createdChannel.id}`,
      );
    });

    test(`paws users should see their subscription to the new series with
      content but not to the series with no content`, async () => {
      const response = await pawsOAClient.get('/api/v1/users/self/subscribedseries');
      expect(response.data.totalElements).toEqual(1);
      expect(response.data.content[0]).toMatchObject({
        id: expect.any(Number),
        channelId: createdChannel.id,
        title: 'intg subscribedseries with content',
        description: 'nah',
      });
    });
  });
});
