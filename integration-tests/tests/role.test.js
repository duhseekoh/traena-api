// @flow
import type RoleView from '../../src/api/views/RoleView';
import {
  getRestClientForUser,
  USER_KEYS,
  getUserConfig,
} from '../setup/testUsers';

describe('GET /roles', () => {
  const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
  let client;
  beforeAll(async () => {
    client = await getRestClientForUser(pawsOAUserConfig.creds);
  });

  test('should return roles that can be assigned in the UI', async () => {
    const response = await client.get('api/v1/roles');
    const roles: RoleView[] = response.data;
    const expectedRoles: $Shape<RoleView>[] = [
      {
        id: 1,
        name: 'Content Consumer',
        description: 'This role can consume content but cannot create it.',
      },
      {
        id: 2,
        name: 'Content Creator',
        description:
          'This role can consume and create content, but cannot adjust organization level data.',
      },
      {
        id: 3,
        name: 'Organization Admin',
        description:
          'This role can consume and create content, and can also administer users and others content.',
      },
    ];

    expect(roles).toHaveLength(3);
    expect(roles).toEqual(expect.arrayContaining(expectedRoles));
  });
});
