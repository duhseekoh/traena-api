// @flow
// Test that the users we're authenticating with against auth0 match up to what
// we are expecting. correct permissions, groups, properties, etc...
import { decode } from 'jsonwebtoken';
import { getTokens, USER_KEYS, getUserConfig } from '../setup/testUsers';

const TRAENA_ADMIN_PERMISSIONS: $ReadOnlyArray<string> = [
  'modify:all:comments',
  'modify:all:content',
  'read:all:comments',
  'read:all:content',
  'modify:all:organizations',
  'modify:all:users',
  'read:all:organizations',
  'read:all:users',
  'read:all:devices',
  'read:all:tasks',
  'modify:all:devices',
  'admin',
  'modify:all:tasks',
  'read:org:comments',
  'modify:org:comments',
  'modify:self:comments',
  'read:org:content',
  'modify:org:content',
  'modify:self:content',
  'read:self:devices',
  'modify:self:devices',
  'read:self:organizations',
  'modify:org:organizations',
  'read:org:tasks',
  'read:self:tasks',
  'modify:org:tasks',
  'modify:self:tasks',
  'read:org:users',
  'modify:org:users',
  'modify:self:users',
  'admin:elasticsearch',
  'modify:self:activity_responses',
  'modify:all:activity_responses',
  'modify:org:activity_responses',
  'read:all:activity_responses',
  'read:self:activity_responses',
  'read:org:activity_responses',
  'read:all:channels',
  'read:org:channels',
  'modify:all:channels',
  'modify:org:channels',
  'modify:self:channels',
];

const ORG_ADMIN_PERMISSIONS: $ReadOnlyArray<string> = [
  'read:org:comments',
  'modify:org:comments',
  'modify:self:comments',
  'read:org:content',
  'modify:org:content',
  'modify:self:content',
  'read:self:devices',
  'modify:self:devices',
  'read:self:organizations',
  'modify:org:organizations',
  'read:org:tasks',
  'read:self:tasks',
  'modify:org:tasks',
  'modify:self:tasks',
  'read:org:users',
  'modify:org:users',
  'modify:self:users',
  'read:org:organizations',
  'modify:self:activity_responses',
  'modify:org:activity_responses',
  'read:self:activity_responses',
  'read:org:activity_responses',
  'read:org:channels',
  'modify:org:channels',
  'modify:self:channels',
];

const CONTENT_CREATOR_PERMISSIONS: $ReadOnlyArray<string> = [
  'read:org:comments',
  'modify:self:comments',
  'read:org:content',
  'modify:self:content',
  'read:self:devices',
  'modify:self:devices',
  'read:self:organizations',
  'read:self:tasks',
  'modify:self:tasks',
  'read:org:users',
  'modify:self:users',
  'modify:self:activity_responses',
  'read:self:activity_responses',
  'read:org:channels',
  'modify:org:channels',
  'modify:self:channels',
];

const CONTENT_CONSUMER_PERMISSIONS: $ReadOnlyArray<string> = [
  'read:org:comments',
  'modify:self:comments',
  'read:org:content',
  'read:self:devices',
  'modify:self:devices',
  'read:self:organizations',
  'read:self:tasks',
  'modify:self:tasks',
  'read:org:users',
  'modify:self:users',
  'modify:self:activity_responses',
  'read:self:activity_responses',
  // TODO - Identified the below incorrect permission on Content Consumers while
  // writing these tests. This permissions needs to be removed from consumers
  // in auth0!
  'modify:org:channels',
];

describe('test traena admin (god mode) token properties', () => {
  const userClaimNamespace = 'https://traena.io/user';
  let decodedIdToken;
  const adminGodConfig = getUserConfig(USER_KEYS.adminGod);

  beforeAll(async () => {
    const tokens = await getTokens(adminGodConfig.creds);
    const idToken = tokens.access_token;
    decodedIdToken = decode(idToken);
  });

  test('has email', () => {
    expect(decodedIdToken[userClaimNamespace].email).toBe(
      'dev+intg.admin.ta@traena.io',
    );
  });

  test('has consumer and traena admin roles', () => {
    expect(decodedIdToken[userClaimNamespace].roles).toEqual([
      'content-consumer',
      'traena-admin',
    ]);
  });

  test('has permissions for consumer and admin roles', () => {
    expect(decodedIdToken[userClaimNamespace].permissions).toEqual(
      TRAENA_ADMIN_PERMISSIONS,
    );
  });

  test('has details', () => {
    expect(decodedIdToken[userClaimNamespace].details).toMatchObject({
      traenaId: adminGodConfig.userId,
      organizationId: adminGodConfig.organizationId,
      firstName: 'God',
      lastName: 'INTG',
      subscribedChannelIds: expect.any(Array),
    });
    expect(
      decodedIdToken[userClaimNamespace].details.subscribedChannelIds,
    ).toHaveLength(1);
  });
});

describe('test org admin token properties', () => {
  const userClaimNamespace = 'https://traena.io/user';
  let decodedIdToken;

  beforeAll(async () => {
    const pawsOAUserConfig = getUserConfig(USER_KEYS.pawsOrgAdmin);
    const tokens = await getTokens(pawsOAUserConfig.creds);
    const idToken = tokens.access_token;
    decodedIdToken = decode(idToken);
  });

  test('has email', () => {
    expect(decodedIdToken[userClaimNamespace].email).toBe(
      'dev+intg.paws.oa@traena.io',
    );
  });

  test('has consumer and org admin roles', () => {
    expect(decodedIdToken[userClaimNamespace].roles).toEqual([
      'content-consumer',
      'org-admin',
    ]);
  });

  test('has permissions for consumer and admin roles', () => {
    expect(decodedIdToken[userClaimNamespace].permissions).toEqual(
      ORG_ADMIN_PERMISSIONS,
    );
  });

  test('has details', () => {
    expect(decodedIdToken[userClaimNamespace].details).toMatchObject({
      traenaId: expect.any(Number),
      organizationId: expect.any(Number),
      firstName: 'PAWS Org Admin',
      lastName: 'Support',
      subscribedChannelIds: expect.any(Array),
    });
    expect(
      decodedIdToken[userClaimNamespace].details.subscribedChannelIds,
    ).toHaveLength(1);
  });
});

describe('test content creator - cat manager', () => {
  const userClaimNamespace = 'https://traena.io/user';
  let decodedIdToken;
  const pawsCatManagerConfig = getUserConfig(USER_KEYS.pawsCatManager);

  beforeAll(async () => {
    const tokens = await getTokens(pawsCatManagerConfig.creds);
    const idToken = tokens.access_token;
    decodedIdToken = decode(idToken);
  });

  test('has email', () => {
    expect(decodedIdToken[userClaimNamespace].email).toBe(
      'dev+intg.paws.cat.cr@traena.io',
    );
  });

  test('has consumer and content creator', () => {
    expect(decodedIdToken[userClaimNamespace].roles).toEqual([
      'content-consumer',
      'content-creator',
    ]);
  });

  test('has permissions for consumer and creator roles', () => {
    expect(decodedIdToken[userClaimNamespace].permissions).toEqual(
      CONTENT_CREATOR_PERMISSIONS,
    );
  });

  test('has details', () => {
    expect(decodedIdToken[userClaimNamespace].details).toMatchObject({
      traenaId: pawsCatManagerConfig.userId,
      organizationId: pawsCatManagerConfig.organizationId,
      firstName: 'PAWS Cat Manager',
      lastName: 'Support',
      subscribedChannelIds: expect.any(Array),
    });
    expect(
      decodedIdToken[userClaimNamespace].details.subscribedChannelIds,
    ).toHaveLength(1);
  });
});

describe('test content consumer - cat worker', () => {
  const userClaimNamespace = 'https://traena.io/user';
  let decodedIdToken;
  const pawsCatWorkerConfig = getUserConfig(USER_KEYS.pawsCatWorker);

  beforeAll(async () => {
    const tokens = await getTokens(pawsCatWorkerConfig.creds);
    const idToken = tokens.access_token;
    decodedIdToken = decode(idToken);
  });

  test('has email', () => {
    expect(decodedIdToken[userClaimNamespace].email).toBe(
      'dev+intg.paws.cat.co@traena.io',
    );
  });

  test('has consumer and content creator', () => {
    expect(decodedIdToken[userClaimNamespace].roles).toEqual([
      'content-consumer',
    ]);
  });

  test('has permissions for consumer and creator roles', () => {
    expect(decodedIdToken[userClaimNamespace].permissions).toEqual(
      CONTENT_CONSUMER_PERMISSIONS,
    );
  });

  test('has details', () => {
    expect(decodedIdToken[userClaimNamespace].details).toMatchObject({
      traenaId: pawsCatWorkerConfig.userId,
      organizationId: pawsCatWorkerConfig.organizationId,
      firstName: 'PAWS Cat Worker',
      lastName: 'Support',
      subscribedChannelIds: expect.any(Array),
    });
    expect(
      decodedIdToken[userClaimNamespace].details.subscribedChannelIds,
    ).toHaveLength(1);
  });
});
