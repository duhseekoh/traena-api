// @flow
// TODO - Start creating a Traena Rest Client. Can use them in tests AND in the
// mobile and web app for a uniform way to communicate with the api.
//
// Possible Features:
// Instantiate with JWT, use everywhere (also update when there is a new JWT)
// Removes httpInterceptor crutch in our applications, any logic surrounding all requests can be done in this client
// Uses flow types to determine what can be passed in to body & query params

import axios from 'axios';
import type { Axios } from 'axios';

export default function getTraenaClient({
  baseURL,
  accessToken,
}: {
  baseURL: string,
  accessToken: string,
}): Axios {
  return axios.create({
    baseURL,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
