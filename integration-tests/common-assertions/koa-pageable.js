// @flow

/**
 * Given a contentItem to test, assert that it matches the correct structure
 */
export function assertArrayPage(page: Object) { // eslint-disable-line import/prefer-default-export
  expect(page).toEqual({
    number: expect.any(Number),
    size: expect.any(Number),
    totalElements: expect.any(Number),
    totalPages: expect.any(Number),
    first: expect.any(Boolean),
    last: expect.any(Boolean),
    content: expect.any(Array),
    numberOfElements: expect.any(Number),
  });
}

// TODO assertIndexedPage
