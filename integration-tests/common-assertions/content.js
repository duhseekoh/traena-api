// @flow
import moment from 'moment';

/**
 * Given a contentItem to test, assert that it matches the correct structure.
 */
// eslint-disable-next-line import/prefer-default-export
export function assertContentItem(contentItem: Object) {
  expect(contentItem.id).toEqual(expect.any(Number));
  expect(contentItem.body.text).toEqual(expect.any(String));
  // $FlowFixMe
  expect(contentItem.body.type).toEqualOneOf(['DailyAction', 'Image', 'TrainingVideo']);
  expect(contentItem.body.title).toEqual(expect.any(String));
  // TODO - check video properties
  // TODO - check image properties
  // $FlowFixMe
  expect(contentItem.status).toEqualOneOf(['PUBLISHED', 'DRAFT']);
  expect(contentItem.activityCount).toEqual(expect.any(Number));
  expect(contentItem.commentCount).toEqual(expect.any(Number));
  expect(contentItem.likeCount).toEqual(expect.any(Number));
  expect(moment(contentItem.createdTimestamp).isValid()).toBe(true);
  expect(moment(contentItem.modifiedTimestamp).isValid()).toBe(true);
  if (contentItem.status === 'PUBLISHED') {
    expect(moment(contentItem.publishedTimestamp).isValid()).toBe(true);
  }
  expect(contentItem.channel.id).toEqual(expect.any(Number));
  expect(contentItem.channel.name).toEqual(expect.any(String));
  expect(contentItem.channel.organizationId).toEqual(expect.any(Number));
  expect(contentItem.channel.description).toEqual(expect.any(String));
  expect(contentItem.completedCount).toEqual(expect.any(Number));
  expect(contentItem.channelId).toEqual(expect.any(Number));
  expect(contentItem.activitiesOrder).toEqual(expect.any(Array));
  expect(contentItem.author.id).toEqual(expect.any(Number));
  expect(contentItem.author.email).toEqual(expect.any(String));
  expect(contentItem.author.firstName).toEqual(expect.any(String));
  expect(contentItem.author.lastName).toEqual(expect.any(String));
  expect(contentItem.author.organization.id).toEqual(expect.any(Number));
  expect(contentItem.author.organization.name).toEqual(expect.any(String));
  // $FlowFixMe
  expect(contentItem.author.organization.organizationType).toEqualOneOf([
    'CLIENT',
    'TRAINING_COMPANY',
  ]);
  expect(contentItem.tags).toEqual(expect.any(Array));
  // $FlowFixMe
  expect(contentItem.series).toEqualOneOf([null]);
}
