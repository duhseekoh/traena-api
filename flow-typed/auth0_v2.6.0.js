/* @flow */
// flow-typed signature: 13ddf072cd0a958b752534cb42cbc6c8
// flow-typed version: <<STUB>>/auth0_v^2.6.0/flow_v0.45.0

declare type auth0$authorization$group = {
  _id: string;
  name: string;
  description?: string;
}


declare class auth0$ClientsManager {
}

declare type auth0$baseUserObject = {
  username?: string,
  given_name?: ?string,
  family_name?: ?string,
  phone_number?: ?string,
  user_metadata?: {},
  email_verified?: boolean,
  verify_email?: boolean,
  phone_verified?: boolean,
  app_metadata?: {},
}

declare type auth0$createUserObject = auth0$baseUserObject & {
  connection: string,
  email: string,
  password: string,
  username?: string,
}

declare type auth0$endUserObject = auth0$baseUserObject & {
  user_id: string,
  created_at: string,
  updated_at: string,
}

declare class auth0$UsersManager {
  create(user: auth0$createUserObject): Promise<auth0$endUserObject>;
  get(opts: { id: string }): Promise<auth0$endUserObject>;
}

declare type auth0$TicketsManager$changePasswordTicketOptions = {
  result_url?: string,
  user_id?: string,
  new_password?: string,
  connection_id?: string,
  email?: string,
}

declare type auth0$TicketsManager$changePasswordTicketResult = {
  ticket: string,
}

declare class auth0$TicketsManager {
  changePassword(user: auth0$TicketsManager$changePasswordTicketOptions
  ): Promise<auth0$TicketsManager$changePasswordTicketResult>;
}

declare module 'auth0' {
  declare type authOptions = {
    token: string,
    domain: string,
  }

  declare class ManagementClient {
    clients: auth0$ClientsManager;
    users: auth0$UsersManager;
    tickets: auth0$TicketsManager;

    createUser(user: auth0$createUserObject): Promise<auth0$endUserObject>;
    deleteUser({ id: string }): Promise<auth0$endUserObject>;
    updateUser({ id: string }, partialUser: *): Promise<auth0$endUserObject>;
    getUsers({ q: string }): Promise<auth0$endUserObject[]>;
    getUser({ id: string }): Promise<?auth0$endUserObject>;
    constructor(opts: authOptions): this;
  }

  declare class AuthenticationClient {
    constructor(opts: {
      domain: string,
      clientId: string,
    }): this;

    oauth: {
      passwordGrant: (*) => {
        access_token: string,
        id_token: string,
        refresh_token?: string,
      },
    },
  }

  declare module.exports: {

    ManagementClient: Class<ManagementClient>,
    AuthenticationClient: Class<AuthenticationClient>,
  }
}
