/* @flow */

declare class objection$Model {
  $id: Function;

  $hasId: Function;

  $hasProps: Function;

  $query: Function;

  $relatedQuery: Function;

  $loadRelated: Function;

  $beforeValidate: Function;

  $validate: Function;

  $afterValidate: Function;

  $parseDatabaseJson: Function;

  $formatDatabaseJson: Function;

  $parseJson: Function;

  $formatJson: Function;

  $setJson: Function;

  $setDatabaseJson: Function;

  $setRelated: Function;

  $appendRelated: Function;

  $toJson: Function;

  toJSON: Function;

  $toDatabaseJson: Function;

  $beforeInsert: Function;

  $afterInsert: Function;

  $beforeUpdate: Function;

  $afterUpdate: Function;

  $afterGet: Function;

  $beforeDelete: Function;

  $afterDelete: Function;

  $omit: Function;

  $pick: Function;

  $values: Function;

  $propKey: Function;

  $clone: Function;

  $traverse: Function;

  $omitFromJson: Function;

  $omitFromDatabaseJson: Function;

  $knex: Function;

  $transaction: Function;

  static fromJson: Function;

  static fromDatabaseJson: Function;

  static omitImpl: Function;

  static createValidator: Function;

  static createNotFoundError: Function;

  static createValidationError: Function;

  static getTableName: Function;

  static getIdColumn: Function;

  static getValidator: Function;

  static getJsonSchema: Function;

  static getJsonAttributes: Function;

  static getColumnNameMappers: Function;

  static columnNameToPropertyName: Function;

  static propertyNameToColumnName: Function;

  static getReadOnlyVirtualAttributes: Function;

  static getIdRelationProperty: Function;

  static getIdColumnArray: Function;

  static getIdPropertyArray: Function;

  static getIdProperty: Function;

  static getRelations: Function;

  static getRelationArray: Function;

  static query: Function;

  static relatedQuery: Function;

  static fetchDbMetadata: Function;

  static getDbMetadata: Function;

  static knex: Function;

  static transaction: Function;

  static raw: Function;

  static fn: Function;

  static knexQuery: Function;

  static uniqueTag: Function;

  static bindKnex: Function;

  static bindTransaction: Function;

  static ensureModel: Function;

  static ensureModelArray: Function;

  static getRelation: Function;

  static loadRelated: Function;

  static traverse: Function;

  static QueryBuilder: any;

  static HasOneRelation: any;
  static HasManyRelation: any;
  static ManyToManyRelation: any;
  static BelongsToOneRelation: any;
  static HasOneThroughRelation: any;

  static JoinEagerAlgorithm: any;
  static NaiveEagerAlgorithm: any;
  static WhereInEagerAlgorithm: any;

  static ValidationError: any;
  static NotFoundError: any;
}

declare module 'objection' {
  declare module.exports: {
    Model: Class<objection$Model>;
    transaction: Function;
    ref: Function,
    QueryBuilder: any,
  }
}
