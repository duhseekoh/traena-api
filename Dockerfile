# Use: defines image for use on CI and when the api is sent over to ECS

FROM node:8.9.4
COPY ./build /src

EXPOSE 3001
WORKDIR /src
CMD ["yarn", "start"]
