const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const packageJson = require('../package.json');

module.exports = {
  entry: [
    'babel-polyfill',
    './server.js',
  ],
  plugins: [
    new ProgressBarPlugin(),
  ],
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'server.bundle.js',
    libraryTarget: 'commonjs2',
  },
  context: path.resolve(__dirname, '../src'),
  target: 'node',
  node: false,
  externals: [nodeExternals()],
  resolve: {
    modules: [path.resolve(__dirname, '../')],
    // extensions: ['', '.webpack.js', '.js', '.jsx', '.json'],
    alias: {
      'any-promise': 'bluebird',
    },
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        // Only run `.js` and `.jsx` files through Babel
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: packageJson.babel.presets,
            plugins: ['transform-class-properties'],
          },
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.html$/,
        loader: 'raw-loader',
      },
    ],
  },
  // watching while in docker-compose environment requires polling.
  watchOptions: {
    poll: 500,
    aggregateTimeout: 1000,
    ignored: '/node_modules/',
  },
};
