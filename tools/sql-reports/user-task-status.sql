-- Users Task Status
SELECT "User"."firstName", "User"."lastName", "Content".body::json->'title' AS title, "Task".status, "User".id AS "userId", "Task"."contentId"
FROM "User"
INNER JOIN "Task"
    INNER JOIN "Content"
    ON "Task"."contentId" = "Content".id
ON "Task"."userId" = "User".id
WHERE "User"."organizationId" = 1038
ORDER BY "firstName", "lastName", "status" DESC
