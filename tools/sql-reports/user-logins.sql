-- User login events
SELECT "User"."firstName", "User"."lastName", "Event".type, "Event"."createdTimestamp"
FROM "User"
INNER JOIN "Event"
ON "Event"."userId" = "User".id
WHERE "User"."organizationId" = 1038
    AND (
        "Event".type = 'USER_LOGGED_IN'
        --OR "Event".type = 'APP_LAUNCHED'
    )
ORDER BY "User"."firstName", "User"."lastName", "Event"."createdTimestamp"
