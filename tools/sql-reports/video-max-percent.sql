-- Video Progress Max percent watched
SELECT DISTINCT "User"."firstName", "User"."lastName", "Event".type, "Event".data->>'percent' AS "percentWatched", "Content".id AS contentId, "Content".body->>'title' AS title
FROM "User"
INNER JOIN "Event"
    INNER JOIN "Content"
    ON ("Event".data->>'contentId')::int = "Content".id
ON "Event"."userId" = "User".id
WHERE "User"."organizationId" = 1038
    AND ("Event".data->>'contentId', ("Event".data->>'percent')::FLOAT) IN
    (
        SELECT "Event".data->>'contentId', max(("Event".data->>'percent')::FLOAT) FROM "Event"
        WHERE "Event".type = 'VIDEO_PROGRESSED'
        GROUP BY "Event".data->>'contentId'
    )
ORDER BY "User"."firstName", "User"."lastName"
