-- Video Progress
SELECT "User"."firstName", "User"."lastName", "Event".type, "Event".data->>'percent' AS "percentWatched", "Content".body::json->'title' AS title, "Event"."createdTimestamp"
FROM "User"
INNER JOIN "Event"
    INNER JOIN "Content"
    ON ("Event".data->>'contentId')::int = "Content".id
ON "Event"."userId" = "User".id
WHERE "User"."organizationId" = 1038
    AND "Event".type = 'VIDEO_PROGRESSED'
ORDER BY "User"."firstName", "User"."lastName", "Event"."createdTimestamp"
