-- Activity completion
SELECT "User"."firstName", "User"."lastName", "Content".body::json->'title' as "Content Title", "Activity"."body" as "Activity", "ActivityResponse"."body" as "Activity Response", "Activity"."contentId", "User".id as "userId", "ActivityResponse"."createdTimestamp"
FROM "User"
INNER JOIN "ActivityResponse"
  INNER JOIN "Activity"
    INNER JOIN "Content"
    ON "Activity"."contentId" = "Content".id
  ON "ActivityResponse"."activityId" = "Activity".id
ON "User".id = "ActivityResponse"."userId"
WHERE "User"."organizationId" = 1;
