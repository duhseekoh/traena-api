/* eslint-disable no-unused-expressions */

import 'babel-polyfill';
import yargs from 'yargs';
import { Model } from 'objection';
import Knex from 'knex';
import chalk from 'chalk';
import Kinesis from 'aws-sdk/clients/kinesis';

import config from '../../src/common/config';
import ContentService from '../../src/services/ContentService';
import UserService from '../../src/services/UserService';
import MediaService from '../../src/services/MediaService';
import EventService from '../../src/services/EventService';
import BatchContentService from './services/BatchContentService';
import ElasticSearchGatheringService from '../../src/services/ElasticSearchGatheringService';

const knex = Knex(config.db); // eslint-disable-line new-cap
Model.knex(knex);

const kinesisClient = new Kinesis();
const kinesisIndexRealtimeStream = config.kinesis.indexRealtimeStream;

const userService = new UserService();
const eventService = new EventService();
const elasticSearchGatheringService = new ElasticSearchGatheringService(kinesisClient, null, kinesisIndexRealtimeStream);
const contentService = new ContentService(eventService, null, elasticSearchGatheringService);
const mediaService = new MediaService(config);
const batchContentService = new BatchContentService(
  mediaService,
  userService,
  contentService,
);

yargs.version('1.0');

yargs
.coerce({
  actions: JSON.parse,
  tags: str => String().split.call(str, ','),
})
.command(
  'content:video:add',
  'Add a video file as content',
  args => args
  .usage('Actions')
  .option('e', { alias: 'email', describe: 'Email' })
  .option('f', { alias: 'file', describe: 'Path to file to add' })
  .option('t', { alias: 'title', describe: 'Title of file' })
  .option('a', {
    alias: 'actions',
    describe: 'Actions to include (as JSON, only works when run directly via node)',
  })
  .option('g', { alias: 'tags', describe: 'Tags (comma separated)' })
  .demandOption(['e', 'f', 't', 'g'])
  ,
  async (argv) => {
    try {
      await batchContentService.processVideo(argv);
    } catch (err) {
      console.log(chalk.red('Could not create content: ', err.message));
      console.log(chalk.red(err.stack));
    }
    knex.destroy();
  })
.command(
  'content:text:add',
  'Add text content',
  args => args
  .option('e', { alias: 'email', describe: 'Email' })
  .option('t', { alias: 'title', describe: 'Title' })
  .option('x', { alias: 'text', describe: 'Text' })
  .option('a', {
    alias: 'actions',
    describe: 'Actions to include (as JSON, only works when run directly via node)',
  })
  .option('g', { alias: 'tags', descrie: 'Tags (comma separated)' })
  .demandOption(['e', 't', 'g'])
  ,
  async (argv) => {
    try {
      await batchContentService.processText(argv);
    } catch (err) {
      console.log(chalk.red('Could not create content: ', err.message));
      console.log(chalk.red(err.stack));
    }
    knex.destroy();
  },
)
.command(
  'content:bulk:create <file>',
  'Add bulk content from csv',
  args => args
  .usage(`$0 - File should be CSV containing bulk content
          see example file, all videos should be relative to csv being uploaded`)
  ,
  async (argv) => {
    try {
      const results = await batchContentService.processCsv(argv);
      console.log(`Added ${results.length} pieces of content with ids: `, results);
    } catch (err) {
      console.log(err);
    }
    knex.destroy();
  },
)
.showHelpOnFail(true)
.usage('Usage: $0 <cmd> - See example csv file, all videos should be relative to the csv being uploaded')
.demandCommand()
  .argv;
