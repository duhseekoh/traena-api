// Note - this cli utility is no longer maintained or working. If we need it at some point
// then we'll have to fix it up.
// Removed flow pragma so it doesn't fail our flow command
import 'babel-polyfill';
import yargs from 'yargs';
import Knex from 'knex';
import { Model } from 'objection';
import chalk from 'chalk';
import Kinesis from 'aws-sdk/clients/kinesis';
import elasticSearch from 'elasticsearch';
import AWS from 'aws-sdk';
import awsEs from 'http-aws-es';

import config from '../../src/common/config';
import BatchUserService from './services/BatchUserService';
import UserService from '../../src/services/UserService';
import AuthService from '../../src/services/AuthService';
import ChannelService from '../../src/services/ChannelService';
import EmailService from '../../src/services/EmailService';
import EventService from '../../src/services/EventService';
import ElasticSearchGatheringService from '../../src/services/ElasticSearchGatheringService';
import ElasticSearchService from '../../src/services/ElasticSearchService';
import OrganizationService from '../../src/services/OrganizationService';
import Auth0Client from '../../src/client/Auth0Client';

const knex = Knex(config.db); // eslint-disable-line new-cap
Model.knex(knex);

const kinesisClient = new Kinesis();

AWS.config.getCredentials(() => {
  console.log('Retrieved aws credentials for debugging purposes', AWS.config.credentials);
});

const esConfig = config.elasticSearch.useAws ? ({
  hosts: config.elasticSearch.apiBaseUrl,
  connectionClass: awsEs,
  amazonES: {
    region: AWS.config.region,
    getCredentials: true,
  },
  log: config.elasticSearch.logLevel,
}) : ({
  host: config.elasticSearch.apiBaseUrl,
});
const elasticSearchClient = new elasticSearch.Client(esConfig);

const eventService = new EventService();
const auth0Client = new Auth0Client(config);
const emailService = new EmailService(config);
const authService = new AuthService(config, emailService, auth0Client);
const channelService = new ChannelService(eventService);
const elasticSearchService = new ElasticSearchService(elasticSearchClient);
const organizationService = new OrganizationService(
  config,
  authService,
  eventService,
  channelService,
  elasticSearchService,
);
const elasticSearchGatheringService =
  new ElasticSearchGatheringService(kinesisClient, 'dummy', 'dummy', eventService);
const userService =
  new UserService(config, authService, organizationService, eventService, elasticSearchGatheringService);
const batchUserService = new BatchUserService(config, userService);


yargs.version('1.0');
yargs.command(  // eslint-disable-line no-unused-expressions
  'user:password:reset',
  'Send a set password link',
  args => args
  .option('e', { alias: 'email', describe: 'Email' })
  .demandOption(['e'])
  ,
  async (argv) => {
    try {
      const link = await authService.getNewPasswordLink(argv.email);
      console.log(chalk.green(`Got a link: ${link.ticket}`));
    } catch (err) {
      console.log(chalk.red('Could not create user: ', err.message));
      console.log(chalk.red(err.stack));
    }
    knex.destroy();
  })
.command(
  'user:create',
  'Create user in Auth0',
  args => args
  .coerce({
    tags: str => String().split.call(str, ','),
    email: str => str.toLowerCase(),
  })
  .option('e', { alias: 'email', describe: 'Email' })
  .option('f', { alias: 'firstname', describe: 'First Name' })
  .option('l', { alias: 'lastname', describe: 'Last Name' })
  .option('o', { alias: 'orgid', describe: 'Organization ID' })
  .option('p', { alias: 'position', describe: 'Position within organization' })
  .option('w', { alias: 'password', describe: 'Password to set' })
  .option('t', { alias: 'tags', describe: 'Tags for user' })
  .demandOption(['e', 'f', 'l', 'o', 'p'])
  ,
  async (argv) => {
    try {
      const user = await userService.create(undefined, {
        email: argv.email,
        firstName: argv.firstname,
        lastName: argv.lastname,
        organizationId: argv.orgid,
        position: argv.position,
        tags: argv.tags,
      }, argv.password);
      console.log(chalk.green(`Created user with id: ${user.id}`));
    } catch (err) {
      console.log(chalk.red(`Could not create user: ${err.message}`));
      console.log(chalk.red(err.stack));
      console.log(err);
    }
    knex.destroy();
  })
.command(
  'user:batch:create <file>',
  'Add bulk users from csv',
  args => args
  .option('w', { alias: 'password', describe: 'Password to set' })
  .usage('$0 - File should be csv containing user entries, see example file')
  ,
  async (argv) => {
    try {
      console.log('Adding users from file: ', argv.file);
      const results = await batchUserService.processCsv(argv, argv.password);
      console.log(`Added ${results.length} users with ids `, results);
    } catch (err) {
      console.log(chalk.red(err));
    }
    knex.destroy();
  },
)
.showHelpOnFail(true)
.usage('Usage: $0 <cmd>')
.demandCommand()
  .argv;
