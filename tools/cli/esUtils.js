// @flow
import 'babel-polyfill';
import program from 'commander';
import { Model } from 'objection';
import Knex from 'knex';
import _ from 'lodash';
import es from 'elasticsearch';
import Kinesis from 'aws-sdk/clients/kinesis';
import config from '../../src/common/config';
import ElasticSearchGatheringService from '../../src/services/ElasticSearchGatheringService';
import type EventService from '../../src/services/EventService';
import * as esTestContentHelper from '../../src/services/testContentGeneration/esTestContentHelper';
import * as esTestUsersHelper from '../../src/services/testContentGeneration/esTestUsersHelper';

const knex = Knex(config.db);
Model.knex(knex);

const esUrl = config.elasticSearch.apiBaseUrl;
if (!(esUrl.includes('localhost') || esUrl.includes('127.0.0.1') || esUrl.includes('elasticsearch:9200'))) {
  throw new Error(`Utility is meant only for running against local elasticsearch instance. Configured for ${esUrl}`);
}

const client = new es.Client({
  host: esUrl,
});

const INDEXES = {
  USERS: 'users',
  CONTENT: 'contents',
};

// Some nasty type casting here to get Flow to be happy. We don't use any methods of the gathering service here
// that require the constructor params, so passing in fake EventService
const kinesisClient = new Kinesis();
const gatheringService = new ElasticSearchGatheringService(kinesisClient, 'dummy', 'dummy', (({}: any): EventService));

/**
 * Utility to populate a LOCAL elastic search index - deletes any existing index and repopulates.
 * NOT SAFE to use for other environments as we would want to create a temp index, then swap for live and then cleanup.
 */
program.version('1.0');

program
  .command('index:users')
  .action(async () => {
    console.log('Indexing users');
    const users = await gatheringService.getAllUsersForIndexing();
    try {
      await client.indices.delete({ index: INDEXES.USERS });
    } catch (err) {
      // swallow errors
    }
    await client.indices.create({ index: INDEXES.USERS });
    console.log('Index created, indexing users');
    const result = await users.map((user) => {
      process.stdout.write('.');
      return client.index({
        index: INDEXES.USERS,
        body: user,
        type: 'profile',
        id: user.id,
      });
    });
    console.log(`${result.length} items indexed`);
    knex.destroy();
    return result;
  });

program
  .command('index:content')
  .action(async () => {
    console.log('Indexing content');
    const contents = await gatheringService.getAllSubscribedContentForAllOrgs();
    try {
      await client.indices.delete({ index: INDEXES.CONTENT });
    } catch (err) {
      // swallow
    }
    await client.indices.create({ index: INDEXES.CONTENT });
    console.log('Index created, indexing content');
    const result = await contents.map((content) => {
      process.stdout.write('.');
      const { id: contentId } = content;
      if (!contentId) {
        return null;
      }
      return client.index({
        index: INDEXES.CONTENT,
        body: content,
        type: _.snakeCase(content.body.type),
        id: `${contentId}_${content.subscriberOrganizationId}`,
      });
    });
    console.log(`${result.length} items indexed`);
    knex.destroy();
  });

program
  .command('index:test:content')
  .option('-n, --name [name]', 'The name of this test index')
  .option('-j, --json [json]', 'JSON config describing an array of sets of all params. See readme for available params')
  .action(async (options) => {
    const name = options.name || 'testcontentindex';
    console.log(`Generating and indexing content into the '${name}' index.`);
    // Clear this test index type if it exists
    try {
      await client.indices.delete({ index: name });
    } catch (err) {
      // swallow
    }
    // Generate content
    let testContent = [];
    if (options.json) {
      testContent = esTestContentHelper.generateTestContentFromJSONDefinition(options.json);
    }
    if (!testContent) {
      console.log('No content was generated. Verify the validity of your arguments.');
      return;
    }
    // Index content
    const result = await testContent.map((content) => {
      process.stdout.write('.');
      return client.index({
        index: name,
        body: content,
        type: 'test-content',
        id: `${content.id}_${content.subscriberOrganizationId}`,
      });
    });
    console.log(result);
    console.log(`${result.length} items indexed`);
  });

program
  .command('index:test:users [type]')
  .action(async (type) => {
    console.log(`Generating and indexing user data of type ${type}`);
    // Clear this test index type if it exists
    try {
      await client.indices.delete({ index: type });
    } catch (err) {
      // swallow
    }
    // Generate content
    const testUsers = esTestUsersHelper.generateTestUsers(type);
    if (!testUsers) {
      console.log(`No users were generated, ${type} may not have a definition`);
      return;
    }
    // Index content
    const result = await testUsers.map((user) => {
      process.stdout.write('.');
      return client.index({
        index: type,
        body: user,
        type: 'test-users',
        id: user.id,
      });
    });
    console.log(`${result.length} items indexed`);
  });

program.parse(process.argv);
