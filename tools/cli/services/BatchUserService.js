/* @flow */
/* eslint-disable class-methods-use-this */
import bluebird from 'bluebird';
import csv from 'csv';
import fs from 'fs-extra';
import chalk from 'chalk';
import type UserService from '../../../src/services/UserService';
import type { AppConfig } from '../../../src/common/config';

const csvParse = bluebird.promisify(csv.parse);

export default class BatchUserService {
  config: AppConfig;
  userService: UserService;

  constructor(config: AppConfig, userService: UserService): BatchUserService {
    this.config = config;
    this.userService = userService;
    return this;
  }

  async processCsv(argv: { file: string }, password: ?string): Promise<string[]> {
    const csvContents = await fs.readFile(argv.file);
    const parsed = await csvParse(csvContents, { columns: true });
    const cleaned = parsed.map(user => ({
      ...user,
      email: user.email.toLowerCase(),
      orgid: +user.orgid,
      tags: user.tags.split(',').map(x => x.trim()).filter(x => x.length),
    }));

    // $FlowFixMe$ Flow is complaining here and I don't know why.
    return bluebird.mapSeries(cleaned, async (user) => {
      try {
        const result = await this.userService.create(undefined, {
          email: user.email,
          firstName: user.firstname,
          lastName: user.lastname,
          organizationId: user.orgid,
          position: user.position,
          tags: user.tags,
        }, password);
        return result.id;
      } catch (err) {
        console.log(chalk.red(`Error processing user with email: ${user.email}`));
        console.log(chalk.red(err.message));
        return '';
      }
    });
  }
}
