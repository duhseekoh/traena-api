import 'babel-polyfill';
import _ from 'lodash';
import chalk from 'chalk';
import path from 'path';
import fs from 'fs-extra';
import csv from 'csv';
import Promise from 'bluebird';

const csvParse = Promise.promisify(csv.parse);

export default class BatchContentService {
  constructor(mediaService, userService, contentService) {
    this.mediaService = mediaService;
    this.userService = userService;
    this.contentService = contentService;
  }

  generateDraftVideoPayload(authorId, orgId, title, tags) { // eslint-disable-line class-methods-use-this
    return {
      authorId,
      authorOrganizationId: orgId,
      status: 'DRAFT',
      body: {
        type: 'TrainingVideo',
        title,
      },
      tags,
    };
  }

  populateVideoPaths(contentModel) {
    return Object.assign(
      {},
      _.omit(contentModel, ['createdTimestamp', 'modifiedTimestamp', 'author', 'channel']),
      {
        status: 'PUBLISHED',
        body: Object.assign({}, contentModel.body, {
          video: {
            videoURI: this.mediaService.getVideoCDNPath(
              contentModel.authorOrganizationId, contentModel.authorId, contentModel.id),
            previewImageURI: this.mediaService.getVideoPreviewCDNPath(
              contentModel.authorOrganizationId, contentModel.authorId, contentModel.id),
          },
        }),
        tags: contentModel.tags.map(tag => tag.text),
      },
    );
  }

  generateTextPayload(authorId, orgId, title, text, tags) { // eslint-disable-line class-methods-use-this
    return {
      authorId,
      authorOrganizationId: orgId,
      status: 'PUBLISHED',
      body: {
        type: 'DailyAction',
        title,
        text,
      },
      tags,
    };
  }

  async processVideo(argv) {
    // Get the orgid and authorid based on email
    const user = await this.userService.findByEmail(argv.email);
    if (!user) {
      throw new Error('User not found for email address ', user.email);
    }
    const newContent = this.generateDraftVideoPayload(
      user.id, user.organizationId, argv.title, argv.tags);
    // Create the content to get the ID for uploading to S3
    const content = await this.contentService.createContentItem(newContent, user.id, user.organizationId);
    console.log(chalk.green('Added content to database with id: ', content.id));

    // Upload to S3
    const inputVideoKey = this.mediaService.getInputVideoFolderKey(
      user.organizationId, user.id, content.id);
    const s3path = inputVideoKey + path.basename(argv.file);
    console.log(chalk.blue(`Uploading file: ${argv.file}`));
    await this.mediaService.uploadFile(argv.file, s3path);
    console.log(chalk.green('Successfully uploaded to S3 at path: ', s3path));

    // kick off the Transcoder lambda after the file is uploaded, we could spin lock and poll for job completion before
    // continuing, but for the batch use case, i think we just want to trust that it does, rather than wait
    const jobId = await this.mediaService.invokeTranscoderLambda(user.organizationId, user.id, content.id, argv.file);
    console.log(chalk.green('Invoked Elastic Transcoder for job:', jobId));

    // Update the original content with the generated URLs
    const updatedVideo = this.populateVideoPaths(content.toJSON());
    await this.contentService.updateContentItem(updatedVideo);
    console.log(chalk.green(
      'Successfully updated content in database, completed adding content with id: ', content.id));
    return content.id;
  }

  async processText(argv) {
    const user = await this.userService.findByEmail(argv.email);
    const newText = this.generateTextPayload(
      user.id, user.organizationId, argv.title, argv.text, argv.tags);
    const content = await this.contentService.createContentItem(newText, user.id, user.organizationId);
    console.log(chalk.green('Added content to database with id: ', content.id));
    return content.id;
  }

  /**
   * Batch add via CSV a set of content (images or files)
   * See the example csv at tools/content/batch-example.csv
   * Video files should be relative to the csv and should be of mp4 file type
   *
   * @param  {Object}  argv { file: `path/to/csv`}
   * @return {Promise<Array<Number>>} Promise with Array of new Content Ids
   */
  async processCsv(argv) {
    console.log(chalk.blue('Processing file: ', argv.file));
    const csvContents = await fs.readFile(argv.file);
    const parsed = await csvParse(csvContents, { columns: true });
    const rootPath = path.dirname(argv.file);
    const cleaned = parsed.map(entry => Object.assign({}, entry, {
      tags: entry.tags.length ? entry.tags.split(',') : [],
      file: entry.file ? `${rootPath}/${entry.file}` : null,
    }));
    /* eslint-disable consistent-return */
    return Promise.mapSeries(cleaned, async (x) => {
      console.log('Processing entry: ', x.title);
      try {
        if (x.type === 'video') {
          console.log('Video');
          return await this.processVideo(x);
        } else if (x.type === 'text') {
          console.log('Text');
          return await this.processText(x);
        }
      } catch (err) {
        console.log(chalk.red('Error processing line with title: ', x.title));
        console.log(chalk.red(err.message));
        return '';
      }
    });
  }
}
