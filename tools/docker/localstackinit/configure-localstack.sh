#!/bin/ash

# create the realtime indexing stream and display it afterwards
aws --endpoint-url=http://localstack:4568 kinesis create-stream --stream-name index-elasticsearch-realtime-stream-local-docker --shard-count 1
aws --endpoint-url=http://localstack:4568 kinesis list-streams

# TODO download zip artifact from github releases instead of including artifact in this repo.
#   that requires us to have access to the
#
# curl https://github.com/Traena/elasticsearch-indexer/releases/download/v1.5.0/elasticsearch-indexer.zip --output elasticsearch-indexer.zip
# ls elasticsearch-indexer.zip

# create the indexing lambda.
# the ES endpoint is configured to be the docker bridge network ip (172.17.0.1).
aws --endpoint-url=http://localstack:4574 lambda create-function \
--function-name kinesis-to-elasticsearch-local-docker \
--environment Variables={ELASTIC_SEARCH_API_BASE_URL=http://172.17.0.1:9201} \
--role=dummy \
--zip-file fileb://elasticsearch-indexer.zip \
--runtime nodejs6.10 \
--handler index.handler \
--timeout 10 \
--memory-size 128

# hook up the indexing lambda to the kinesis stream
aws --endpoint-url=http://localstack:4574 lambda create-event-source-mapping \
--event-source-arn arn:aws:kinesis:us-west-2:000000000000:stream/index-elasticsearch-realtime-stream-local-docker \
--function-name kinesis-to-elasticsearch-local-docker \
--starting-position LATEST

# display our newly created function
aws --endpoint-url=http://localstack:4574 lambda list-functions
