/* eslint-disable max-len */

import 'babel-polyfill';
import Knex from 'knex';
import Promise from 'bluebird';

if (!process.env.DATABASE_URL || !process.env.CDN_BASE_URL) {
  throw new Error('Required environment variables: DATABASE_URL, CDN_BASE_URL');
}

const knex = Knex(process.env.DATABASE_URL);

knex.select().from('Content').whereRaw('body @> \'{"type":"TrainingVideo"}\'')
  .then(results =>
    Promise.mapSeries(results, (result) => {
      console.log(result.id);
      console.log(result.body);
      const video = {
        ...result.body.video,
        videoURI: `${process.env.CDN_BASE_URL}/organizations/${result.authorOrganizationId}/users/${result.authorId}/content/${result.id}/video-output/video.m3u8`,
        previewImageURI: `${process.env.CDN_BASE_URL}/organizations/${result.authorOrganizationId}/users/${result.authorId}/content/${result.id}/images/thumbnail-00002.png`,
      };
      const body = {
        ...result.body,
        video,
      };
      console.log('Updated body:');
      console.log(body);
      return knex('Content').where('id', result.id).update('body', body);
    }),
  )
  .then((results) => {
    console.log('RESULTS');
    console.log(results);
  });
