import {} from 'dotenv/config';
import webpack from 'webpack';
import path from 'path';
import cp from 'child_process';
import webpackConfig from './webpack.config';
import copy from './copy';

let server = null;
const { output } = webpackConfig;
const serverPath = path.join(output.path, output.filename);

function runServer() {
  if (server) {
    server.kill('SIGTERM');
  }
  server = cp.fork([serverPath], {
    env: Object.assign({ NODE_ENV: 'local' }, process.env),
    silent: false,
  });
}


async function watch() {
  await copy();
  const compiler = webpack(webpackConfig);
  compiler.watch({}, (err, stats) => {
    if (err) {
      server.kill('SIGTERM');
    } else {
      console.log('restarting server');
      runServer();
    }

    const jsonStats = stats.toJson();
    if (jsonStats.errors.length > 0) {
      jsonStats.errors.forEach((error) => {
        console.log(error);
      });
    }
    if (jsonStats.warnings.length > 0) {
      jsonStats.warnings.forEach((warning) => {
        console.log(warning);
      });
    }
  });
}

watch();
