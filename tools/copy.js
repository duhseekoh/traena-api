import fs from 'fs-extra';
import pkg from '../package.json';

/**
 * Copies static files such as robots.txt, favicon.ico to the
 * output (build) folder.
 */
async function copy() {
  /* eslint-disable global-require */
  try {
    const result = await fs.stat('build');
    console.log('build fs stat');
    console.log(result);
  } catch (e) {
    await fs.mkdir('build');
  }

  try {
    const result = await fs.stat('build/src');
    console.log('build/src fs stat');
    console.log(result);
  } catch (e) {
    await fs.mkdir('build/src');
  }

  console.log('Inside copy command, going to copy folders'); // eslint-disable-line no-console
  await fs.writeFile('build/package.json', JSON.stringify({
    private: true,
    engines: pkg.engines,
    dependencies: pkg.dependencies,
    scripts: {
      start: 'node server.bundle.js',
    },
  }));
  await fs.copy('db', 'build/db');
  await fs.copy('src/static/swagger-api', 'build/src/static/swagger-api');
  await fs.copy('node_modules/swagger-ui-dist/', 'build/src/static/swagger-ui/');
  await fs.copy('src/static/swagger-ui/index.html', 'build/src/static/swagger-ui/index.html');

  console.log('finished copying folders'); // eslint-disable-line no-console
}

export default copy;
