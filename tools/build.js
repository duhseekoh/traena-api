import copy from './copy';
import bundle from './bundle';
//
/**
 * Compiles the project from source files into a distributable
 * format and copies it to the output (build) folder.
 */
async function build() {
  await copy();
  await bundle();
}

build();
