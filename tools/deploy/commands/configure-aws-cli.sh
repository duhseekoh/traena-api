#!/usr/bin/env bash

echo '============================================================'
echo '>>> configuring AWS CLI environment'

if [ $# -lt 6 ]
then
    echo '>>> missing arguments'
    exit
fi

AWS_REGION=$1
ECS_CLUSTER=$2
ECS_SERVICE=$3
ECS_TASK_FAMILY=$4
ECS_CONTAINER=$5
ECR_REPOSITORY=$6

configure_aws_cli(){
  echo "config - AWS_REGION: $AWS_REGION, ECS_CLUSTER: $ECS_CLUSTER, ECS_SERVICE: $ECS_SERVICE, ECS_TASK_FAMILY: $ECS_TASK_FAMILY, ECS_CONTAINER: $ECS_CONTAINER, ECR_REPOSITORY: $ECR_REPOSITORY"

	aws --version
	aws configure set default.region $AWS_REGION
	aws configure set default.output json
}

configure_aws_cli
