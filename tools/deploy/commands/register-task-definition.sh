#!/usr/bin/env bash

echo '============================================================'
echo '>>> registering task definition'

### env variables
if [ "$ECS_TASK_FAMILY" == "" ]; then
  echo '>>> missing env variable  ECS_TASK_FAMILY'
  {(exit 1); exit 1;}
fi

if [ "$TASK_DEFINITION" == "" ]; then
  echo '>>> missing env variable TASK_DEFINITION'
  {(exit 1); exit 1;}
fi

JQ="jq --raw-output --exit-status"

register_task_definition() {
    if revision=$(aws ecs register-task-definition --container-definitions "$TASK_DEFINITION" --family $ECS_TASK_FAMILY | $JQ '.taskDefinition.taskDefinitionArn'); then
        echo "register_definition - Revision: $revision"
        export TASK_REVISION=$revision
    else
        echo "Failed to register task definition"
        {(exit 1); exit 1;}
    fi
}

register_task_definition
