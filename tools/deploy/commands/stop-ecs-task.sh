#!/usr/bin/env bash

echo '============================================================'
echo '>>> stopping ECS Task'

if [ $# -lt 2 ]
then
    echo '>>> missing arguments'
    exit
fi

JQ="jq --raw-output --exit-status"
ECS_CLUSTER=$1
ECS_SERVICE=$2

stop_old_task() {
    echo "WARNING!!! This function is a work around until a more graceful deployment can be figured out.  It will cause a brief outage AND may not work for mulit-task deployments"
    runningTaskId=$(aws ecs list-tasks --cluster $ECS_CLUSTER --service-name $ECS_SERVICE | $JQ '.taskArns[0]')
    echo "stopping task: $runningTaskId"
    if [ "$runningTaskId" = "null" ]
    then
      echo "WARNING!!! There were 0 running tasks."
    else
      aws ecs stop-task --cluster $ECS_CLUSTER --task $runningTaskId
    fi
}

stop_old_task
