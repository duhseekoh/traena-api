#!/usr/bin/env bash

echo '============================================================'
echo '>>> creating task definition'

if [ $# -lt 2 ]
then
    echo '>>> missing arguments {ENVIRONMENT} {DATABASE_URL}'
    {(exit 1); exit 1;}
fi


ENVIRONMENT=$1
DATABASE_URL=$2

if [ "$ENVIRONMENT" == "" ]; then
  echo '>>> missing argument ENVIRONMENT'
  {(exit 1); exit 1;}
fi

if [ "$DATABASE_URL" == "" ]; then
  echo '>>> missing argument DATABASE_URL'
  {(exit 1); exit 1;}
fi

### env variables
if [ "$CONTAINER_PORT" == "" ]; then
  echo '>>> missing env variable CONTAINER_PORT'
  {(exit 1); exit 1;}
fi

if [ "$HOST_PORT" == "" ]; then
  echo '>>> missing env variable HOST_PORT'
  {(exit 1); exit 1;}
fi

if [ "$AWS_REGION" == "" ]; then
  echo '>>> missing env variable AWS_REGION'
  {(exit 1); exit 1;}
fi

if [ "$ECS_CONTAINER" == "" ]; then
  echo '>>> missing env variable ECS_CONTAINER'
  {(exit 1); exit 1;}
fi

if [ "$ECR_REPOSITORY" == "" ]; then
  echo '>>> missing env variable ECR_REPOSITORY'
  {(exit 1); exit 1;}
fi

if [ "$AWS_ACCOUNT_ID" == "" ]; then
  echo '>>> missing env variable AWS_ACCOUNT_ID'
  {(exit 1); exit 1;}
fi

if [ "$CIRCLE_SHA1" == "" ]; then
  echo '>>> missing env variable CIRCLE_SHA1'
  {(exit 1); exit 1;}
fi

if [ "$USER_JWT_SECRET" == "" ]; then
  echo '>>> missing env variable USER_JWT_SECRET'
  {(exit 1); exit 1;}
fi

if [ "$CLIENT_JWT_SECRET" == "" ]; then
  echo '>>> missing env variable CLIENT_JWT_SECRET'
  {(exit 1); exit 1;}
fi

if [ "$ELASTIC_SEARCH_API_BASE_URL" == "" ]; then
  echo '>>> missing env variable ELASTIC_SEARCH_API_BASE_URL'
  {(exit 1); exit 1;}
fi

if [ "$SNS_IOS_PLATFORM_APPLICATION_ARN" == "" ]; then
  echo '>>> missing env variable SNS_IOS_PLATFORM_APPLICATION_ARN'
  {(exit 1); exit 1;}
fi

if [ "$SNS_ANDROID_PLATFORM_APPLICATION_ARN" == "" ]; then
  echo '>>> missing env variable SNS_ANDROID_PLATFORM_APPLICATION_ARN'
  {(exit 1); exit 1;}
fi

if [ "$LOGGLY_TOKEN" == "" ]; then
  echo '>>> missing env variable LOGGLY_TOKEN'
  {(exit 1); exit 1;}
fi

if [ "$KINESIS_INDEX_STREAM" == "" ]; then
  echo '>>> missing env variable KINESIS_INDEX_STREAM'
  {(exit 1); exit 1;}
fi

if [ "$KINESIS_INDEX_REALTIME_STREAM" == "" ]; then
  echo '>>> missing env variable KINESIS_INDEX_REALTIME_STREAM'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_MGMT_CLIENT_ID" == "" ]; then
  echo '>>> missing env variable AUTH_MGMT_CLIENT_ID'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_MGMT_CLIENT_SECRET" == "" ]; then
  echo '>>> missing env variable AUTH_MGMT_CLIENT_SECRET'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_AUDIENCE" == "" ]; then
  echo '>>> missing env variable AUTH_AUDIENCE'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_DOMAIN" == "" ]; then
  echo '>>> missing env variable AUTH_DOMAIN'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_CONNECTION_ID" == "" ]; then
  echo '>>> missing env variable AUTH_CONNECTION_ID'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_CONNECTION_NAME" == "" ]; then
  echo '>>> missing env variable AUTH_CONNECTION_NAME'
  {(exit 1); exit 1;}
fi

if [ "$AUTH_AUTHORIZATION_API_URI" == "" ]; then
  echo '>>> missing env variable AUTH_AUTHORIZATION_API_URI'
  {(exit 1); exit 1;}
fi

if [ "$JWT_USER_CLAIM_NAMESPACE" == "" ]; then
  echo '>>> missing env variable JWT_USER_CLAIM_NAMESPACE'
  {(exit 1); exit 1;}
fi

if [ "$AWS_CONTENT_BUCKET" == "" ]; then
  echo '>>> missing env variable AWS_CONTENT_BUCKET'
  {(exit 1); exit 1;}
fi

if [ "$TRANSCODER_LAMBDA_NAME" == "" ]; then
  echo '>>> missing env variable TRANSCODER_LAMBDA_NAME'
  {(exit 1); exit 1;}
fi

if [ "$IMAGE_PROCESSOR_LAMBDA_NAME" == "" ]; then
  echo '>>> missing env variable IMAGE_PROCESSOR_LAMBDA_NAME'
  {(exit 1); exit 1;}
fi

if [ "$SEGMENT_WRITE_KEY" == "" ]; then
  echo '>>> missing env variable SEGMENT_WRITE_KEY'
  {(exit 1); exit 1;}
fi

create_task_definition(){
    task_template='[
        {
            "name": "%s",
            "image": "%s.dkr.ecr.%s.amazonaws.com/%s:%s",
            "essential": true,
            "memory": 800,
            "cpu": 10,
            "portMappings": [
                {
                    "containerPort": %s,
                    "hostPort": %s
                }
            ],
            "environment" : [
                { "name" : "DATABASE_URL", "value" : "%s" },
                { "name" : "NODE_ENV", "value" : "%s" },
                { "name" : "PORT", "value" : "%s" },
                { "name" : "USER_JWT_SECRET", "value" : "%s" },
                { "name" : "CLIENT_JWT_SECRET", "value" : "%s" },
                { "name" : "ELASTIC_SEARCH_API_BASE_URL", "value" : "%s" },
                { "name" : "SNS_IOS_PLATFORM_APPLICATION_ARN", "value" : "%s" },
                { "name" : "SNS_ANDROID_PLATFORM_APPLICATION_ARN", "value" : "%s" },
                { "name" : "AWS_REGION", "value" : "%s" },
                { "name" : "LOGGLY_TOKEN", "value" : "%s" },
                { "name" : "KINESIS_INDEX_STREAM", "value" : "%s" },
                { "name" : "KINESIS_INDEX_REALTIME_STREAM", "value" : "%s" },
                { "name" : "AUTH_MGMT_CLIENT_ID", "value" : "%s" },
                { "name" : "AUTH_MGMT_CLIENT_SECRET", "value" : "%s" },
                { "name" : "AUTH_AUDIENCE", "value" : "%s" },
                { "name" : "AUTH_DOMAIN", "value" : "%s" },
                { "name" : "AUTH_CONNECTION_ID", "value" : "%s" },
                { "name" : "AUTH_CONNECTION_NAME", "value" : "%s" },
                { "name" : "AUTH_AUTHORIZATION_API_URI", "value" : "%s" },
                { "name" : "JWT_USER_CLAIM_NAMESPACE", "value" : "%s" },
                { "name" : "AWS_CONTENT_BUCKET", "value" : "%s" },
                { "name" : "CDN_BASE_URL", "value" : "%s" },
                { "name" : "TRANSCODER_LAMBDA_NAME", "value" : "%s" },
                { "name" : "IMAGE_PROCESSOR_LAMBDA_NAME", "value" : "%s" },
                { "name" : "SEGMENT_WRITE_KEY", "value" : "%s" }
            ]
        }
    ]'

    echo "task_template: $task_template"
	  export TASK_DEFINITION=$(printf "$task_template" "${ECS_CONTAINER}" "${AWS_ACCOUNT_ID}" "${AWS_REGION}" "${ECR_REPOSITORY}" "${CIRCLE_SHA1}" "${CONTAINER_PORT}" "${HOST_PORT}" "${DATABASE_URL}" "${ENVIRONMENT}" "${CONTAINER_PORT}" "${USER_JWT_SECRET}" "${CLIENT_JWT_SECRET}" "${ELASTIC_SEARCH_API_BASE_URL}" "${SNS_IOS_PLATFORM_APPLICATION_ARN}" "${SNS_ANDROID_PLATFORM_APPLICATION_ARN}" "${AWS_REGION}" "${LOGGLY_TOKEN}" "${KINESIS_INDEX_STREAM}" "${KINESIS_INDEX_REALTIME_STREAM}" "${AUTH_MGMT_CLIENT_ID}" "${AUTH_MGMT_CLIENT_SECRET}" "${AUTH_AUDIENCE}" "${AUTH_DOMAIN}" "${AUTH_CONNECTION_ID}" "${AUTH_CONNECTION_NAME}" "${AUTH_AUTHORIZATION_API_URI}" "${JWT_USER_CLAIM_NAMESPACE}" "${AWS_CONTENT_BUCKET}" "${CDN_BASE_URL}" "${TRANSCODER_LAMBDA_NAME}" "${IMAGE_PROCESSOR_LAMBDA_NAME}" "${SEGMENT_WRITE_KEY}")
}

create_task_definition
