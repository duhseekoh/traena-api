#!/usr/bin/env bash

if [ $# -lt 2 ]
then
    echo '>>> missing arguments'
    exit
fi

REGION=$1
SECURITY_GROUP=$2
PORT=$3

curl http://checkip.amazonaws.com/ | xargs -n 1 -I % aws ec2 authorize-security-group-ingress --region $REGION --group-id $SECURITY_GROUP --protocol tcp --port $PORT --cidr %/32
