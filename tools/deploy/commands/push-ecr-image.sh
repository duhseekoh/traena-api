#!/usr/bin/env bash

echo '============================================================'
echo '>>> pushing ECR image'

if [ $# -lt 4 ]
then
    echo '>>> missing arguments'
    exit
fi

AWS_REGION=$1
AWS_ACCOUNT_ID=$2
ECR_REPOSITORY=$3
CIRCLE_SHA1=$4

push_ecr_image(){
	eval $(aws ecr get-login --no-include-email --region $AWS_REGION)

  echo "push_ecr_image - AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID CIRCLE_SHA1: $CIRCLE_SHA1"
	docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$ECR_REPOSITORY:$CIRCLE_SHA1
}

push_ecr_image
