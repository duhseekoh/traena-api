#!/usr/bin/env bash

# more bash-friendly output for jq
JQ="jq --raw-output --exit-status"

export AWS_REGION="us-west-2"
export ECS_CLUSTER="traena-production"
export ECS_SERVICE="traena-production"
export ECS_TASK_FAMILY="traena-production"
export ECS_CONTAINER="traena-production"
export ECR_REPOSITORY="traena-api"
export CONTAINER_PORT="3001"
export HOST_PORT="8000"
export ENVIRONMENT="production"
DEPLOY_ENV="PRODUCTION"

export USER_JWT_SECRET=$(eval echo \$USER_JWT_SECRET_${DEPLOY_ENV})
export CLIENT_JWT_SECRET=$(eval echo \$CLIENT_JWT_SECRET_${DEPLOY_ENV})
export ELASTIC_SEARCH_API_BASE_URL=$(eval echo \$ELASTIC_SEARCH_API_BASE_URL_${DEPLOY_ENV})
export SNS_IOS_PLATFORM_APPLICATION_ARN=$(eval echo \$SNS_IOS_PLATFORM_APPLICATION_ARN_${DEPLOY_ENV})
export SNS_ANDROID_PLATFORM_APPLICATION_ARN=$(eval echo \$SNS_ANDROID_PLATFORM_APPLICATION_ARN_${DEPLOY_ENV})
export LOGGLY_TOKEN=$(eval echo \$LOGGLY_TOKEN_${DEPLOY_ENV})
export KINESIS_INDEX_STREAM=$(eval echo \$KINESIS_INDEX_STREAM_${DEPLOY_ENV})
export KINESIS_INDEX_REALTIME_STREAM=$(eval echo \$KINESIS_INDEX_REALTIME_STREAM_${DEPLOY_ENV})
export AUTH_MGMT_CLIENT_ID=$(eval echo \$AUTH_MGMT_CLIENT_ID_${DEPLOY_ENV})
export AUTH_MGMT_CLIENT_SECRET=$(eval echo \$AUTH_MGMT_CLIENT_SECRET_${DEPLOY_ENV})
export AUTH_AUDIENCE=$(eval echo \$AUTH_AUDIENCE_${DEPLOY_ENV})
export AUTH_DOMAIN=$(eval echo \$AUTH_DOMAIN_${DEPLOY_ENV})
export AUTH_CONNECTION_ID=$(eval echo \$AUTH_CONNECTION_ID_${DEPLOY_ENV})
export AUTH_CONNECTION_NAME=$(eval echo \$AUTH_CONNECTION_NAME_${DEPLOY_ENV})
export AUTH_AUTHORIZATION_API_URI=$(eval echo \$AUTH_AUTHORIZATION_API_URI_${DEPLOY_ENV})
export JWT_USER_CLAIM_NAMESPACE=$(eval echo \$JWT_USER_CLAIM_NAMESPACE_${DEPLOY_ENV})
export AWS_CONTENT_BUCKET=$(eval echo \$AWS_CONTENT_BUCKET_${DEPLOY_ENV})
export CDN_BASE_URL=$(eval echo \$CDN_BASE_URL_${DEPLOY_ENV})
export TRANSCODER_LAMBDA_NAME=$(eval echo \$TRANSCODER_LAMBDA_NAME_${DEPLOY_ENV})
export IMAGE_PROCESSOR_LAMBDA_NAME=$(eval echo \$IMAGE_PROCESSOR_LAMBDA_NAME_${DEPLOY_ENV})
export SEGMENT_WRITE_KEY=$(eval echo \$SEGMENT_WRITE_KEY_${DEPLOY_ENV})

DATABASE_URL=$(eval echo \$DATABASE_URL_${DEPLOY_ENV})

deploy_cluster() {
    echo "deploy_cluster - start"

    echo "deploy_cluster - about to call update-service, revision: $TASK_REVISION"

    if [ "$TASK_REVISION" == "" ]; then
      echo '>>> missing env variable TASK_REVISION'
      exit 1
    fi

    if [[ $(aws ecs update-service --cluster $ECS_CLUSTER --service $ECS_SERVICE --task-definition $TASK_REVISION | \
                   $JQ '.service.taskDefinition') != $TASK_REVISION ]]; then
        echo "Error updating service."
        exit 1
    fi

    #the cluster is updated but the old task is still running... there's a better way to do this
    #stop_old_task
    ./commands/stop-ecs-task.sh "$ECS_CLUSTER" "$ECS_SERVICE"

    # wait for older revisions to disappear
    # not really necessary, but nice for demos
    for attempt in {1..30}; do
        if stale=$(aws ecs describe-services --cluster $ECS_CLUSTER --services $ECS_SERVICE | \
                       $JQ ".services[0].deployments | .[] | select(.taskDefinition != \"$TASK_REVISION\") | .taskDefinition"); then
            echo "Waiting for stale deployments:"
            echo "$stale"
            sleep 5
        else
            echo "Deployed!"
            return 0
        fi
    done
    echo "Service update took too long."
    return 1
}

### prevent relative path issues
scriptDir=${BASH_SOURCE%/*}
echo "Script Directory: $scriptDir"
cd $scriptDir

./commands/configure-aws-cli.sh "$AWS_REGION" "$ECS_CLUSTER" "$ECS_SERVICE" "$ECS_TASK_FAMILY" "$ECS_CONTAINER" "$ECR_REPOSITORY"
./commands/push-ecr-image.sh "$AWS_REGION" "$AWS_ACCOUNT_ID" "$ECR_REPOSITORY" "$CIRCLE_SHA1"
source ./commands/create-task-definition.sh "${ENVIRONMENT}" "${DATABASE_URL}"
source ./commands/register-task-definition.sh
deploy_cluster
