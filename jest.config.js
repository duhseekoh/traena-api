// configuration only for unit tests!
// integration test config is stored in ./integration-tests/jest.config.js
module.exports = {
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/integration-tests/'],
  verbose: true,
};
