const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });

module.exports = {
  cdnBaseUrl: process.env.CDN_BASE_URL, // https://d1wy52lishi7rq.cloudfront.net
};
