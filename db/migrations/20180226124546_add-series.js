const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

exports.up = function up(knex) {
  return knex.schema.createTable('Series', (table) => {
    table.increments('id').primary();
    table.integer('authorId').notNullable().references('User.id');
    table.integer('channelId').notNullable().references('Channel.id');
    table.string('title').notNullable();
    table.string('description');
    addAuditTimestamps(knex, table);
  })
  .createTable('Series_Content', (table) => {
    table.integer('seriesId').notNullable().references('Series.id');
    table.integer('contentId').notNullable().references('Content.id');
    table.primary(['seriesId', 'contentId'])
    table.integer('order');
    addAuditTimestamps(knex, table);
  });
};

exports.down = function down(knex) {
  return knex.schema
  .raw('DROP TABLE IF EXISTS "Series" CASCADE')
  .raw('DROP TABLE IF EXISTS "Series_Content" CASCADE');
};
