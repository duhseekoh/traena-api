/**
 * Add a new visibility column to Subscription.
 * Fill in existing subscriptions' visibility as entire organization.
 */
exports.up = knex =>
  knex.schema
    .alterTable('Subscription', table => {
      table.string('visibility').defaultTo('ADMINS');
      return table;
    })
    .then(() =>
      knex('Subscription').update({
        visibility: 'ORGANIZATION',
      }),
    );

/**
 * Remove visibility column from Subscription
 */
exports.down = knex =>
  knex.schema.alterTable('Subscription', table => {
    table.dropColumn('visibility');
  });
