exports.up = async (knex) => {
  await knex.schema.alterTable('Task', (table) => {
    table.dropColumn('actions');
  });
  return knex.schema.raw('UPDATE "Content" SET body = body - \'actions\'');
};

exports.down = async (knex) => {
  return knex.schema.alterTable('Task', (table) => {
    table.jsonb('actions');
  });
};
