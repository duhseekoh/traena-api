const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

exports.up = function (knex) {
  return knex.schema.createTable('Role', (table) => {
    table.increments('id').primary();
    table.string('auth0RoleId').notNullable().unique();
    table.string('name').notNullable();
    table.string('description').notNullable();
    table.boolean('isAssignable').defaultTo(true);
    addAuditTimestamps(knex, table);
  });
};

exports.down = function (knex) {
  return knex.schema.raw('DROP TABLE IF EXISTS "Role" CASCADE');
};
