// to delete channels we need to make sure there are no FK contraints
// violated. By adding cascade on delete we can be sure references to
// the channel are also deleted. In the case of unpublished content we
// do not want to delete it instead give it a null channel.
exports.up = knex => knex.schema
  .alterTable('Content', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id')
      .onDelete('SET NULL');
  })
  .alterTable('Subscription', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id')
      .onDelete('CASCADE');
  })
  .alterTable('Series_Content', (table) => {
    table.dropForeign('seriesId');
    table.foreign('seriesId')
      .references('Series.id')
      .onDelete('CASCADE');
    table.dropForeign('contentId');
    table.foreign('contentId')
      .references('Content.id')
      .onDelete('CASCADE');
  })
  .alterTable('Series', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id')
      .onDelete('CASCADE');
  });

exports.down = knex => knex.schema
  .alterTable('Content', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id');
  })
  .alterTable('Subscription', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id');
  })
  .alterTable('Series_Content', (table) => {
    table.dropForeign('seriesId');
    table.foreign('seriesId')
      .references('Series.id');
    table.dropForeign('contentId');
    table.foreign('contentId')
      .references('Content.id');
  })
  .alterTable('Series', (table) => {
    table.dropForeign('channelId');
    table.foreign('channelId')
      .references('Channel.id');
  });
