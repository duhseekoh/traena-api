const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

const createDefaultChannelQuery = `
  insert into "Channel" (name, description, "organizationId", visibility, "default")
    select o.name || ' default', 'Default ' || :channel_type || ' channel', o.id, :channel_type, true
      from "Organization" o
      where o."organizationType" = :org_type`;

const updateContentWithChannelQuery = `
  update "Content" con set ("channelId") = 
    (select id from "Channel" chan 
      where chan."organizationId" = con."authorOrganizationId")`;

// eslint-disable-next-line camelcase
const populateSubscriptionFromOrganizationProvider_OrganizationConsumerQuery = `
  insert into "Subscription" ("organizationId", "channelId")
    select o."consumerOrganizationId", c.id
      from "Channel" c
      inner join "OrganizationProvider_OrganizationConsumer" o on o."providerOrganizationId" = c."organizationId"
  `;

const rollbackPopulateSubscriptionQuery = `
  insert into "OrganizationProvider_OrganizationConsumer" ("consumerOrganizationId", "providerOrganizationId") 
    select s."organizationId", c."organizationId"
      from "Channel" c 
      inner join "Subscription" s on s."channelId" = c.id;
`;

// Create Channel and Subscription table and migrate content from ProviderOrganization_ConsumerOrganization
exports.up = async function (knex) {
  await knex.schema.createTable('Channel', (table) => {
    table.increments('id').primary();
    table.string('name').unique().notNullable();
    table.string('description').notNullable();
    table.integer('organizationId').references('Organization.id');
    table.enum('visibility', ['MARKET', 'INTERNAL', 'LIMITED']);
    table.boolean('default').defaultTo(false);
    table.timestamp('createdTimestamp').defaultTo(knex.fn.now());
    table.timestamp('modifiedTimestamp').defaultTo(knex.fn.now());
    table.unique(['organizationId', 'name']);
  })
  // unique index to ensure there is at MOST one default channel per organization
  .raw('CREATE UNIQUE INDEX "Idx_Channel_singleDefaultChannel" ON "Channel" ("organizationId") WHERE "default"')
  .raw('ALTER SEQUENCE "Channel_id_seq" RESTART WITH 1000;')
  .createTable('Subscription', (table) => {
    table.integer('organizationId').references('Organization.id');
    table.integer('channelId').references('Channel.id');
    table.primary(['organizationId', 'channelId']);
  })
  .alterTable('Content', (table) => {
    table.integer('channelId').references('Channel.id');
  });

  // create default channels for each org, 'market' visibility for trainer orgs, 'internal' visibility for client orgs
  await knex.raw(createDefaultChannelQuery, { channel_type: 'INTERNAL', org_type: 'CLIENT' });
  await knex.raw(createDefaultChannelQuery, { channel_type: 'MARKET', org_type: 'TRAINING_COMPANY' });

  // Add channel id to existing content
  await knex.raw(updateContentWithChannelQuery);
  await knex.raw(populateSubscriptionFromOrganizationProvider_OrganizationConsumerQuery);

  // kill it with fire
  return knex.schema.dropTableIfExists('OrganizationProvider_OrganizationConsumer');
};

exports.down = function (knex) {
  return knex.schema
  .createTable('OrganizationProvider_OrganizationConsumer', (table) => {
    table.integer('providerOrganizationId').notNullable().references('Organization.id');
    table.integer('consumerOrganizationId').notNullable().references('Organization.id');

    addAuditTimestamps(knex, table);

    table.index(['providerOrganizationId', 'consumerOrganizationId'],
      'Idx_OrganizationProvider_OrganizationConsumer_providerOrganizationId_consumerOrganizationId');
  })
  .alterTable('Content', (table) => {
    table.dropColumn('channelId');
  })
  .raw(rollbackPopulateSubscriptionQuery)
  .raw('DROP TABLE IF EXISTS "Channel" CASCADE')
  .raw('DROP TABLE IF EXISTS "Subscription", CASCADE');
};
