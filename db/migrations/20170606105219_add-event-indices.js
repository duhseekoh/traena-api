exports.up = function (knex) {
  return knex.schema.alterTable('Event', (table) => {
    table.index(['userId', 'createdTimestamp', 'type'], 'Idx_Event_userId_createdTimestamp_type');
  });
};

exports.down = function (knex) {
  return knex.schema.alterTable('Event', (table) => {
    table.dropIndex(['userId', 'createdTimestamp', 'type'], 'Idx_Event_userId_createdTimestamp_type');
  });
};
