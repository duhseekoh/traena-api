const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

exports.up = function (knex) {
  return knex.schema.createTable('TranscodeJob', (table) => {
    table.increments('id').primary();
    table.string('jobId').notNullable().unique();
    table.integer('contentId').references('Content.id').notNullable();
    table.string('status').notNullable();
    table.timestamp('completedTimestamp');
    addAuditTimestamps(knex, table);
  });
};

exports.down = function (knex) {
  return knex.schema.raw('DROP TABLE IF EXISTS "TranscodeJob" CASCADE');
};
