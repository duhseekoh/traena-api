
exports.up = async (knex) => {
  await knex.schema.raw('UPDATE "Comment" set "deleted" = false WHERE "deleted" IS NULL');
  return knex.schema.alterTable('Comment', (table) => {
    table.boolean('deleted').notNullable().defaultTo(false).alter();
  });
};

exports.down = (knex) => {
  return knex.schema.alterTable('Comment', (table) => {
    table.boolean('deleted').nullable().alter();
  });
};
