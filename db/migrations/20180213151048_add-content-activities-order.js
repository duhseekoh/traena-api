exports.up = function up(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.jsonb('activitiesOrder');
    return table;
  }).then(() => knex('Content').update({
    activitiesOrder: [],
  }));
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.dropColumn('activitiesOrder');
  });
};
