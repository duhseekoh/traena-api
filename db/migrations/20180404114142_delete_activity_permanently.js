
exports.up = async knex => knex.transaction((trx) => {
  const subquery = knex.select('id').from('Activity').where('deleted', true);
  return knex('ActivityResponse')
    .whereIn('activityId', subquery)
    .del()
    .transacting(trx)
    .then(() => knex('Activity')
      .where('deleted', true)
      .del()
      .transacting(trx))
    .then(trx.commit)
    .then(() => knex.schema.alterTable('Activity', (table) => {
      table.dropColumn('deleted');
    }))
    .catch(trx.rollback);
});

exports.down = knex => knex.schema.table('Activity', (table) => {
  table.boolean('deleted').defaultTo('false');
});
