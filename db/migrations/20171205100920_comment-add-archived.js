
exports.up = (knex) => {
  return knex.schema.table('Comment', (table) => {
    table.boolean('deleted');
  });
};

exports.down = (knex) => {
  return knex.schema.table('Comment', (table) => {
    table.dropColumn('deleted');
  });
};
