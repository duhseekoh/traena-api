exports.up = function up(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.jsonb('activitiesOrder').defaultTo(JSON.stringify([])).alter();
    return table;
  }).then(() => knex('Content').update({
    activitiesOrder: JSON.stringify([]),
  }));
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.jsonb('activitiesOrder').defaultTo(null).alter();
  }).then(() => knex('Content').update({
    activitiesOrder: [],
  }));
};
