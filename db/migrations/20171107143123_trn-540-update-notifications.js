
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('Notification', (table) => {
    table.string('notificationType');
    return table;
  }).then(() => knex('Notification').update({
    notificationType: 'TAG_IN_COMMENT',
  }));
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('Notification', (table) => {
    table.dropColumn('notificationType');
  });
};
