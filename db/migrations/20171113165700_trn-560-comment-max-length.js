
exports.up = function up(knex) {
  return knex.schema.alterTable('Comment', t =>
    t.text('text').alter());
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Comment', t =>
    t.string('text', 255).alter());
};
