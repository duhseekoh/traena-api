
exports.up = function up(knex) {
  return knex.schema.raw('ALTER TABLE "Channel" DROP CONSTRAINT IF EXISTS channel_name_unique')
    .alterTable('Channel', (table) => {
      table.unique(['name', 'organizationId']);
      return table;
    })
    .then(() => knex('Channel').update({
      name: 'General',
    }));
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Channel', (table) => {
    table.dropUnique(['name', 'organizationId']);
  });
};
