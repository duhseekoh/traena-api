exports.up = function up(knex) {
  return knex.schema.alterTable('Activity', (table) => {
    table.dropColumn('order');
  });
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Activity', (table) => {
    table.integer('order');
  });
};
