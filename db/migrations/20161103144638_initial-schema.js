require('babel-register'); // eslint-disable-line import/no-extraneous-dependencies

const constants = require('../../src/domain/fieldConstants');

const DEFAULT_CONTENT_ITEM_STATUS = constants.CONTENT_ITEM_STATUSES.DRAFT;
const DEFAULT_NOTIFICATION_STATUS = constants.NOTIFICATION_STATUSES.READY_TO_SEND;
const DEFAULT_TASK_STATUS = constants.TRAENA_TASK_STATUSES.INCOMPLETE;

const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

exports.up = function up(knex) {
  return knex.schema.createTable('Organization', (table) => {
    table.increments('id').primary();
    table.string('name').unique().notNullable();
    table.string('organizationType').notNullable();
    table.boolean('isDisabled');
    table.string('auth0Connection');
    table.string('auth0OrganizationName');
    table.string('auth0OrganizationId').notNullable();
    table.jsonb('authDomains');
    table.jsonb('commonSearchTerms');

    addAuditTimestamps(knex, table);
  })
  .raw('ALTER SEQUENCE "Organization_id_seq" RESTART WITH 1000;')
  .createTable('OrganizationProvider_OrganizationConsumer', (table) => {
    table.integer('providerOrganizationId').notNullable().references('Organization.id');
    table.integer('consumerOrganizationId').notNullable().references('Organization.id');

    addAuditTimestamps(knex, table);

    table.index(['providerOrganizationId', 'consumerOrganizationId'],
      'Idx_OrganizationProvider_OrganizationConsumer_providerOrganizationId_consumerOrganizationId');
  })
  .createTable('User', (table) => {
    table.increments('id').primary();
    table.string('email').unique().notNullable();
    table.integer('organizationId').notNullable().references('Organization.id');
    table.string('firstName').notNullable();
    table.string('lastName').notNullable();
    table.string('profileImageURI');
    table.boolean('isDisabled');
    table.string('position');
    table.jsonb('tags');

    addAuditTimestamps(knex, table);

    table.index(['organizationId'], 'Idx_User_organizationId');
  })
  .raw('ALTER SEQUENCE "User_id_seq" RESTART WITH 1000;')
  .createTable('PushDevice', (table) => {
    table.increments('id').primary();
    table.integer('userId').references('User.id').notNullable();
    table.string('deviceToken').unique().notNullable();
    table.string('endpointArn').unique().notNullable();

    addAuditTimestamps(knex, table);

    table.index(['userId'], 'Idx_PushDevice_userId');
  })
  .raw('ALTER SEQUENCE "PushDevice_id_seq" RESTART WITH 1000;')
  .createTable('Content', (table) => {
    table.increments('id').primary();
    table.integer('authorOrganizationId').notNullable().references('Organization.id');
    table.integer('authorId').notNullable().references('User.id');
    table.jsonb('body').notNullable(); // json representing each piece of content, different format per content type
    table.string('status').notNullable().defaultTo(DEFAULT_CONTENT_ITEM_STATUS);

    addAuditTimestamps(knex, table);

    table.index('authorOrganizationId', 'Idx_PushDevice_authorOrganizationId');
    table.index(['authorId'], 'Idx_Content_authorId');
  })
  .raw('ALTER SEQUENCE "Content_id_seq" RESTART WITH 1000;')
  .createTable('Like', (table) => {
    table.integer('userId').notNullable().references('User.id');
    table.integer('contentId').notNullable().references('Content.id');
    // storing organizationId to limit complexity of queries
    table.integer('organizationId').notNullable().references('Organization.id');

    addAuditTimestamps(knex, table);

    table.primary(['userId', 'contentId']); // user can only like content item once
    table.index(['userId'], 'Idx_Like_userId');
    table.index(['contentId'], 'Idx_Like_contentId');
    table.index(['organizationId'], 'Idx_Like_organizationId');
  })
  .createTable('Comment', (table) => {
    table.increments('id').primary();
    table.string('text').notNullable();
    table.integer('authorId').notNullable().references('User.id');
    table.integer('contentId').notNullable().references('Content.id');
    // storing organizationId to limit complexity of queries
    table.integer('organizationId').notNullable().references('Organization.id');

    addAuditTimestamps(knex, table);

    table.index(['authorId'], 'Idx_Comment_authorId');
    table.index(['contentId'], 'Idx_Comment_contentId');
    table.index(['organizationId'], 'Idx_Comment_organizationId');
  })
  .raw('ALTER SEQUENCE "Comment_id_seq" RESTART WITH 1000;')
  .createTable('Comment_TaggedUser', (table) => {
    table.integer('userId').notNullable().references('User.id');
    table.integer('commentId').notNullable().references('Comment.id');

    addAuditTimestamps(knex, table);

    table.primary(['userId', 'commentId']);
  })
  .createTable('Notification', (table) => {
    table.increments('id').primary();
    table.integer('commentId').references('Comment.id');
    table.integer('contentId').references('Content.id');
    table.integer('fromUserId').references('User.id');
    table.integer('toUserId').notNullable().references('User.id');
    table.string('status').notNullable().defaultTo(DEFAULT_NOTIFICATION_STATUS);

    addAuditTimestamps(knex, table);

    table.index(['commentId'], 'Idx_Notification_commentId');
    table.index(['contentId'], 'Idx_Notification_contentId');
    table.index(['fromUserId'], 'Idx_Notification_fromUserId');
    table.index(['toUserId'], 'Idx_Notification_toUserId');
  })
  .raw('ALTER SEQUENCE "Notification_id_seq" RESTART WITH 1000;')
  .createTable('Tag', (table) => {
    table.integer('contentId').notNullable().references('Content.id');
    table.string('text').notNullable();

    addAuditTimestamps(knex, table);

    table.index(['contentId'], 'Idx_Tag_contentId');
    table.index(['contentId', 'text'], 'Idx_Tag_contentId_text');
  })
  .createTable('Task', (table) => {
    table.increments('id').primary();
    table.integer('userId').notNullable().references('User.id');
    table.integer('contentId').notNullable().references('Content.id');
    table.jsonb('actions');
    table.integer('rating');
    table.string('status').notNullable().defaultTo(DEFAULT_TASK_STATUS);
    table.timestamp('completedTimestamp');

    addAuditTimestamps(knex, table);

    table.index(['userId'], 'Idx_Task_userId');
    table.index(['contentId'], 'Idx_Task_contentId');
  })
  .raw('ALTER SEQUENCE "Task_id_seq" RESTART WITH 1000;')
  .createTable('Event', (table) => {
    table.increments('id').primary();
    table.string('type').notNullable();
    table.string('source').notNullable();
    table.integer('userId').references('User.id').nullable();
    table.jsonb('data').notNullable();

    addAuditTimestamps(knex, table);
  });
};

exports.down = function down(knex) {
  return knex.schema
  .raw('DROP TABLE IF EXISTS "PushDevice" CASCADE')
  .raw('DROP TABLE IF EXISTS "Tag" CASCADE')
  .raw('DROP TABLE IF EXISTS "Like" CASCADE')
  .raw('DROP TABLE IF EXISTS "Comment" CASCADE')
  .raw('DROP TABLE IF EXISTS "Task" CASCADE')
  .raw('DROP TABLE IF EXISTS "OrganizationProvider_OrganizationConsumer" CASCADE')
  .raw('DROP TABLE IF EXISTS "Notification" CASCADE')
  .raw('DROP TABLE IF EXISTS "Category" CASCADE')
  .raw('DROP TABLE IF EXISTS "Content" CASCADE')
  .raw('DROP TABLE IF EXISTS "User" CASCADE')
  .raw('DROP TABLE IF EXISTS "Organization" CASCADE')
  .raw('DROP TABLE IF EXISTS "Comment_TaggedUser" CASCADE')
  .raw('DROP TABLE IF EXISTS "Event" CASCADE')
  ;
};
