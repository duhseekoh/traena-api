/**
 * Adds a new table to track users subscribed to channels. This is used when
 * an org is subscribed to a channel and specifying individual access to that
 * org.
 */
exports.up = knex =>
  knex.schema
    .createTable('UserSubscription', (table) => {
      table.integer('userId')
        .notNullable()
        .references('User.id')
        .onDelete('CASCADE'); // User deleted -> rows here deleted
      table.integer('channelId')
        .notNullable()
        .references('Channel.id')
        .onDelete('CASCADE'); // Channel deleted -> rows here deleted
      table.primary(['userId', 'channelId']);
    });

/**
 * Remove UserSubscription table
 */
exports.down = knex =>
  knex.schema
    .raw('DROP TABLE IF EXISTS "UserSubscription" CASCADE');
