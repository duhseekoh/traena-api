exports.up = function (knex) {
  return knex.schema.alterTable('Event', (table) => {
    table.jsonb('data').alter().nullable();
  });
};

exports.down = function (knex) {
  return knex.schema
  .raw(`update "Event" set "data" = '{}' where "data" is null`)
  .alterTable('Event', (table) => {
    table.jsonb('data').alter().notNullable();
  });
};
