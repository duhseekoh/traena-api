exports.up = function up(knex) {
  return knex.schema.createTable('Bookmark', (table) => {
    table.integer('contentId').references('Content.id');
    table.integer('userId').references('User.id');
    table.timestamp('createdTimestamp').defaultTo(knex.fn.now());
    table.primary(['contentId', 'userId']);
  })

};

exports.down = function down(knex) {
  return knex.schema
  .raw('DROP TABLE IF EXISTS "Bookmark" CASCADE')
};
