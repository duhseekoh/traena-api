const addAuditTimestamps = require('../migrationUtils').addAuditTimestamps;

exports.up = function up(knex) {
  return knex.schema.createTable('Activity', (table) => {
    table.increments('id').primary();
    table.integer('contentId').notNullable().references('Content.id');
    table.integer('version').notNullable().defaultTo(1);
    table.jsonb('body').notNullable();
    table.string('type').notNullable();
    table.integer('order').notNullable();
    table.boolean('deleted');

    table.index(['contentId'], 'Idx_Activity_contentId');

    addAuditTimestamps(knex, table);
  })
  .createTable('ActivityResponse', (table) => {
    table.increments('id').primary();
    table.integer('activityId').notNullable().references('Activity.id');
    table.integer('activityVersion').notNullable();
    table.integer('userId').notNullable().references('User.id');
    table.jsonb('body').notNullable();

    table.index(['userId'], 'Idx_ActivityResponse_userId');
    table.index(['activityId'], 'Idx_ActivityResponse_activityId');

    addAuditTimestamps(knex, table);
  });
};

exports.down = function down(knex) {
  return knex.schema
  .raw('DROP TABLE IF EXISTS "ActivityResponse" CASCADE')
  .raw('DROP TABLE IF EXISTS "Activity" CASCADE');
};
