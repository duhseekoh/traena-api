/**
 * Adding new fields to a transcode job and most importantly removing
 * the tie to a specific contentId. This allows us to transcode videos
 * before saving that video uri to a content item.
 * Clears out all existing jobs as well.
 */
exports.up = knex =>
  knex('TranscodeJob')
    .del()
    .then(() =>
      knex.schema.alterTable('TranscodeJob', (table) => {
        table.string('videoUri');
        table.string('thumbnailUri');
        table
          .integer('userId')
          .notNullable()
          .references('User.id');
        table.dropColumn('contentId');
        return table;
      }),
    );

/**
 * Clears out all existing jobs because the newer data just doesn't translate
 * into the older model (e.g. no contentId).
 * But, it does reset it to its previous schema.
 */
exports.down = knex =>
  knex('TranscodeJob')
    .del()
    .then(() =>
      knex.schema.alterTable('TranscodeJob', (table) => {
        table.dropColumn('userId');
        table.dropColumn('videoUri');
        table.dropColumn('thumbnailUri');
        table
          .integer('contentId')
          .references('Content.id')
          .notNullable();
        return table;
      }),
    );
