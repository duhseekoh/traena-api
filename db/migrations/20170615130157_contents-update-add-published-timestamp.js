
exports.up = function up(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.timestamp('publishedTimestamp');
    return table;
  }).then(() => knex('Content').update({
    // text Content was being added as DRAFT status and never set to PUBLISHED
    status: 'PUBLISHED',
    publishedTimestamp: knex.raw('"createdTimestamp"'),
  }));
};

exports.down = function down(knex) {
  return knex.schema.alterTable('Content', (table) => {
    table.dropColumn('publishedTimestamp');
  });
};
