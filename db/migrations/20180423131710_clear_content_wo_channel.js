/*
 * We cannot have DRAFTs without related channels
 * so instead set these content items to ARCHIVED
 */
exports.up = knex =>
  knex('Content')
    .update({ status: 'ARCHIVED' })
    .where({ channelId: null });

/*
 * No way to recover previous statuss, do nothing.
 */

exports.down = () => Promise.resolve();
