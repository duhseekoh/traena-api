/**
 * We haven't displayed descriptions anywhere in the UI up until this release.
 * There's default ugly descriptions that we're clearing. Admins can create
 * their own descriptions in the UI now.
 */
exports.up = knex =>
  knex('Channel').update({
    description: '',
  });

/**
 * No way to recover old descriptions, do nothing.
 */
exports.down = () => Promise.resolve();
