const organization = require('../../organization');

const user = {
  id: 27,
  email: 'dev+ahco2@traena.io',
  organizationId: organization.id,
  firstName: 'Consumer2',
  lastName: 'Content',
  profileImageURI: null,
  position: 'Content Consumer',
  tags: JSON.stringify(['dummy', 'test', 'consumer']),
};

module.exports = user;
