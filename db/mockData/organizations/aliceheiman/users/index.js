const aliceUser = require('./alice/user');
const aliceContent = require('./alice/content');

const ahco = require('./ahco/user');
const ahco2 = require('./ahco2/user');
const ahcr = require('./ahcr/user');
const ahcr2 = require('./ahcr2/user');

const aliceData = {
  user: aliceUser,
  content: aliceContent,
};

const allContent = [...aliceContent.allAsArray];
const allTags = [...aliceContent.tags];

module.exports = {
  aliceData,
  allUsers: [aliceUser, ahco, ahco2, ahcr, ahcr2],
  allContent,
  allTags,
};
