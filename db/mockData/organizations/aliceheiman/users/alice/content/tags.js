const dailyActions = require('./mockDailyActions');
const trainingVideos = require('./mockTrainings');

module.exports = [
  { contentId: dailyActions.one.id, text: 'Client Engagement' },
  { contentId: dailyActions.one.id, text: 'Relationship Building' },

  { contentId: trainingVideos.one.id, text: 'Meeting Prep' },
  { contentId: trainingVideos.one.id, text: 'Sales Call' },

  { contentId: trainingVideos.two.id, text: 'Communication' },
  { contentId: trainingVideos.two.id, text: 'Conflict Resolution' },
  { contentId: trainingVideos.two.id, text: 'Solutioning' },

  { contentId: trainingVideos.three.id, text: 'Decision Making' },
  { contentId: trainingVideos.three.id, text: 'Customer Relationships' },

  { contentId: trainingVideos.four.id, text: 'Analyze' },
  { contentId: trainingVideos.four.id, text: 'Management' },
  { contentId: trainingVideos.four.id, text: 'Metrics' },

  { contentId: trainingVideos.five.id, text: 'Sales' },
  { contentId: trainingVideos.five.id, text: 'Positioning' },
  { contentId: trainingVideos.five.id, text: 'Marketing' },

  { contentId: trainingVideos.six.id, text: 'Goals' },
  { contentId: trainingVideos.six.id, text: 'Strategy' },

  { contentId: trainingVideos.seven.id, text: 'Executive' },
  { contentId: trainingVideos.seven.id, text: 'Thought Leadership' },
  { contentId: trainingVideos.seven.id, text: 'Positioning' },

  // { contentId: trainingVideos.eight.id, body: 'Social' },
  // { contentId: trainingVideos.eight.id, body: 'New Techniques' },
  // { contentId: trainingVideos.eight.id, body: 'Power of People' },
  //
  // { contentId: trainingVideos.nine.id, body: 'Closing Plan' },
  // { contentId: trainingVideos.nine.id, body: 'Strategy' },
  //
  // { contentId: trainingVideos.ten.id, body: 'Cold Calls' },
  // { contentId: trainingVideos.ten.id, body: 'Positioning' },
  // { contentId: trainingVideos.ten.id, body: 'Marketing' },
  //
  // { contentId: trainingVideos.eleven.id, body: 'Leadership' },
  // { contentId: trainingVideos.eleven.id, body: 'Management' },
  //
  // { contentId: trainingVideos.twelve.id, body: 'Customer Relationships' },
  // { contentId: trainingVideos.twelve.id, body: 'Forward Thinking' },
  // { contentId: trainingVideos.twelve.id, body: 'Value Time' },
  //
  // { contentId: trainingVideos.thirteen.id, body: 'Behavioral' },
  // { contentId: trainingVideos.thirteen.id, body: 'Tips' },
  // { contentId: trainingVideos.thirteen.id, body: 'Mindset' },
  //
  // { contentId: trainingVideos.fifteen.id, body: 'Planning' },
  // { contentId: trainingVideos.fifteen.id, body: 'Strategy' },
  // { contentId: trainingVideos.fifteen.id, body: 'Execution' },
  //
  // { contentId: trainingVideos.sixteen.id, body: 'Creativity' },
  // { contentId: trainingVideos.sixteen.id, body: 'Socialization' },
  // { contentId: trainingVideos.sixteen.id, body: 'Thought Leadership' },
  //
  // { contentId: trainingVideos.seventeen.id, body: 'Sales' },
  // { contentId: trainingVideos.seventeen.id, body: 'Automation' },
  // { contentId: trainingVideos.seventeen.id, body: 'Positioning' },
  //
  // { contentId: trainingVideos.eighteen.id, body: 'Networking' },
  // { contentId: trainingVideos.eighteen.id, body: 'Strategy' },

];
