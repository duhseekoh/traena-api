// Mock Daily Action ids start at 200
const user = require('../user');

const aliceActionOne = {
  id: 200,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Un-stall your sales',
    text: 'Sales reps often get stuck and can’t figure out why they can’t get a sale to move forward. Why do sales stall? Why do prospects go silent? There are many reasons.  It’s you job to keep them engaged. The number one way to do that is to understand their world. What comprises their day besides your deal? Once you understand their pace, their pressures and their worries, you can determine how to keep the sale moving forward.',
    actions: [
      { id: 'some-uuid-da-200-1',
        text: 'Build relationships.' },
      { id: 'some-uuid-da-200-2',
        text: 'Ask questions about their priorities and where this purchase fits.' },
      { id: 'some-uuid-da-200-3',
        text: 'Ask what pressures they are under that might stall the process.' },
      { id: 'some-uuid-da-200-4',
        text: 'Determine together the best way to move forward.' },
    ],
  }),
};

module.exports = {
  one: aliceActionOne,
};
