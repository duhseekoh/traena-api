const user = require('../user');
const { createVideoCDNPath, createVideoPreviewCDNPath } = require('../../../../../../util/contentPathCreator');

// Mock training ids start at 1
const aliceTrainingOne = {
  id: 1,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Before you Make a Sales Call',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 1),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 1),
    },
    actions: [
      { id: 'some-uuid-t-1-1',
        text: 'Write down everything you know that’s relevant to this sales opportunity.' },
      { id: 'some-uuid-t-1-2',
        text: 'Determine what needs to happen before the deal can close.' },
      { id: 'some-uuid-t-1-3',
        text: 'Identify logical next steps to determine at the end of the meeting.' },
    ],
  }),
};

const aliceTrainingTwo = {
  id: 2,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Objections',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 2),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 2),
    },
    actions: [
      { id: 'some-uuid-t-2-1',
        text: 'Listen – take the time to fully understand the customer’s concern. Let them talk.' },
      { id: 'some-uuid-t-2-2',
        text: 'Validate – make a statement that shows you heard them.' },
      { id: 'some-uuid-t-2-3',
        text: 'Ask – ask questions to confirm your understanding.' },
      { id: 'some-uuid-t-2-4',
        text: 'Solve – Provide a solution.' },
      { id: 'some-uuid-t-2-5',
        text: 'Confirm – Confirm that you have provided a solution that is acceptable to them.' },
    ],
  }),
};

const aliceTrainingThree = {
  id: 3,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Help Your Customers Make Great Decisions',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 3),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 3),
    },
    actions: [
      { id: 'some-uuid-t-27-1',
        text: 'Listen – take the time to fully understand the customer’s concern. Let them talk.' },
      { id: 'some-uuid-t-27-2',
        text: 'Validate – make a statement that shows you heard them.' },
      { id: 'some-uuid-t-27-3',
        text: 'Ask – ask questions to confirm your understanding.' },
      { id: 'some-uuid-t-27-4',
        text: 'Solve – Provide a solution.' },
      { id: 'some-uuid-t-27-5',
        text: 'Confirm – Confirm that you have provided a solution that is acceptable to them.' },
    ],
  }),
};

const aliceTrainingFour = {
  id: 4,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Help Your Salespeople Analyze Their Activity',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 4),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 4),
    },
    actions: [
      { id: 'some-uuid-t-255-1',
        text: 'Write down the key takeaway from this lesson.' },
      { id: 'some-uuid-t-255-2',
        text: 'Highlight an upcoming time to utilize this lesson' },
      { id: 'some-uuid-t-255-3',
        text: 'share with a person you think would appreciate this video' },
    ],
  }),
};

const aliceTrainingFive = {
  id: 5,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'End the War Between Sales and Marketing',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 5),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 5),
    },
    actions: [
      { id: 'some-uuid-t-2-1',
        text: 'Listen – take the time to fully understand the customer’s concern. Let them talk.' },
      { id: 'some-uuid-t-2-2',
        text: 'Validate – make a statement that shows you heard them.' },
      { id: 'some-uuid-t-2-3',
        text: 'Ask – ask questions to confirm your understanding.' },
      { id: 'some-uuid-t-2-4',
        text: 'Solve – Provide a solution.' },
      { id: 'some-uuid-t-2-5',
        text: 'Confirm – Confirm that you have provided a solution that is acceptable to them.' },
    ],
  }),
};

const aliceTrainingSix = {
  id: 6,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Get in the Right Mindset for a Sales Call.',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 6),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 6),
    },
    actions: [
      { id: 'some-uuid-t-8-1',
        text: 'Write the key takeaway from this lesson.' },
      { id: 'some-uuid-t-8-2',
        text: 'Highlight an upcoming time to utilize this lesson.' },
      { id: 'some-uuid-t-8-3',
        text: 'Share with a person you think would appreciate this video.' },
    ],
  }),
};

const aliceTrainingSeven = {
  id: 7,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Think Like an Executive',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 7),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 7),
    },
    actions: [
      { id: 'some-uuid-t-9-191',
        text: 'Write the key takeaway from this lesson' },
      { id: 'some-uuid-t-9-292',
        text: 'Highlight an upcoming time to utilize this lesson.' },
      { id: 'some-uuid-t-9-392',
        text: 'Share with a person you think would appreciate this video' },
    ],
  }),
};

module.exports = {
  one: aliceTrainingOne,
  two: aliceTrainingTwo,
  three: aliceTrainingThree,
  four: aliceTrainingFour,
  five: aliceTrainingFive,
  six: aliceTrainingSix,
  seven: aliceTrainingSeven,
};
