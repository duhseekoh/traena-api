const organization = require('../../organization');
const { createUserPicCDNPath } = require('../../../../../util/contentPathCreator');

const user = {
  id: 50,
  email: 'alice@10host.top',
  organizationId: organization.id,
  firstName: 'Alice',
  lastName: 'Heiman',
  profileImageURI: createUserPicCDNPath(organization.id, 50),
};

module.exports = user;
