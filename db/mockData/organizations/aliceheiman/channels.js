module.exports = [{
  id: 10,
  name: 'Alice Heiman Public Channel',
  organizationId: 10,
  description: 'Default public channel for Alice Heiman',
  visibility: 'MARKET',
  default: true,
}];
