module.exports = {
  id: 10,
  name: 'Alice Heiman Training',
  organizationType: 'TRAINING_COMPANY',
  authDomains: JSON.stringify(['10host.top']),
  auth0OrganizationName: 'trainers_aliceheiman',
  auth0OrganizationId: '70827e6b-8c85-4af3-8e1f-46b345cf5bd1',
  auth0Connection: 'traena-user-database',
};
