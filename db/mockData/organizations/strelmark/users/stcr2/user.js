const organization = require('../../organization');

const user = {
  id: 25,
  email: 'dev+stcr2@traena.io',
  organizationId: organization.id,
  firstName: 'Creator2',
  lastName: 'Content',
  profileImageURI: null,
  position: 'Content Creator',
  tags: JSON.stringify(['dummy', 'test', 'creator']),
};

module.exports = user;
