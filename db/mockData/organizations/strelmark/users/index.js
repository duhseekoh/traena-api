const hilaryUser = require('./hilary/user');
const hilaryContent = require('./hilary/content');

const stco = require('./stco/user');
const stco2 = require('./stco2/user');
const stcr = require('./stcr/user');
const stcr2 = require('./stcr2/user');

const hilaryData = {
  user: hilaryUser,
  content: hilaryContent,
};

const allContent = [...hilaryContent.allAsArray];
const allTags = [...hilaryContent.tags];

module.exports = {
  hilaryData,
  allUsers: [hilaryUser, stco, stco2, stcr, stcr2],
  allContent,
  allTags,
};
