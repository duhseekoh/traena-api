const organization = require('../../organization');
const { createUserPicCDNPath } = require('../../../../../util/contentPathCreator');

const user = {
  id: 70,
  email: 'hfordwich@7rent.top',
  organizationId: organization.id,
  firstName: 'Hilary',
  lastName: 'Fordwich',
  profileImageURI: createUserPicCDNPath(organization.id, 70),
};

module.exports = user;
