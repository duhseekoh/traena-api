const user = require('../user');
const { createVideoCDNPath, createVideoPreviewCDNPath } = require('../../../../../../util/contentPathCreator');

// Mock training ids start at 1
const hilaryTrainingOne = {
  id: 15,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Generating "Want"',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 15),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 15),
    },
    actions: [
      {
        id: 'some-uuid-t-3-1',
        text: 'Write down the traits of people that you like.'
      },
      {
        id: 'some-uuid-t-3-2',
        text: 'Use the clients name four times in communication.'
      },
    ],
  }),
};

module.exports = {
  one: hilaryTrainingOne,
};
