const dailyActions = require('./mockDailyActions');
const trainingVideos = require('./mockTrainings');

module.exports = [
  { contentId: dailyActions.one.id, text: 'Productivity' },
  { contentId: dailyActions.one.id, text: 'Outcome Focused' },

  { contentId: trainingVideos.one.id, text: 'Preception' },
  { contentId: trainingVideos.one.id, text: 'Communication' },
  { contentId: trainingVideos.one.id, text: 'Networking' },
];
