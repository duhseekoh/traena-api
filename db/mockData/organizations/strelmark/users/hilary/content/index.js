const _ = require('lodash');
const dailyActions = require('./mockDailyActions');
const trainingVideos = require('./mockTrainings');
const tags = require('./tags');

module.exports = {
  dailyActions,
  trainingVideos,
  allAsArray: [..._.values(dailyActions), ..._.values(trainingVideos)],
  tags,
};
