// Mock Daily Action ids start at 200
const user = require('../user');

const hilaryActionOne = {
  id: 202,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Human Engineering is a thing we apparently say now',
    text: 'Science of human engineering is what drives sales - only 15% of a sale dependent on technical competence. Apply the pyramid of human engineering to help drive your sales.',
    actions: [
      { id: 'some-uuid-da-202-1',
        text: 'Understand the clients want.' },
      { id: 'some-uuid-da-202-2',
        text: 'Outline items to generate likability, trust, and respect.' },
      { id: 'some-uuid-da-202-3',
        text: 'Practice the 5 C’s: Concern, Candor, Communication, Competence, Connection.' },
    ],
  }),
};

module.exports = {
  one: hilaryActionOne,
};
