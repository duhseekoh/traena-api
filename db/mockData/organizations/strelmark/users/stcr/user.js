const organization = require('../../organization');

const user = {
  id: 24,
  email: 'dev+stcr@traena.io',
  organizationId: organization.id,
  firstName: 'Creator1',
  lastName: 'Content',
  profileImageURI: null,
  position: 'Content Creator',
  tags: JSON.stringify(['dummy', 'test', 'creator']),
};

module.exports = user;
