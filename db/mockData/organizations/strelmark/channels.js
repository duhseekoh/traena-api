module.exports = [{
  id: 12,
  name: 'Strelmark Public Channel',
  organizationId: 12,
  description: 'Default public channel for Strelmark',
  visibility: 'MARKET',
  default: true,
}];
