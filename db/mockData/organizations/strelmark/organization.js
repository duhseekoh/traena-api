module.exports = {
  id: 12,
  name: 'Strelmark',
  organizationType: 'TRAINING_COMPANY',
  authDomains: JSON.stringify(['7rent.top']),
  auth0OrganizationName: 'trainers_strelmark',
  auth0OrganizationId: '12ec57b1-bcf4-4d9f-ba68-132734f5a4cc',
  auth0Connection: 'traena-user-database',
};
