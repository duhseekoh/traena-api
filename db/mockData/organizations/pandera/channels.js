module.exports = [{
  id: 2,
  name: 'Pandera Systems Internal Channel',
  organizationId: 2,
  description: 'Default internal channel for Pandera Systems',
  visibility: 'INTERNAL',
  default: true,
}];
