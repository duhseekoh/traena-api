const organization = require('../../organization');
const { createUserPicCDNPath } = require('../../../../../util/contentPathCreator');

const user = {
  id: 63,
  email: 'ryan.redmann@panderasystems.com',
  organizationId: organization.id,
  firstName: 'Ryan',
  lastName: 'Redmann',
  profileImageURI: createUserPicCDNPath(organization.id, 63),
};

module.exports = user;
