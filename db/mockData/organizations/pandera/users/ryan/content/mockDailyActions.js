// Mock Daily Action ids start at 200
const user = require('../user');

const ryanActionOne = {
  id: 208,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Pandera Labs',
    text: 'Pandera Labs builds custom software products / solutions for our clients; they also are an incubator for Pandera’s own SaaS products',
    actions: [
      { id: 'some-uuid-da-203-71',
        text: 'Did you know about Pandera Labs?' },
      { id: 'some-uuid-da-203-72',
        text: 'Talk to Ryan about Pandera Labs capabilities' },
      { id: 'some-uuid-da-203-74',
        text: 'Articulate what pandera labs does.' },
    ],
  }),
};

const ryanActionTwo = {
  id: 209,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Pandera Labs Experience',
    text: 'Experience is a huge strength for Pandera Labs. Our product owners / engineers / designers have built some of the biggest, baddest platforms in the world. They have built hundreds of applications that have touched over 100M people.',
    actions: [
      { id: 'some-uuid-da-206-11',
        text: 'Talk to some people at Pandera Labs to see what they’ve worked on.' },
      { id: 'some-uuid-da-203-32',
        text: 'Use that in your next call.' },
    ],
  }),
};

const ryanActionThree = {
  id: 210,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Experts Say',
    text: 'Experts have stated that the average lifespan of companies listed on the S&P dropped from 67 years in the 1920s to 15 years today. It is estimated that 75% of today\'s S&P listed firms will be replaced by new companies or absorbed by faster growing ones by the year 2027. Industrial churn is accelerating and institutional companies are disappearing, but by whom? The answer: smaller, more innovative companies who rely on more advanced technology and analytics to scale their vision as opposed to human decision-makers.',
    actions: [
      { id: 'some-uuid-da-203-71',
        text: 'Which of your customers are experiencing this trend?' },
      { id: 'some-uuid-da-203-72',
        text: 'Talk to a customer about this trend.' },
      { id: 'some-uuid-da-203-74',
        text: 'Use this trend in your next sales call to create urgency.' },
    ],
  }),
};

module.exports = {
  one: ryanActionOne,
  two: ryanActionTwo,
  three: ryanActionThree,
};
