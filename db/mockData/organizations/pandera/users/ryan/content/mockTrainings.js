const user = require('../user');
const { createVideoCDNPath, createVideoPreviewCDNPath } = require('../../../../../../util/contentPathCreator');

// Mock training ids start at 1
const ryanTrainingOne = {
  id: 10,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'What is Pandera Labs?',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 10),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 10),
    },
    actions: [
      {
        id: 'some-uuid-t-1-23',
        text: 'In your own words, what is Pandera Labs?'
      },
      {
        id: 'some-uuid-t-1-24',
        text: 'Schedule an introductory conversation with Ryan'
      },
    ],
  }),
};

const ryanTrainingTwo = {
  id: 11,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Smarter apps, built smarter.',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 11),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 11),
    },
    actions: [
      {
        id: 'some-uuid-t-1-25',
        text: 'What is a simple name for apps that use analytics?'
      },
    ],
  }),
};

module.exports = {
  one: ryanTrainingOne,
  two: ryanTrainingTwo,
};
