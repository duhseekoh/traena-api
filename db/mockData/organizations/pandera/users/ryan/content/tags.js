const dailyActions = require('./mockDailyActions');
const trainingVideos = require('./mockTrainings');

module.exports = [
  { contentId: dailyActions.one.id, text: 'Pandera Labs' },

  { contentId: trainingVideos.one.id, text: 'Pandera Labs' },
  { contentId: trainingVideos.one.id, text: 'Communication' },
  { contentId: trainingVideos.one.id, text: 'Build Relationships' },
  { contentId: trainingVideos.one.id, text: 'Human Engineering' },
];
