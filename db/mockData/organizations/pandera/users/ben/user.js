const organization = require('../../organization');

const user = {
  id: 88,
  email: 'ben.madore@panderasystems.com',
  organizationId: organization.id,
  firstName: 'Ben',
  lastName: 'Madore',
  profileImageURI: null,
};

module.exports = user;
