const dylanUser = require('./dylan/user');
const dylanContent = require('./dylan/content');

const ryanUser = require('./ryan/user');
const ryanContent = require('./ryan/content');

const benUser = require('./ben/user');

const psco = require('./psco/user');
const psco2 = require('./psco2/user');
const pscr = require('./pscr/user');
const pscr2 = require('./pscr2/user');
const psoa = require('./psoa/user');
const psta = require('./psta/user');

const dylanData = { user: dylanUser, content: dylanContent };
const ryanData = { user: ryanUser, content: ryanContent };
const benData = { user: benUser };

const allContent = [...dylanContent.allAsArray, ...ryanContent.allAsArray];
const allTags = [...dylanContent.tags, ...ryanContent.tags];

module.exports = {
  dylanData,
  ryanData,
  benData,
  allUsers: [dylanUser, ryanUser, benUser,
    psco, psco2, pscr, pscr2, psoa, psta],
  allContent,
  allTags,
};
