const user = require('../user');
const { createVideoCDNPath, createVideoPreviewCDNPath } = require('../../../../../../util/contentPathCreator');

// Mock training ids start at 1
const dylanTrainingOne = {
  id: 8,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Pandera Labs origins w/ Dylan',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 8),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 8),
    },
    actions: [
      { id: 'some-uuid-t-1-22',
        text: 'Have you met Dylan?' },
    ],
  }),
};

const dylanTrainingTwo = {
  id: 9,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'TrainingVideo',
    title: 'Enova Decisions',
    video: {
      videoURI: createVideoCDNPath(user.organizationId, user.id, 9),
      previewImageURI: createVideoPreviewCDNPath(user.organizationId, user.id, 9),
    },
    actions: [
      { id: 'some-uuid-t-1-22',
        text: 'Have you met Dylan?' },
    ],
  }),
};

module.exports = {
  one: dylanTrainingOne,
  two: dylanTrainingTwo,
};
