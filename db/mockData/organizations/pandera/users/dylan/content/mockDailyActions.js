// Mock Daily Action ids start at 200
const user = require('../user');

const dylanActionOne = {
  id: 203,
  authorId: user.id,
  authorOrganizationId: user.organizationId,
  channelId: user.organizationId,
  publishedTimestamp: new Date(),
  status: 'PUBLISHED',
  body: JSON.stringify({
    type: 'DailyAction',
    title: 'Execution and Strategery',
    text: 'Execution and strategy are not mutually exclusive. A strategy needs to assess feasibility continuously and throughout execution you need to assess it realization towards the strategic value proposition. This starts with a commitment towards a share iterative approach with both the business and technical side in any engagement.',
    actions: [
      { id: 'some-uuid-da-203-1',
        text: 'What is the value prop of the product?' },
      { id: 'some-uuid-da-203-2',
        text: 'Identify who will answer this?' },
      { id: 'some-uuid-da-203-3',
        text: 'What is the cost?' },
      { id: 'some-uuid-da-203-4',
        text: 'Is this the highest value towards costs item at the moment?' },
    ],
  }),
};

module.exports = {
  one: dylanActionOne,
};
