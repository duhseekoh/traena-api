const dailyActions = require('./mockDailyActions');
const trainingVideos = require('./mockTrainings');

module.exports = [
  { contentId: dailyActions.one.id, text: 'Productivity' },
  { contentId: dailyActions.one.id, text: 'Outcome Focused' },

  { contentId: trainingVideos.one.id, text: 'Pandera Labs' },

  { contentId: trainingVideos.two.id, text: 'Pandera Labs' },
  { contentId: trainingVideos.two.id, text: 'Trends' },
  { contentId: trainingVideos.two.id, text: 'Decision Management' },
  { contentId: trainingVideos.two.id, text: 'Enova' },
  { contentId: trainingVideos.two.id, text: 'Financial' },
];
