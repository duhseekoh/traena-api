const organization = require('../../organization');
const { createUserPicCDNPath } = require('../../../../../util/contentPathCreator');

const user = {
  id: 60,
  email: 'dylan.raleigh@panderasystems.com',
  organizationId: organization.id,
  firstName: 'Dylan',
  lastName: 'Raleigh',
  profileImageURI: createUserPicCDNPath(organization.id, 60),
};

module.exports = user;
