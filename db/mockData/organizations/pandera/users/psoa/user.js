const organization = require('../../organization');

const user = {
  id: 20,
  email: 'dev+psoa@traena.io',
  organizationId: organization.id,
  firstName: 'OAdmin',
  lastName: 'Org',
  profileImageURI: null,
  position: 'Org Admin',
  tags: JSON.stringify(['dummy', 'test', 'admin']),
};

module.exports = user;
