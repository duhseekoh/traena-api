const organization = require('../../organization');

const user = {
  id: 21,
  email: 'dev+psta@traena.io',
  organizationId: organization.id,
  firstName: 'TAdmin',
  lastName: 'Traena',
  profileImageURI: null,
  position: 'Traena Admin',
  tags: JSON.stringify(['dummy', 'test', 'admin']),
};

module.exports = user;
