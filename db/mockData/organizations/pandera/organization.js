module.exports = {
  id: 2,
  name: 'Pandera Systems',
  organizationType: 'CLIENT',
  authDomains: JSON.stringify(['panderasystems.com']),
  auth0OrganizationName: 'customers_panderasystems',
  auth0OrganizationId: '30675d1e-d20d-4b54-8545-25976d5d6a39',
  auth0Connection: 'traena-user-database',
};
