const organization = require('../../organization');

const user = {
  id: 82,
  email: 'ed@traena.io',
  organizationId: organization.id,
  firstName: 'Ed',
  lastName: 'Siok',
  profileImageURI: null,
};

module.exports = user;
