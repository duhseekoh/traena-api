const organization = require('../../organization');

const user = {
  id: 81,
  email: 'dom@traena.io',
  organizationId: organization.id,
  firstName: 'Dom',
  lastName: 'Dicicco',
  profileImageURI: null,
};

module.exports = user;
