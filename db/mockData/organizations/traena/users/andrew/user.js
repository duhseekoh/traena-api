const organization = require('../../organization');

const user = {
  id: 31,
  email: 'andrew@traena.io',
  organizationId: organization.id,
  firstName: 'Andrew',
  lastName: 'Reilly',
  profileImageURI: null,
  position: 'Product POwner',
  tags: JSON.stringify(['solutions', 'ux', 'design', 'testing']),
};

module.exports = user;
