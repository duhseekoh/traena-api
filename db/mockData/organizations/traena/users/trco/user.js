const organization = require('../../organization');

const user = {
  id: 14,
  email: 'dev+trco@traena.io',
  organizationId: organization.id,
  firstName: 'Consumer1',
  lastName: 'Content',
  profileImageURI: null,
  position: 'Content Consumer',
  tags: JSON.stringify(['dummy', 'test', 'consumer']),
};

module.exports = user;
