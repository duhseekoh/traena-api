const organization = require('../../organization');

const user = {
  id: 13,
  email: 'dev+trcr2@traena.io',
  organizationId: organization.id,
  firstName: 'Creator2',
  lastName: 'Content',
  profileImageURI: null,
  position: 'Content Creator',
  tags: JSON.stringify(['dummy', 'test', 'creator']),
};

module.exports = user;
