const organization = require('../../organization');

const user = {
  id: 80,
  email: 'ben@traena.io',
  organizationId: organization.id,
  firstName: 'Ben',
  lastName: 'Madore',
  profileImageURI: null,
};

module.exports = user;
