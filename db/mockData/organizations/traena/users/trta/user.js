const organization = require('../../organization');

const user = {
  id: 10,
  email: 'dev+trta@traena.io',
  organizationId: organization.id,
  firstName: 'TAdmin',
  lastName: 'Traena',
  profileImageURI: null,
  position: 'Traena Admin',
  tags: JSON.stringify(['dummy', 'test', 'admin']),
};

module.exports = user;
