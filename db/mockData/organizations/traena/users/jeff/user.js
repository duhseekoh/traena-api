const organization = require('../../organization');

const user = {
  id: 30,
  email: 'jeff@traena.io',
  organizationId: organization.id,
  firstName: 'Jeff',
  lastName: 'Hassberger',
  profileImageURI: null,
  position: 'Product Owner',
  tags: JSON.stringify(['solutions', 'ux', 'design', 'testing']),
};

module.exports = user;
