const ben = require('./ben/user');
const dom = require('./dom/user');
const ed = require('./ed/user');
const jeff = require('./jeff/user');
const andrew = require('./andrew/user');
const trco = require('./trco/user');
const trco2 = require('./trco2/user');
const trcr = require('./trcr/user');
const trcr2 = require('./trcr2/user');
const troa = require('./troa/user');
const trta = require('./trta/user');

const allContent = []; // this org has produced no content
const allTags = [];

module.exports = {
  ben,
  dom,
  ed,
  allUsers: [dom, ed, ben, jeff, andrew, trco, trco2, trcr, trcr2, troa, trta],
  allContent,
  allTags,
};
