const organization = require('./organization');
const users = require('./users');
const channels = require('./channels');

module.exports = {
  organization,
  users,
  channels,
};
