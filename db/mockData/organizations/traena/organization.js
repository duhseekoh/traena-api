module.exports = {
  id: 1,
  name: 'Traena',
  organizationType: 'CLIENT',
  authDomains: JSON.stringify(['traena.io']),
  auth0OrganizationName: 'customers_traena',
  auth0OrganizationId: '8da16144-3bba-4bd0-a74c-2cc8db2ab72d',
  auth0Connection: 'traena-user-database',
};
