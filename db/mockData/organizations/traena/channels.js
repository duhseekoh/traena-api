module.exports = [{
  id: 1,
  name: 'Traena Internal Channel',
  organizationId: 1,
  description: 'Default internal channel for Traena',
  visibility: 'INTERNAL',
  default: true,
}];
