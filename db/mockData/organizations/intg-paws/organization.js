// @flow

module.exports = {
  id: 1002,
  name: 'PAWS',
  organizationType: 'CLIENT',
  auth0OrganizationName: 'PAWS_CLIENT',
  auth0OrganizationId: '089349dc-8ed1-41e0-bfda-44ba3fe0917a',
  auth0Connection: 'traena-user-database',
};
