// This org is used exlusively for integration tests.
// It was originally setup using API requests in the live dev environment.
// (https://development.api-traena.io, traena-dev.auth0.com)
//
// To add other users to this organization, they need to be added in the live
// dev environment first. Any other resources (channels, content, etc...)
// should be created during test setup time.

const organization = require('./organization');
const users = require('./users');
const channels = require('./channels');

module.exports = {
  organization,
  users,
  channels,
};
