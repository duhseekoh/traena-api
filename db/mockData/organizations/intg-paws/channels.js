// other integration channels are created during test setup
const organization = require('./organization');

module.exports = [{
  id: 1064,
  name: 'General',
  organizationId: organization.id,
  description: '',
  visibility: 'INTERNAL',
  default: true,
  createdTimestamp: '2018-04-30T19:05:38.604Z',
  modifiedTimestamp: '2018-04-30T19:05:38.604Z',
}];
