const organization = require('../../organization');

const user = {
  id: 1168,
  firstName: 'PAWS Cat Manager',
  lastName: 'Support',
  email: 'dev+intg.paws.cat.cr@traena.io',
  organizationId: organization.id,
  profileImageURI: null,
  position: 'Cat Manager - Creator',
  tags: JSON.stringify(['support']),
};

module.exports = user;
