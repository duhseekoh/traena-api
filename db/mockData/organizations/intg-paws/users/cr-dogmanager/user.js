const organization = require('../../organization');

const user = {
  id: 1169,
  firstName: 'PAWS Dog Manager',
  lastName: 'Support',
  email: 'dev+intg.paws.dog.cr@traena.io',
  organizationId: organization.id,
  profileImageURI: null,
  position: 'Dog Manager - Creator',
  tags: JSON.stringify(['support']),
};

module.exports = user;
