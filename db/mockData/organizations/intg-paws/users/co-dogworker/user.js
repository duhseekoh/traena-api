const organization = require('../../organization');

const user = {
  id: 1170,
  firstName: 'PAWS Dog Worker',
  lastName: 'Support',
  email: 'dev+intg.paws.dog.co@traena.io',
  organizationId: organization.id,
  profileImageURI: null,
  position: 'Dog Worker - Consumer',
  tags: JSON.stringify(['support']),
};

module.exports = user;
