const oa = require('./oa/user');
const catmanager = require('./cr-catmanager/user');
const catworker = require('./co-catworker/user');
const dogmanager = require('./cr-dogmanager/user');
const dogworker = require('./co-dogworker/user');

module.exports = {
  allUsers: [oa, catmanager, catworker, dogmanager, dogworker],
  allContent: [],
  allTags: [],
};
