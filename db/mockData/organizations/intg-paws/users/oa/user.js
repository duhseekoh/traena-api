const organization = require('../../organization');

const user = {
  id: 1164,
  email: 'dev+intg.paws.oa@traena.io',
  organizationId: organization.id,
  firstName: 'PAWS Org Admin',
  lastName: 'Support',
  profileImageURI: null,
  position: 'Org Admin',
  tags: JSON.stringify(['dummy', 'test', 'admin']),
};

module.exports = user;
