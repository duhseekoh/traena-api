const organization = require('../../organization');

const user = {
  id: 1171,
  firstName: 'PAWS Cat Worker',
  lastName: 'Support',
  email: 'dev+intg.paws.cat.co@traena.io',
  organizationId: organization.id,
  profileImageURI: null,
  position: 'Cat Worker - Consumer',
  tags: JSON.stringify(['support']),
};

module.exports = user;
