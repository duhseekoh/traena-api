// other integration channels are created during test setup
const organization = require('./organization');

module.exports = [
  {
    id: 1073,
    name: 'General',
    organizationId: organization.id,
    description: '',
    visibility: 'INTERNAL',
    default: true,
    createdTimestamp: '2018-05-07T18:38:43.480Z',
    modifiedTimestamp: '2018-05-07T18:38:43.480Z',
  },
];
