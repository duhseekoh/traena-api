// @flow

module.exports = {
  id: 1006,
  name: 'INTG Traena Admin Org',
  organizationType: 'CLIENT',
  auth0OrganizationName: 'INTG Traena Admin Org_CLIENT',
  auth0OrganizationId: '5bb0271e-caa8-43e0-acaf-333799fe3176',
  auth0Connection: 'traena-user-database',
};
