const organization = require('../../organization');

const user = {
  id: 1172,
  email: 'dev+intg.admin.ta@traena.io',
  organizationId: organization.id,
  firstName: 'God',
  lastName: 'INTG',
  profileImageURI: null,
  position: 'Traena Admin',
  tags: JSON.stringify(['support']),
};

module.exports = user;
