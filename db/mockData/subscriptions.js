const { organization: traenaOrganization } = require('./organizations/traena');
const {
  organization: aliceheimanOrganization,
} = require('./organizations/aliceheiman');
const {
  organization: strelmarkOrganization,
} = require('./organizations/strelmark');
const {
  organization: panderaOrganization,
} = require('./organizations/pandera');
const { organization: pawsOrg } = require('./organizations/intg-paws');
const { organization: adminOrg } = require('./organizations/intg-admin');

const { channels: traenaChannels } = require('./organizations/traena');
const {
  channels: aliceheimanChannels,
} = require('./organizations/aliceheiman');
const { channels: strelmarkChannels } = require('./organizations/strelmark');
const { channels: panderaChannels } = require('./organizations/pandera');
const { channels: pawsChannels } = require('./organizations/intg-paws');
const { channels: adminChannels } = require('./organizations/intg-admin');

module.exports = [
  // aliceheiman org as provider (provides to itself and others)
  {
    channelId: aliceheimanChannels[0].id,
    organizationId: aliceheimanOrganization.id,
    visibility: 'ORGANIZATION',
  },
  {
    channelId: aliceheimanChannels[0].id,
    organizationId: traenaOrganization.id,
    visibility: 'ORGANIZATION',
  },
  {
    channelId: aliceheimanChannels[0].id,
    organizationId: panderaOrganization.id,
    visibility: 'ORGANIZATION',
  },

  // strelmark org as provider (provides to itself and others)
  {
    channelId: strelmarkChannels[0].id,
    organizationId: strelmarkOrganization.id,
    visibility: 'ORGANIZATION',
  },
  {
    channelId: strelmarkChannels[0].id,
    organizationId: traenaOrganization.id,
    visibility: 'ORGANIZATION',
  },
  {
    channelId: strelmarkChannels[0].id,
    organizationId: panderaOrganization.id,
    visibility: 'ORGANIZATION',
  },

  // pandera org as provider (provides to itself and others)
  {
    channelId: panderaChannels[0].id,
    organizationId: panderaOrganization.id,
    visibility: 'ORGANIZATION',
  },
  {
    channelId: panderaChannels[0].id,
    organizationId: traenaOrganization.id,
    visibility: 'ORGANIZATION',
  },

  // traena org as provider (only provides to itself)
  {
    channelId: traenaChannels[0].id,
    organizationId: traenaOrganization.id,
    visibility: 'ORGANIZATION',
  },

  // paws org as provider (only provides to itself)
  {
    channelId: pawsChannels[0].id,
    organizationId: pawsOrg.id,
    visibility: 'ORGANIZATION',
  },

  // admin org as provider (only provides to itself)
  {
    channelId: adminChannels[0].id,
    organizationId: adminOrg.id,
    visibility: 'ORGANIZATION',
  },
];
