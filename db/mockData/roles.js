module.exports = [
  {
    id: 1,
    auth0RoleId: '9c8af4e9-e79d-4d21-8723-a8b214cb3488',
    name: 'Content Consumer',
    description: 'This role can consume content but cannot create it.',
    isAssignable: true,
  },
  {
    id: 2,
    auth0RoleId: 'b609b3f8-c2f8-4b04-8fc4-95851a5b7d97',
    name: 'Content Creator',
    description: 'This role can consume and create content, but cannot adjust organization level data.',
    isAssignable: true,
  },
  {
    id: 3,
    auth0RoleId: '2c8178b3-b5b4-4d25-b4bb-e3cc4228b286',
    name: 'Organization Admin',
    description: 'This role can consume and create content, and can also administer users and others content.',
    isAssignable: true,
  },
  {
    id: 4,
    auth0RoleId: '814fa76c-4570-44a9-940f-41856796d5f5',
    name: 'Traena Admin',
    description: 'Traena Employees',
    isAssignable: false,
  },
];
