/* eslint no-use-before-define: off */
const envConfig = require('../../envConfig');
const aliceheimanOrgData = require('../../mockData/organizations/aliceheiman');
const strelmarkOrgData = require('../../mockData/organizations/strelmark');
const panderaOrgData = require('../../mockData/organizations/pandera');
const traenaOrgData = require('../../mockData/organizations/traena');
const pawsOrgData = require('../../mockData/organizations/intg-paws');
const adminOrgData = require('../../mockData/organizations/intg-admin');
const subscriptionData = require('../../mockData/subscriptions');
const roleData = require('../../mockData/roles');

exports.seed = (knex) => {
  if (!envConfig.cdnBaseUrl) {
    throw new Error('Must provide a CDN_BASE_URL environment variable.');
  }

  // Deletes ALL existing entries
  return deleteAllData(knex)
    .then(() => {
      console.log('All seed data deleted');
    })
    .then(() => {
      console.log('populate aliceheiman org');
      return populateOrganization(knex, aliceheimanOrgData);
    })
    .then(() => {
      console.log('populate strelmark org');
      return populateOrganization(knex, strelmarkOrgData);
    })
    .then(() => {
      console.log('populate pandera org');
      return populateOrganization(knex, panderaOrgData);
    })
    .then(() => {
      console.log('populate traena org');
      return populateOrganization(knex, traenaOrgData);
    })
    .then(() => {
      console.log('populate paws integration testing org');
      return populateOrganization(knex, pawsOrgData);
    })
    .then(() => {
      console.log('populate admin integration testing org');
      return populateOrganization(knex, adminOrgData);
    })
    .then(() => {
      console.log('population subscriptions');
      return populateSubscriptions(knex, subscriptionData);
    })
    .then(() => {
      console.log('population roles');
      return populateRoles(knex, roleData);
    });
};

const populateOrganization = (knex, orgData) => {
  const { organization, users, channels } = orgData;
  const { allUsers, allContent, allTags } = users;
  // console.log(JSON.stringify(allTags));

  return knex('Organization')
    .insert(organization)
    .then(() => knex('User').insert(allUsers))
    .then(() => knex('Channel').insert(channels))
    .then(() => knex('Content').insert(allContent))
    .then(() => knex('Tag').insert(allTags));
};

const populateSubscriptions = (knex, subscriptions) =>
  knex('Subscription').insert(subscriptions);
const populateRoles = (knex, roles) => knex('Role').insert(roles);

/**
 * Delete all of the data in the tables in order by table.
 * Order matters because of foreign key relationships.
 */
const deleteAllData = (knex) => {
  const tableNames = [
    'Role',
    'PushDevice',
    'Event',
    'Tag',
    'Like',
    'Comment_TaggedUser',
    'Subscription',
    'Notification',
    'Comment',
    'Task',
    'TranscodeJob',
    'Bookmark',
    'Series_Content',
    'Series',
    'Content',
    'Channel',
    'User',
    'Organization',
  ];
  let delAllTablesPromise;

  tableNames.forEach((tableName) => {
    if (!delAllTablesPromise) {
      console.log(`deleting ${tableName}`);
      delAllTablesPromise = knex(tableName).del();
    } else {
      delAllTablesPromise = delAllTablesPromise.then(() => {
        console.log(`deleting ${tableName}`);
        return knex(tableName).del();
      });
    }
  });

  return delAllTablesPromise;
};
