const cdnBaseUrl = require('../envConfig').cdnBaseUrl;

const createVideoCDNPath = (orgId, userId, contentId) =>
  `${cdnBaseUrl}/organizations/${orgId}/users/${userId}/content/${contentId}/video-output/video.m3u8`;

const createVideoPreviewCDNPath = (orgId, userId, contentId) =>
  `${cdnBaseUrl}/organizations/${orgId}/users/${userId}/content/${contentId}/images/thumbnail-00002.png`;

const createUserPicCDNPath = (orgId, userId) =>
  `${cdnBaseUrl}/organizations/${orgId}/users/${userId}/profilepic.jpg`;

module.exports = {
  createUserPicCDNPath,
  createVideoCDNPath,
  createVideoPreviewCDNPath,
};
