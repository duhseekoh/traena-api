module.exports = {
  addAuditTimestamps(knex, table) {
    table.timestamp('createdTimestamp').defaultTo(knex.fn.now());
    table.timestamp('modifiedTimestamp').defaultTo(knex.fn.now());
    return table;
  },
};
